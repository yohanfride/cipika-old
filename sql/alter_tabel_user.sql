ALTER TABLE `tbl_user`
	ADD COLUMN `id_propinsi` int(11) NULL AFTER `id_kota`;
ALTER TABLE `tbl_user`
	ADD COLUMN `id_kabupaten` int(11) NULL AFTER `id_propinsi`;
ALTER TABLE `tbl_user`
	ADD COLUMN `id_kecamatan` int(11) NULL AFTER `id_kabupaten`;