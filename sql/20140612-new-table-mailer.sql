CREATE TABLE IF NOT EXISTS `mailer` (
  `idmailer` int(11) NOT NULL AUTO_INCREMENT,
  `mailer_module` varchar(30) NOT NULL,
  `mailer_from` varchar(200) NOT NULL,
  `mailer_to` varchar(200) NOT NULL,
  `mailer_subject` varchar(200) NOT NULL,
  `mailer_message` text NOT NULL,
  `mailer_status` varchar(10) NOT NULL,
  `mailer_created` datetime DEFAULT NULL,
  `mailer_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`idmailer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;