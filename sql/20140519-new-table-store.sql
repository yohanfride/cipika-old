DROP TABLE IF EXISTS `tbl_store`; 

CREATE TABLE IF NOT EXISTS `tbl_store` (
  `id_store` int(11) NOT NULL AUTO_INCREMENT,
  `id_kota` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_store` varchar(25) NOT NULL,
  `nama_pemilik` varchar(200) NOT NULL,
  `tgl_lahir_pemilik` date NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telpon` varchar(25) NOT NULL,
  `ym` varchar(45) DEFAULT NULL,
  `fb` varchar(45) DEFAULT NULL,
  `tw` varchar(45) DEFAULT NULL,
  `bb` varchar(45) DEFAULT NULL,
  `wa` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `bank_nama` varchar(100) NOT NULL,
  `bank_norek` varchar(100) NOT NULL,
  `bank_pemilik` varchar(100) NOT NULL,
  `store_status` varchar(20) NOT NULL COMMENT 'pending,active,block',
  `date_added` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_store`),
  KEY `fk_tbl_store_tbl_kota1_idx` (`id_kota`),
  KEY `fk_tbl_store_tbl_user1_idx` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;