CREATE  TABLE IF NOT EXISTS `tbl_settlement` (
  `id_settlement` INT NOT NULL AUTO_INCREMENT ,
  `tanggal` TIMESTAMP NULL ,
  `admin` VARCHAR(45) NULL ,
  `kode_settlement` VARCHAR(45) NULL ,
  `total` INT NULL DEFAULT 0 ,
  PRIMARY KEY (`id_settlement`) ,
  UNIQUE INDEX `kode_settlement_UNIQUE` (`kode_settlement` ASC) );

CREATE  TABLE IF NOT EXISTS `tbl_settlement_item` (
  `id_settlement_item` INT NOT NULL AUTO_INCREMENT ,
  `id_settlement` INT NULL ,
  `kode_order` VARCHAR(45) NULL ,
  `nama_store` VARCHAR(45) NULL ,
  `bank_nama` VARCHAR(45) NULL ,
  `bank_norek` VARCHAR(45) NULL ,
  `bank_pemilik` VARCHAR(45) NULL ,
  `total_request` INT NULL DEFAULT 0 ,
  `total_transfer` INT NULL DEFAULT 0 ,
  PRIMARY KEY (`id_settlement_item`) ,
  INDEX `fk_tbl_settlement_item_tbl_settlement1_idx` (`id_settlement` ASC) ,
  CONSTRAINT `fk_tbl_settlement_item_tbl_settlement1`
    FOREIGN KEY (`id_settlement` )
    REFERENCES `tbl_settlement` (`id_settlement` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);