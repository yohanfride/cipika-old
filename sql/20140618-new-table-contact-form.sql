CREATE TABLE IF NOT EXISTS `contact_form` (
  `idcf` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `pesan` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `notes` varchar(200) NOT NULL,
  `tgl_kirim` datetime NOT NULL,
  `read_status` int(1) NOT NULL DEFAULT '0' COMMENT '0 = unread, 1 = read',
  `delete_status` int(1) NOT NULL DEFAULT '1' COMMENT '0 = delete true, 1 = delete false',
  PRIMARY KEY (`idcf`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;