ALTER TABLE `tbl_cart`
	ADD COLUMN `nama_produk` VARCHAR(50) NOT NULL AFTER `id_produk`,
	ADD COLUMN `berat` INT NOT NULL AFTER `nama_produk`,
	ADD COLUMN `dimensi` INT NOT NULL AFTER `berat`,
	CHANGE COLUMN `jml` `qty` INT(11) NULL DEFAULT NULL AFTER `dimensi`,
	ADD COLUMN `harga` INT(11) NULL DEFAULT NULL AFTER `qty`,
	ADD COLUMN `diskon` INT(11) NULL DEFAULT NULL AFTER `harga`,
	ADD COLUMN `harga_diskon` INT(11) NULL DEFAULT NULL AFTER `diskon`,
	ADD COLUMN `total` INT(11) NULL DEFAULT NULL AFTER `harga_diskon`,
	ADD COLUMN `id_merchant` INT(11) NULL DEFAULT NULL AFTER `total`,
	DROP INDEX `fk_tbl_cart_tbl_user1_idx`,
	DROP INDEX `fk_tbl_cart_tbl_produk1_idx`,
	DROP FOREIGN KEY `fk_tbl_cart_tbl_produk1`,
	DROP FOREIGN KEY `fk_tbl_cart_tbl_user1`;