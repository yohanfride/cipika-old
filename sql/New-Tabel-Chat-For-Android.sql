CREATE TABLE `tbl_private_messages` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`to_user_id` INT(11) NOT NULL,
	`from_user_id` INT(11) NOT NULL,
	`time` DATETIME NOT NULL,
	`message` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`status` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDX_130C545829F6EE60` (`to_user_id`),
	INDEX `IDX_130C54582130303A` (`from_user_id`),
	CONSTRAINT `FK_130C54582130303A` FOREIGN KEY (`from_user_id`) REFERENCES `tbl_user` (`id_user`),
	CONSTRAINT `FK_130C545829F6EE60` FOREIGN KEY (`to_user_id`) REFERENCES `tbl_user` (`id_user`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT
AUTO_INCREMENT=334