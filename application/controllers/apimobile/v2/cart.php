<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends API_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('models-api/home_m');
        $this->load->model('models-api/payment_m');
        $this->load->model('models-api/order_m');
        $this->load->model('models-api/cart_m');
        $this->load->library('lib_rest');
    }

	function index_get{
		
	}
	
	function add_get(){
		
	}
	
    public function kota(){
        $id = $_GET['id'];
        $data = $this->cart_m->get_data_where('tbl_kota', 'id_provinsi', $id);
        echo json_encode($data);
    }
    
    public function kabupaten(){
        $id = $_GET['id'];
        $data = $this->cart_m->get_data_where('tbl_kabupaten', 'id_propinsi', $id);
        echo json_encode($data);
    }
    
    public function kecamatan(){
        $id = $_GET['id'];
        $data = $this->cart_m->get_data_where('tbl_kecamatan', 'id_kabupaten', $id);
        echo json_encode($data);
    }

    public function cekjne($from='', $tujuan=''){
        require_once 'jne.php';
        $rest = new Jne(array(
            'server' => 'http://api.ongkir.info/'
        ));   
        $result = $rest->post('cost/find', array(
            'from'  => strtoupper($from),
            'to'        => strtoupper($tujuan),
            'weight'    => 1000, 
            'courier'   => 'jne',
            'API-Key'   => 'b669d36c28c480da8117ec0f58752f84'
        ));
        $hasil = '';
        try   {
            $status = $result['status'];            
            if ($status AND $status->code == 0){
                $hasil = $result['price'];
            }
            else{
                $hasil = 'gagal';
            }
        }
        catch (Exception $e){
            $hasil = 'gagal';
        }
        return $hasil;
    }
    
    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
