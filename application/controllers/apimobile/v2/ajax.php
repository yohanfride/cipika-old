<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Ajax extends API_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('pluploadhandler');
		$this->load->model('models-api/ajax_m');
		$this->load->model('models-api/produk_m');
	}

	public function index()
	{
		redirect(base_url());
	}

	public function follow_post(){
		if(count($this->user)!=0){
			$id_user_following=$this->auth_m->get_user()->id_user;
			$id_user_followed=$this->input->post('id_user');
			if($id_user_following!=$id_user_followed){
				$insert=$this->ajax_m->insert('tbl_follow', array('id_user_following'=>$id_user_following, 'id_user_followed'=>$id_user_followed, 'date_added'=>date('Y-m-d H:i:s')));
				/*$out=array('status'=>1, 'message'=>'success');
				$this->respoonse($out);*/
				//if ($this->db->_error_message()!=''){
					$this->response(array(
						'status' => 1
					));
				/*} else {
					$this->response(array(
						'status' => 0,
						'error'  => "We're sorry, You're request is failed. We will investigate this error.",
						'error_db' => $this->db->_error_message()
					));
				}*/	
			}
		}
	}

	public function unfollow_post(){
		if(count($this->user)!=0){
			$id_user_following=$this->auth_m->get_user()->id_user;
			$id_user_followed=$this->input->post('id_user');
			if($id_user_following!=$id_user_followed){
				$delete=$this->ajax_m->delete2('tbl_follow', 'id_user_following', 'id_user_followed', $id_user_following, $id_user_followed);
				if ($delete){
					$this->response(array(
						'status' => 1
					));
				} else {
					$this->response(array(
						'status' => 0,
						'error'  => "We're sorry, You're request is failed. We will investigate this error.",
						'error_db' => $this->db->_error_message()
					));
				}
			}
		}
	}
	
	public function comment_list_get(){
		
		$id = $this->_check_request($this->get('id'), 1, 'id', 'integer');
		$data['status']  = 1;
		$data['comment']=$this->produk_m->get_comment($id, 10, 0);
		$this->response($data);
        
		/*foreach($comment as $komen) {
        echo '<li>
                <section id="komen-'. $komen->id_comment. '" class="comment-item clearfix">
                  <div class="comment-avatar">
                    <a href="'. base_url() .'store/id/'. $komen->id_user .'">';
                      if (isset($komen->image)){
                        echo '<img class="img-responsive img-rounded" src="'.base_url(). 'asset/upload/profil/'.$komen->image. '">';
                      } else { 
                        echo '<img class="img-responsive img-rounded" src="'. base_url().'asset/img/no-avatar-single.png">';
                      }
                    echo '</a>                    
                  </div>
                  <div class="comment-detail">
                    <h4 class="comment-author"><a href="#">'. $komen->username .'</a></h4>
                    <p>'. $komen->comment .'</p>
                    <p class="small"><time>'. $komen->date_added .'</time></p>
                  </div>  
                </section>
              </li>';
      	}*/
	}

	public function comment_post(){
		$input=array(
			'id_user' => $this->auth_m->get_user()->id_user,
			'id_produk' => $this->input->post('id_produk'),
			'comment' => $this->input->post('komen'),
			'publish' => 1,
			'parent_id' => $this->input->post('parent_id'),
			'date_added' => date('Y-m-d H:i:s'),
			'date_modified' => date('Y-m-d H:i:s')
		);
		$insert=$this->ajax_m->insert('tbl_comment', $input);
		if($insert){
			$out=array('status'=>1, 'message'=>'success');
		}else{
			$out=array('status'=>0, 'message'=>'failed');
		}
		$this->response($out);
	}

	public function love_post(){
		if(count($this->user)==0){}
		else{
			$id_produk=$this->input->post('id');
			$id_user=$this->auth_m->get_user()->id_user;
			$cek_love=$this->ajax_m->cek('tbl_love', 'id_produk', 'id_user', $id_produk, $id_user);
			$loved=$this->ajax_m->get_loved($id_produk);
			if($cek_love<1){
				$add_love=$this->ajax_m->insert('tbl_love', array('id_produk'=>$id_produk, 'id_user'=>$id_user));
				$total=$loved+1;
				$this->ajax_m->update('tbl_produk', 'id_produk', $id_produk, array('loved'=>$total));
				$out=array('status'=>'1', 'value'=>'+1');
			}else{
				$delete_love=$this->ajax_m->delete_love($id_produk, $id_user);
				$total=$loved-1;
				$this->ajax_m->update('tbl_produk', 'id_produk', $id_produk, array('loved'=>$total));
				if($delete_love){$out=array('status'=>'success', 'value'=>'-1');}
			}
			$this->response($out);
		}
	}
        
    public function cek_merchant(){
		$data=$this->ajax_m->get_jumlah_merchant_baru();
		echo $data;
	}
        
	public function view_merchant(){
		$data=$this->ajax_m->get_data_merchant_baru();
		$this->response($data);
		foreach ($data as $row) {
			echo '<li><a href="'.admin_url().'user/edit_merchant/'.$row->id_store.'">'.$row->nama_store.' <small class="time">'.$row->date_added.'</small></a></li>';
		}
		// echo '<li class="divider"></li>
  //               <li><a href="#">View All</a></li>';
	}
        
	public function cek_produk(){
		$data=$this->ajax_m->get_jumlah_produk_baru();
		echo $data;
	}
        
        

	public function view_produk(){
		$data=$this->ajax_m->get_data_produk_baru();
		foreach ($data as $row) {
			echo '<li><a href="'.admin_url().'produk/edit/'.$row->id_produk.'">'.$row->nama_produk.' <small class="time">'.$row->date_added.'</small></a></li>';
		}
		// echo '<li class="divider"></li>
  //               <li><a href="#">View All</a></li>';
	}

	public function cek_order(){
		$data=$this->ajax_m->get_jumlah_order_baru();
		echo $data;
	}

	public function view_order(){
		$data=$this->ajax_m->get_data_order_baru();
		foreach ($data as $row) {
			echo '<li><a href="'.admin_url().'order/detail/'.$row->id_order.'">'.$row->kode_order.' <small class="time">'.$row->date_added.'</small></a></li>';
		}
		// echo '<li class="divider"></li>
  //               <li><a href="#">View All</a></li>';
	}

	public function upload_post()
	{
		/*
		pluploadhandler::no_cache_headers();
		pluploadhandler::cors_headers();

		if (isset($_REQUEST["name"])) {
			$fileName = $_REQUEST["name"];
		} elseif (!empty($_FILES)) {
			$fileName = $_FILES["file"]["name"];
		} else {
			$fileName = uniqid("file_");
		}

		$up=pluploadhandler::handle(array(
			'target_dir' => 'asset/upload/',
			'allow_extensions' => 'jpg,jpeg,png'
		));
		if (!$up) {
			die(json_encode(array(
				'status' => 0, 
				'error' => array(
					'code' => pluploadhandler::get_error_code(),
					'msg' => pluploadhandler::get_error_message()
				)
			)));
		} else {
			die(json_encode(array('status' => 1, 'msg'=> 'success', 'res'=>array('src'=> base_url().'asset/upload/'.$fileName))));
		}
		*/
/**/
		/* ********************************** PROSES MANIPULASI GAMBAR *********************************** */
		pluploadhandler::no_cache_headers();
		pluploadhandler::cors_headers();

		if (isset($_REQUEST["name"])) {
			$fileName = $_REQUEST["name"];
		} elseif (!empty($_FILES)) {
			$fileName = $_FILES["file"]["name"];
		} else {
			$fileName = uniqid("file_");
		}

		$config['upload_path'] = './asset/produk/original/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']	= $fileName;
		$config['max_size']	= '2000';
		$config['max_width']  = '2000';
		$config['max_height']  = '2000';
		$this->load->library('upload', $config);
		if($fileName !='') {
			if ( ! $this->upload->do_upload('file')){
				$this->response(array(
						'status' => 0,
						'error' =>$this->upload->display_errors()
					));				
			} else {
				// $nama_baru = $this->input->post('id_produk').'_'.date('Ymd_His');
				$gambar = $this->upload->data();
				// resize 
				$config['source_image']	= './asset/produk/original/'.$fileName;
				$config['new_image']	= './asset/produk/thumb/'.$fileName;
				// $config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = FALSE;
				$config['width']	 = 620;
				$config['height']	= 620;
				$this->load->library('image_lib', $config); 
				if ( ! $this->image_lib->resize()){	echo $this->image_lib->display_errors();}
				$this->image_lib->clear();
	
			// water marking
				$config['source_image']	= './asset/produk/thumb/'.$fileName;
				$config['new_image']	= './asset/produk/thumb/'.$fileName;
				$config['create_thumb'] = FALSE;
				$config['quality'] = '90%';
				$config['wm_type'] = 'overlay';
				$config['wm_overlay_path'] = './asset/img/watermark.png';
				$config['wm_opacity'] = '50';
				$config['wm_x_transp'] = '4';
				$config['wm_y_transp'] = '4';
			// $config['wm_padding'] = '20';

			// $config['wm_text'] = 'CIPIKA Store';
			// $config['wm_font_path'] = './asset/font/comic.ttf';
			// $config['wm_font_size']	= '24';
			// $config['wm_font_color'] = 'ff0000';
				$config['wm_vrt_alignment'] = 'bottom';
				$config['wm_hor_alignment'] = 'right';
				$config['wm_vrt_offset'] = '20';
				$config['wm_hor_offset'] = '20';
				// $config['wm_padding'] = '20';
				// $config['wm_shadow_color'] = 'ffffff';
				// $config['wm_shadow_distance'] = '3'; 
				$this->image_lib->initialize($config);
				if ( ! $this->image_lib->watermark()){
					$this->response(array(
						'status' => 0,
						'error' =>$this->image_lib->display_errors()
					));				
				}

			// resize THUMB
			// $this->image_lib->clear();
			// $config['source_image']	= './asset/produk/original/'.$gambar['file_name'];
			// $config['new_image']	= './asset/produk/thumb/'.$fileName.'300x300'.$gambar['file_ext'];
			// // $config['create_thumb'] = TRUE;
			// $config['maintain_ratio'] = FALSE;
			// $config['width']	 = 300;
			// $config['height']	= 300;
			// $this->image_lib->initialize($config); 
			// if ( ! $this->image_lib->resize()){	echo $this->image_lib->display_errors();}
			// // resize THUMB
			// $this->image_lib->clear();
			// $config['source_image']	= './asset/produk/original/'.$gambar['file_name'];
			// $config['new_image']	= './asset/produk/thumb/'.$fileName.'80x80'.$gambar['file_ext'];
			// // $config['create_thumb'] = TRUE;
			// $config['maintain_ratio'] = FALSE;
			// $config['width']	 = 80;
			// $config['height']	= 80;
			// $this->image_lib->initialize($config); 
			// if ( ! $this->image_lib->resize()){	echo $this->image_lib->display_errors();}

			// delete gambar asli
			// if(isset($gambar) AND file_exists('./asset/upload/'.$gambar['file_name'])){
			// 	unlink('./asset/upload/'.$gambar['file_name']);
			// }
				$this->response(array('status' => 1, 'msg'=> 'success', 'res'=>array('src'=> base_url().'asset/produk/thumb/'.$fileName, 'filename'=>$fileName)));
			}		
		} else {
			$this->response(array(
			'status' => 0, 
			'error' => $fileName
			));
		}
	}	
	public function category_post(){
		$id=$this->input->post('id');
		//$i=$this->input->post('i');
		//$j=$this->input->post('j');
		$i=$this->input->post('i');
		$data['status'] = 1;
		$data['data-sub']=$this->input->post('j');
		$data['sub-category']=$this->ajax_m->get_category_child($id);		
		if($data){
	     /* echo '<div class="form-group">
	        <label for="" class="col-lg-2 control-label">Sub Category '.$j.'</label>
	        <div class="col-lg-4">
	          <select name="id_kategori[]" class="form-control sub_category" data-sub="'.$j.'" id="id_kategori" placeholder="">
	            <option value="">-- Choose Sub Category --</option>';
	            foreach ($data as $kat) {
	            echo '<option value="'.$kat->id_kategori. '">'. ucwords($kat->nama_kategori). '</option>';
	            }
	          echo '</select>
	        </div>
	      </div>';*/
		  $this->response($data);
        }
	}

	public function delete_post(){

		$post = $this->input->post('src');
		$this->ajax_m->delete('tbl_produkfoto', 'image', $post);
		$len = strlen(base_url());
		$new_path = substr($post, $len, strlen($post)-$len);		
		$new_path2 = str_replace('thumb', 'original', $new_path);
		$del = unlink($new_path);
		$del2 = unlink($new_path2);		
		if(!$del){
			$this->response(array('status' => 0, 'msg'=> 'failed'));
		}else{
			$this->response(array('status' => 1, 'msg'=> 'success'));
		}
	}

	public function cekjne_get($from='', $id_kota=''){
		$kota=$this->ajax_m->get_single('tbl_kota', 'id_kota', $id_kota);
		require_once 'jne.php';
		$rest = new Jne(array(
			'server' => 'http://api.ongkir.info/'
		));   
		$result = $rest->post('cost/find', array(
			'from' 	=> strtoupper($from),
			'to' 		=> strtoupper($kota->nama_kota),
			'weight'	=> 1000, 
			'courier'	=> 'jne',
			'API-Key' 	=> 'b669d36c28c480da8117ec0f58752f84'
		));
		$hasil = '';
		try   {
			$status = $result['status'];			
			if ($status AND $status->code == 0){
				$hasil = $result['price'];
			}
			else{
				$hasil = 'gagal';
			}
		}
		catch (Exception $e){
			$hasil = 'gagal';
		}

		if($hasil !='gagal') {
			$i=0;
			$data['status'] = 1;
			foreach($hasil->item as $item) { 
				$i++;
				$data['item'] = $item;
				//echo '<option value="'.$item->value.'" ';
				//if ($i==1) echo 'checked';
				//echo '>'. $item->service. ' Rp. '. $item->value .'</option>';               
			}
			$this->response($data);
		} else {
			$this->response(array(
				'status' => 0,
				'error' =>'The shipping fee is not found'
			),403);
			//echo '<option>Biaya pengiriman tidak ditemukan</option>';		
			// echo '<input value="2000" class="ongkir_sementara hidden" type="radio" name="ongkir_sementara" checked>';
		} 
	}

	public function cek_jne_get($from='', $tujuan=''){
		// $kota=$this->ajax_m->get_single('tbl_kota', 'id_kota', $id_kota);
		require_once 'jne.php';
		$rest = new Jne(array(
			'server' => 'http://api.ongkir.info/'
		));   
		$result = $rest->post('cost/find', array(
			'from' 	=> strtoupper($from),
			'to' 		=> strtoupper($tujuan),
			'weight'	=> 1000, 
			'courier'	=> 'jne',
			'API-Key' 	=> 'b669d36c28c480da8117ec0f58752f84'
		));
		$hasil = '';
		try   {
			$status = $result['status'];			
			if ($status AND $status->code == 0){
				$hasil = $result['price'];
			}
			else{
				$hasil = 'gagal';
			}
		}
		catch (Exception $e){
			$hasil = 'gagal';
		}

		if($hasil !='gagal') {
			$i=0;
			$data['status'] = 1;
			foreach($hasil->item as $item) { 
				$i++;
				$data['item'] = $item;
				//echo '<option value="'.$item->value.'" ';
				//if ($i==1) echo 'checked';
				//echo '>'. $item->service. ' Rp. '. $item->value .'</option>';               
			}
			$this->response($data);
		} else {
			$this->response(array(
				'status' => 0,
				'error' =>'The shipping fee is not found'
			),403);
			//echo '<option>Biaya pengiriman tidak ditemukan</option>';		
			// echo '<input value="2000" class="ongkir_sementara hidden" type="radio" name="ongkir_sementara" checked>';
		} 
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */