<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class lokasi extends API_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('lib_lokasi');
		$this->load->model('models-api/lokasi_m');
	}
    
	protected $methods = array(
        'dropdown_kabupaten'  => array('key' => FALSE),
        'dropdown_kecamatan'  => array('key' => FALSE)
    );
	
    /* AJAX : get kabupaten by propinsi */
    function dropdown_kabupaten_post()
    {
        $id_propinsi                    = $this->input->post('id_propinsi');        
        $data['kabupaten']              = $this->lokasi_m->get_kabupaten($id_propinsi);
        $data['nama_input_kabupaten']   = $this->input->post('nama_input_kabupaten');
        
        //echo $this->load->view('publik/lokasi/dropdown_kabupaten_v',$data,TRUE);
		$this->response($data);
    }
    
    /* AJAX : get kecamatan by kabupaten */
    function dropdown_kecamatan_post()
    {
        $id_kabupaten                   = $this->input->post('id_kabupaten');        
        $data['kecamatan']              = $this->lib_lokasi->get_kecamatan($id_kabupaten);
        $data['nama_input_kecamatan']   = $this->input->post('nama_input_kecamatan');
        
        //echo $this->load->view('publik/lokasi/dropdown_kecamatan_v',$data,TRUE);
		$this->response($data);
    }
    
}