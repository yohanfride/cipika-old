<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Home extends API_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/home_m');
		$this->load->model('models-api/user_m');
		$this->load->driver('cache');
		$this->load->helper('app_helper');
		// $this->lib_facebook->cek_connect();
	}
	protected $methods = array(
        'index_get'  => array('key' => FALSE),
        'get_banner_get'  => array('key' => FALSE)
    );	
	
	/*Function Index*/
	public function index_get()
	{
		$data=array();
		$data['status'] = 1;
		//refresh stok produk, back stock after 4 hours never paid
        //sementara diremark dulu oleh andy, karena blm berani testing
		//$this->cancel_order();

		if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
	    else{$data['by_search'] = '';}
		$limit=12;
		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
			else{$offset=0;}
		$data['produk']=$this->home_m->get_produk($limit, $offset);
		$data['produk_love1']=$this->home_m->get_produk_love(0,1);
		$data['produk_love']=$this->home_m->get_produk_love(1,4);
		$data['produk_pick1']=$this->home_m->get_produk_pick(0,1);
		$data['produk_pick']=$this->home_m->get_produk_pick(1,4);
		$data['produk_disqus']=$this->home_m->get_produk_disqus();
		$data['provinsi']=$this->home_m->get_all_data_order('tbl_propinsi', 'id_propinsi', 'asc');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		// $data['tag']=$this->home_m->get_all_data('tbl_tag', 'nama_tag', 'asc');
		$data['banner']=$this->home_m->get_all_data_order('banner', 'idbanner', 'asc');
		$this->response($data);
	}
	
	function get_banner_get(){
		$data['status'] = 1;
		$data['banner']=$this->home_m->get_all_data_order('banner', 'idbanner', 'asc');
		$this->response($data);
	}
	
	/*Function Ini Hanya ditempel, belum mengerti cara menggunakannya*/
	function cek_login(){
		$konek = $this->connect();		
		if(isset($konek['user_profile'])){
			if($konek['user']){
			
				$user = $this->user_m->get_email_user($konek['user_profile']['email']); 
				if(!empty($user)){ /* registered, force login */
					echo "udah daftar";
				}else{
					$password = random_password();
					$email    = $konek['user_profile']['email'];
					$input1 = array(
								"email" 		=> $email,
								"password" 		=> md5($password),
								"firstname" 	=> $konek['user_profile']['first_name'],
								"lastname" 	=> $konek['user_profile']['last_name'],
								"telpon" 	=> 0,
								"fb_token" 		=> $konek['token']
							);
							
					$insert1=$this->user_m->insert('tbl_user', $input1);
				
					echo "berhasil daftar";		
				}
			}
		}
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
