<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Page extends API_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/page_m');
	}
	
	protected $methods = array(
        'index_get'  => array('key' => FALSE),
        'sitemap_get'  => array('key' => FALSE)
    );
	
	public function index_get() {
		$id = $this->get('id');
        $data['data']=$this->page_m->get_single('tbl_page', 'id_page', $id);
        if($data['data']==NULL || $id=='') redirect(base_url(), 'refresh');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->response($data);
    }

    public function sitemap_get() {
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['profil'] = array(
            array('id_page' => 'myproduct', 'title' => 'My Product'),
            array('id_page' => 'order', 'title' => 'Order'),
            array('id_page' => 'myarticle', 'title' => 'Article'),
            array('id_page' => 'user/wishlist', 'title' => 'Wishlist'),
            array('id_page' => 'user/profile', 'title' => 'Profile'),
            array('id_page' => 'user/account', 'title' => 'Account'),
        );
        $this->response($data);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */