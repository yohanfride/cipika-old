<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Merchant extends API_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/home_m');
		$this->load->model('models-api/user_m');
		$this->load->model('models-api/meta_m');
		$this->load->model('models-api/merchant_m');
		$this->load->model('models-api/auth_m');        
		$this->auth_m->check();
	}
    
    /* Daftar Merchant : Step 1 Profile */
	function register_get(){
		$this->load->library('lib_lokasi');
		$data['status']=1;
		$data['js_lokasi']      = TRUE;
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['propinsi']       = $this->lib_lokasi->get_propinsi();
        $data['data_sales']     = $this->merchant_m->get_sales();
        
        //# Pastikan masih sbg member , level 7 = merchant --> redir ke step 2, upload product
        $user = $this->merchant_m->get_single('tbl_user','id_user',(int)$this->user->id_user); 
        if($user->id_level==7){
            redirect(base_url('merchant/get_started'));			
			//get_started_get();
        }
		$this->response($data);
	}
	
    /* Daftar Merchant : Step 1 Profile (Proses Input)*/
    function register_post()
    {
        $this->load->library('lib_lokasi');
        $this->load->library('form_validation');        
        $user = $this->merchant_m->get_single('tbl_user','id_user',(int)$this->user->id_user); 
		$data['curr_sales']     = $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_sales');
        if($user->id_level==7){
            redirect(base_url('merchant/get_started'));
        }
			$nama_store    = self::_check_request($this->post('nama_store'), 1, 'nama_store', 'required');
			$nama_pemilik    = self::_check_request($this->post('nama_pemilik'), 1, 'nama_pemilik', 'required');
			$deskripsi    = self::_check_request($this->post('deskripsi'), 1, 'deskripsi', 'required');
			$deskripsi    = self::_check_request($this->post('deskripsi'), 1, 'deskripsi', 'required');
			$tgl_lahir_pemilik    = self::_check_request($this->post('tgl_lahir_pemilik'), 1, 'tgl_lahir_pemilik', 'required');
			$telpon    = self::_check_request($this->post('telpon'), 1, 'telpon', 'required');
			$bank_nama    = self::_check_request($this->post('bank_nama'), 1, 'bank_nama', 'required');
			$bank_norek    = self::_check_request($this->post('bank_norek'), 1, 'bank_norek', 'required');
			$bank_pemilik    = self::_check_request($this->post('bank_pemilik'), 1, 'bank_pemilik', 'required');
			$syarat_ketentuan    = self::_check_request($this->post('syarat_ketentuan'), 1, 'syarat_ketentuan', 'required');
			
          	if (isset($syarat_ketentuan)){
                $country            = get_visitor_country();
                $insert = array('id_store'              => null,
                                'id_kota'               => $this->input->post('kecamatan'),
                                'id_user'               => (int)$this->user->id_user,
                                'id_sales'              => $this->input->post('merchant_sales'),
                                'negara'                => $country['country'],
                                'nama_store'            => $this->input->post('nama_store'),
                                'deskripsi'             => $this->input->post('deskripsi'),
                                'nama_pemilik'          => $this->input->post('nama_pemilik'),
                                'tgl_lahir_pemilik'     => strftime("%Y-%m-%d",strtotime($this->input->post('tgl_lahir_pemilik'))),
                                'alamat'                => '',
                                'telpon'                => $this->input->post('telpon'),
                                'ym'                    => '',
                                'fb'                    => '',
                                'tw'                    => '',
                                'bb'                    => '',
                                'wa'                    => '',
                                'email'                 => $this->user->email,
                                'bank_nama'             => $this->input->post('bank_nama'),
                                'bank_norek'            => $this->input->post('bank_norek'),
                                'bank_pemilik'          => $this->input->post('bank_pemilik'),
                                'store_status'          => 'pending',
                                'date_added'            => date('Y-m-d H:i:s'),
                                'date_modified'         => date('Y-m-d H:i:s')
                                );
                $this->merchant_m->insert('tbl_store',$insert);
                
                //# Update status level user ke merchant (7)
                $update = array('id_level' => 7);
                $this->merchant_m->update('tbl_user', 'id_user', (int)$this->user->id_user, $update);
                
                //# If sales changes, resend email
                if($data['curr_sales']!=''){
                    $sales      = $this->merchant_m->get_single('tbl_user','id_user',$this->input->post('merchant_sales'));
                    $message    = $this->input->post('merchant_name').' Telah memilih anda sebagai Salesnya';
                    send_mail($sales->email,'noreply@cipika.com','Pemberitahuan Merchant Baru',$message);
                }                
                //redirect(base_url('merchant/get_started'));
				$this->response(array(
					'status' => 1,
					'message' => 'Success'
				));
            }else{                
                //$data['error'] = validation_errors();
				$this->response(array(
					'status' => 0,
					'error' => 'Syarat dan Ketentuan harus diisi'
				),403);
            }        		        
    }
    
	function get_started_get()
    {
        $this->load->model('models-api/myproduct_m');
        $this->load->model('models-api/produk_m');
        
        //# Pastikan merchant blm punya produk
        $data['merchant_product']   = $this->merchant_m->get_merchant_product($this->user->id_user);
        if(count($data['merchant_product'])>0){
            //redirect(base_url('merchant/verification'));
        }
		$data['status']			= 1;
        // $data['page']        	= $this->home_m->get_all_data('tbl_page', 'sorting', 'asc');
        $data['kategori']       = $this->myproduct_m->get_data_where('tbl_kategori', 'id_parent', '0');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['provinsi']       = $this->produk_m->get_all_data_order('tbl_provinsi', 'nama_provinsi', 'asc');
        $store                  = $this->merchant_m->get_single('tbl_store','id_user',$this->user->id_user);
		$this->response($data);
	}
    /* Daftar Merchant : Step 2 Upload 1st Product */
    function get_started_post()
    {
        $this->load->model('models-api/myproduct_m');
        $this->load->model('models-api/produk_m');
        
        //# Pastikan merchant blm punya produk
        $data['merchant_product']   = $this->merchant_m->get_merchant_product($this->user->id_user);
        if(count($data['merchant_product'])>0){
            redirect(base_url('merchant/verification'));
        }                
			$nama_produk    = self::_check_request($this->post('nama_produk'), 1, 'nama_produk', 'required');
			$deskripsi    	= self::_check_request($this->post('deskripsi'), 1, 'deskripsi', 'skip');
			$berat    		= self::_check_request($this->post('berat'), 1, 'berat', 'required');
			$panjang    	= self::_check_request($this->post('panjang'), 1, 'panjang', 'integer');
			$lebar    		= self::_check_request($this->post('lebar'), 1, 'lebar', 'integer');
			$tinggi    		= self::_check_request($this->post('tinggi'), 1, 'tinggi', 'integer');
			$stok_produk    = self::_check_request($this->post('stok_produk'), 1, 'stok_produk', 'required');
			$harga_produk   = self::_check_request($this->post('harga_produk'), 1, 'harga_produk', 'required');
			$diskon    		= $this->post('diskon');
			$id_kategori    = self::_check_request($this->post('id_kategori'), 1, 'id_kategori','required');
			$harga_produk   = self::_check_request($this->post('harga_produk'), 1, 'harga_produk', 'numeric');
			$photo_img    	= self::_check_request($this->post('photo_img'), 1, 'photo_img', 'callback__check_foto_produk');
											  
            $volume = $this->input->post('panjang')*$this->input->post('lebar')*$this->input->post('tinggl');
            $volume_kon = $volume/6000;
            if($volume_kon > $this->input->post('berat')){
				$jne_berat = $volume_kon;
			} else {
				$jne_berat = $this->input->post('berat');
            }
			$input=array(
				'id_produk' => null,
				'nama_produk' => $nama_produk,
				'deskripsi' => $deskripsi,
				'berat' => $berat,
				'stok_produk' => $stok_produk,
				'harga_produk' => $harga_produk,
				'diskon' => $diskon,
				'publish' => 0,
				'view_notif' => 'N',
				'id_user' => $this->user->id_user,
				// 'id_kota' => $this->input->post('id_kota'),
				'panjang' => $panjang,
				'lebar' => $lebar,
				'tinggi' => $tinggi,
				'shipping_area' => $this->input->post('shipping_area'),
                'jne_berat' => $jne_berat,
				'date_added' => date('Y-m-d H:i:s'),
				'date_modified' => date('Y-m-d H:i:s')
			);
				//database transaction
			$this->db->trans_begin();

			$insert=$this->produk_m->insert('tbl_produk', $input);				
//			                      start afandi
			if (!empty($_POST['nama_tag'])) {
				$tag = explode(',', $_POST['nama_tag']);
                foreach ($tag as $a){
					$b = trim($a);
                                    //select tag
                    $select_tag = $this->db->query("select * from tbl_tag where nama_tag = '".$b."'");
                    if ($select_tag->num_rows() > 0) {
						$result = $select_tag->row();
                        $idtag = $result->id_tag;
                    } else {
						$insert_tag = array('id_tag' => null,
											'nama_tag' => $b,
                                            'date_added' => date('Y-m-d H:i:s'),
                                            'date_modified' => date('Y-m-d H:i:s')
                                        );
                        $idtag = $this->produk_m->insert('tbl_tag', $insert_tag);
					}
                                    //insert tag_relation
                    $insert_relation = array('id_produk_tag' => null,
                                        'id_produk' => $insert,
                                        'id_tag' => $idtag,
                                        'date_added' => date('Y-m-d H:i:s'),
                                        'date_modified' => date('Y-m-d H:i:s')
                                    );
                    $this->produk_m->insert('tbl_produk_tag', $insert_relation);
				}
			}
                           
//                                end afandi
                                
			if($insert){
				if(!empty($_POST['photo_img'])){
					foreach ($_POST['photo_img'] as $foto) {
						if($foto!=''){
							$inpdet=array(
                                'id_produk' => $insert,
                                'image'	 => $foto,
                                'date_added' => date('Y-m-d H:i:s'),
								'date_modified' => date('Y-m-d H:i:s')
                            );
							$produk_foto=$this->produk_m->insert('tbl_produkfoto', $inpdet);
						}
					}
				}
				foreach ($_POST['id_kategori'] as $id) {
					if($id!=''){
						$prod_kat=array('id_kategori'=>$id, 'id_produk'=>$insert, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
						$ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
					}
				}
			}
			// throw new Exception('ghghjgj');
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			    $this->response(array(
					'status' => 0,
					'error' =>'Failed'
				),403);				    
			} else {
			    $this->db->trans_commit();
			    $this->response(array(
					'status' => 1,
					'message' =>'success'
				));				    
			}
    }
    
    /* Daftar Merchant : Step 3 Status verifikasi oleh admin */
    function verification_get()
    {
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['status']=1;
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');		
		$data['merchant']= $this->merchant_m->get_single_merchant((int)$this->user->id_user);
        $this->response($data);
    }
    
    /* Setting Merchant Profile */
    public function index_get()
    {
        $this->load->library('lib_lokasi');
		$data['js_lokasi']      = TRUE;
		$data['menu_profile']   = "class='active'";
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['merchant']       = $this->merchant_m->get_single_merchant((int)$this->user->id_user);
		$data['propinsi']       = $this->lib_lokasi->get_propinsi();
        $data['data_sales']     = $this->merchant_m->get_sales();
        $data['curr_sales']     = $this->meta_m->get_meta_value('tbl_user',$this->user->id_user,'merchant_sales');        
		$this->response($data);
    }
    /*Mengganti Data Merchant*/
	public function change_merchant_post(){
		$this->load->library('lib_lokasi');
        $this->load->library('form_validation');        
		$data['merchant']       = $this->merchant_m->get_single_merchant((int)$this->user->id_user);
		$data['curr_sales']     = $this->meta_m->get_meta_value('tbl_user',$this->user->id_user,'merchant_sales');        
			$nama_store    = self::_check_request($this->post('nama_store'), 1, 'nama_store', 'required');
			$nama_pemilik    = self::_check_request($this->post('nama_pemilik'), 1, 'nama_pemilik', 'required');
			$deskripsi    = self::_check_request($this->post('deskripsi'), 1, 'deskripsi', 'required');
			$tgl_lahir_pemilik    = self::_check_request($this->post('tgl_lahir_pemilik'), 1, 'tgl_lahir_pemilik', 'required');
			$telpon    = self::_check_request($this->post('telpon'), 1, 'telpon', 'required');
			$bank_nama    = self::_check_request($this->post('bank_nama'), 1, 'bank_nama', 'required');
			$bank_norek    = self::_check_request($this->post('bank_norek'), 1, 'bank_norek', 'required');
			$bank_pemilik    = self::_check_request($this->post('bank_pemilik'), 1, 'bank_pemilik', 'required');
			          	
                $country            = get_visitor_country();
                $update = array('id_kota'               => $this->input->post('kecamatan'),
                                'id_sales'              => $this->input->post('merchant_sales'),
                                'negara'                => $country['country'],
                                'nama_store'            => $this->input->post('nama_store'),
                                'deskripsi'             => $this->input->post('deskripsi'),
                                'nama_pemilik'          => $this->input->post('nama_pemilik'),
                                'tgl_lahir_pemilik'     => strftime("%Y-%m-%d",strtotime($this->input->post('tgl_lahir_pemilik'))),
                                'alamat'                => '',
                                'telpon'                => $this->input->post('telpon'),
                                'bank_nama'             => $this->input->post('bank_nama'),
                                'bank_norek'            => $this->input->post('bank_norek'),
                                'bank_pemilik'          => $this->input->post('bank_pemilik'),                                
                                'date_modified'         => date('Y-m-d H:i:s')
                                );
                $this->merchant_m->update('tbl_store','id_store',(int)$data['merchant']->id_store,$update);                
                //# If sales changes, resend email
                if($data['curr_sales']!=''){
                    $sales      = $this->merchant_m->get_single('tbl_user','id_user',$this->input->post('merchant_sales'));
                    $message    = $this->input->post('merchant_name').' Telah memilih anda sebagai Salesnya';
                    send_mail($sales->email,'noreply@cipika.com','Pemberitahuan Merchant Baru',$message);
                }                
                //redirect(base_url('merchant/get_started'));
				$this->response(array(
					'status' => 1,
					'message' => 'Success'
				));
                   		        
	}
    /* Change Merchant Email */
    public function email_get()
    {
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['status'] = 1;
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['user']           = $this->merchant_m->get_single('tbl_user','id_user', $this->user->id_user);
		$this->response($data);
    }
	public function email_post()
    {       
        //# Update user email
            $email       = self::_check_request($this->post('email'), 1, 'email', 'required');
            $email_confirm       = self::_check_request($this->post('email_confirm'), 1, 'email_confirm', 'required');		
            if(isset($email) && ($email == $email_confirm)){
                $this->user_m->update('tbl_user', 'id_user', $this->user->id_user, array('email'=>$this->input->post('email')));
                $this->response(array(
					'status' => 1,
					'message' => 'success'
				));
            } else {
				$this->response(array(
					'status' => 0,
					'error' => 'failed'
				));
			}                
        //$this->load->view('publik/merchant/change_email_v', $data);
    }
    
    /* Setting food delivery city */
    public function food_delivery_get()
    {
		$data['status'] = 1;
		$data['delivery_city']   = $this->meta_m->get_meta('tbl_user',$this->user->id_user);
        $data['provinsi']    = $this->merchant_m->get_provinsi();
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        		
        $this->response($data);
    }
    
    /* Manage Order */
    public function orders_get()
    {
        $this->load->model('models-api/order_m');
        //$data['menu_order']     = "class='active'";
        //$data['page']           = $this->home_m->get_all_data('tbl_page', 'sorting', 'asc');
        $data['orders']         = $this->merchant_m->get_merchant_order((int)$this->user->id_user,0,100,'done');
        
        $this->response($data);
    }
    
    /* Edit Order */
    public function edit_order_get()
    {
        $this->load->model('models-api/order_m');
        $this->load->library('lib_lokasi');
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        //$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $idorder                = $this->uri->segment(3);        
		$data['order']          = $this->merchant_m->get_order($idorder);		        	
        $this->response($data);
    }
    
    /* Update Order Status */
    public function update_order_post()
    {
        $this->load->model('models-api/order_m');
        $this->load->library('lib_lokasi');
        
        /*$this->load->library('form_validation');
		$this->form_validation->set_rules('ongkir_merchant', 'Ongkir', 'numeric|callback__input_kirim');
		$this->form_validation->set_rules('noresi', 'AWB Number', 'callback__input_kirim');
		$this->form_validation->set_rules('delivery_date', 'Tgl Kirim', 'callback__input_kirim');
        
        $this->form_validation->set_message('numeric', '%s, harus berupa angka.');
        
        if ($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
		}else{*/
			$ongkir    			= self::_check_request($this->post('ongkir_merchant'), 1, 'ongkir_merchant', 'integer');
			$noresi    			= self::_check_request($this->post('noresi'), 1, 'noresi', 'skip');
			$delivery_date  	= self::_check_request($this->post('delivery_date'), 1, 'delivery_date', 'skip');
			$status_delivery  	= self::_check_request($this->post('status_delivery'), 1, 'status_delivery', 'skip');
            //$status_delivery    = $this->input->post('status_delivery');
        
            if( ($noresi!='') ){
                $status_delivery = "proses pengiriman";
            }
            
            $delivery_date = ($this->input->post('delivery_date')!='')?strftime("%Y-%m-%d",strtotime($this->input->post('delivery_date'))):'';
            $input  = array(                       
                        'status_delivery'   => $status_delivery,
                        'ongkir_merchant'   => $this->input->post('ongkir_merchant'),
                        'noresi'            => $this->input->post('noresi'),
                        'delivery_date'     => $delivery_date
                        );                        
            $insert=$this->order_m->update('tbl_order', 'id_order', $this->input->post('id_order'), $input);
            $this->response(array(
				'status' => 1,
				'message' => 'success'
			));
			//redirect('merchant/orders');
        //}
        
        /*$data['menu_order']     = "class='active'";
        $data['page']           = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $idorder                = $this->input->post('id_order');        
		$data['order']          = $this->merchant_m->get_order($idorder);		
        */
        //$this->load->view('publik/merchant/edit_order_v', $data);
    }
    
    /* AJAX : Get all kota by idprovinsi */
    public function get_city()
    {
        $idprovinsi     = $this->input->post('provinsi');
        $data['city']   = $this->merchant_m->get_city($idprovinsi);
        
        echo $this->load->view('publik/merchant/ajax_city_v', $data,TRUE);
    }
    
    /* AJAX : Add Food Delivery City */
    public function add_city_post()
    {
        
        $idcity     = $this->input->post('idcity');
        $metas      = array('merchant_food_delivery_city' => $idcity);
        $this->meta_m->save_meta('tbl_user',$this->user->id_user,$metas,'add');
		$this->response(array(
			'status' => 1,
			'message' => 'success'
		));
    }
    
    /* AJAX : Get all delivery city */
    public function get_delivery_city_get()
    {
        $data['delivery_city']   = $this->meta_m->get_meta('tbl_user',$this->user->id_user);
        
        echo $this->load->view('publik/merchant/ajax_delivery_city_v', $data,TRUE);
    }
        
    /* AJAX : remove 1 delivery city */
    public function remove_delivery_city_post()
    {
        $idmeta = $this->input->post('idmeta');
        $this->meta_m->delete_meta($idmeta);
		$this->response(array(
			'status' => 1,
			'message' => 'success'
		));
    }
 
	//Private Function///
    function _input_kirim_post()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( (($ongkir==0) || ($ongkir=='')) && ($noresi=='') && ($tgl_kirim=='') ){    
            return TRUE;
        }else{
            
            if( ($ongkir>0) && ($noresi!='') && ($tgl_kirim!='')  ){
                return TRUE;
            }else{
                $this->form_validation->set_message('_input_kirim', '%s Tidak boleh kosong ');
                return FALSE;
            }
                
        }
    }
 
    function _check_ongkir()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( ($ongkir==0) || ($ongkir=='') ){
            return TRUE;
        }else{
            $msg = "";
            
            if($noresi==''){
                $msg.="No Resi ";
            }
            
            if($tgl_kirim==''){
                $msg.="Tgl Kirim ";
            }
            
            $this->form_validation->set_message('_check_ongkir', $msg.'tidak boleh kosong');                
            
            return FALSE;
        }
    }
    
    function _check_noresi()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( $noresi=='' ){
            return TRUE;
        }else{
            $msg = "";
            
            if( ($ongkir==0) || ($ongkir=='') ){
                $msg.="Ongkir ";
            }
            
            if($tgl_kirim==''){
                $msg.="Tgl Kirim ";
            }
            
            $this->form_validation->set_message('_check_noresi', $msg.'tidak boleh kosong');                
            
            return FALSE;
        }
    }
    
    function _check_tglkirim()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( $tgl_kirim=='' ){
            return TRUE;
        }else{
            $msg = "";
            
            if( ($ongkir==0) || ($ongkir=='') ){
                $msg.="Ongkir ";
            }
            
            if($noresi==''){
                $msg.="No Resi ";
            }
            
            $this->form_validation->set_message('_check_tglkirim', $msg.'tidak boleh kosong');                
            
            return FALSE;
        }
    }
	
	/*Private Function*/
	protected function _check_request($request, $mandatory, $name, $validate, $param = NULL, $return = NULL)
    {
        // Request ada
        if ($request !== FALSE) {
            $bool = TRUE;

            if ($validate != 'skip') {
                if (is_null($param))
                    $bool = $this->form_validation->$validate($request);
                else
                    $bool = $this->form_validation->$validate($request, $param);
            }

            if ($bool === FALSE) {
                $this->response(array(
                    'status'=> 0,
                    'error' => "Your parameter '" . $name . "' is not valid."
                ));
            }

            if(empty($request) && $mandatory) {
                $this->response(array(
                    'status'    => 0,
                    'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
                ), 403);
            }

            //kirim kembali request
            $return = $request;
        }
        // Request kosong, mandatory
        else if (is_null($request) && $mandatory) {
            // tampilkan error
            $this->response(array(
                'status'    => 0,
                'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
            ), 403);
        }

        return $return;
    }
}
