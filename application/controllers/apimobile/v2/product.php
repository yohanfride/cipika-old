<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Product extends API_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/produk_m');
		$this->load->model('models-api/myproduct_m');
	}
	
	protected $methods = array(
        'detail_get'  => array('key' => FALSE)        
    );
	
	public function detail_get() {
		$id = $this->_check_request($this->get('id'), 1, 'id', 'integer');
		$data=array();
		$data['status'] = 1;
		$data['produk']     = $this->produk_m->get_produk($id);        
		$data['merchant']   = $this->produk_m->get_single('tbl_store','id_user',$data['produk']->id_user);
		$data['produk_other']=$this->produk_m->get_produk_other($id, 0, 4);
		$data['produk_foto']=$this->produk_m->get_foto($id);
		$data['jml_komen']=$this->produk_m->count_comment($id);
		$data['jml_love']=$this->produk_m->count_love($id);
		$data['jml_list']=$this->produk_m->count_list($data['produk']->id_user);
		$data['jml_love_all']=$this->produk_m->count_love_all($data['produk']->id_user);
		$data['jml_follower']=$this->produk_m->count_follower($data['produk']->id_user);
		$data['jml_following']=$this->produk_m->count_following($data['produk']->id_user);
		$data['comment']=$this->produk_m->get_comment($id, 5, 0);
		$data['kategori']=$this->myproduct_m->get_kategori(md5($id));
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['tag']=$this->produk_m->get_data_join_where('tbl_produk_tag', 'tbl_tag', 'id_tag', 'id_produk', $id);
		$total_view=$data['produk']->viewed+1;
		$this->produk_m->update('tbl_produk', 'id_produk', $id, array('viewed'=>$total_view));
                $data['meta'] = true;

		if(count($this->user)!=0){
			$id_user_followed=$id;
			$id_user_following=$this->auth_m->get_user()->id_user;
			$cek=$this->auth_m->cek('tbl_follow', 'id_user_following', 'id_user_followed', $id_user_following, $id_user_followed);
			if($cek>0){
				//$data['btn_follow']='<a href="#" class="btn btn-sm btn-unfollow btn-primary">Unfollow</a>';
				$data['btn_follow']='Unfollow';				
			} else {
				//$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
				$data['btn_follow']='Follow';
			}
		} else {
			//$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
			$data['btn_follow']='Follow';
		}		
		        
        if(!empty($data['merchant'])){
            if($data['merchant']->store_status!='approve'){
                $this->response(array(
					'status' => 0,
					'message' =>'Sorry no products currently available, return to the previous page.'
				));
            }
        }
		$this->response($data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */