<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Myarticle extends API_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('models-api/article_m');
    }
	
	protected $methods = array(
        'index_get' => array('level' => 1),
        'upload_post'        => array('level' => 1),
        'edit_get'      => array('level' => 1),
        'edit_product_post'      => array('level' => 1)   
    );
	
    public function index_get($offset = 0) {
        $this->load->model('models-api/auth_m');
        if (count($this->user)==0) {
            redirect(base_url(), 'refresh');
        }
        $data = array();
        $id_user = $this->user->id_user;
        if(isset($_GET['hal'])){$hal=$_GET['hal'];}
        else {$hal='';}

        $dataPerhalaman=5;
        ($hal=='')?$nohalaman = 1:$nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs( (int) $offset);
        $data['offset']=$offset;
        $jmldata=$this->article_m->count_all_data_where('tbl_article', 'id_user', $id_user);
        $data['paginator']=$this->article_m->page($jmldata, $dataPerhalaman, $hal);

        $data['datas']=$this->article_m->get_article_user($id_user, $dataPerhalaman, $off);
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->response($data);
    }

    public function add_get() {
        $data = array();        
        $this->load->library('form_validation');
		$title    = self::_check_request($this->post('title'), 1, 'title', 'required');
		$content  = self::_check_request($this->post('content'), 1, 'content', 'skip');
            
            /*// First, delete old captchas
            $expiration = time()-7200; // Two hour limit
            $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	

            // Then see if a captcha exists:
            $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = '".$_POST['captcha']."' AND ip_address = '".$this->input->ip_address()."' AND captcha_time > '".$expiration."'";
            $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
            $query = $this->db->query($sql, $binds);
            $row = $query->row();
            
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {
                
                if ($row->count == 0)
                {
                    $data['error'] = "You must submit the word that appears in the image";
                }else{
               */ 
                $input = array(
                    'id_article' => null,
                    'article_slug' => $this->commonlib->generate_permalink($title, 'tbl_article', 'id_article', 'article_slug'),
                    'title' => $title,
                    'id_user' => $this->user->id_user,
                    'publish' => 0,
                    'content' => $content,
                    'date_added' => date('Y-m-d H:i:s'),
                    'date_modified' => date('Y-m-d H:i:s'),
                );
                $insert = $this->article_m->insert('tbl_article', $input);
                if ($insert){
                    $this->response(array(
						'status' => 1,
						'message' =>'succses'
					));	
				} else {
                    $this->response(array(
						'status' => 0,
						'error' =>'Failed'
					),403);	
				}
                /*}
                } */              
    }
	
	public function edit_get(){
		$id_article = $this->_check_request($this->get('id_article'), 1, 'id_article', 'skip');
		$id_user = $this->_check_request($this->get('id_user'), 1, 'id_user', 'skip');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['article'] = $this->article_m->get_article_single($id_article, $id_user);
        $this->response($data);
	}
	
    public function edit_post($id_article = '', $id_user = '') {
        $data = array();        
        $this->load->library('form_validation');
        $title    = self::_check_request($this->post('title'), 1, 'title', 'required');
		$content  = self::_check_request($this->post('content'), 1, 'content', 'skip');
		if ($this->form_validation->run() == FALSE) {
			$data['error'] = validation_errors();
		} else {
			$input = array(
                    'article_slug' => $this->commonlib->generate_permalink($this->input->post('title'), 'tbl_article', 'id_article', 'article_slug'),
                    'title' => $this->input->post('title'),
                    'id_user' => $this->session->userdata('member')->id_user,
                    'publish' => 0,
                    'content' => $this->input->post('content'),
                    'date_modified' => date('Y-m-d H:i:s'),
			);
            $insert = $this->produk_m->update('tbl_article', 'md5(id_article)', $id_article, $input);
                
				if ($insert){
                    $this->response(array(
						'status' => 1,
						'message' =>'succses'
					));	
				} else {
                    $this->response(array(
						'status' => 0,
						'error' =>'Failed'
					),403);	
				}
        
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');        
    }

    public function delete($id_article = '', $id_user = '') {
        $data = array();
        //$data['success'] = '';
        //$data['error'] = '';
        $del_produk = $this->article_m->delete_user($id_article, $id_user);
        //redirect(base_url('myarticle?delete=success'));
		$this->response(array(
			'status' => 1,
			'message' =>'succses'
		));	
    }
    /**/
    public function redactor_upload() {
        $img = 'file';
        $config['file_name'] = preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES[$img]['name']) . date('dmYHis');
        $config['upload_path'] = "upload/media/";
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size'] = '5060';
        $config['max_width'] = '10000';
        $config['max_height'] = '10000';
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($img)) {
            $return['error'] = $this->upload->display_errors();
            $return['file_name'] = '';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $return['error'] = '-';
            $return['file_name'] = $data['upload_data']['file_name'];
        }
        $this->upload->file_name = '';

        //# Save Sticker to database
//        $insert = array('idmedia' => null,
//            'media_url' => base_url('upload/media') . "/" . $return['file_name'],
//            'media_type' => 'image'
//        );
//
//        $this->media_m->save('media', $insert);
        $array = array(
            'filelink' => base_url('upload/media') . "/" . $return['file_name']
        );
        echo stripslashes(json_encode($array));
    }
    
    public function redactor_file() {
        $img = 'file';
        $config['file_name'] = preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES[$img]['name']) . date('dmYHis');
        $config['upload_path'] = "upload/file/";
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = '*';
        $config['max_size'] = '50060';
        $config['max_width'] = '10000';
        $config['max_height'] = '10000';
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($img)) {
            $return['error'] = $this->upload->display_errors();
            $return['file_name'] = '';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $return['error'] = '-';
            $return['file_name'] = $data['upload_data']['file_name'];
        }
        $this->upload->file_name = '';

        //# Save Sticker to database
//        $insert = array('idmedia' => null,
//            'media_url' => base_url('upload/file/') . $return['file_name'],
//            'media_type' => 'file'
//        );
//
//        $this->media_m->save('media', $insert);

        if ($return['file_name'] == '') {
            echo "Upload Failed, " . $return['error'];
        } else {
            $array = array(
                'filelink' => base_url('upload/file/') . $return['file_name']
            );
            echo stripslashes(json_encode($array));
        }
    }

	
	/*Private Function*/
	protected function _check_request($request, $mandatory, $name, $validate, $param = NULL, $return = NULL)
    {
        // Request ada
        if ($request !== FALSE) {
            $bool = TRUE;

            if ($validate != 'skip') {
                if (is_null($param))
                    $bool = $this->form_validation->$validate($request);
                else
                    $bool = $this->form_validation->$validate($request, $param);
            }

            if ($bool === FALSE) {
                $this->response(array(
                    'status'=> 0,
                    'error' => "Your parameter '" . $name . "' is not valid."
                ));
            }

            if(empty($request) && $mandatory) {
                $this->response(array(
                    'status'    => 0,
                    'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
                ), 403);
            }

            //kirim kembali request
            $return = $request;
        }
        // Request kosong, mandatory
        else if (is_null($request) && $mandatory) {
            // tampilkan error
            $this->response(array(
                'status'    => 0,
                'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
            ), 403);
        }

        return $return;
    }
}

/* End of file article.php */
/* Location: ./application/controllers/article.php */