<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'core/API_Controller.php';

class Store extends API_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/store_m');
		$this->load->model('models-api/produk_m');
	}
	
	protected $methods = array(
        'id_get'  => array('key' => FALSE),
        'follower_get'  => array('key' => FALSE),
        'following_get'  => array('key' => FALSE)
    );
	
	public function index_get(){
		redirect(base_url());
	}

	public function id_get(){
		$id = $this->_check_request($this->get('id'), 1, 'id', 'skip');
		if($id=='')redirect(base_url());
		$data=array();
		$data['status'] = 1;
		$data['id']=$id;
		if(isset($_GET['s'])){
			$data['by_search'] = $_GET['s'];
		} else {
			$data['by_search'] = '';
		}
		//infinite scroll
		$limit=12;
		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
		else{$offset=0;}

		$data['produk']=$this->store_m->get_listing($id, $limit, $offset);

		$data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
                
        if(count($this->user)!=0){
			$id_user_followed=$id;
			$id_user_following=$this->auth_m->get_user()->id_user;
			$cek=$this->auth_m->cek('tbl_follow', 'id_user_following', 'id_user_followed', $id_user_following, $id_user_followed);
			if($cek>0){
				//$data['btn_follow']='<a href="#" class="btn btn-sm btn-unfollow btn-primary">Unfollow</a>';
				$data['btn_follow']='Unfollow';				
			} else {
				//$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
				$data['btn_follow']='Follow';
			}
		} else {
			//$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
			$data['btn_follow']='Follow';
		}		
		$this->response($data);
	}

	public function follower_get(){
		$id = $this->_check_request($this->get('id'), 1, 'id', 'integer');
		if($id=='')redirect(base_url());
		$data=array();
		$data['status'] = 1;
		$data['id']=$id;
		$data['follower']=$this->store_m->get_follower($id);

		$data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
		$this->response($data);
	}

	public function following_get(){
		$id = $this->_check_request($this->get('id'), 1, 'id', 'integer');
		if($id=='')redirect(base_url());
		$data=array();
		$data['status'] = 1;
		$data['id']=$id;
		$data['following']=$this->store_m->get_following($id);

		$data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
		$this->response($data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */