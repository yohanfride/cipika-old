<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

class Auth extends REST_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/auth_m');
        $this->load->model('models-api/user_m');       
		$this->load->library('form_validation');
	}
	protected $methods = array(
        'login_post'            => array('key' => FALSE),
        'register_post'         => array('key' => FALSE),
        'activate_get'         	=> array('key' => FALSE),
        'login_fb_get'         		=> array('key' => FALSE),
        'forgot_password_post'  => array('key' => FALSE)
    );
	function index()
	{
	
	}	
	/**
     * Register user baru, dengan email.
     */
    public function register_post()
    {
        $data = array();
        // cek parameter
        $email       = self::_check_request($this->post('email'), 1, 'email', 'valid_email');
        $password    = self::_check_request($this->post('password'), 1, 'password', 'skip');
        $app_version = self::_check_request($this->post('app_version'), 0, 'app_version', 'skip', NULL, 0);
        $fb_token    = self::_check_request($this->post('fb_token'), 0, 'fb_token', 'skip', '');
        // jika register menggunakan facebook connect
        if ( !empty($fb_token)  && ($fb_token!='')){
            //$fb_uid             = self::_check_request($this->post('fb_uid'), 1, 'fb_uid', 'skip');
            $fb_token_expired   = self::_check_request($this->post('fb_token_expired'), 1, 'fb_token_expired', 'skip');
			$password = randomPassword();
			$activation = 1;
        } else {
			$fb_token_expired = '';
			$activation = 0;
		}
        // cek email
        if(self::_email_exist($email)) {
            $this->response(array(
                    'status'    => 0,
                    'error'     => 'Email is already taken.'
                ), 403);
        } else {       
			// insert data user
			$kode_random = random_string('alnum', 21);
			$input=array(
				'id_user' => null,
				// 'username' => 'Anonymous',
				'password' => md5($password),
				'email' => $email,
				'activation_code' => $kode_random,
				'id_level' => 6,
				'date_added' => date('Y-m-d H:i:s'),
				'fb_token' => $fb_token,
				'fb_token_expired' => $fb_token_expired,
				'active' => $activation
			);
			$kode_random = random_string('alnum', 21);			
			$hasil=$this->auth_m->insert('tbl_user', $input);                      
			$id_user = $hasil;
			/* buat key */
			$key = self::_generate_key();

			/* simpan key */
			$id_key = self::_insert_key($key, array('level' => 1, 'ignore_limits' => 1));
			$this->auth_m->update('tbl_user', 'id_user', $id_user,array('id_key' => $id_key));
			
			if($activation == 0) {
				/*Kirim Email Validasi*/			
				$email_dihash = str_replace('@', md5('@'), $this->input->post('email'));
				$data_email = '<strong>Member CipikaStore Yth,</strong>

							<p>Selamat! Anda berhasil melakukan pendaftaran Member CipikaStore dengan detil informasi sebagai

							berikut:</p>

							<table style="border:none;">
							<tr>
							<td style="width:150px;">Username</td><td>:</td><td>'. $input['email'] .'</td>
							</tr>
							<tr>
							<td>Link Validasi</td><td>:</td><td>'.base_url().$this->config->slash_item('index_page').'auth/activate/'.$hasil.'/'.$email_dihash.'/'.$kode_random.$this->config->item('url_suffix').'</td>
							</tr>
							</table>
							<p>
							Segera klik Link di atas untuk validasi akun, dan abaikan kode verifikasi dalam proses ini. Terima 	
							kasih.</p> 
							<br>
							<p>Sales & Administration</p>';
				$config=$this->_mail_win();
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from($this->config->item('email_from'), 'Cipika Store');
				$this->email->to($input['email']);
				$this->email->subject('Aktivasi registrasi member cipikastore.com');
				$this->email->message($data_email);
				$send = $this->email->send();
			} else {
				$this->response(array(
					'status' => 1,
					'key'    => $key,
					'message' => 'Login Success'
				));
			}			
			// send response
			$this->response(array(
				'status' => 1,
				'key'    => $key,
				'message' => 'Register Success'
			));
		}
    }		
	
	public function activate_get($id_user='', $email='', $kode_aktivasi=''){		
		if($id_user=='' OR $email=='') redirect(base_url(), 'refresh');
		$data = array();		
		$data['aktivasi_berhasil'] = FALSE;
		if($kode_aktivasi==''){
			$data['belum_diaktifkan'] = TRUE;
			$data['email']=str_replace(md5('@'), '@', $email);
		}
		else {
			$data_update = array(
					'id_user'			=> (int)$id_user,
					'email'			=> $email,
					'kode_aktivasi'	=> $kode_aktivasi,
					'status_aktif'	=> 1,
				);
				
			$data['aktivasi_berhasil'] = $this->auth_m->update_registrasi($data_update);
			if ( $data['aktivasi_berhasil'] ){  
				$email =str_replace(md5('@'), '@', $email);
				//# Set auto logged in
				$user				= self::_email_exist($email);
				$hasil['status']    = 1;
				$hasil['key'] 		= self::_check_key($user);
				$this->response($hasil, 200);
			} else {
				$this->response(array(
					'status'    => 0,
					'error'     => 'Your activation link is wrong.'
				), 403);
			}
		}		
	}
	
	//log the user in
	function login_post()
	{
		$gcm_id      = self::_check_request($this->post('gcm_id'), 0, 'gcm_id', 'skip');
        $app_version = self::_check_request($this->post('app_version'), 0, 'app_version', 'skip', '');

        // login with email
    	$email      = self::_check_request($this->post('email'), 0, 'email', 'valid_email');
        // login with facebook
        $fb_token   = self::_check_request($this->post('fb_token'), 1, 'fb_token', 'skip');
        if (!is_null($fb_token)) {
            //cek param token           
            //ambil data user
            $userfb = $this->db->where('email', $email)->get('tbl_user')->row();
            // jika user ada
            if ($userfb) {
                // jika fb_token tidak sama, update
                if (!$userfb->fb_token = $fb_token){
                    $this->db->set('fb_token', $fb_token)->where('email', $email)->update('tbl_user');
                }

                // build response
                $hasil['status']    = 1;
                $hasil['key'] = self::_check_key($userfb);

                // update GCM token
                $this->db->set(array(
                    'gcm_id' => $gcm_id,
                    'app_version' => $app_version
                ))->where('id_user', $userfb->id_user)->update('tbl_user');

                // send response
                $this->response($hasil, 200);
            } else {
                $this->response(array(
                    'status'    => 0,
                    'error'     => 'User not found.'
                ), 403);
            }
        }
		if (!is_null($email)){
            // check password
            $password = md5(self::_check_request($this->post('password'), 1, 'password', 'skip'));			
            /* get data user */
            $user          = self::_email_exist($email);
            if ($user) {
				//Cek Active User
				if($user->active == 1){
					/* email dan password benar*/
					if ($user->password == $password){
						$hasil['status']    = 1;
						$hasil['key'] = self::_check_key($user);

						// update GCM token
						if (isset($gcm_id)){
							$this->db->set(array(
								'gcm_id' => $gcm_id,
								'app_version' => $app_version
							))->where('id_user', $user->id_user)->update('tbl_user');
						}
						// send response
						$this->response($hasil, 200);
					} else {  /* password salah */
						$this->response(array(
							'status'    => 0,
							'error'     => 'Your password is invalid.'
						), 403);
					}
				} else {
					$this->response(array(
						'status'    => 0,
						'error'     => 'Your account is not active.'
					), 403);
				}	
            } else { /* email salah */ 
                $this->response(array(
                    'status'    => 0,
                    'error'     => 'User not found.'
                ), 403);
            }
        }  				
        $this->response(array(
            'status'    => 0,
            'error'     => "Your parameter is not complete."
        ), 403);
	}
	
	///Login Dengan Facebook
	/*function login_fb_get() {
        /* $konek = $this->lib_facebook->connect();
        if (isset($konek['user_profile'])) {
            if ($konek['user']) {
                $user = $this->user_m->get_email_user($konek['user_profile']['email']);
                if (!empty($user)) { registered, force login 
//                    echo 'adsd';
//                    $remember = 1;
					if ($this->auth_m->login_fb($user->email)){
						$user				= self::_email_exist($email);
						$hasil['status']    = 1;
						$hasil['key'] 		= self::_check_key($user);
						$this->response($hasil, 200);
					}	
                } else {
                    $password = random_password();
                    $email = $konek['user_profile']['email'];
                    $input1 = array(
                        "username" => $konek['user_profile']['username'],
                        "email" => $email,
                        "password" => md5($password),
                        "firstname" => $konek['user_profile']['first_name'],
                        "lastname" => $konek['user_profile']['last_name'],
                        "fb_token" => $konek['token'],
                        "active" => 1
                    );

                    $this->user_m->insert('tbl_user', $input1);
                    if ($this->auth_m->login_fb($email)){
                        $user				= self::_email_exist($email);
						$hasil['status']    = 1;
						$hasil['key'] 		= self::_check_key($user);
						$this->response($hasil, 200);
                    }                        
                }
            }
        } else {
			$this->response(array(
				'status' => 0,
				'error' =>'failed'
			),403);
		} 
    }*/
	/**
     * Request to change password, send to email
     */
    public function forgot_password_post()
    {
		// cek param
        $email  = self::_check_request($this->post("email"), 1, 'email', 'valid_email');
        $identity = $this->auth_m->cek_email($email);				
        if($identity==0) {
			$this->response(array(
                'status'    => 0,
                'error'     => 'User can\'t be found.'
            ));
        } else {
			$new_pass=$this->randomPassword();
			$insert=array(
				'password'=>md5($new_pass)
			);
			$this->auth_m->update('tbl_user', 'email', $email, $insert);
			$data_email='<p>Password Anda telah direset, berikut informasi login Anda yang baru: </p>';
			$data_email.='<ul><li>Email: '.$email.'</li><li>Password: '.$new_pass.'</li></ul>';

			$config=$this->_mail_win();
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($this->config->item('email_from'), 'Cipika Store');
			$this->email->to($email);
			// $this->email->bcc($this->config->item('email_tujuan'));
			$this->email->subject('Lupa Password');
			$this->email->message($data_email);
			$send = $this->email->send();
			
			if($send){
				$this->response(array(
					'status' => 1,
					"message" => "Profile detail and reset password has been sent to your email."
				));
			}           
        }			
	}

	function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}
	
	
	/* Private Data Methods */

    // Cek key sudah ada atau belum
    private function _key_exists($key)
    {
        return $this->db->where(config_item('rest_key_column'), $key)->count_all_results(config_item('rest_keys_table')) > 0;
    }

    // Cek email sudah ada atau belum
    private function _email_exist($email)
    {
        //return $this->db->where('email', $email)->get('tbl_user')->row();
		$sql = "SELECT * FROM tbl_user WHERE email='".$email."'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data;
    }
	
    // Ambil key berdasarkan $id
    private function _get_key($id)
    {
        return $this->db->where('id', $id)->get(config_item('rest_keys_table'))->row();
    }

    /* Private Helper */

    /**
     * Cek user key, dan generate jika belum punya
     *
     * @param $user
     * @return string $key
     */
    private function _check_key($user)
    {
        // cek sudah ada key
        if(is_null($user->id_key)){
            // buat key
            $key = self::_generate_key();

            // simpan key
            $id_key = self::_insert_key($key, array('level' => 1, 'ignore_limits' => 1));
            $this->db->set('id_key', $id_key)->where('id_user', $user->id_user)->update('tbl_user');

            // return key
            return $key;
        } else {
            // return key
            $keycolumn = config_item('rest_key_column');
            return self::_get_key($user->id_key)->$keycolumn;
        }
    }	
	
    /* Helper Methods */

    /**
     * Generate Key
     *
     * @return string
     */
    private function _generate_key()
    {
        $this->load->helper('security');

        do
        {
            $salt = do_hash(time().mt_rand());
            $new_key = substr($salt, 0, config_item('rest_key_length'));
        }

            // Already in the DB? Fail. Try again
        while (self::_key_exists($new_key));

        return $new_key;
    }

    /**
     * Insert key to table
     *
     * @param $key
     * @param $data
     * @return mixed
     */
    private function _insert_key($key, $data)
    {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();

        $this->db->set($data)->insert(config_item('rest_keys_table'));

        return $this->db->insert_id();
    }

    /**
     * Check request
     *
     * @param $request      "request yang didapat"
     * @param $mandatory    "mandatory dari parameter"
     * @param $name         "nama parameter"
     * @param $validate     "metode validasi"
     * @param $param        "parameter untuk validasi"
     * @param $return       "return default value jika kosong"
     * @return
     */
    protected function _check_request($request, $mandatory, $name, $validate, $param = NULL, $return = NULL)
    {
        // Request ada
        if ($request !== FALSE) {
            $bool = TRUE;

            if ($validate != 'skip') {
                if (is_null($param))
                    $bool = $this->form_validation->$validate($request);
                else
                    $bool = $this->form_validation->$validate($request, $param);
            }

            if ($bool === FALSE) {
                $this->response(array(
                    'status'=> 0,
                    'error' => "Your parameter '" . $name . "' is not valid."
                ));
            }

            if(empty($request) && $mandatory) {
                $this->response(array(
                    'status'    => 0,
                    'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
                ), 403);
            }

            //kirim kembali request
            $return = $request;
        }
        // Request kosong, mandatory
        else if (is_null($request) && $mandatory) {
            // tampilkan error
            $this->response(array(
                'status'    => 0,
                'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
            ), 403);
        }

        return $return;
    }
	/**Function Kirim Email*/
	function _mail_win()
    {
        $config = array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }
}