<?php
/* afandi */

require_once APPPATH . 'core/API_Controller.php';

class chat extends API_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/chat_m');
		$this->load->model('models-api/user_m');
		$this->load->driver('cache');
		$this->load->helper('app_helper');
	}
		
	public function addMessageAction_post(){
		$to       = self::_check_request($this->post('to'), 0, 'to', 'skip');
		$from     = $this->user->id_user;//self::_check_request($this->post('from'), 0, 'from', 'skip');
		$message  = self::_check_request($this->post('message'), 0, 'from', 'skip');
		$cekto	  = $this->chat_m->cek_user($to);
		$cekfrom  = $this->chat_m->cek_user($from);
		if($cekto == 0){
			$this->response(array(
				'status' => 0,
				'error' => "User to can't found"
			));
		}
		if($cekfrom == 0){
			$this->response(array(
				'status' => 0,
				'error' => "User from can't found"
			));
		}
		$insert = array(
			'to_user_id' => $to,
			'from_user_id' => $from,
			'time' => date("Y-m-d H:i:s"),
			'message' => $message,
			'status' => 0
		);
		$ins = $this->chat_m->insert('tbl_private_messages',$insert);
		$id = $this->db->insert_id();
		$data['status'] = 1;
		$data['new_message']=$this->chat_m->get_chat_id($id);
		$this->response($data);
	}
	
	public function getListUserMessageAction_get(){
		$to = $this->user->id_user;
		if( isset($_GET['limit']) ){
			$limit = $this->get('limit');
		} else {
			$limit = 20;
		}		
		if ( isset($_GET['offset']) ){
			$offset = $this->get('offset');
		} else {
			$offset = 0;
		}
		$data = $this->chat_m->get_list_chat($to,$limit,$offset);		
		foreach ($data as $rows){
			$jumlah = $this->chat_m->get_count_chat_unread($rows->from_user_id,$rows->to_user_id);
			if ($rows->from_user_id == $to){
                continue;
            } else {
                $hasil['user'][]= array(
                    'id_user' => $rows->from_user_id,
                    'name' => $rows->username,
                    'email' => $rows->email,
                    'avatar' => $rows->image,
                    'unread' => $jumlah
                );
            }
		}
		$hasil['status'] = 1;
		$this->response($hasil);
	}
	public function getMessageDetailAction_get(){
		$to       = $this->user->id_user;
		$from 	  = self::_check_request($this->get('from'), 0, 'to', 'skip');
		if( isset($_GET['limit']) ){
			$limit	  = self::_check_request($this->get('limit'), 0, 'to', 'skip');
		} else {
			$limit = 20;
		}		
		if ( isset($_GET['offset']) ){
			$offset	  = self::_check_request($this->get('offset'), 0, 'to', 'skip');
		} else {
			$offset = 0;
		}		
		$data  = $this->chat_m->get_detail_chat($from,$to,$limit,$offset);		
		foreach ($data as $rows){
				$this->chat_m->update_status($from,$to);			                
				$hasil['user'][]= array(
                    'id_post' => $rows->id,
					'id_user' => $rows->from_user_id,
                    'name' => $rows->username,
                    'email' => $rows->email,
                    'avatar' => $rows->image,
                    'message' => $rows->message,
					'time' => $rows->time,
					'status' => $rows->status
                );
            
		}
		$hasil['status'] = 1;
		$this->response($hasil);
	}
	public function getUnreadMessageCountAction_get()
    {
		$to = $this->user->id_user;
		$hasil['unreadMessageCount'] = $this->chat_m->get_count_unread($to);
		if($hasil['unreadMessageCount']!=0){
			$hasil['status'] =  1;		
		} else {
			$hasil['status'] =  0;		
		}
		$this->response($hasil);
	}
	
}

/* afandi */
?>

