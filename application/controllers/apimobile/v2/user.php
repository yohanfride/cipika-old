<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . '/core/API_Controller.php';

class User extends API_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('models-api/home_m');
		$this->load->model('models-api/user_m');
		$this->load->model('models-api/auth_m');
		$this->load->model('models-api/produk_m');
		//$this->auth_m->check();
	}
	
    protected $methods = array(
        'get_active_user_get' => array('level' => 1),
        'get_user_get'        => array('level' => 1),
        'edit_user_post'      => array('level' => 1),
        'account_get'      => array('level' => 1),
        'edit_account_post'      => array('level' => 1),
        'delete_wishlish_get'      => array('level' => 1),
        'wishlist_get'      => array('level' => 1)
    );
	/**
     * Get Active User
     * Ambil data user aktif
     * 
     * @access public
     */
	public function get_active_user_get()
    {
        // ambil data user melalui API_Controller
        $response['user'] = ($this->user);
		if(($this->user->image !='' ) && ($this->user->image != null)) {
			$response['user-img-url'] = base_url().'asset/upload/profil/'.($this->user->image); 
		} else {
			$response['user-img-url'] = base_url().'asset/img/no-avatar-single.png';
		}
		$id = $this->user->id_user;
		$response['jml_komen']=$this->produk_m->count_comment($id);
		$response['jml_love']=$this->produk_m->count_love($id);
		$response['jml_list']=$this->produk_m->count_list($id);
		$response['jml_love_all']=$this->produk_m->count_love_all($id);
		$response['jml_follower']=$this->produk_m->count_follower($id);
		$response['jml_following']=$this->produk_m->count_following($id);
        $response['status'] = 1;
		
        $this->response($response);
    }
	
	//Edit User 
	public function edit_user_post()
    {
		$data = array();		
		$app_version 	= self::_check_request($this->post('app_version'), 0, 'app_version', 'skip', NULL, 0);		
        $username    	= self::_check_request($this->post('username'), 1, 'username', 'skip');
        $firstname    	= self::_check_request($this->post('firstname'), 1, 'firstname', 'skip');
        $lastname    	= self::_check_request($this->post('lastname'), 1, 'lastname', 'skip');
        $birthdate    	= self::_check_request($this->post('birthdate'), 1, 'birthdate', 'skip');
        $gender    		= self::_check_request($this->post('gender'), 1, 'gender', 'skip');
        $gcm_id    		= self::_check_request($this->post('gcm_id'), 1, 'gcm_id', 'skip');
        $address    	= self::_check_request($this->post('address'), 1, 'address', 'skip');
        $id_propinsi    = self::_check_request($this->post('id_propinsi'), 1, 'id_propinsi', 'integer');
        $id_kabupaten  	= self::_check_request($this->post('id_kabupaten'), 1, 'id_kabupaten', 'integer');
        $id_kecamatan  	= self::_check_request($this->post('id_kecamatan'), 1, 'id_kecamatan', 'integer');
        $phone    		= self::_check_request($this->post('phone'), 1, 'phone', 'skip');
        $hp    			= self::_check_request($this->post('hp'), 1, 'hp', 'skip');
        $input=array(
			'username'  => $username,
			'firstname' => $firstname,
			'lastname'  => $lastname,
			'birthdate'  => strftime("%Y-%m-%d",strtotime($birthdate)),
			// 'bio'       => remove_contact($this->input->post('bio')),
            'gender'    => $gender,
			'alamat'    => $address,
			'telpon'    => $phone,
			'hp'            => $hp,
			'id_propinsi'    => $id_propinsi,
			'id_kabupaten'    => $id_kabupaten,
			'gcm_id'		=> $gcm_id,
			'app_version' 	=>$app_version,
			'id_kecamatan'    => $id_kecamatan
    	);
    	$update=$this->user_m->update('tbl_user', 'id_user', $this->user->id_user, $input);
		//if($update) {		
    	if($_FILES["avatar"]["name"]){
    		$image=$this->user_m->get_image($this->user->id_user);
    		if($image){
    			unlink('asset/upload/profil/'.$image);
    		}
    		$nama_baru=date('YmdHis') . $_FILES["avatar"]["name"];
    		move_uploaded_file($_FILES["avatar"]["tmp_name"], "asset/upload/profil/" . $nama_baru);
    		$inp_pp=array(
    			'image' => $nama_baru
			);
    		$this->user_m->update('tbl_user', 'id_user', $this->user->id_user, $inp_pp);
    	}
		
		$this->response(array(
			'status' => 1,
			'message' =>'Succsess'
		));        
    	//self::get_active_user_get();
		/*} else {
			$this->response(array(
                "status" => 0,
                "error" => "User not updated"
            ));
		}*/		
    }
	//Ubah Akun
	public function edit_account_post()
	{
        $data = array();
        $email       = self::_check_request($this->post('email'), 1, 'email', 'valid_email');
        $old_password= self::_check_request($this->post('old_password'), 1, 'old_password', 'skip');
        $password    = self::_check_request($this->post('password'), 1, 'password', 'skip');
        $passconf    = self::_check_request($this->post('passconf'), 1, 'passconf', 'skip');
		if($password == $passconf){
			$cek_password = $this->user_m->cek_password($this->user->id_user, md5($old_password));
            if ($cek_password > 0){
				$ganti = $this->user_m->update('tbl_user', 'id_user', $this->user->id_user, array('email' => $this->input->post('email'), 'password' => md5($this->input->post('password'))));
                if ($ganti) {
					$this->response(array(
						'status' => 1,
						'message' =>'Succsess'
					));
                }
			} else {
				$this->response(array(
					'status' => 0,
					'error' =>'Wrong Old Password'
				),403);
			}
		} else {
			$this->response(array(
				'status' => 0,
				'error' =>'Confirmation Password is wrong'
			),403);
		}		
    }
	//Data Akun
	function account_get(){
		$data['status'] = 1;
		$data['email'] = $this->user->email;
		$this->response($data);
	}
	
	//Data Whist
	public function wishlist_get(){
        $data = array();
        $data['status'] = 1;
		$data['data'] = $this->produk_m->get_product_wishlist($this->user->id_user);        		
		$this->response($data);
    }
	//Delete Wishlist
	function delete_wishlish_get(){
		$id_produk    = self::_check_request($this->get('id_produk'), 1, 'id_produk', 'skip');
		$id_user    = self::_check_request($this->get('id_user'), 1, 'id_user', 'skip');
		$del_produk=$this->user_m->delete_love($id_produk, $id_user);
        if($del_produk){
			self::wishlist_get();
		} else {
			$this->response(array(
				'status' => 0,
				'error' =>'failed'
			),403);
		}
    }
	
	//*2 FUNCTION DIBAWAH INI BELUM BISA DIGUNAKAN, JADI INI CUMA pASANGAN AJA*//
	public function confirm_product($id = ''){
        $this->load->model('cart_m');
        $this->load->library('upload');
        
        
        $data=array();
        $data['success'] = '';
        
        if($this->input->post('simpan')){
               $config['upload_path'] = './asset/upload/confirm/';
               $config['allowed_types'] = 'jpg|png|jpeg|gif';
               $config['max_size'] = '20000';
               $config['remove_spaces'] = TRUE;
            for($i=0; $i<$this->input->post('no');$i++){
//                echo $_POST['file'.$i];
                
                $file_name = "";
                if(isset($_FILES['file'.$i])){
                $config['file_name'] = preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES['file'.$i]['name']) . date('dmYHis');

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('file'.$i)) {
                    $data['error'] = $this->upload->display_errors();
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $file_name = base_url('asset/upload/confirm')."/" . $data['upload_data']['file_name'];
                }
                }
                $input=array(
                        'id_confirm' => null,
                        'id_order' => $this->input->post('id_order'.$i),
                        'id_produk' => $this->input->post('id_produk'.$i),
                        'status' => $this->input->post('status'.$i),
                        'gambar' => $file_name,
                        'date_added' => date('Y-m-d H:i:s'),
                        'date_modified' => date('Y-m-d H:i:s')
                        );
                $insert=$this->cart_m->insert('tbl_confirm', $input);
                
                $data['success'] = 'success';
            }
        }
        $data['id'] = $id;
        $data['order'] = $this->cart_m->get_order($id);
        $data['order_item']=$this->cart_m->get_order_item($id);
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        
        $this->load->view('publik/confirmation_product_v', $data);
//        echo 'aku afandi';
    }
	public function confirm(){
		$data=array();
		$data['text']="Silakan cek email untuk konfimasi";
		$this->load->view('publik/user_confirm_v');
	}
	
	
	/*Private Function*/
	protected function _check_request($request, $mandatory, $name, $validate, $param = NULL, $return = NULL)
    {
        // Request ada
        if ($request !== FALSE) {
            $bool = TRUE;

            if ($validate != 'skip') {
                if (is_null($param))
                    $bool = $this->form_validation->$validate($request);
                else
                    $bool = $this->form_validation->$validate($request, $param);
            }

            if ($bool === FALSE) {
                $this->response(array(
                    'status'=> 0,
                    'error' => "Your parameter '" . $name . "' is not valid."
                ));
            }

            if(empty($request) && $mandatory) {
                $this->response(array(
                    'status'    => 0,
                    'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
                ), 403);
            }

            //kirim kembali request
            $return = $request;
        }
        // Request kosong, mandatory
        else if (is_null($request) && $mandatory) {
            // tampilkan error
            $this->response(array(
                'status'    => 0,
                'error'     => "Your parameter is not complete. Missing parameter '" . $name . "'."
            ), 403);
        }

        return $return;
    }
}
?>	