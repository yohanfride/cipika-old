<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Experttalk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_m');
	}

	public function index()
	{
		$data=array();
		if(isset($_GET['hal'])){$hal=$_GET['hal'];}
		else {$hal='';}

		$dataPerhalaman=5;
		($hal=='')?$nohalaman = 1:$nohalaman = $hal;
		$offset = ($nohalaman - 1) * $dataPerhalaman;
		$off = abs( (int) $offset);
		$data['offset']=$offset;
		$jmldata=$this->article_m->count_all_data_where('tbl_produk', 'publish', '1');
		$data['paginator']=$this->article_m->page($jmldata, $dataPerhalaman, $hal);

		$data['article']=$this->article_m->get_article_publish($dataPerhalaman, $off);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/expert_talk_v', $data);
	}
        
    function single($slug = ''){
        $data=array();
        
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['data']=$this->article_m->get_article_slug($slug);
        $this->load->view('publik/expert_talk_single_v', $data);
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */