<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_m');
	}

	public function index($id='')
	{
        $data['data']=$this->page_m->get_single('tbl_page', 'id_page', $id);
        if($data['data']==NULL||$id=='') redirect(base_url(), 'refresh');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/page_v', $data);
    }

    public function sitemap()
    {
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['profil'] = array(
            array('id_page' => 'myproduct', 'title' => 'My Product'),
            array('id_page' => 'order', 'title' => 'Order'),
            array('id_page' => 'myarticle', 'title' => 'Article'),
            array('id_page' => 'user/wishlist', 'title' => 'Wishlist'),
            array('id_page' => 'user/profile', 'title' => 'Profile'),
            array('id_page' => 'user/account', 'title' => 'Account'),
        );
        $this->load->view('publik/sitemap_v', $data);
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */