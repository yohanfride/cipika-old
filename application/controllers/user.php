<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require '../src/facebook.php';

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_m');
		$this->load->model('user_m');
		$this->load->model('auth_m');
		$this->load->model('produk_m');
		$this->auth_m->check();
	}

	public function index()
	{
            redirect(base_url());
//		$data=array();
//		$limit=12;
//		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
//			else{$offset=0;}
//		$data['produk']=$this->home_m->get_produk($limit, $offset);
//		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
//		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
//		$this->load->view('publik/home_v', $data);
	}

	public function profile()
	{
		$data=array();
		$data['success']='';
		$data['error']='';
		$data['sidebar_member_profile']="class='active'";        
        
		if(isset($_GET['continue'])){
			$data['continue']=$_GET['continue'];
		} else {
			$data['continue']='';
		}
		if($this->input->post('simpan')){
            // if(is_numeric($this->input->post('telpon'))){

            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Nama Tampilan', 'required');
            $this->form_validation->set_rules('firstname', 'Nama Depan', 'required');
            $this->form_validation->set_rules('lastname', 'Nama Belakang', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('telpon', 'Telpon', 'required|numeric');
            $this->form_validation->set_rules('hp', 'Hp', 'required|numeric');
            $this->form_validation->set_rules('id_propinsi', 'Propinsi', 'required');
            $this->form_validation->set_rules('id_kabupaten', 'Kabupaten', 'required');
            $this->form_validation->set_rules('id_kecamatan', 'Kecamatan', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
            }
            else
            {

                $input=array(
				'username'  => $this->input->post('username'),
				'firstname' => $this->input->post('firstname'),
				'lastname'  => $this->input->post('lastname'),
				'birthdate'  => strftime("%Y-%m-%d",strtotime($this->input->post('birthdate'))),
				// 'bio'       => remove_contact($this->input->post('bio')),
                'gender'    => $this->input->post('gender'),
				'alamat'    => $this->input->post('alamat'),
				'telpon'    => $this->input->post('telpon'),
				'hp'            => $this->input->post('hp'),
				'id_propinsi'    => $this->input->post('id_propinsi'),
				'id_kabupaten'    => $this->input->post('id_kabupaten'),
				'id_kecamatan'    => $this->input->post('id_kecamatan')
    			);
    			$update=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('member')->id_user, $input);
    			if($_FILES["user_image"]["name"]){
    				$image=$this->user_m->get_image($this->session->userdata('member')->id_user);
    				if($image){
    					unlink('asset/upload/profil/'.$image);
    				}
    				$nama_baru=date('YmdHis') . $_FILES["user_image"]["name"];
    				move_uploaded_file($_FILES["user_image"]["tmp_name"], "asset/upload/profil/" . $nama_baru);
    				$inp_pp=array(
    					'image' => $nama_baru
    				);
    				$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('member')->id_user, $inp_pp);
    			}
    			if(isset($_GET['continue'])){
    				redirect(base_url('cart/'.$_GET['continue']));
    			}
    			$data['success']='Data Profile Anda berhasil tersimpan.';
            }
            $data['data']= (object) $_POST;
            $dataDb = $this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
            $data['data']->image = $dataDb->image;
            if(empty($_POST['gender'])) $data['data']->gender = $dataDb->gender;
            if(empty($_POST['id_propinsi'])) $data['data']->id_propinsi = $dataDb->id_propinsi; 
            if(empty($_POST['id_kabupaten'])) $data['data']->id_kabupaten = null; 
            if(empty($_POST['id_kecamatan'])) $data['data']->id_kecamatan = null; 
		}else{
            $data['data']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
            // $data['nama_propinsi']=$this->produk_m->get_single('tbl_propinsi', 'id_propinsi', $data['data']->id_propinsi);
            // $data['nama_kabupaten']=$this->produk_m->get_single('tbl_kabupaten', 'id_kabupaten', $data['data']->id_kabupaten);
            // $data['nama_kecamatan']=$this->produk_m->get_single('tbl_kecamatan', 'id_kecamatan', $data['data']->id_kecamatan);
        }
		$data['provinsi']=$this->produk_m->get_all_data_order('tbl_propinsi', 'nama_propinsi', 'asc');        
//		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');		
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/profile_v', $data);
	}
        
	public function account()
	{
        $data = array();
        $data['sidebar_member_account'] = "class='active'";
        $data['success'] = '';
        $data['error'] = '';
        if ($this->input->post('simpan'))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('old_password', 'Password Lama', 'required');
            $this->form_validation->set_rules('password', 'Password Baru', 'required|matches[passconf]|min_length[6]');
            $this->form_validation->set_rules('passconf', 'Konfirmasi Password Baru', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
                $data['array_error'] = $this->form_validation->error_array();
            }
            else
            {
                $cek_password = $this->user_m->cek_password($this->session->userdata('member')->id_user, md5($this->input->post('old_password')));
                if ($cek_password > 0)
                {
                    $ganti = $this->user_m->update('tbl_user', 'id_user', $this->session->userdata('member')->id_user, array('email' => $this->input->post('email'), 'password' => md5($this->input->post('password'))));
                    if ($ganti)
                    {
                        $data['success'] = 'Data berhasil disimpan.';
                    } else {
                        $data['success'] = 'Data berhasil disimpan.';
                    }
                }
                else
                {
                    $data['error'] = 'Password Anda salah';
                }
            }
        }
        $data['data'] = $this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/account_v', $data);
    }
	public function password()
	{
		$data=array();
		$data['success']='';
		$data['error']='';
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('old_password', 'Old Password', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
		    	$cek_password=$this->user_m->cek_password($this->session->userdata('member')->id_user, md5($this->input->post('old_password')));
		    	if($cek_password>0){
		    		$ganti=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('member')->id_user, array('password'=>md5($this->input->post('password'))));
		    		if($ganti){$data['success']='Password changed successfully';}
		    	}else{
		    		$data['error'] = 'Input password salah!';
		    	}
		    }
		}
		$data['data']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/user_password_v', $data);
	}

	public function confirm(){
		$data=array();
		$data['text']="Silakan cek email untuk konfimasi";
		$this->load->view('publik/user_confirm_v');
	}
    
    public function wishlist()
    {
        $data = array();
        $data['sidebar_member_wishlist'] = "class='active'";
        $data['success'] = '';
        $data['error'] = '';
        $data['data'] = $this->produk_m->get_product_wishlist($this->session->userdata('member')->id_user);
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/wishlist_v', $data);
    }
    
    public function confirm_product($id = ''){
        $this->load->model('cart_m');
        $this->load->library('upload');
        
        
        $data=array();
        $data['success'] = '';
        
        if($this->input->post('simpan')){
               $config['upload_path'] = './asset/upload/confirm/';
               $config['allowed_types'] = 'jpg|png|jpeg|gif';
               $config['max_size'] = '20000';
               $config['remove_spaces'] = TRUE;
            for($i=0; $i<$this->input->post('no');$i++){
//                echo $_POST['file'.$i];
                
                $file_name = "";
                if(isset($_FILES['file'.$i])){
                $config['file_name'] = preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES['file'.$i]['name']) . date('dmYHis');

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('file'.$i)) {
                    $data['error'] = $this->upload->display_errors();
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $file_name = base_url('asset/upload/confirm')."/" . $data['upload_data']['file_name'];
                }
                }
                $input=array(
                        'id_confirm' => null,
                        'id_order' => $this->input->post('id_order'.$i),
                        'id_produk' => $this->input->post('id_produk'.$i),
                        'status' => $this->input->post('status'.$i),
                        'gambar' => $file_name,
                        'date_added' => date('Y-m-d H:i:s'),
                        'date_modified' => date('Y-m-d H:i:s')
                        );
                $insert=$this->cart_m->insert('tbl_confirm', $input);
                
                $data['success'] = 'success';
            }
        }
        $data['id'] = $id;
        $data['order'] = $this->cart_m->get_order($id);
        $data['order_item']=$this->cart_m->get_order_item($id);
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        
        $this->load->view('publik/confirmation_product_v', $data);
//        echo 'aku afandi';
    }
    
    function delete_wishlish($id_produk='', $id_user=''){
        $data=array();
        $data['success']='';
        $data['error']='';
        $del_produk=$this->user_m->delete_love($id_produk, $id_user);
        redirect(base_url('user/wishlist'));
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
