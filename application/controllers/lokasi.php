<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lokasi extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('lib_lokasi');
	}
    
    /* AJAX : get kabupaten by propinsi */
    function dropdown_kabupaten()
    {
        $id_propinsi                    = $this->input->post('id_propinsi');        
        $data['kabupaten']              = $this->lib_lokasi->get_kabupaten($id_propinsi);
        $data['nama_input_kabupaten']   = $this->input->post('nama_input_kabupaten');
        
        echo $this->load->view('publik/lokasi/dropdown_kabupaten_v',$data,TRUE);
    }
    
    /* AJAX : get kecamatan by kabupaten */
    function dropdown_kecamatan()
    {
        $id_kabupaten                   = $this->input->post('id_kabupaten');        
        $data['kecamatan']              = $this->lib_lokasi->get_kecamatan($id_kabupaten);
        $data['nama_input_kecamatan']   = $this->input->post('nama_input_kecamatan');
        
        echo $this->load->view('publik/lokasi/dropdown_kecamatan_v',$data,TRUE);
    }
    
}