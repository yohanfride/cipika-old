<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Mandiriclickpay extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_m');
        $this->load->model('order_m');
        $this->load->model('user_m');
    }

    public function post_transfer_get()
    {
        $card_no = $this->get('card_no');
        $amount = $this->get('amount');
        $trans_id = $this->get('trans_id');
        $token = $this->get('token');
        $price = $this->get('price');
        $shipping = $this->get('shipping');
        $xml = "<payment_request>";
        $xml.="<user_id>user</user_id>";
        $xml.="<password>pwd</password> ";
        $xml.="<card_no>" . $card_no . "</card_no> ";
        $xml.="<amount>" . $amount . "</amount> ";
        $xml.="<transaction_id>" . $trans_id . "</transaction_id> ";
        $xml.="<data_field1>" . substr($card_no, -10) . "</data_field1> ";
        $xml.="<data_field2>" . $amount . "</data_field2> ";
        $xml.="<data_field3>0</data_field3> ";
        $xml.="<token_response>" . $token . "</token_response> ";
        $xml.="<date_time>" . date("yymmddhis") . "</date_time> ";
        $xml.="<bank_id>1</bank_id> ";
        $xml.="<items count=\"2\"> ";
        $xml.="<item no=\"1\" name=\"item1\" price=\"" . $price . "\" qty=\"1\" /> ";
        $xml.="<item no=\"2\" name=\"shippingfee\" price=\"" . $shipping . "\" qty=\"1\" /> ";
        $xml.="</items> ";
        $xml.="</payment_request>";

        $url = 'http://127.0.0.1:22334/velispayment/';

        $body = $xml;

        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($c, CURLOPT_POSTFIELDS, $body);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $page = curl_exec($c);
        curl_close($c);
        $deXml = simplexml_load_string($page);
        $deJson = json_encode($deXml);
        $xml_array = json_decode($deJson, TRUE);

        $this->response($xml_array);
    }
}

/* End of file page.php */
/* Location: ./application/controllers/api/banktranfer.php */