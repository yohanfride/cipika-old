<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Shipping extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function cek_ongkir_get()
    {
        $data = array(
            'transaction_id' => $this->get('transaction_id'),
            's_propinsi' => $this->get('s_propinsi'),
            's_kota_kab' => $this->get('s_kota_kab'),
            'd_kecamatan' => $this->get('d_kecamatan'),
            'd_kota_kab' => $this->get('d_kota_kab'),
            'd_propinsi' => $this->get('d_propinsi'),
            'weight' => $this->get('weight'),
            'volume' => $this->get('volume'),
        );
        $data_string = json_encode($data);

        $url = "http://124.81.102.190:543/shipment/page/api_jne.php?key=" . urlencode($data_string);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec($ch);
        curl_close($ch);

        $login = json_decode($login);

        if ($login)
        {
            $this->response($login);
        }
        else
        {
            $this->response(array("status" => 0, "respond" => "failed"));
        }
    }

    public function get_destination_get()
    {
        $username = $this->config->item('username_jne');
        $api_key = $this->config->item('api_key_jne');
        $tujuan = $this->get('destination');

        $url = "http://api.jne.co.id:8889/tracing/cipikastore/dest/key/" . $tujuan;

        $fields = array(
            'username' => $username,
            'api_key' => $api_key
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $out = json_decode($result);

        if (empty($out->error))
        {
            $this->response($out);
        }
        else
        {
            $this->response(array('error' => $out->error));
        }
    }
    
    public function get_origin_get()
    {
        $username = $this->config->item('username_jne');
        $api_key = $this->config->item('api_key_jne');
        $dari = $this->get('origin');

        $url = "http://api.jne.co.id:8889/tracing/cipikastore/origin/key/" . $dari;

        $fields = array(
            'username' => $username,
            'api_key' => $api_key
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $out = json_decode($result);

        if (empty($out->error))
        {
            $this->response($out);
        }
        else
        {
            $this->response(array('error' => $out->error));
        }
    }
    
    public function get_tarif_get()
    {
        $username = $this->config->item('username_jne');
        $api_key = $this->config->item('api_key_jne');
        $from = $this->get('origin');
        $to = $this->get('destination');
        $weight = $this->get('weight');

        $url = "http://api.jne.co.id:8889/tracing/cipikastore/price/";

        $fields = array(
            'username' => $username,
            'api_key' => $api_key,
            'from' => $from,
            'thru' => $to,
            'weight' => $weight,
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $out = json_decode($result);

        if (empty($out->error))
        {
            $this->response($out);
        }
        else
        {
            $this->response(array('error' => $out->error));
        }
    }
    
    public function add_resi_get()
    {
        $username = $this->config->item('username_jne');
        $api_key = $this->config->item('api_key_jne');
        $trans_id= $this->get('trans_id');
        $resi = $this->get('resi');

        $url = "http://api.jne.co.id:8889/tracing/cipikastore/insertCnote";

        $fields = array(
            'username' => $username,
            'api_key' => $api_key,
            'ORDER_ID' => $trans_id,
            'AWB_NUMBER' => $resi
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $out = json_decode($result);

        if (empty($out->error))
        {
            $this->response($out);
        }
        else
        {
            $this->response(array('error' => $out->error));
        }
    }
    
    public function track_resi_get()
    {
        $username = $this->config->item('username_jne');
        $api_key = $this->config->item('api_key_jne');
        $resi = $this->get('resi');

        $url = "http://api.jne.co.id:8889/tracing/cipikastore/list/cnote/".$resi;

        $fields = array(
            'username' => $username,
            'api_key' => $api_key,
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $out = json_decode($result);

        if (empty($out->error))
        {
            $this->response($out);
        }
        else
        {
            $this->response(array('error' => $out->error));
        }
    }
}
/* End of file page.php */
/* Location: ./application/controllers/api/banktranfer.php */