<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Banktranfer extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_m');
        $this->load->model('order_m');
        $this->load->model('user_m');
    }

    public function get_virtual_account_get()
    {
        $merchant_id = $this->get("merchant_id");
        $key = $this->get("key");

        $url = "https://tb.indosatm2.com/paygate/gettrans_check.php?id=" . $merchant_id . "&key=" . $key . "";

        // inisialisasi CURL
        $data = curl_init();
        // setting CURL
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_SSLVERSION, 3);
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($data, CURLOPT_SSL_VERIFYHOST, 2);
        // menjalankan CURL untuk membaca isi file
        $hasil = curl_exec($data);
        curl_close($data);

        $res = json_decode($hasil);
        $this->response(
                array("result" => $res->result, 'value' => $res->value)
        );
    }

    public function get_payment_tranfer_get()
    {
        $key = SECRET_ID_PAYMENT_TRANFER;

        $merchant_id = USER_ID_PAYMENT_TRANFER;
        $transaction_id = $this->get("transaction_id");
        $amount = $this->get("amount");
        $cust_email = $this->get("cust_email");
        $adm_email = $this->get("adm_email");
        $minute = 0;
        $hour = 4;

        $data = array(
            'merchant_id' => $merchant_id,
            'transaction_id' => $transaction_id,
            'amount' => $amount,
            'cust_email' => $cust_email,
            'adm_email' => $adm_email,
            'ip' => IP_SERVER,
            'minute' => $minute,
            'hour' => $hour,
        );
        $input = implode(";", $data);
        if (DEV_PAYMENT!=TRUE)
            $input = $input . ";";

        $hasil = $this->encrypt_rijndael_256($key, $input);
        
        if(DEV_PAYMENT==TRUE)
        {
            $url = "http://devtb.duniadigital.co.id/gettrans_check.php?id=" . $merchant_id . "&key=" . $hasil . "";
        }
        else
        {
            $url = "http://tb.indosatm2.com/paygate/gettrans_check.php?id=" . $merchant_id . "&key=" . $hasil . "";
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $head = curl_exec($ch);
        curl_close($ch);
        
        $inputan = array(
            'kode_order' => $transaction_id,
            'payment' => 'BANKTRANFER',
            'amount' => $amount,
        );
        $insert = $this->payment_m->insert('tbl_payment_buyers', $inputan);

        if ($head)
        {
            $res = json_decode($head);

            $input = array(
                'status' => $res->result,
                'respond' => $res->value
            );
            $inserts = $this->payment_m->update('tbl_payment_buyers', 'id_payments', $insert, $input);

            $this->response(
                    array("result" => $res->result, 'value' => $res->value, 'hasil' => $hasil)
            );
        }
    }

    public function get_notice_get()
    {

        $data = array(
            'merchant_id' => $merchant_id,
            'transaction_id' => $transaction_id,
            'amount' => $amount,
            'cust_email' => $cust_email,
            'adm_email' => $adm_email,
            'minute' => $minute,
            'hour' => $hour,
        );
        $input = implode(";", $data);

        $hasil = $this->encrypt_rijndael_256($key, $input);
    }

    public function callback_bank_transfer_get()
    {
        $params = $_GET['params'];
        
        $get_params = $this->decrypt_rijndael_256(SECRET_ID_PAYMENT_TRANFER, $params);
        
        $out = unserialize($get_params);

        if ($out['result'] == 1)
        {
            $test = explode("_", $out['order_id']);
            $transid = $test[0];

            $single = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $transid);

            if ($single)
            {
                $this->payment_m->update('tbl_payment_buyers', 'kode_order', $out['order_id'], array('paid' => 1));
                $this->order_m->update('tbl_invoices', 'kode_invoice', $transid, array('status_payment' => "Done"));
                $this->order_m->update('tbl_order', 'kode_invoice', $single->kode_invoice, array('status_payment' => "Done"));

                $inputan_shipping = array();
                $inputan_merchant = array();
                $order_items = array();

                $this->load->database();
                $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $transid);
                foreach ($orderdetail as $v)
                {
                    $nama_store = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                    $email = $this->cart_m->get_merchant($v->id_merchant)->email;
                    $order_item = $this->cart_m->get_order_item(md5($v->id_order));
                    $shipping = $this->cart_m->get_order_shipping($v->id_order);

                    $inputan_shipping[$v->id_order] = $this->cart_m->get_order_shipping($v->id_order);
                    $inputan_merchant[$v->id_merchant] = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                    $order_items[$v->id_order] = $this->cart_m->get_order_item(md5($v->id_order));

                    $send = new \Cipika\View\Helper\EmailPoMerchant($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
                    $sending = $send->sends($email, $v, $shipping, $nama_store, $order_item, $this->config->slash_item('index_page'));
                }

                $nama_payment = $this->payment_m->get_single('tbl_payment', 'id_payment', $single->id_payment);
                $user = $this->payment_m->get_single('tbl_user', 'id_user', $single->id_user);

                $send_to_buyer = new \Cipika\View\Helper\EmailBuyerPembayaran($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
                $sending_to_buyer = $send_to_buyer->sends($user->email, $single, $orderdetail, $inputan_shipping, $inputan_merchant, $order_items, $this->config->slash_item('index_page'), $nama_payment, $user);
            
            }
            $this->response(
                    array("result" => 1, 'error' => "")
            );
        }
        else
        {
            $this->response(
                    array("result" => 0, 'error' => "failed")
            );
        }
    }

    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function encrypt_rijndael_256($secretkey, $filecipher)
    {
        $td = mcrypt_module_open('rijndael-256', '', 'ecb', '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $secretkey, $iv);
        $encrypted = mcrypt_generic($td, $filecipher);
        $encrypted1 = base64_encode($iv) . ";" . base64_encode($encrypted);
        $result = base64_encode($encrypted1);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $result;
    }

    function decrypt_rijndael_256($secretkey, $string)
    {
        list($iv, $filecipher) = @split(";", base64_decode($string));
        $td = mcrypt_module_open('rijndael-256', '', 'ecb', '');
        mcrypt_generic_init($td, $secretkey, base64_decode($iv));
        $result = mdecrypt_generic($td, base64_decode($filecipher));
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $result;
    }
    
    public function confirm_get()
    {
        $van = $this->get("van_id");

        $single = $this->payment_m->get_single('tbl_payment_buyers', 'respond', $van);

        if ($single)
        {
            $test = explode("_", $single->kode_order);
            $transid = $test[0];

            $invoice = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $transid);
            $data = array(
                'result' => 1,
                'virtual_account' => str_replace(" ", "", $single->respond),
                'buyer_transfer' => $invoice->total + $invoice->ongkir + $invoice->payment_fee,
                'bill' => $invoice->total + $invoice->ongkir + $invoice->payment_fee,
                'order_id' => $single->kode_order,
            );

            $input = serialize($data);

            $hasil = $this->encrypt_rijndael_256(SECRET_ID_PAYMENT_TRANFER, $input);

            $url = base_url() . "api/banktranfer/callback_bank_transfer?params=" . $hasil;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $call = curl_exec($ch);

            echo $call;
        }
        else
        {
            $this->response(
                    array("result" => 0, 'error' => "failed")
            );
        }
    }
    
}
/* End of file page.php */
/* Location: ./application/controllers/api/banktranfer.php */