<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Dompetku extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_m');
        $this->load->model('order_m');
    }

    public function get_signature_get()
    {
        $initiator = $this->get('initiator');
        $initiator_pin = $this->get('initiator_pin');

        $signA = time() . $initiator_pin;
        $signB = strrev($initiator_pin) . $initiator;
        $signC = $signA . "|" . $signB;
        $signature = $this->encrypt_tripledes($signC, SECRET_KEY_DOMPETKU);
        $this->response(
                array(
                    "signature" => $signature
                )
        );
    }

    public function get_password_get()
    {
        $key = SECRET_KEY_DOMPETKU;
        $initiator = INITIATOR_DOMPETKU;
        $initiator_pin = INITIATOR_PIN_DOMPETKU;

        $user_id = USER_ID_DOMPETKU;
        $amount = $this->get('amount');
        $couponid = $this->get('coupon_id');
        $msisdn = $this->get('msisdn');
        $transid = $this->get('trans_id');
        $signature = $this->get('signature');

        $signA = time() . $initiator_pin;
        $signB = strrev($initiator_pin) . $initiator;
        $signC = $signA . "|" . $signB;
        $signature = $this->encrypt_tripledes($signC, SECRET_KEY_DOMPETKU);

        if (DEV_PAYMENT)
            $url = 'http://dev.payment.indosatm2.com/merchant/do_transfer_token?';
        else
            $url = 'http://payment.indosatm2.com/merchant/do_transfer_token?';

        $url.="userid=" . $user_id;
        $url.="&signature=" . $signature;
        $url.="&amount=" . $amount;
        $url.="&couponid=" . $couponid;
        $url.="&msisdn=" . $msisdn;
        $url.="&transid=" . $transid;

        $inputan = array(
            'kode_order' => $transid,
            'payment' => 'DOMPETKU',
            'amount' => $amount,
        );
        $insert = $this->payment_m->insert('tbl_payment_buyers', $inputan);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec($ch);
        curl_close($ch);

        if ($login)
        {
            $xml = simplexml_load_string($login);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);

            if (!empty($array))
            {
                $input = array(
                    'status' => $array['status'],
                    'respond' => $array['msg']
                );
                $inserts = $this->payment_m->update('tbl_payment_buyers', 'id_payments', $insert, $input);

                $out = array(
                    'status' => $array['status'],
                    'message' => $array['msg']
                );
            }
            else
            {
                $input = array(
                    'status' => 0,
                    'respond' => "Failed"
                );
                $inserts = $this->payment_m->update('tbl_payment_buyers', 'id_payments', $insert, $input);

                $out = array(
                    'status' => 0,
                    'message' => 'Failed'
                );
            }

            $this->response($out);
        }
        else
        {
            $this->response(
                    array("respond" => "No Connection")
            );
        }
    }

    public function post_transfer_get()
    {
        $key = SECRET_KEY_DOMPETKU;
        $initiator = INITIATOR_DOMPETKU;
        $initiator_pin = INITIATOR_PIN_DOMPETKU;

        $user_id = USER_ID_DOMPETKU;
        $amount = $this->get('amount');
        $couponid = $this->get('coupon_id');
        $msisdn = $this->get('msisdn');
        $transid = $this->get('trans_id');
        $signature = $this->get('signature');

        $signA = time() . $initiator_pin;
        $signB = strrev($initiator_pin) . $initiator;
        $signC = $signA . "|" . $signB;
        $signature = $this->encrypt_tripledes($signC, SECRET_KEY_DOMPETKU);

        if (DEV_PAYMENT)
            $url = 'http://dev.payment.indosatm2.com/merchant/do_transfer_token?';
        else
            $url = 'http://202.155.150.34/merchant/do_transfer_token?';

        $url.="userid=" . $user_id;
        $url.="&signature=" . $signature;
        $url.="&amount=" . $amount;
        $url.="&couponid=" . $couponid;
        $url.="&msisdn=" . $msisdn;
        $url.="&transid=" . $transid;

        $inputan = array(
            'kode_order' => $transid,
            'payment' => 'DOMPETKU',
            'amount' => $amount,
            'msisdn' => $msisdn,
        );
        $insert = $this->payment_m->insert('tbl_payment_buyers', $inputan);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec($ch);
        curl_close($ch);

        if ($login)
        {
            $xml = simplexml_load_string($login);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);

            if (!empty($array))
            {
                if ($array['status'] == 0)
                {
                    $this->payment_m->update('tbl_payment_buyers', 'kode_order', $transid, array('paid' => 1));

                    $test = explode("_", $transid);
                    $transid = $test[0];

                    $this->order_m->update('tbl_invoices', 'kode_invoice', $transid, array('status_payment' => "Done"));

                    $single = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $transid);
                    $this->order_m->update('tbl_order', 'kode_invoice', $single->kode_invoice, array('status_payment' => "Done"));

//                    $inputan_order = array();
//                    $inputan_merchant = array();
//                    $order_item = array();
//                    $to_email = array();
//                    $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $transid);
//                    $inputan_order = $orderdetail;
//                    foreach ($inputan_order as $value)
//                    {
//                        $inputan_shipping[$value->id_order] = $this->cart_m->get_order_shipping($value->id_order);
//                        $inputan_merchant[$value->id_merchant] = $this->cart_m->get_merchant($value->id_merchant)->nama_store;
//                        $order_item[$value->id_order] = $this->cart_m->get_order_item(md5($value->id_order));
//
//                        $to_email[$value->id_merchant] = $this->cart_m->get_merchant($value->id_merchant)->email;
//                    }
//                    $this->load->database();
//                    foreach ($to_email as $b)
//                    {
//                        $send = new \Cipika\View\Helper\EmailPoMerchant($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
//                        $sending = $send->send($b, $inputan_order, $inputan_shipping, $inputan_merchant, $order_item, $this->config->slash_item('index_page'));
//                    }
//
//                    $single = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $transid);
//                    $payment = $this->payment_m->get_single('tbl_payment', 'id_payment', $single->id_payment);
//                    $user = $this->payment_m->get_single('tbl_user', 'id_user', $single->id_user);
//
//                    $to_email_buyer = $user->email;
//
//                    $send_to_buyer = new \Cipika\View\Helper\EmailBuyerPembayaran($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
//                    $sending_to_buyer = $send_to_buyer->send($to_email_buyer, $single, $inputan_order, $inputan_shipping, $inputan_merchant, $order_item, $this->config->slash_item('index_page'), $payment, $user);

                    $input = array(
                        'status' => 1,
                        'paid' => 1,
                        'respond' => $array['msg']
                    );
                }
                else
                {
                    $input = array(
                        'status' => $array['status'],
                        'respond' => $array['msg']
                    );
                }
                $inserts = $this->payment_m->update('tbl_payment_buyers', 'id_payments', $insert, $input);

                $out = array(
                    'status' => $array['status'],
                    'message' => $array['msg']
                );
            }
            else
            {
                $input = array(
                    'status' => 0,
                    'respond' => "Failed"
                );
                $inserts = $this->payment_m->update('tbl_payment_buyers', 'id_payments', $insert, $input);

                $out = array(
                    'status' => 0,
                    'message' => 'Failed'
                );
            }

            $this->response($out);
        }
        else
        {
            $this->response(
                    array("respond" => "No Connection")
            );
        }
    }

    public function get_status_get()
    {
        $user_id = USER_ID_DOMPETKU;
        $transid = $this->get('trans_id');
        $signature = $this->get('signature');

        if (DEV_PAYMENT)
            $url = 'http://dev.payment.indosatm2.com/merchant/get_trx_status?';
        else
            $url = 'http://payment.indosatm2.com/merchant/get_trx_status?';

        $url.="userid=" . $user_id;
        $url.="&signature=" . $signature;
        $url.="&transid=" . $transid;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec($ch);

        if ($login)
        {
            $this->response(
                    array("respond" => $login)
            );
        }
        else
        {
            $this->response(
                    array("respond" => "No Connection")
            );
        }
    }

    public function post_reversal_post()
    {
        $user_id = USER_ID_DOMPETKU;
        $transid = $this->post('trans_id');
        $signature = $this->post('signature');
        $amount = $this->post('amount');

        if (DEV_PAYMENT)
            $url = 'http://dev.payment.indosatm2.com/merchant/do_reversal?';
        else
            $url = 'http://payment.indosatm2.com/merchant/do_reversal?';

        $url.="userid=" . $user_id;
        $url.="&signature=" . $signature;
        $url.="&transid=" . $transid;
        $url.="&amount=" . $transid;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec($ch);

        if ($login)
        {
            $this->response(
                    array("respond" => $login)
            );
        }
        else
        {
            $this->response(
                    array("respond" => "No Connection")
            );
        }
    }

    private function encrypt_tripledes($value, $key)
    {
        if (!$value)
        {
            return false;
        }
        $td = mcrypt_module_open('tripledes', '', 'ecb', '');
        $iv_size = mcrypt_enc_get_iv_size($td);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $pad = $iv_size - (strlen($value) % $iv_size);
        $value = $value . str_repeat(chr($pad), $pad);
        $crypttext = mcrypt_encrypt(MCRYPT_TRIPLEDES, $key, $value, MCRYPT_MODE_ECB, $iv);
        return trim(base64_encode($crypttext));
    }

    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

}

/* End of file page.php */
/* Location: ./application/controllers/api/page.php */
