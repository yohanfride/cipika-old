<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Order extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_m');
        $this->load->model('order_m');
    }

    public function confirm_order_get()
    {
        $transaction_id = $this->get('kode_invoice');
        $id_payment = $this->get('id_payment');

        $payment = $this->payment_m->get_payment_single($transaction_id, $id_payment);

        $test = explode("_", $payment->kode_order);
        $transid = $test[0];

        $update_payment_buyer = $this->payment_m->update('tbl_payment_buyers', 'kode_order', $transid, array('paid' => 1));

        $update_invoice = $this->order_m->update('tbl_invoices', 'kode_invoice', $transid, array('status_payment' => "Done"));

        $single = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $transid);
        $update_order = $this->order_m->update('tbl_order', 'kode_invoice', $single->kode_invoice, array('status_payment' => "Done"));

        $data['payment'] = $this->payment_m->get_payment($single->kode_invoice, $single->id_payment);

        $sub_total = 0;
        $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $transaction_id);
        $send_email = array();
        foreach ($orderdetail as $value)
        {
            $cek_notif = $this->cart_m->get_single('notif_merchant', 'kode_order', $value->kode_order);
            if (!$cek_notif)
            {
                $shipping_info = $this->cart_m->get_order_shipping($value->id_order);
                $email_merchant = '<style>* {font-family:Arial, serif;font-size:12px} table tr td {padding:2px 5px;} tr.judul td {font-weight:bold;}</style>';
                $email_merchant.= "<h3>Bapak/Ibu " . ucwords($this->cart_m->get_merchant($value->id_merchant)->nama_store) . " Yth.</h3>
                <br />
                <p>
                    Anda menerima order pembelian produk melalui Cipika Store. Mohon segera mengirimkan produk ke alamat pengiriman melalui jasa & paket ekspedisi seperti tercantum dalam detil informasi pembelian berikut:
                </p>
                ";
                $email_merchant .= "
                <table style='margin: 2em 0;'>
                    <tr>
                        <td>Nama : </td>
                        <td>" . $shipping_info->nama . "</td>
                    </tr>
                    <tr>
                        <td>Email : </td>
                        <td>" . $shipping_info->email . "</td>
                    </tr>
                    <tr>
                        <td>Phone : </td>
                        <td>" . $shipping_info->telpon . "</td>
                    </tr>
                    <tr>
                        <td>Alamat : </td>
                        <td>" . $shipping_info->alamat . "</td>
                    </tr>
                    <tr>
                        <td>Kota/Kab : </td>
                        <td>" . strtoupper($shipping_info->nama_kabupaten) . "</td>
                    </tr>
                    <tr>
                        <td>Provinsi : </td>
                        <td>" . strtoupper($shipping_info->nama_propinsi) . "</td>
                    </tr>
                    
                    <tr>
                        <td>Paket Pengiriman: </td>
                        <td>" . ucwords($value->paket_ongkir) . "</td>
                    </tr>
                    </table>";
                $email_merchant .= '<table style="border-collapse:collapse; width:100%;">
                <tr style="background:#666666; color:white; text-align:center" class="judul"><td width="10%">No</td><td width="5%">ID Barang</td><td>Nama Produk</td><td width="10%">Jumlah</td><td width="15%">Harga</td><td width="10%">Diskon</td><td width="15%">Harga setelah diskon</td><td width="20%">Subtotal</td></tr>';
                $itemorder = $this->cart_m->get_order_item(md5($value->id_order));
                $i = 0;
                foreach ($itemorder as $items)
                {
                    if ($items->diskon > 0)
                        $harga_diskon = ($items->harga) * ($items->diskon / 100);
                    else
                        $harga_diskon = $items->harga;
                    $total_sub = $harga_diskon * $items->jml_produk;
                    $i++;
                    $email_merchant .= '<tr><td>' . $i . '</td> 
                        <td><a href="' . base_url() . $this->config->slash_item('index_page') . 'product/detail/' . $items->id_produk . '" >' . $items->id_produk . '</a></td>
                        <td>' . $items->nama_produk . '</strong></td>
                        <td>' . $items->jml_produk . '</td>
                        <td>' . $items->harga . '</td>                        
                        <td>' . $items->diskon . '</td>
                        <td>' . $harga_diskon . '</td>
                        <td style="text-align:right">Rp. ' . $this->cart->format_number($total_sub) . '</td>
                    </tr>';

                    $sub_total += $total_sub;
                }
                $email_merchant .= "</table><br><br>
                <p>
                    Total nilai pembelian dalam informasi di atas belum termasuk biaya pengiriman. Silahkan kirim bukti pengiriman melalui email ke e-care.store@cipika.co.id. Cipika Store akan mengganti biaya pengiriman bersamaan dengan proses settlement.
                </p>
                </table>";
                $email_merchant .= '<br/><p>Terima kasih atas perhatian dan kerjasama yang baik,<br/><br/><br/>
                    <strong>Cipika Store &trade;</strong><br>
                    <a href="' . base_url() . '">www.cipikastore.com</a>
                    </p>
                    </div>
                    ';
                $list = array($this->config->item('email_tokoon'), $this->config->item('email_sales'));

                $config = $this->_mail_win();
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->config->item('email_from'), 'Cipika Store');
                $this->email->to($this->cart_m->get_merchant($value->id_merchant)->email);
                $this->email->bcc($list);
                $this->email->subject('Informasi Pembelian di Cipika Store');
                $this->email->message($email_merchant);
                $send = $this->email->send();
                if ($send)
                {
                    $insert_notif = $this->cart_m->insert('notif_merchant', array("kode_order" => $value->kode_order, "send_time" => date("d-m-Y H:i:s")));
                }
            }
        }
        $this->response(
                array(
                    "status" => 1,
                    "payment_buyer" => $update_payment_buyer,
                    "invoice" => $update_invoice,
                    "order" => $update_order,
                )
        );
    }

    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

}

/* End of file page.php */
/* Location: ./application/controllers/api/banktranfer.php */