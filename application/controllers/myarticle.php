<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Myarticle extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('article_m');
    }

    public function index($offset = 0) {

        $this->load->model('auth_m');
        $this->auth_m->check();
        $data = array();
        $data['sidebar_member_myarticle'] = "class='active'";
        $id_user = $this->session->userdata('member')->id_user;
        if(isset($_GET['hal'])){$hal=$_GET['hal'];}
        else {$hal='';}

        $dataPerhalaman=5;
        ($hal=='')?$nohalaman = 1:$nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs( (int) $offset);
        $data['offset']=$offset;
        $jmldata=$this->article_m->count_all_data_where('tbl_article', 'id_user', $id_user);
        $data['paginator']=$this->article_m->page($jmldata, $dataPerhalaman, $hal);

        $data['datas']=$this->article_m->get_article_user($id_user, $dataPerhalaman, $off);
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/myarticle_v', $data);
    }

    public function add() {
        $data = array();
        $data['success'] = '';
        $data['error'] = '';
        $data['sidebar_member_myarticle'] = "class='active'";
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['sidebar_member_myarticle'] = "class='active'";

        if ($this->input->post('simpan')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('content', 'Content', 'required');
//            $this->form_validation->set_rules('captcha', 'Captcha', 'required');
            $resp = $this->lib_recaptcha->auth_recaptcha($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

            $data['title'] = $this->input->post('title');
            $data['content'] = $this->input->post('content');

            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {

                if (!$resp->is_valid) {
                    $data['error'] = "CAPTCHA Yang anda masukan salah.";
                } else {

                    $input = array(
                        'id_article' => null,
                        'article_slug' => $this->commonlib->generate_permalink($this->input->post('title'), 'tbl_article', 'id_article', 'article_slug'),
                        'title' => $this->input->post('title'),
                        'id_user' => $this->session->userdata('member')->id_user,
                        'publish' => 0,
                        'content' => $this->input->post('content'),
                        'date_added' => date('Y-m-d H:i:s'),
                        'date_modified' => date('Y-m-d H:i:s'),
                    );
                    $insert = $this->article_m->insert('tbl_article', $input);
                    if ($insert)
                        $data['success'] = 'success';
                    else
                        $data['error'] = 'failed';
                }
            }
        }
        $this->load->view('publik/myarticle_add_v', $data);
    }

    public function edit($id_article = '', $id_user = '') {
        $data = array();
        $data['success'] = '';
        $data['error'] = '';
        $data['sidebar_member_myarticle'] = "class='active'";

        if ($this->input->post('simpan')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('content', 'Content', 'required');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {
                $input = array(
                    'article_slug' => $this->commonlib->generate_permalink($this->input->post('title'), 'tbl_article', 'id_article', 'article_slug'),
                    'title' => $this->input->post('title'),
                    'id_user' => $this->session->userdata('member')->id_user,
                    'publish' => 0,
                    'content' => $this->input->post('content'),
                    'date_modified' => date('Y-m-d H:i:s'),
                );
                $insert = $this->produk_m->update('tbl_article', 'md5(id_article)', $id_article, $input);
                
                if ($insert)
                    $data['success'] = 'success';
                else
                    $data['error'] = 'failed';
            }
        }
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['article'] = $this->article_m->get_article_single($id_article, $id_user);
        $this->load->view('publik/myarticle_edit_v', $data);
    }

    public function delete($id_article = '', $id_user = '') {
        $data = array();
        $data['success'] = '';
        $data['error'] = '';
        $del_produk = $this->article_m->delete_user($id_article, $id_user);
        redirect(base_url('myarticle?delete=success'));
    }
    
    public function redactor_upload() {
        $img = 'file';
        $config['file_name'] = preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES[$img]['name']) . date('dmYHis');
        $config['upload_path'] = "upload/media/";
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size'] = '5060';
        $config['max_width'] = '10000';
        $config['max_height'] = '10000';
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($img)) {
            $return['error'] = $this->upload->display_errors();
            $return['file_name'] = '';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $return['error'] = '-';
            $return['file_name'] = $data['upload_data']['file_name'];
        }
        $this->upload->file_name = '';

        //# Save Sticker to database
//        $insert = array('idmedia' => null,
//            'media_url' => base_url('upload/media') . "/" . $return['file_name'],
//            'media_type' => 'image'
//        );
//
//        $this->media_m->save('media', $insert);
        $array = array(
            'filelink' => base_url('upload/media') . "/" . $return['file_name']
        );
        echo stripslashes(json_encode($array));
    }
    
    public function redactor_file() {
        $img = 'file';
        $config['file_name'] = preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES[$img]['name']) . date('dmYHis');
        $config['upload_path'] = "upload/file/";
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = '*';
        $config['max_size'] = '50060';
        $config['max_width'] = '10000';
        $config['max_height'] = '10000';
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($img)) {
            $return['error'] = $this->upload->display_errors();
            $return['file_name'] = '';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $return['error'] = '-';
            $return['file_name'] = $data['upload_data']['file_name'];
        }
        $this->upload->file_name = '';

        //# Save Sticker to database
//        $insert = array('idmedia' => null,
//            'media_url' => base_url('upload/file/') . $return['file_name'],
//            'media_type' => 'file'
//        );
//
//        $this->media_m->save('media', $insert);

        if ($return['file_name'] == '') {
            echo "Upload Failed, " . $return['error'];
        } else {
            $array = array(
                'filelink' => base_url('upload/file/') . $return['file_name']
            );
            echo stripslashes(json_encode($array));
        }
    }

}

/* End of file article.php */
/* Location: ./application/controllers/article.php */