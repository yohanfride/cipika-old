<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_m');
		$this->load->model('user_m');
		$this->load->helper('app_helper');
		// $this->lib_facebook->cek_connect();
	}

	public function index()
	{
		$data=array();

		//refresh stok produk, back stock after 4 hours never paid
        //sementara diremark dulu oleh andy, karena blm berani testing
		//$this->cancel_order();
		if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
	    else{$data['by_search'] = '';}
	    if(isset($_GET['cat'])){$data['by_category'] = $_GET['cat'];}
	    else{$data['by_category'] = '';}
	    if(isset($_GET['loc'])){$data['by_location'] = $_GET['loc'];}
	    else{$data['by_location'] = '';}
	    if(isset($_GET['pr'])){$data['by_price'] = $_GET['pr'];}
	    else{$data['by_price'] = '';}
	    if(isset($_GET['tag'])){$data['by_tag'] = $_GET['tag'];}
	    else{$data['by_tag'] = '';}
	    if(isset($_GET['sort'])){$data['by_sort'] = $_GET['sort'];}
		else{$data['by_sort'] = '';}

		if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
	    else{$data['by_search'] = '';}
		$limit=12;
		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
			else{$offset=0;}
		$data['produk']=$this->home_m->get_produk($limit, $offset);
		$data['produk_love1']=$this->home_m->get_produk_love(0,1);
		$data['produk_love']=$this->home_m->get_produk_love(1,4);
		$data['produk_pick1']=$this->home_m->get_produk_pick(0,1);
		$data['produk_pick']=$this->home_m->get_produk_pick(1,4);
		$data['produk_disqus']=$this->home_m->get_produk_disqus();
		$data['provinsi']=$this->home_m->get_all_data_order('tbl_propinsi', 'id_propinsi', 'asc');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		// $data['tag']=$this->home_m->get_all_data('tbl_tag', 'nama_tag', 'asc');
		$data['banner']=$this->home_m->get_all_data_order('banner', 'idbanner', 'asc');
		$this->load->view('publik/home_v', $data);
	}

	function cek_login(){
		$konek = $this->connect();		
		if(isset($konek['user_profile'])){
			if($konek['user']){
			
				$user = $this->user_m->get_email_user($konek['user_profile']['email']); 
				if(!empty($user)){ /* registered, force login */
					echo "udah daftar";
				}else{
					$password = random_password();
					$email    = $konek['user_profile']['email'];
					$input1 = array(
								"email" 		=> $email,
								"password" 		=> md5($password),
								"firstname" 	=> $konek['user_profile']['first_name'],
								"lastname" 	=> $konek['user_profile']['last_name'],
								"telpon" 	=> 0,
								"fb_token" 		=> $konek['token']
							);
							
					$insert1=$this->user_m->insert('tbl_user', $input1);
				
					echo "berhasil daftar";		
				}
			}
		}
	}
    
    
    function contactus()
    {
        $this->load->library('form_validation');
        $this->load->model('contact_form_m');
        
        $data['page']   = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        
        if($this->input->post('submit')){
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('pesan', 'Pesan', 'required');
            
            $this->form_validation->set_message('required', '%s tidak boleh kosong.');
            $this->form_validation->set_message('valid_email', 'Format email tidak valid.');
            
            if($this->form_validation->run()){
                $insert = array( 'idcf'         => null,
                                 'nama'         => $this->input->post('nama'),
                                 'pesan'        => $this->input->post('pesan'),
                                 'email'        => $this->input->post('email'),
                                 'notes'        => '',
                                 'tgl_kirim'    => date('Y-m-d H:i:s'),
                                 'read_status'  => 0,
                                 'delete_status'=> 1
                                );
                $this->contact_form_m->insert('contact_form',$insert);
                
                //# send mail nitification to admin
                
                $this->load->library('lib_mailer');                    
                $mail_data  = array('data' => $insert);                
                $message    = $this->load->view('email/admin_contact_form_v',$mail_data,TRUE);
                    $mailer     = array('module'    => 'Contact Form',
                                        'from'      => $this->config->item('email_from'),
                                        'to'        => $this->config->item('email_contact_form'),
                                        'subject'   => "Kontak Form dari ".$insert['nama'],
                                        'message'   => $message
                                        );
                    $this->lib_mailer->save($mailer);
                
                
                redirect(base_url('contact-us?s=1'));
            }else{
                $data['error']  = validation_errors();
            }
        }
    
        $this->load->view('publik/contactus_v', $data);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
