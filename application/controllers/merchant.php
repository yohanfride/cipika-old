<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_m');
		$this->load->model('user_m');
		$this->load->model('meta_m');
		$this->load->model('merchant_m');
		$this->load->model('auth_m');        
		$this->auth_m->check();
	}
    
    /* Daftar Merchant : Step 1 Profile */
    function register()
    {
        $this->load->library('lib_lokasi');
        $this->load->library('form_validation');
        
        $data['js_lokasi']      = TRUE;
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['propinsi']       = $this->lib_lokasi->get_propinsi();
        $data['data_sales']     = $this->merchant_m->get_sales();
        
        //# Pastikan masih sbg member , level 7 = merchant --> redir ke step 2, upload product
        $user = $this->merchant_m->get_single('tbl_user','id_user',(int)$this->session->userdata('member')->id_user); 
        if($user->id_level==7){
            redirect(base_url('merchant/get_started'));
        }
        
        if($this->input->post('button_register')){
            $this->form_validation->set_rules('nama_store', 'Nama Merchant', 'required');
            $this->form_validation->set_rules('deskripsi', 'Biodata', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('kecamatan', 'Alamat Kecamatan', 'required');
            $this->form_validation->set_rules('nama_pemilik', 'Nama Pemilik', 'required');
            $this->form_validation->set_rules('birthdate', 'Tgl Lahir', 'required');
            $this->form_validation->set_rules('telpon', 'Telp', 'required');
            $this->form_validation->set_rules('hp', 'No HP', 'required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('bank_nama', 'Nama Bank', 'required');
            $this->form_validation->set_rules('bank_norek', 'No Rekening', 'required');
            $this->form_validation->set_rules('bank_pemilik', 'Pemegang Rekening', 'required');
            $this->form_validation->set_rules('syarat_ketentuan', 'Syarat dan Ketentuan', 'required');
            
          	if ($this->form_validation->run() == true){
                $country            = get_visitor_country();
                $insert = array('id_store'              => null,
                                'id_kota'               => $this->input->post('kecamatan'),
                                'id_user'               => (int)$this->session->userdata('member')->id_user,
                                'id_sales'              => $this->input->post('merchant_sales'),
                                'negara'                => $country['country'],
                                'nama_store'            => $this->input->post('nama_store'),
                                'deskripsi'             => $this->input->post('deskripsi'),
                                'nama_pemilik'          => $this->input->post('nama_pemilik'),
                                'tgl_lahir_pemilik'     => strftime("%Y-%m-%d",strtotime($this->input->post('birthdate'))),
                                'alamat'                => $this->input->post('alamat'),
                                'telpon'                => $this->input->post('telpon'),
                                'merchant_hp'           => $this->input->post('hp'),
                                'merchant_gender'       => $this->input->post('gender'),
                                'ym'                    => '',
                                'fb'                    => '',
                                'tw'                    => '',
                                'bb'                    => '',
                                'wa'                    => '',
                                'email'                 => $this->session->userdata('member')->email,
                                'bank_nama'             => $this->input->post('bank_nama'),
                                'bank_norek'            => $this->input->post('bank_norek'),
                                'bank_pemilik'          => $this->input->post('bank_pemilik'),
                                'store_status'          => 'pending',
                                'date_added'            => date('Y-m-d H:i:s'),
                                'date_modified'         => date('Y-m-d H:i:s')
                                );
                $this->merchant_m->insert('tbl_store',$insert);
                
                //# Update status level user ke merchant (7)
                $update = array('id_level' => 7);
                $this->merchant_m->update('tbl_user', 'id_user', (int)$this->session->userdata('member')->id_user, $update);
                
                //# If sales changes, resend email
                if($data['curr_sales']!=''){
                    $sales      = $this->merchant_m->get_single('tbl_user','id_user',$this->input->post('merchant_sales'));
                    $message    = $this->input->post('merchant_name').' Telah memilih anda sebagai Salesnya';
                    send_mail($sales->email,'noreply@cipika.com','Pemberitahuan Merchant Baru',$message);
                }
                
                redirect(base_url('merchant/get_started'));
            }else{                
                $data['error'] = validation_errors();
            }
            
            $data['data']= (object) $_POST;
            $dataDb = $this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
            if(empty($_POST['gender'])) $data['data']->gender = $dataDb->gender;
            if(empty($_POST['id_propinsi'])) $data['data']->id_propinsi = $dataDb->id_propinsi; 
            if(empty($_POST['id_kabupaten'])) $data['data']->id_kabupaten = null; 
            if(empty($_POST['id_kecamatan'])) $data['data']->id_kecamatan = $_POST['kecamatan']; 
            }else{
            $data['data']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
        }
    
        $this->load->view('publik/merchant/register_v', $data);
    }
    
    /* Daftar Merchant : Step 2 Upload 1st Product */
    function get_started()
    {
        $this->load->model('myproduct_m');
        $this->load->model('produk_m');
        
        //# Pastikan merchant blm punya produk
        $data['merchant_product']   = $this->merchant_m->get_merchant_product($this->session->userdata('member')->id_user);
        if(count($data['merchant_product'])>0){
            redirect(base_url('merchant/verification'));
        }
    
        // $data['page']           = $this->home_m->get_all_data('tbl_page', 'sorting', 'asc');
        $data['kategori']       = $this->myproduct_m->get_data_where('tbl_kategori', 'id_parent', '0');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['provinsi']       = $this->produk_m->get_all_data_order('tbl_provinsi', 'nama_provinsi', 'asc');
        $store                  = $this->merchant_m->get_single('tbl_store','id_user',$this->session->userdata('member')->id_user);
        $data['success']        = '';
		$data['error']          = '';
    
        if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_produk', 'Nama produk', 'required');			
            $this->form_validation->set_rules('deskripsi', 'Deskripsi', '');
            $this->form_validation->set_rules('berat', 'Berat', '');
			$this->form_validation->set_rules('panjang', 'Panjang', 'numeric');
			$this->form_validation->set_rules('lebar', 'Lebar', 'numeric');
			$this->form_validation->set_rules('tinggi', 'Tinggi', 'numeric');
			$this->form_validation->set_rules('stok_produk', 'Stok', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'required');
			$this->form_validation->set_rules('diskon', 'Diskon', 'required');
			$this->form_validation->set_rules('id_kategori', 'Kategori', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'numeric');
			
            if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}else{
			
                            $volume = $this->input->post('panjang')*$this->input->post('lebar')*$this->input->post('tinggl');
                            $volume_kon = $volume/6000;
                            if($volume_kon > $this->input->post('berat'))
                                $jne_berat = $volume_kon;
                            else {
                                $jne_berat = $this->input->post('berat');
                            }
                            
                            $input=array(
					'id_produk'     => null,
					'nama_produk'   => $this->input->post('nama_produk'),
					'deskripsi'     => $this->input->post('deskripsi'),
					'berat'         => $this->input->post('berat'),
					'stok_produk'   => $this->input->post('stok_produk'),
					'harga_produk'  => $this->input->post('harga_produk'),
					'diskon'        => $this->input->post('diskon'),
					'publish'       => 0,
					'view_notif'    => 'N',
					'id_user'       => $this->session->userdata('member')->id_user,
					'id_kota'       => $this->input->post('id_kota'),
					'panjang'       => $this->input->post('panjang'),
					'lebar'         => $this->input->post('lebar'),
					'tinggi'        => $this->input->post('tinggi'),
                                        'jne_berat'     => $jne_berat,
					'date_added'    => date('Y-m-d H:i:s')
				);
				$insert = $this->produk_m->insert('tbl_produk', $input);		
                                
                if (!empty($_POST['nama_tag'])) {
                    $tag = explode(',', $_POST['nama_tag']);
                    foreach ($tag as $a){
                                    $b = trim($a);
                                    //select tag
                                    $select_tag = $this->db->query("select * from tbl_tag where nama_tag = '".$b."'");
                                    if ($select_tag->num_rows() > 0) {
                                        $result = $select_tag->row();
                                        $idtag = $result->id_tag;
                                    }else{
                                        $insert_tag = array('id_tag' => null,
                                            'nama_tag' => $b,
                                            'date_added' => date('Y-m-d H:i:s'),
                                            'date_modified' => date('Y-m-d H:i:s')
                                        );
                                        $idtag = $this->produk_m->insert('tbl_tag', $insert_tag);
                                    }
                                    //insert tag_relation
                                     $insert_relation = array('id_produk_tag' => null,
                                        'id_produk' => $insert,
                                        'id_tag' => $idtag,
                                        'date_added' => date('Y-m-d H:i:s'),
                                        'date_modified' => date('Y-m-d H:i:s')
                                    );
                                    $this->produk_m->insert('tbl_produk_tag', $insert_relation);
                    }
                }
                                
                if($insert){
					foreach ($_POST['photo_img'] as $foto) {
                        if($foto!=''){
                            $inpdet=array(
                                'id_produk' => $insert,
                                'image'	 => $foto,
                                'date_added' => date('Y-m-d H:i:s'),
                                'date_modified' => date('Y-m-d H:i:s')
                            );
                            $produk_foto=$this->produk_m->insert('tbl_produkfoto', $inpdet);
                        }
					}
                    
					foreach ($_POST['id_kategori'] as $id) {
                        if($id!=''){
                            $prod_kat=array('id_kategori'=>$id, 'id_produk'=>$insert, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
                            $ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
                        }
					}
				}
				if($insert)	$data['success']='success';
				else $data['error']='failed';
		    }
		}
        
        $this->load->view('publik/merchant/get_started_v', $data);
    }
    
    /* Daftar Merchant : Step 3 Status verifikasi oleh admin */
    function verification()
    {
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['merchant']       = $this->merchant_m->get_single_merchant((int)$this->session->userdata('member')->id_user);
        
    
        $this->load->view('publik/merchant/verification_v', $data);
    }
    
    /* Setting Merchant Profile */
    public function index()
    {
        $this->load->library('lib_lokasi');
         $this->load->library('form_validation');
        
		$data['js_lokasi']      = TRUE;
		$data['menu_profile']   = "class='active'";
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['merchant']       = $this->merchant_m->get_single_merchant((int)$this->session->userdata('member')->id_user);
		$data['propinsi']       = $this->lib_lokasi->get_propinsi();
        $data['data_sales']     = $this->merchant_m->get_sales();
        $data['curr_sales']     = $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_sales');
        
        //# Save Merchant Data
        if($this->input->post('simpan')){

            $this->form_validation->set_rules('nama_store', 'Nama Merchant', 'required');
            $this->form_validation->set_rules('nama_pemilik', 'Nama Pemilik', 'required');
            $this->form_validation->set_rules('deskripsi', 'Biodata', 'required');
            $this->form_validation->set_rules('tgl_lahir_pemilik', 'Tgl Lahir', 'required');
            $this->form_validation->set_rules('propinsi', 'Propinsi', 'required');
            $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');
            $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
            $this->form_validation->set_rules('telpon', 'Telp', 'required|numeric');
            $this->form_validation->set_rules('merchant_hp', 'No HP', 'required|numeric');
            $this->form_validation->set_rules('bank_nama', 'Nama Bank', 'required');
            $this->form_validation->set_rules('bank_norek', 'No Rekening', 'required');
            $this->form_validation->set_rules('bank_pemilik', 'Pemegang Rekening', 'required');
            
            if($this->form_validation->run()){
                $country            = get_visitor_country();
                $update = array(    'id_kota'               => $this->input->post('kecamatan'),                                
                                    'id_sales'              => $this->input->post('merchant_sales'),
                                    'negara'                => $country['country'],
                                    'nama_store'            => $this->input->post('nama_store'),                                
                                    'deskripsi'             => $this->input->post('deskripsi'),
                                    'nama_pemilik'          => $this->input->post('nama_pemilik'),
                                    'tgl_lahir_pemilik'     => strftime("%Y-%m-%d",strtotime($this->input->post('tgl_lahir_pemilik'))),                                
                                    'telpon'                => $this->input->post('telpon'),
                                    'bank_nama'             => $this->input->post('bank_nama'),
                                    'bank_norek'            => $this->input->post('bank_norek'),
                                    'bank_pemilik'          => $this->input->post('bank_pemilik'),                                
                                    'merchant_hp'           => $this->input->post('merchant_hp'),                                
                                    'date_modified'         => date('Y-m-d H:i:s')
                                    );
                $this->merchant_m->update('tbl_store','id_store',(int)$data['merchant']->id_store,$update);
                
//                $update = array('birthdate' => strftime("%Y-%m-%d",strtotime($this->input->post('tgl_lahir_pemilik'))) );
//                $this->merchant_m->update('tbl_user','id_user',(int)$data['merchant']->id_user,$update);

                //# If sales changes, resend email
                if($data['curr_sales']!=$this->input->post('merchant_sales')){
                    $sales      = $this->merchant_m->get_single('tbl_user','id_user',$this->input->post('merchant_sales'));
                    $message    = $this->input->post('merchant_name').' Telah memilih anda sebagai Salesnya';
                    send_mail($sales->email, $this->config->item('email_from'), 'Pemberitahuan Merchant Baru',$message);
                }
                $data['success'] = "Success";
                redirect(base_url('merchant?p=s'));
            }else{                
                $data['error'] = validation_errors();
            }

        }
        
        
		$this->load->view('publik/merchant/edit_merchant_profile_v', $data);
    }
    
    /* Change Merchant Email */
    public function email()
    {
        $data['menu_email']     = "class='active'";
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['user']           = $this->merchant_m->get_single('tbl_user','id_user', $this->session->userdata('member')->id_user);
        $data['error']          = '';
        
        //# Update user email
        if($this->input->post('simpan')){
            $this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Email', 'required|matches[email_confirm]|is_unique[tbl_user.email]|valid_email');
			$this->form_validation->set_rules('email_confirm', 'Email Confirmation', 'required|valid_email');
            $this->form_validation->set_message('is_unique', 'Email %s sudah terpakai, gunakan email yg lain.');
            
            if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}else{
                $this->user_m->update('tbl_user', 'id_user', $this->session->userdata('member')->id_user, array('email'=>$this->input->post('email')));
                redirect(base_url('merchant/email'));
            }
        }
        
        $this->load->view('publik/merchant/change_email_v', $data);
    }
    
    /* Setting food delivery city */
    public function food_delivery()
    {
        $data['menu_food']   = "class='active'";
        $data['propinsi']    = $this->merchant_m->get_provinsi();
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        
        $this->load->view('publik/merchant/food_delivery_v', $data);
    }
    
    /* Manage Order */
    public function orders()
    {
        $this->load->model('order_m');
        $data['menu_order']     = "class='active'";
        $data['page']           = $this->home_m->get_all_data('tbl_page', 'sorting', 'asc');
        $data['orders']         = $this->merchant_m->get_merchant_order((int)$this->session->userdata('member')->id_user,0,100,'done');
        
        $this->load->view('publik/merchant/orders_v', $data);
    }
    
    /* Edit Order */
    public function edit_order()
    {
        $this->load->model('order_m');
        $this->load->library('lib_lokasi');
        
        $data['menu_order']     = "class='active'";
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']           = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $idorder                = $this->uri->segment(3);
        
		$data['order']          = $this->merchant_m->get_order($idorder);		
        
		
        $this->load->view('publik/merchant/edit_order_v', $data);
    }
    
    /* Update Order Status */
    public function update_order()
    {
        $this->load->model('order_m');
        $this->load->library('lib_lokasi');
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('ongkir_merchant', 'Ongkir', 'numeric|callback__input_kirim');
		$this->form_validation->set_rules('noresi', 'AWB Number', 'callback__input_kirim');
		$this->form_validation->set_rules('delivery_date', 'Tgl Kirim', 'callback__input_kirim');
		$this->form_validation->set_rules('paket_ongkir_merchant', 'Paket Pengiriman', 'callback__input_kirim');
        
        $this->form_validation->set_message('numeric', '%s, harus berupa angka.');
        
        if ($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
		}else{
            $ongkir             = $this->input->post('ongkir_merchant');
            $noresi             = $this->input->post('noresi');
            $tgl_kirim          = $this->input->post('delivery_date');
            $status_delivery    = $this->input->post('status_delivery');
            
        
            if( ($noresi!='') ){
                $status_delivery = "proses pengiriman";
            }
            
            $delivery_date = ($this->input->post('delivery_date')!='')?strftime("%Y-%m-%d",strtotime($this->input->post('delivery_date'))):'';
            $input  = array(                       
                        'paket_ongkir_merchant'     => $this->input->post('paket_ongkir_merchant'),
                        'status_delivery'           => $status_delivery,
                        'ongkir_merchant'           => $this->input->post('ongkir_merchant'),
                        'noresi'                    => $this->input->post('noresi'),
                        'delivery_date'             => $delivery_date
                        );                        
            $insert=$this->order_m->update('tbl_order', 'id_order', $this->input->post('id_order'), $input);
            redirect('merchant/orders');
        }
        
        $data['menu_order']     = "class='active'";
        $data['page']           = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $idorder                = $this->input->post('id_order');        
		$data['order']          = $this->merchant_m->get_order($idorder);		
        
        $this->load->view('publik/merchant/edit_order_v', $data);
    }
    
    /* AJAX : Get all kota by idprovinsi */
    public function get_city()
    {
        $idprovinsi     = $this->input->post('provinsi');
        $data['city']   = $this->merchant_m->get_city($idprovinsi);
        
        echo $this->load->view('publik/merchant/ajax_city_v', $data,TRUE);
    }
    
    /* AJAX : Add Food Delivery City */
    public function add_city()
    {
        
        $idcity     = $this->input->post('idcity');
        $metas      = array('merchant_food_delivery_city' => $idcity);
        $this->meta_m->save_meta('tbl_user',$this->session->userdata('member')->id_user,$metas,'add');        
    }
    
    /* AJAX : Get all delivery city */
    public function get_delivery_city()
    {
        $data['delivery_city']   = $this->meta_m->get_meta('tbl_user',$this->session->userdata('member')->id_user);
        
        echo $this->load->view('publik/merchant/ajax_delivery_city_v', $data,TRUE);
    }
        
    /* AJAX : remove 1 delivery city */
    public function remove_delivery_city()
    {
        $idmeta = $this->input->post('idmeta');
        $this->meta_m->delete_meta($idmeta);
    }
 
 
    function _input_kirim()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        $paket      = $this->input->post('paket_ongkir_merchant');
        
        if( (($ongkir==0) || ($ongkir=='')) && ($noresi=='') && ($tgl_kirim=='') && ($paket=='') ){     
            return TRUE;
        }else{
            
            if( ($ongkir>0) && ($noresi!='') && ($tgl_kirim!='') && ($paket!='')  ){
                return TRUE;
            }else{
                $this->form_validation->set_message('_input_kirim', '%s Tidak boleh kosong ');
                return FALSE;
            }
            
            
        }
    }
 
    function _check_ongkir()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( ($ongkir==0) || ($ongkir=='') ){
            return TRUE;
        }else{
            $msg = "";
            
            if($noresi==''){
                $msg.="No Resi ";
            }
            
            if($tgl_kirim==''){
                $msg.="Tgl Kirim ";
            }
            
            $this->form_validation->set_message('_check_ongkir', $msg.'tidak boleh kosong');                
            
            return FALSE;
        }
    }
    
    function _check_noresi()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( $noresi=='' ){
            return TRUE;
        }else{
            $msg = "";
            
            if( ($ongkir==0) || ($ongkir=='') ){
                $msg.="Ongkir ";
            }
            
            if($tgl_kirim==''){
                $msg.="Tgl Kirim ";
            }
            
            $this->form_validation->set_message('_check_noresi', $msg.'tidak boleh kosong');                
            
            return FALSE;
        }
    }
    
    function _check_tglkirim()
    {
        $ongkir     = $this->input->post('ongkir_merchant');
        $noresi     = $this->input->post('noresi');
        $tgl_kirim  = $this->input->post('delivery_date');
        
        if( $tgl_kirim=='' ){
            return TRUE;
        }else{
            $msg = "";
            
            if( ($ongkir==0) || ($ongkir=='') ){
                $msg.="Ongkir ";
            }
            
            if($noresi==''){
                $msg.="No Resi ";
            }
            
            $this->form_validation->set_message('_check_tglkirim', $msg.'tidak boleh kosong');                
            
            return FALSE;
        }
    }
}