<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dompetku extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // $this->lib_facebook->cek_connect();
    }

    public function index() {
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/dompetku_v', $data);
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
