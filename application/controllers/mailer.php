<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailer extends CI_Controller {
   
    /* Send all pending email */
	public function index($limit=5,$debug=false)
	{
        $this->load->library('lib_mailer');
    
        $emails = $this->lib_mailer->get_emails($limit);
        
        if(!empty($emails)){
            $config = $this->_mail_win();
            $this->load->library('email', $config);
            
            foreach($emails as $row){
                $this->email->set_newline("\r\n");
                $this->email->from($row->mailer_from, 'Cipika Store');
                $this->email->to($row->mailer_to);
                
                if($row->mailer_cc!=''){
                    $this->email->cc($row->mailer_cc);
                }
                
                if($row->mailer_bcc!=''){
                    $this->email->bcc($row->mailer_bcc);
                }
                
                $this->email->subject($row->mailer_subject);
                $this->email->message($row->mailer_message);
                $send = $this->email->send();
                
                $status = (!$this->email->send())?'not sent':'sent';
                $this->lib_mailer->update($row->idmailer,array('mailer_status' => $status,'mailer_sent' => date('Y-m-d H:i:s')));                
                
                if($status!='sent'){
                    log_message('mailer error', $this->email->print_debugger());
                }
                
                if($debug){
                    print_r($row);
                    echo "<br/>*******************************************<br/>";
                    echo $this->email->print_debugger();
                    echo '<hr/>';
                    
                }
            }
        }
	}
    
    /* Send single email */
	public function send_mail($idmailer='')
	{
        if($idmailer!=''){
            $this->load->library('lib_mailer');    
            $row = $this->lib_mailer->get_single_email($idmailer);        
            if(!empty($row)){
                $config = $this->_mail_win();
                $this->load->library('email', $config);                
                
                $this->email->set_newline("\r\n");
                $this->email->from($row->mailer_from, 'Cipika Store');
                $this->email->to($row->mailer_to); 

                if($row->mailer_cc!=''){
                    $this->email->cc($row->mailer_cc);
                }
                
                if($row->mailer_bcc!=''){
                    $this->email->bcc($row->mailer_bcc);
                }
                
                $this->email->subject($row->mailer_subject);
                $this->email->message($row->mailer_message);
                $send = $this->email->send();

                $status = (!$this->email->send())?'not sent':'sent';
                $this->lib_mailer->update($row->idmailer,array('mailer_status' => $status,'mailer_sent' => date('Y-m-d H:i:s')));
                
                if($status!='sent'){
                    log_message('mailer error', $this->email->print_debugger());
                }
            }
        }
	}
    
    function _mail_win()
    {
        $config = Array(
            'protocol'      => $this->config->item('protocol'),
            'smtp_host'     => $this->config->item('smtp_host'),
            'smtp_port'     => $this->config->item('smtp_port'),
            'smtp_user'     => $this->config->item('smtp_user'),
            'smtp_pass'     => $this->config->item('smtp_pass'),
            'mailtype'      => $this->config->item('mailtype'),
            'charset'       => $this->config->item('charset'),
            'smtp_crypto'   => $this->config->item('smtp_crypto'),
            'bcc_batch_mode'   => true,
            'bcc_batch_size'   => 5            
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype'  => $this->config->item('mailtype'),
            'charset'   => $this->config->item('charset'),
            'bcc_batch_mode'   => true,
            'bcc_batch_size'   => 5            
        );
        return $config;
    }
}