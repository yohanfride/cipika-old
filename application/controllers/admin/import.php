<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {

	public function __construct() {

        parent::__construct();
        $this->load->library('auth');        
        $this->auth->check();
    }
    
	public function index()
	{   
        redirect(base_url('admin/import/sales'));
	}
    
    public function sales()
	{   
        $data['title']      = 'Import Sales';
        $data['current']    = 'import-sales';
		
		$this->load->view('admin/import/sales_v', $data);  
	}
    
    public function merchant()
	{   
        $data['title']      = 'Import Merchant';
        $data['current']    = 'import-merchant';
		
		$this->load->view('admin/import/merchant_v', $data);  
	}
    
    public function product()
	{   
        $data['title']      = 'Import Product';
        $data['current']    = 'import-product';
		
		$this->load->view('admin/import/product_v', $data);  
	}
    
    
    function import_sales()
    {
        $this->load->model('user_m');
        $uploaddir  = 'asset/upload/';
        $uploadfile = $uploaddir . basename($_FILES['file_excel']['name']);

        if(move_uploaded_file($_FILES['file_excel']['tmp_name'], $uploadfile)) {
            
            require_once 'plugin/excel.reader/excel_reader2.php';
            $data = new Spreadsheet_Excel_Reader($uploadfile);
            
            $row = $data->rowcount();            
            if($row>0){
                for($i=2;$i<=$row;$i++){
                    
                    if(!$this->user_m->duplicate_email(trim($data->val($i,2)))){                        
                        $plaint_password = random_password();
                        $insert = array(
                                        'id_user'       => null,
                                        'username'      => trim($data->val($i,1)),
                                        'password'      => md5($plaint_password),
                                        'email'         => trim($data->val($i,2)),
                                        'firstname'     => trim($data->val($i,3)),
                                        'lastname'      => trim($data->val($i,4)),
                                        'id_level'      => 3, // 3 = level sales
                                        'telpon'        => trim($data->val($i,5)),
                                        'active'        => 1,
                                        'id_kota'       => 134, // <-- jakarta
                                        'id_propinsi'   => 5,   // <-- dki jakarta
                                        'id_kabupaten'  => 5149, // <-- senen
                                        'id_kecamatan'  => 1,
                                        'id_status'     => 1, // confirmed
                                        'date_added'    => date('Y-m-d H:i:s')
                                    );
                        $this->user_m->insert('tbl_user', $insert);
                        
                        $message = "Password anda untuk Login Cipika : ".$plaint_password;
                        send_mail(trim($data->val($i,2)),'noreply@cipikastore.com','Password Cipika',$message);
                    }
                }
            }
            
            @unlink($uploadfile);
            redirect(base_url('admin/import/sales/?s=success'));
        }else{
            @unlink($uploadfile);
            redirect(base_url('admin/import/sales/?e='.$_FILES["file_excel"]["error"]));
        }
    }
    
    function import_merchant()
    {
        $this->load->model('user_m');
        $this->load->model('merchant_m');
        $uploaddir  = 'asset/upload/';
        $uploadfile = $uploaddir . basename($_FILES['file_excel']['name']);

        if(move_uploaded_file($_FILES['file_excel']['tmp_name'], $uploadfile)) {
            
            require_once 'plugin/excel.reader/excel_reader2.php';
            $data = new Spreadsheet_Excel_Reader($uploadfile);
            
            $row = $data->rowcount();            
            if($row>0){
                for($i=2;$i<=$row;$i++){
                    
                    if(!$this->user_m->duplicate_email(trim($data->val($i,2)))){
                        
                        //# Insert User
                        $plaint_password = random_password();
                        $insert = array(
                                        'id_user'       => null,
                                        'username'      => trim($data->val($i,3)).trim($data->val($i,4)),
                                        'password'      => md5($plaint_password),
                                        'email'         => trim($data->val($i,2)),
                                        'firstname'     => trim($data->val($i,3)),
                                        'lastname'      => trim($data->val($i,4)),
                                        'id_level'      => 7, // 7 = level merchant
                                        'telpon'        => trim($data->val($i,5)),
                                        'active'        => 1,
                                        'id_kota'       => 134, // <-- jakarta (default)                                        
                                        'id_propinsi'   => trim($data->val($i,8)), 
                                        'id_kabupaten'  => trim($data->val($i,9)), 
                                        'id_kecamatan'  => trim($data->val($i,10)),
                                        'id_status'     => 1, // confirmed
                                        'date_added'    => date('Y-m-d H:i:s')
                                    );
                        $iduser = $this->user_m->insert('tbl_user', $insert);
                        
                        $message = "Password anda untuk Login Cipika : ".$plaint_password;
                        //send_mail(trim($data->val($i,2)),'noreply@cipikastore.com','Password Cipika',$message);
                        
                        //#Insert Merchant
                        $insert = array('id_store'      => null,
                                'id_kota'               => trim($data->val($i,10)), 
                                'id_user'               => (int)$iduser,
                                'id_sales'              => (int)trim($data->val($i,14)), 
                                'negara'                => 'Indonesia',
                                'nama_store'            => trim($data->val($i,1)),
                                'nama_pemilik'          => trim($data->val($i,3)).' '.trim($data->val($i,4)),
                                'tgl_lahir_pemilik'     => '',
                                'deskripsi'             => '',
                                'alamat'                => trim($data->val($i,7)),
                                'telpon'                => $this->input->post('telpon'),
                                'ym'                    => '',
                                'fb'                    => '',
                                'tw'                    => '',
                                'bb'                    => '',
                                'wa'                    => '',
                                'email'                 => trim($data->val($i,2)),
                                'bank_nama'             => trim($data->val($i,11)),
                                'bank_norek'            => trim($data->val($i,12)),
                                'bank_pemilik'          => trim($data->val($i,13)),
                                'store_status'          => 'approve',
                                'date_added'            => date('Y-m-d H:i:s'),
                                'date_modified'         => date('Y-m-d H:i:s')
                                );
                        $this->merchant_m->insert('tbl_store',$insert);
                    }
                }
            }
            
            @unlink($uploadfile);
            redirect(base_url('admin/import/merchant/?s=success'));
        }else{
            @unlink($uploadfile);
            redirect(base_url('admin/import/merchant/?e='.$_FILES["file_excel"]["error"]));
        }
    }

    function import_product()
    {
        $this->load->model('produk_m');
        $uploaddir  = 'asset/upload/';
        $uploadfile = $uploaddir . basename($_FILES['file_excel']['name']);

        if(move_uploaded_file($_FILES['file_excel']['tmp_name'], $uploadfile)) {
            
            require_once 'plugin/excel.reader/excel_reader2.php';
            $data = new Spreadsheet_Excel_Reader($uploadfile);
            
            $row = $data->rowcount();            
            if($row>0){
                for($i=2;$i<=$row;$i++){
                   
                    $kol_dimensi = trim($data->val($i,8));
                    $dimensi = ($kol_dimensi!='')?explode("x",$kol_dimensi):array(0,0,0);
                    
                    $input = array(
                                    'id_produk'     => null,
                                    'nama_produk'   => trim($data->val($i,1)),
                                    'deskripsi'     => trim($data->val($i,2)),
                                    'berat'         => trim($data->val($i,7))/1000,
                                    'stok_produk'   => trim($data->val($i,3)),
                                    'harga_produk'  => trim($data->val($i,4)),
                                    'harga_jual'    => trim($data->val($i,5)),
                                    'diskon'        => trim($data->val($i,6)),
                                    'publish'       => 1,
                                    'id_user'       => (int)trim($data->val($i,10)),
                                    'id_kota'       => 240, // default
                                    'panjang'       => (isset($dimensi[0]))?$dimensi[0]:'',
                                    'lebar'         => (isset($dimensi[1]))?$dimensi[1]:'',
                                    'tinggi'        => (isset($dimensi[2]))?$dimensi[2]:'',
                                    'date_added'    => date('Y-m-d H:i:s')
                                );
                    $idproduk = $this->produk_m->insert('tbl_produk', $input);
                    
                    //# Produk Kategori
                    $prod_kat=array('id_kategori'=>trim($data->val($i,9)), 'id_produk'=>$idproduk, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
					$ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
                    
                    //# Produk Image
                    $kolomimg = 11;
                    for($j=1;$j<=3;$j++){
                        $image = array(
                                'id_produk'     => $idproduk,
                                'image'	        => "http://cipikastage.getshoop.com/asset/upload/".trim($data->val($i,$kolomimg)),
                                'date_added'    => date('Y-m-d H:i:s'),
                                'date_modified' => date('Y-m-d H:i:s')
                        );
                        $produk_foto=$this->produk_m->insert('tbl_produkfoto', $image);
                        $kolomimg++;
                    }
                }
            }
            
            @unlink($uploadfile);
            redirect(base_url('admin/import/product/?s=success'));
        }else{
            @unlink($uploadfile);
            redirect(base_url('admin/import/product/?e='.$_FILES["file_excel"]["error"]));
        }
    }
    
    function tes()
    {
        $kol_dimensi = '10x5x6';
        $dimensi     = ($kol_dimensi!='')?explode("x",$kol_dimensi):array(0,0,0);
        
        print_r($dimensi);
    }
}