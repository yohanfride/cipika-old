<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settlement extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('settlement_m');
        $this->load->library('auth');
        $this->auth->check();
    }
    
    public function index()
    {
        $data = array();
        $data['success']='';
        $data['error']='';
        $data['title'] = 'Settlement';

        if($this->input->post('simpan')){

            $this->load->library('form_validation');
            $this->form_validation->set_rules('id_order', 'Check box', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
            }
            else
            {
                $total = 0;
                foreach ($this->input->post('id_order') as $key) {
                    $order = $this->settlement_m->get_order($key);
                    $total += $order->total + $order->ongkir_sementara;
                }

                //S2014060001 MASIH BELUM FIX
                if($this->settlement_m->check_kode(date('Ym'))!=0){
                    $id = substr($this->settlement_m->get_last_kode(),7);
                    $id_baru = (int) $id + 10000 + 1;
                    $id_kode = substr($id_baru, 1);
                    $kode = 'S'.date('Ym').$id_kode;
                } else{
                    $kode = 'S'.date('Ym').'0001';
                }                

                $input=array(
                    'id_settlement' => null,
                    'tanggal' => date('Y-m-d H:i:s'),
                    'admin' => $this->session->userdata('admin_session')->username,
                    'kode_settlement' => $kode,
                    'total' => $total
                );

                //database transaction
                $this->db->trans_begin();

                $insert = $this->settlement_m->insert('tbl_settlement', $input);

                foreach ($this->input->post('id_order') as $id_order) {
                    $item = $this->settlement_m->get_settlement_item($id_order);
                    $input2 = array(
                        'id_settlement_item' => null,
                        'id_settlement' => $insert,
                        'kode_order' => $item->kode_order,
                        'nama_store' => $item->nama_store,
                        'bank_nama' => $item->bank_nama,
                        'bank_norek' => $item->bank_norek,
                        'bank_pemilik' => $item->bank_pemilik,
                        'total_request' => $item->total + $item->ongkir_sementara,
                        'total_transfer' => $item->total + $item->ongkir_sementara,
                    );
                    $insert2 = $this->settlement_m->insert('tbl_settlement_item', $input2);
                    // $update = $this->settlement_m->update('tbl_order', 'id_order', $id_order, array('settlement' => 'Y'));
                }

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $data['error']='Data gagal disimpan.';
                    
                }
                else
                {
                    $this->db->trans_commit();
                    $data['success']='Data berhasil disimpan.';
                }
            }
        }

        $data['datas'] = $this->settlement_m->get_tagihan();
        $this->load->view('admin/settlement/settlement_v', $data);
    }

    public function view(){
        $data=array();
        $data['title']='Settlement List';
        $data['datas'] = $this->settlement_m->get_all_data_order('tbl_settlement', 'id_settlement', 'desc');
        $this->load->view('admin/settlement/settlement_list_v', $data);
    }

    public function detail($id=''){
        $data=array();
        $data['title']='Settlement Detail';
        $data['settlement']=$this->settlement_m->get_single('tbl_settlement', 'id_settlement', $id);
        $data['datas'] = $this->settlement_m->get_data_where('tbl_settlement_item', 'id_settlement', $id);
        $this->load->view('admin/settlement/settlement_detail_v', $data);
    }

    public function export($id=''){
        $this->db->where('id_settlement', $id);
        $query = $this->db->get('tbl_settlement_item');
        $settlement = $this->settlement_m->get_single('tbl_settlement', 'id_settlement', $id);
 
        if(!$query)
            return false;
 
        // Starting the PHPExcel library
        $this->load->library('Excel');
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()
                    ->setCellValue('A1', 'Tanggal')
                    ->setCellValue('A2', 'Kode Settlement')
                    ->setCellValue('A3', 'Total (Rp)')
                    ->setCellValue('A4', 'Admin')
                    ->setCellValue('B1', ':')
                    ->setCellValue('B2', ':')
                    ->setCellValue('B3', ':')
                    ->setCellValue('B4', ':')
                    ->setCellValue('C1', $settlement->tanggal)
                    ->setCellValue('C2', $settlement->kode_settlement)
                    ->setCellValue('C3', $settlement->total)
                    ->setCellValue('C4', $settlement->admin)
                    ->getStyle('C3')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()
                    ->getStyle('B1:B4')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        $i=0;
        foreach ($fields as $field)
        {
            $i++;
            if($i>2){
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 5, $field);
                $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65+$col))->setAutoSize(true);
                $col++;
            }
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            $i=0;
            foreach ($fields as $field)
            {
                $i++;
                if($i>2){
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
                }
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Settlement'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');
    } 

}

/* End of file  */
/* Location: ./application/controllers/ */