<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Provinsi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('provinsi_m');
        $this->load->library('auth');
        $this->auth->check();
    }
    
    public function index()
    {
        $data = array();
        $data['alert'] = '';
        $data['title'] = 'provinsi';
        $data['datas'] = $this->provinsi_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
        $this->load->view('admin/provinsi_v', $data);
    }

    public function add()
    {
        $data = array();
        $data['alert'] = '';
        if ($this->input->post('simpan'))
        {
            $input = array(
                'id_provinsi' => null,
                'nama_provinsi' => $this->input->post('nama_provinsi')
            );
            $insert = $this->provinsi_m->insert('tbl_provinsi', $input);
            if ($insert)
                $data['alert'] = 'success';
            else
                $data['alert'] = 'failed';
        }
        $this->load->view('admin/provinsi_add_v', $data);
    }

    public function edit($id = '')
    {
        $data = array();
        $data['alert'] = '';
        if ($this->input->post('simpan'))
        {
            $input = array(
                'nama_provinsi' => $this->input->post('nama_provinsi')
            );
            $insert = $this->provinsi_m->update('tbl_provinsi', 'id_provinsi', $id, $input);
            if ($insert)
                $data['alert'] = 'success';
            else
                $data['alert'] = 'failed';
        }
        $data['data'] = $this->provinsi_m->get_single('tbl_provinsi', 'id_provinsi', $id);
        $this->load->view('admin/provinsi_edit_v', $data);
    }

    public function delete($id = '')
    {
        $del = $this->provinsi_m->delete('tbl_provinsi', 'id_provinsi', $id);
        if ($del)
            redirect(admin_url() . 'provinsi');
    }

}

/* End of file  */
/* Location: ./application/controllers/ */