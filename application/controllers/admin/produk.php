<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_m');
		$this->load->model('banner_m');
		$this->load->model('myproduct_m');
		$this->load->model('merchant_m');
        $this->load->library('auth');
        $this->auth->check();
	}
	
	public function index()
	{
		$data=array();
		$data['title']='Users';
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}

        if(isset($_GET['hal'])){$hal=$_GET['hal'];}
        else {$hal='';}

        $dataPerhalaman=10;
        ($hal=='')?$nohalaman = 1:$nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs( (int) $offset);
        $data['offset']=$offset;

        if(!isset($_GET['s'])){
            $jmldata=$this->produk_m->count_all_product();
            $data['paginator']=$this->produk_m->page($jmldata, $dataPerhalaman, $hal);
            $data['datas']=$this->produk_m->get_all_produk($dataPerhalaman, $off);
            $data['s']='';
            $data['cat']=null;
            $data['sal']='';
            $data['stat']='';
            $data['mer']='';
            $data['qty']='';
            $data['fr']='';
            $data['to']='';
            $data['fe']='';
            $data['pri']='';
            $data['loc']='';
        }else{
            $jmldata=$this->produk_m->count_all_product_search(
                $_GET['s'],
                $_GET['cat'],
                $_GET['sal'],
                $_GET['stat'],
                $_GET['mer'],
                $_GET['qty'],
                $_GET['fr'],
                $_GET['to'],
                $_GET['fe'],
                $_GET['pri'],
                strtoupper($_GET['loc']),
                $dataPerhalaman,
                $off
            );
            $data['paginator']=$this->produk_m->page($jmldata, $dataPerhalaman, $hal);
            $data['datas']=$this->produk_m->get_produk_by_search(
                $_GET['s'],
                $_GET['cat'],
                $_GET['sal'],
                $_GET['stat'],
                $_GET['mer'],
                $_GET['qty'],
                $_GET['fr'],
                $_GET['to'],
                $_GET['fe'],
                $_GET['pri'],
                strtoupper($_GET['loc']),
                $dataPerhalaman,
                $off
            );
            $data['s']=$_GET['s'];
            $data['cat']=$_GET['cat'];
            $data['sal']=$_GET['sal'];
            $data['stat']=$_GET['stat'];
            $data['mer']=$_GET['mer'];
            $data['qty']=$_GET['qty'];
            $data['fr']=$_GET['fr'];
            $data['to']=$_GET['to'];
            $data['fe']=$_GET['fe'];
            $data['pri']=$_GET['pri'];
            $data['loc']=$_GET['loc'];
        }
        $data['sales'] = $this->produk_m->getSales();
        $data['merchant'] = $this->produk_m->getMerchant();
		$this->load->view('admin/produk_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['success']='';
		$data['error']='';
		$data['title']='Add Produk';
		$data['kategori']=$this->myproduct_m->get_data_where('tbl_kategori', 'id_parent', '0');
		$data['page']=$this->produk_m->get_all_data('tbl_page', 'urutan', 'asc');
		$data['provinsi']=$this->produk_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');

		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_produk', 'Nama produk', 'required');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
			$this->form_validation->set_rules('berat', 'Berat', 'required');
			$this->form_validation->set_rules('diskon', 'Diskon', 'required');
			// $this->form_validation->set_rules('nama_tag', 'Tag', 'required');
			$this->form_validation->set_rules('stok_produk', 'Stok', 'required');
			$this->form_validation->set_rules('id_user', 'Merchant', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'required');
			$this->form_validation->set_rules('id_kategori', 'Kategori', 'required|callback__check_category');
			// $this->form_validation->set_rules('panjang', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('lebar', 'Lebar', 'numeric');
			// $this->form_validation->set_rules('tinggi', 'Tinggi', 'numeric');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'numeric');
            $this->form_validation->set_rules('photo_img', 'Foto Produk', 'callback__check_foto_produk');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
				$data['post'] = array(
					'nama_produk' => $this->input->post('nama_produk'),
					'pick' => $this->input->post('pick'),
					'unggulan' => $this->input->post('unggulan'),
					'deskripsi' => $this->input->post('deskripsi'),
					'berat' => $this->input->post('berat'),
					'stok_produk' => $this->input->post('stok_produk'),
					'harga_produk' => $this->input->post('harga_produk'),
					'harga_jual' => $this->input->post('harga_jual'),
					'diskon' => $this->input->post('diskon'),
					'panjang' => $this->input->post('panjang'),
					'lebar' => $this->input->post('lebar'),
					'tinggi' => $this->input->post('tinggi')					
				);
			}
		    else{
                        $volume = $this->input->post('panjang')*$this->input->post('lebar')*$this->input->post('tinggl');
                        $volume_kon = $volume/6000;
                        if($volume_kon > $this->input->post('berat'))
                            $jne_berat = $volume_kon;
                        else {
                            $jne_berat = $this->input->post('berat');
                        }

                $berat = str_replace(',', '.', $this->input->post('berat'));
                if($berat < 0){
                	$berat = 0;
                }
                $panjang = str_replace(',', '.', $this->input->post('panjang'));
                if($panjang < 0){
                	$panjang = 0;
                }
                $lebar = str_replace(',', '.', $this->input->post('lebar'));
                if($lebar < 0){
                	$lebar = 0;
                }
                $tinggi = str_replace(',', '.', $this->input->post('tinggi'));
                if($tinggi < 0){
                	$tinggi = 0;
                }
                $stok = $this->input->post('stok_produk');
                if($stok < 0){
                	$stok = 0;
                }

				$input=array(
					'id_produk' => null,
					'nama_produk' => $this->input->post('nama_produk'),
					'pick' => $this->input->post('pick'),
					'unggulan' => $this->input->post('unggulan'),
					'deskripsi' => $this->input->post('deskripsi'),
					'berat' => $berat,
					'stok_produk' => $stok,
					'harga_produk' => $this->input->post('harga_produk'),
					'harga_jual' => $this->input->post('harga_jual'),
					'diskon' => $this->input->post('diskon'),
					'publish' => 0,
					'id_user' => $this->input->post('id_user'),
					// 'id_kota' => $this->input->post('id_kota'),
					'panjang' => $panjang,
					'lebar' => $lebar,
					'tinggi' => $tinggi,
                                        'jne_berat' => $jne_berat,
					'date_added' => date('Y-m-d H:i:s'),
					'date_modified' => date('Y-m-d H:i:s')
				);
				//database transaction
				$this->db->trans_begin();
				$insert=$this->produk_m->insert('tbl_produk', $input);				
//				$inptag=array(
//					'nama_tag' => $this->input->post('nama_tag'),
//					'date_added' => date('Y-m-d H:i:s')
//				);
//		    	$id_tag=$this->produk_m->insert('tbl_tag', $inptag);
//		    	$prod_tag=$this->produk_m->insert('tbl_produk_tag', array('id_produk'=>$insert, 'id_tag'=>$id_tag, 'date_added'=>date('Y-m-d H:i:s')));
				
//                                start afandi
                                
                                if (!empty($_POST['nama_tag'])) {
                                $tag = explode(',', $_POST['nama_tag']);
                                foreach ($tag as $a){
                                    $b = trim($a);
                                    //select tag
                                    $select_tag = $this->db->query("select * from tbl_tag where nama_tag = '".$b."'");
                                    if ($select_tag->num_rows() > 0) {
                                        $result = $select_tag->row();
                                        $idtag = $result->id_tag;
                                    }else{
                                        $insert_tag = array('id_tag' => null,
                                            'nama_tag' => $b,
                                            'date_added' => date('Y-m-d H:i:s'),
                                            'date_modified' => date('Y-m-d H:i:s')
                                        );
                                        $idtag = $this->produk_m->insert('tbl_tag', $insert_tag);
                                    }
                                    //insert tag_relation
                                     $insert_relation = array('id_produk_tag' => null,
                                        'id_produk' => $insert,
                                        'id_tag' => $idtag,
                                        'date_added' => date('Y-m-d H:i:s'),
                                        'date_modified' => date('Y-m-d H:i:s')
                                    );
                                    $this->produk_m->insert('tbl_produk_tag', $insert_relation);
                                }
                            }
                           
//                                end afandi 
                                
                                if($insert){
                    if(!empty($_POST['photo_img'])){
                        foreach ($_POST['photo_img'] as $foto) {
                            if($foto!=''){
                                $inpdet=array(
                                    'id_produk' => $insert,
                                    'image'	 => $foto,
                                    'date_added' => date('Y-m-d H:i:s'),
                                    'date_modified' => date('Y-m-d H:i:s')
                                );
                                $produk_foto=$this->produk_m->insert('tbl_produkfoto', $inpdet);
                            }
                        }
                    }
					foreach ($_POST['id_kategori'] as $id) {
                        if($id!=''){
							$prod_kat=array('id_kategori'=>$id, 'id_produk'=>$insert, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
							$ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
						}
					}
				}
				if ($this->db->trans_status() === FALSE)
				{
				    $this->db->trans_rollback();
				    $data['error']='Produk gagal disimpan.';
				    
				}
				else
				{
				    $this->db->trans_commit();
				    $data['success']='Produk berhasil disimpan.';
				}
		    }
		}
		$data['member'] = $this->merchant_m->get_merchant(0,1000);
		// $data['member']=$this->produk_m->get_data_where('tbl_user', 'id_level', '6');
		$this->load->view('admin/produk_add_v', $data);
	}

	public function edit($id=''){
		$data=array();
		$data['success']='';
		$data['error']='';
		$data['title']='Edit Produk';		
		$this->produk_m->update('tbl_produk', 'id_produk', $id, array('view_notif'=>'Y'));
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_produk', 'Nama produk', 'required');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
			$this->form_validation->set_rules('berat', 'Berat', 'required');
			$this->form_validation->set_rules('diskon', 'Diskon', 'required');
			// $this->form_validation->set_rules('nama_tag', 'Tag', 'required');
			$this->form_validation->set_rules('stok_produk', 'Stok', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'required');
			$this->form_validation->set_rules('id_kategori', 'Kategori', 'required');
			// $this->form_validation->set_rules('panjang', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('lebar', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('tinggi', 'Panjang', 'numeric');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'numeric');
			$this->form_validation->set_rules('photo_img', 'Foto Produk', 'callback__check_foto_produk');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
                        $volume = $this->input->post('panjang')*$this->input->post('lebar')*$this->input->post('tinggl');
                        $volume_kon = $volume/6000;
                        if($volume_kon > $this->input->post('berat'))
                            $jne_berat = $volume_kon;
                        else {
                            $jne_berat = $this->input->post('berat');
                        }

                $berat = str_replace(',', '.', $this->input->post('berat'));
                if($berat < 0){
                	$berat = 0;
                }
                $panjang = str_replace(',', '.', $this->input->post('panjang'));
                if($panjang < 0){
                	$panjang = 0;
                }
                $lebar = str_replace(',', '.', $this->input->post('lebar'));
                if($lebar < 0){
                	$lebar = 0;
                }
                $tinggi = str_replace(',', '.', $this->input->post('tinggi'));
                if($tinggi < 0){
                	$tinggi = 0;
                }
                $stok = $this->input->post('stok_produk');
                if($stok < 0){
                	$stok = 0;
                }

				$input=array(
					'nama_produk' => $this->input->post('nama_produk'),
					'deskripsi' => $this->input->post('deskripsi'),
					'berat' => $berat,
					'stok_produk' => $stok,
					'harga_produk' => $this->input->post('harga_produk'),
					'harga_jual' => $this->input->post('harga_jual'),
					'diskon' => $this->input->post('diskon'),
					'publish' => $this->input->post('publish'),
					'pick' => $this->input->post('pick'),
					'unggulan' => $this->input->post('unggulan'),
					'panjang' => $panjang,
					'lebar' => $lebar,
					'tinggi' => $tinggi,
                    'jne_berat' => $jne_berat,
					'date_modified' => date('Y-m-d H:i:s')
				);

				//database transaction
				$this->db->trans_begin();
				$insert=$this->produk_m->update('tbl_produk', 'id_produk', $id, $input);

				// $insert = $this->produk_m->update('tbl_produk', 'id_produk', $id, $input);
                
                //# if verified, send email to merchant
                if( ($this->input->post('publish')==1) && ($this->input->post('publish')!=$this->input->post('curr_publish')) ){
                    $this->load->library('lib_mailer');
                    
                    $produk     = $this->produk_m->get_single('tbl_produk','id_produk',$id);
                    $merchant   = $this->produk_m->get_single('tbl_user','id_user',$this->input->post('id_user'));
                    
                    $mail_data  = array('produk' => $produk,'merchant' => $merchant);
                    $message    = $this->load->view('email/admin_verify_produk_v',$mail_data,TRUE);
                    $mailer     = array('module'    => 'Produk Verified',
                                        'from'      => $this->config->item('email_from'),
                                        'to'        => $merchant->email,
                                        'subject'   => "Produk telah verifikasi [".$this->input->post('nama_produk')."]",
                                        'message'   => $message
                                        );
                    $this->lib_mailer->save($mailer);
                }

                                
                                
                                $this->produk_m->clear_tag($id);
                                
                                if (!empty($_POST['nama_tag'])) {
                                $tag = explode(',', $_POST['nama_tag']);
                                foreach ($tag as $a){
                                    $b = trim($a);
                                    //select tag
                                    $select_tag = $this->db->query("select * from tbl_tag where nama_tag = '".$b."'");
                                    if ($select_tag->num_rows() > 0) {
                                        $result = $select_tag->row();
                                        $idtag = $result->id_tag;
                                    }else{
                                        $insert_tag = array('id_tag' => null,
                                            'nama_tag' => $b,
                                            'date_added' => date('Y-m-d H:i:s'),
                                            'date_modified' => date('Y-m-d H:i:s')
                                        );
                                        $idtag = $this->produk_m->insert('tbl_tag', $insert_tag);
                                    }
                                    //insert tag_relation
                                     $insert_relation = array('id_produk_tag' => null,
                                        'id_produk' => $id,
                                        'id_tag' => $idtag,
                                        'date_added' => date('Y-m-d H:i:s'),
                                        'date_modified' => date('Y-m-d H:i:s')
                                    );
                                    $this->produk_m->insert('tbl_produk_tag', $insert_relation);
                                }
                            }
                            
				if($insert){
                    if(!empty($_POST['photo_img'])){
                        foreach ($_POST['photo_img'] as $foto) {
                            $cek_foto=$this->myproduct_m->cek_foto($foto);
                            if($cek_foto<1){
                                $inpdet=array(
                                    'id_produk' => $id,
                                    'image'	 => $foto,
                                    'date_added' => date('Y-m-d H:i:s'),
                                    'date_modified' => date('Y-m-d H:i:s')
                                );
                                $produk_foto=$this->produk_m->insert('tbl_produkfoto', $inpdet);
                            }
                        }
                    }
					if($_POST['id_kategori'][0]!=''){
						$this->myproduct_m->delete_kategori(md5($id));
						$idp=$id;
						foreach ($_POST['id_kategori'] as $id_kat) {
							if($id_kat!=''){
								$prod_kat=array('id_kategori'=>$id_kat, 'id_produk'=>$idp, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
								$ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
							}
						}
					}
					// if(isset($_POST['id_kota'])){
					// 	$this->produk_m->update('tbl_produk', 'id_produk', $id, array('id_kota'=> $_POST['id_kota']));
					// }
				}
				if ($this->db->trans_status() === FALSE)
				{
				    $this->db->trans_rollback();
				    $data['error']='Produk gagal disimpan.';
				    
				}
				else
				{
				    $this->db->trans_commit();
				    $data['success']='Perubahan data berhasil disimpan.';
				}
		    }
		}
		$data['produk']=$this->produk_m->get_produk($id);

		// print_r($data['produk']);exit;
		$data['fotos']=$this->produk_m->get_foto($id);
		$data['tags']=$this->produk_m->get_tag($id);
		$data['count_tag']=$this->produk_m->count_tag($id);
		$data['provinsi']=$this->produk_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
		$data['kategoris']=$this->myproduct_m->get_data_where('tbl_kategori', 'id_parent', '0');
		$data['kategori']=$this->myproduct_m->get_kategori(md5($id));
		$this->load->view('admin/produk_edit_v', $data);
	}

	public function delete($id='')
	{
		// $foto=$this->produk_m->get_data_where('tbl_produkfoto', 'id_produk', $id);

		// foreach($foto as $row){
		// 	$post = $row->image;
		// 	$this->produk_m->delete('tbl_produkfoto', 'image', $post);
		// 	$len = strlen(base_url());
		// 	$new_path = substr($post, $len, strlen($post)-$len);		
		// 	$del = unlink($new_path);			
		// }

		// $del=$this->produk_m->delete('tbl_produk', 'id_produk', $id);
		$del=$this->produk_m->update('tbl_produk', 'id_produk', $id, array('deleted'=>1));
		if($del) redirect(admin_url().'produk/?alert=success');		
	}
        
        public function update_jne_berat(){
            $data=$this->produk_m->get_all_produk();
            
            foreach ($data as $value) {
                $volume = $value->panjang*$value->lebar*$value->tinggi;
                $volume_kon = $volume/6000;
                if($volume_kon > $value->berat)
                    $jne_berat = $volume_kon;
                else {
                    $jne_berat = $value->berat;
                }
//                echo $value->id_produk." ".$jne_berat.'<br>';
                        $input=array(
                                'jne_berat' => $jne_berat,
                        );
                        $insert=$this->produk_m->update('tbl_produk', 'id_produk', $value->id_produk, $input);
            }
            
        }
        
    function _check_category()
    {
        if($_POST['id_kategori'][0]!=''){            
            return TRUE;
        }else{
            $this->form_validation->set_message('_check_category', 'Pilih salah satu Kategori');
            return FALSE;
        }
    }
    
    function _check_foto_produk()
    {
        if(!empty($_POST['photo_img'])){            
            return TRUE;
        }else{
            $this->form_validation->set_message('_check_foto_produk', 'Upload minimal 1 foto produk');
            return FALSE;
        }
    }

    public function setting(){
    	$data=array();
    	$data['title']='setting produk';
    	$data['success']='';
    	$data['error']='';

        if($this->input->post('simpan')){

        	if($_FILES['watermark']['name']!=''){
        		if(file_exists('./asset/img/watermark.png')){
        			unlink('./asset/img/watermark.png');
        		}
        	$config['upload_path'] = './asset/img/';
			$config['allowed_types'] = 'png';
			$config['file_name']	= 'watermark';
			$config['max_size']	= '2000';
			$config['max_width']  = '2000';
			$config['max_height']  = '2000';
			$this->load->library('upload', $config);

			if (! $this->upload->do_upload('watermark')){
				$data['error']=$this->upload->display_errors();
			} 

            // $insert = array(
            //     'option_name'   => 'watermark',
            //     'option_value'  => $watermark;
            // );

            // $cek=$this->banner_m->cek_option($insert['option_name']);

            // if($cek==0){
            //     $this->banner_m->insert('tbl_options',$insert);
            // } else {
            //     $this->banner_m->update('tbl_options', 'option_name', $insert['option_name'], $insert);
            // }
				else{
            	$data['success']='Data berhasil disimpan.';
        		}
        	}
        }

    	$this->load->view('admin/produk_setting_v', $data);
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
