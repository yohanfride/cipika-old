<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Level extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('level_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		$data['alert']='';
		$data['title']='level';
		$data['datas']=$this->level_m->get_all_data('tbl_level', 'id_level', 'asc');
		$this->load->view('admin/level_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'id_level' => null,
				'nama_level' => $this->input->post('nama_level')
			);
			$insert=$this->level_m->insert('tbl_level', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$this->load->view('admin/level_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'nama_level' => $this->input->post('nama_level')
			);
			$insert=$this->level_m->update('tbl_level', 'id_level', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['data']=$this->level_m->get_single('tbl_level', 'id_level', $id);
		$this->load->view('admin/level_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->level_m->delete('tbl_level', 'id_level', $id);
		if($del) redirect(admin_url().'level');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */