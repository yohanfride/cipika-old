<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_m');
        $this->load->model('order_m');
        $this->load->library('auth');
        $this->auth->check();
    }

    public function index()
    {
        $data = array();
        $data['alert'] = '';
        $data['title'] = 'Payment';
        $data['datas'] = $this->payment_m->get_all_data('tbl_payment', 'id_payment', 'asc');
        $data['option'] = $this->payment_m->get_option('cost_payment', '5000');
        $this->load->view('admin/payment_v', $data);
    }

    public function add()
    {
        $data = array();
        $data['alert'] = '';
        $data['title'] = 'Payment';
        if ($this->input->post('simpan'))
        {
            $input = array(
                'id_payment' => null,
                'nama_payment' => $this->input->post('nama_payment')
            );
            $insert = $this->payment_m->insert('tbl_payment', $input);
            if ($insert)
                $data['alert'] = 'success';
            else
                $data['alert'] = 'failed';
        }
        $this->load->view('admin/payment_add_v', $data);
    }

    public function edit($id = '')
    {
        $data = array();
        $data['alert'] = '';
        $data['title'] = 'Payment';
        if ($this->input->post('simpan'))
        {
            $input = array(
                'nama_payment' => $this->input->post('nama_payment')
            );
            $insert = $this->payment_m->update('tbl_payment', 'id_payment', $id, $input);
            if ($insert)
                $data['alert'] = 'success';
            else
                $data['alert'] = 'failed';
        }
        $data['data'] = $this->payment_m->get_single('tbl_payment', 'id_payment', $id);
        $this->load->view('admin/payment_edit_v', $data);
    }

    public function delete($id = '')
    {
        $del = $this->payment_m->delete('tbl_payment', 'id_payment', $id);
        if ($del)
            redirect(admin_url() . 'payment');
    }

    public function update_option()
    {
        $value = $_POST['cost_payment'];
        $id = $_POST['id_option'];

        $data = array();
        $data['alert'] = '';
        $data['title'] = 'Payment';
        $data['datas'] = $this->payment_m->get_all_data('tbl_payment', 'id_payment', 'asc');
        $data['option'] = $this->payment_m->get_option('cost_payment', $value, $id);
        $this->load->view('admin/payment_v', $data);
    }

    public function all_payment()
    {
        $data = array();
        $data['alert'] = '';
        $data['title'] = ' All Payment';
        if (isset($_GET['search']))
            $search = $_GET['search'];
        else
            $search = NULL;
        
        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 20;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);

        $jmldata = $this->payment_m->count_all_data('tbl_payment_buyers');
        $data['paginator'] = $this->payment_m->page($jmldata, $dataPerhalaman, $hal);

        $data['datas'] = $this->payment_m->get_payment_buyer($dataPerhalaman, $off, $search);
        
        $this->load->view('admin/payment_all_v', $data);
    }

    public function release_payment($id)
    {
        $user = $this->payment_m->get_single("tbl_payment_buyers", 'md5(id_payments)', $id);
        $order = $this->order_m->get_single('tbl_order', 'kode_order', $user->kode_order);

        if (count($order) == 1)
        {
            $update = $this->order_m->update('tbl_order', 'id_order', $order->id_order, array('status_payment' => 'Paid'));
        }
        redirect(admin_url() . 'payment/all_payment');
    }

    public function release_all()
    {
        $val = $_POST['check'];
        $act = $_POST['action'];

        if ($act == "release")
        {
            foreach ($val as $v)
            {
                $user = $this->payment_m->get_single("tbl_payment_buyers", 'id_payments', $v);
                $order = $this->order_m->get_single('tbl_order', 'kode_order', $user->kode_order);
                if ($order)
                    $update = $this->order_m->update('tbl_order', 'id_order', $order->id_order, array('status_payment' => 'Paid'));
            }
        }
        else
        {
            foreach ($val as $v)
            {
                $user = $this->payment_m->get_single("tbl_payment_buyers", 'id_payments', $v);
                if ($user)
                    $update = $this->payment_m->delete('tbl_payment_buyers', 'id_payments', $v);
            }
        }
        redirect(admin_url() . 'payment/all_payment');
    }

}

/* End of file  */
/* Location: ./application/controllers/ */