<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_m');
        $this->load->model('payment_m');
        $this->load->model('produk_m');
        $this->load->model('merchant_m');
        $this->load->library('auth');
        $this->auth->check();
    }

    public function index()
    {
        $data = array();
        $data['alert'] = '';
        $data['title'] = 'order';
        if(isset($_GET['hal'])){$hal=$_GET['hal'];}
        else {$hal='';}

        $dataPerhalaman=5;
        ($hal=='')?$nohalaman = 1:$nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs( (int) $offset);
        $data['offset']=$offset;

        if(!isset($_GET['s'])){
            $jmldata=$this->order_m->count_all_order();
            $data['paginator'] = $this->order_m->page($jmldata, $dataPerhalaman, $hal);
            $data['datas'] = $this->order_m->get_order_all($dataPerhalaman, $off);
            $data['s']='';
            $data['sal']='';
            $data['mer']='';
            $data['fr']='';
            $data['to']='';
            $data['loc_mer']='';
            $data['loc_buy']='';
            $data['shi']='';
            $data['pay']='';
        }else{
            $jmldata=$this->order_m->count_all_order_search(
                $_GET['s'],
                $_GET['sal'],
                $_GET['mer'],
                $_GET['fr'],
                $_GET['to'],
                strtoupper($_GET['loc_mer']),
                strtoupper($_GET['loc_buy']),
                $_GET['shi'],
                $_GET['pay'],
                $dataPerhalaman,
                $off
            );
            $data['paginator']=$this->produk_m->page($jmldata, $dataPerhalaman, $hal);
            $data['datas']=$this->order_m->get_order_by_search(
                $_GET['s'],
                $_GET['sal'],
                $_GET['mer'],
                $_GET['fr'],
                $_GET['to'],
                strtoupper($_GET['loc_mer']),
                strtoupper($_GET['loc_buy']),
                $_GET['shi'],
                $_GET['pay'],
                $dataPerhalaman,
                $off
            );
            $data['s'] = $_GET['s'];
            $data['sal'] = $_GET['sal'];
            $data['mer'] = $_GET['mer'];
            $data['fr'] = $_GET['fr'];
            $data['to'] = $_GET['to'];
            $data['loc_mer'] = $_GET['loc_mer'];
            $data['loc_buy'] = $_GET['loc_buy'];
            $data['shi'] = $_GET['shi'];
            $data['pay'] = $_GET['pay'];
        }
        $data['sales'] = $this->produk_m->getSales();
        $data['merchant'] = $this->produk_m->getMerchant();
        foreach ($data['datas'] as $v)
        {
            $inv = $this->order_m->get_single("tbl_order", "kode_order", $v->kode_order);
            $data['invoices'][$v->kode_order] = $inv->kode_invoice;
        }
        $this->load->view('admin/order_v', $data);
    }

    public function detail($id = '')
    {
        $this->load->library('lib_lokasi');
    
        $data = array();
        $data['title'] = 'order';
        $data['alert'] = '';
        $this->order_m->update('tbl_order', 'id_order', $id, array('view_notif' => 'Y'));
        
        $data['order']      = $this->order_m->get_single('tbl_order','id_order', $id);
        $data['shipping']   = $this->order_m->get_order_shipping($data['order']->id_order);
        $data['payment']    = $this->order_m->get_payment_detail($data['order']->kode_order);
        $data['merchant']   = $this->commonlib->get_seller($data['order']->id_merchant);
        $data['buyer']      = $this->order_m->get_single('tbl_user','id_user', $data['order']->id_user);
        $data['datas']      = $this->order_m->get_order_item('a.id_order', $id);
        
        foreach ($data['datas'] as $v)
        {
            $data['prod'][$v->id_produk] = $this->produk_m->get_single("tbl_produk", "id_produk", $v->id_produk);
        }
        $this->load->view('admin/order_detail_v', $data);
    }

    public function edit($id = '')
    {
        $data = array();
        $data['title']      = 'order';
        $data['success']    = '';
        $data['error']      = '';
        $data['mode']       = $this->uri->segment(5,'shipping');
        $data['data'] = $this->order_m->get_single('tbl_order', 'id_order', $id);
        $data['invoice'] = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $data['data']->kode_invoice);
        
        $data['act']      = 0;
        if ($data['data']->status_payment == 'paid')
        {
            $data['act'] = 1;
        }
        if ($this->input->post('simpan'))
        {
            $mode = $this->input->post('mode');
            if ($mode == "payment")
            {
                $sending="";
                $sending_to_buyer="";
                $payment = $this->input->post('status_payment');
                if ($payment == 'paid')
                {
                    $data['act'] = 1;
                }
                $input = array(
                    'status_payment' => $this->input->post('status_payment'),
                );
//                $update = $this->order_m->update('tbl_order', 'id_order', $id, $input);
                $update_invoice = $this->order_m->update('tbl_invoices', 'kode_invoice', $data['data']->kode_invoice, array('status_payment' => $this->input->post('status_payment')));
                $update_order = $this->order_m->update('tbl_order', 'kode_invoice', $data['data']->kode_invoice, array('status_payment' => $this->input->post('status_payment')));
                if ($update_invoice || $update_order)
                {
                    $data['success'] = 'Berhasil Update';
                    $data['mode'] = $this->input->post('mode');
                }
                else
                {
                    $data['success'] = 'Tidak Berhasil Update';
                    $data['mode'] = $this->input->post('mode');
                }
                if($this->input->post('status_payment')!='paid'){
                    $data['act'] = 0;
                }
                $data['data'] = $this->order_m->get_single('tbl_order', 'id_order', $id);
            }
            else
            {
                $input = array(
                    'status_delivery' => $this->input->post('status_delivery'),
                    'ongkir_merchant' => $this->input->post('ongkir_merchant'),
                    'noresi' => $this->input->post('noresi'),
                    'paket_ongkir_merchant' => $this->input->post('paket_ongkir_merchant'),
                    'delivery_date' => $this->input->post('delivery_date'),
                );
                $insert = $this->order_m->update('tbl_order', 'id_order', $id, $input);
                if ($insert)
                    $data['success'] = 'Data berhasil disimpan.';
                else
                    $data['success'] = 'Tidak ada data yang diubah.';

                $data['out'] = $input;
                $data['mode'] = $this->input->post('mode');
            }
        }
        $this->load->database();
        if ($this->input->post('send_po'))
        {
            $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $data['data']->kode_invoice);
            foreach ($orderdetail as $v)
            {
                if ($id == $v->id_order)
                {
                    $nama_store = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                    $email = $this->cart_m->get_merchant($v->id_merchant)->email;
                    $order_item = $this->cart_m->get_order_item(md5($v->id_order));
                    $shipping = $this->cart_m->get_order_shipping($v->id_order);

                    $inputan_shipping[$v->id_order] = $this->cart_m->get_order_shipping($v->id_order);
                    $inputan_merchant[$v->id_merchant] = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                    $order_items[$v->id_order] = $this->cart_m->get_order_item(md5($v->id_order));

                    $send = new \Cipika\View\Helper\EmailPoMerchant($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
                    $sending = $send->sends($email, $v, $shipping, $nama_store, $order_item, $this->config->slash_item('index_page'));
                    $order_det[$v->id_order] = $v;
                }
            }
            $data['success'] = "PO berhasil terkirim ke Merchant";
            $data['mode'] = $this->input->post('mode');
            $data['act'] = 1;
        }
        if ($this->input->post('send_confirm'))
        {
            $this->load->database();
            $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $data['data']->kode_invoice);
            foreach ($orderdetail as $v)
            {
                $inputan_shipping[$v->id_order] = $this->cart_m->get_order_shipping($v->id_order);
                $inputan_merchant[$v->id_merchant] = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                $order_items[$v->id_order] = $this->cart_m->get_order_item(md5($v->id_order));
            }
            
            $nama_payment = $this->payment_m->get_single('tbl_payment', 'id_payment', $data['invoice']->id_payment);
            $user = $this->payment_m->get_single('tbl_user', 'id_user', $data['invoice']->id_user);

            $send_to_buyer = new \Cipika\View\Helper\EmailBuyerPembayaran($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
            $sending_to_buyer = $send_to_buyer->sends($user->email, $data['invoice'], $orderdetail, $inputan_shipping, $inputan_merchant, $order_items, $this->config->slash_item('index_page'), $nama_payment, $user);
            
            $data['success'] = 'Payment Thank You berhasil terkirim.';
            $data['mode'] = $this->input->post('mode');
            $data['act'] = 1;
        }

        $data['order'] = $this->merchant_m->get_order($id);
        $this->load->view('admin/order_edit_v', $data);
    }

    public function delete($id = '')
    {
        $del = $this->order_m->delete('tbl_order', 'id_order', $id);
        if ($del)
            redirect(admin_url() . 'order');
    }

    public function detail_payment($id)
    {
        if (!isset($id))
            redirect(admin_url() . 'order');
        
        $data = array();
        
        
        $data['title'] = 'Order Payment Detail';
        $data['alert'] = '';
        $data['order'] = $this->order_m->get_single("tbl_order", "id_order", $id);
//        $data['payment']= $this->order_m->get_payment("tbl_payment_buyers", "kode_order",$data['order']->kode_invoice);
        //$data['payment']= $this->payment_m->get_payment($data['order']->kode_invoice,$data['order']->id_payment);
        $data['payment']= $this->order_m->get_payment_detail($data['order']->kode_order);
        $this->load->view('admin/order_detail_payment_v', $data);
    }

    
    public function detail_delivery($id)
    {
        $data['title']      = 'Status Delivery Detail';
        $data['alert']      = '';
        $data['order']      = $this->order_m->get_single("tbl_order", "id_order", $id);
        
        $this->load->view('admin/status_delivery_detail_v', $data);        
    }
    
    public function detail_invoice($kode_invoice)
    {
        $this->load->model('payment_m');
        $this->load->library('lib_lokasi');
    
        $data['title']          = 'Invoice Detail';
        $data['alert']          = '';
        
        $data['invoice']        = $this->order_m->get_single('tbl_invoices','kode_invoice',$kode_invoice);
        $data['buyer']          = $this->order_m->get_single('tbl_user','id_user',$data['invoice']->id_user);
        $data['orders']         = $this->order_m->get_order_by_invoice($kode_invoice);
        $data['payment']        = $this->payment_m->get_payment_single($data['invoice']->kode_invoice, $data['invoice']->id_payment);        
        $data['nilai_admin']    = $this->payment_m->get_option('cost_payment');
        
        $this->load->view('admin/invoice_detail_v', $data);        
    }
    
    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }
}

/* End of file  */
/* Location: ./application/controllers/ */
