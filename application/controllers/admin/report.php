<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller 
{

	public function __construct() {

        parent::__construct();
        $this->load->library('auth');        
        $this->load->model('report_m');        
        $this->load->model('user_m');        
        $this->load->model('meta_m');        
        $this->auth->check();
    }
    
    /* Transaction report */
	public function transaction()
	{   
        $data['title']  = 'Transaction Report';
		
        $data['statuses']   = $this->report_m->get_payment_delivery_statuses();
        $data['result']     = $this->report_m->get_orders($this->input->post('period_start'),$this->input->post('period_end'),$this->input->post('status_delivery'),$this->input->post('status_payment'),0,100);
            
		$this->load->view('admin/report/transaction_v', $data);        
	}
    
    /* Price Comparison report */
	public function price_comparison()
	{   
        $this->load->model('kategori_m');
        $this->load->library('lib_lokasi');
        $data['title']  = 'Price Comparison Report';
		
        //$data['kota']       = $this->report_m->get_all_data_order('tbl_kota','nama_kota','asc');
        $data['kecamatan']  = $this->lib_lokasi->get_kecamatan();
        $data['kategori']   = $this->report_m->list_category('0');         //# parent category
        $data['result']     = $this->report_m->get_price_comparison($this->input->post('id_kategori'),$this->input->post('id_kecamatan'),0,200);
        
        $data['id_kecamatan']  = $this->input->post('id_kecamatan');
        $data['id_kategori']   = $this->input->post('id_kategori');
            
		$this->load->view('admin/report/price_comparison_v', $data);        
	}
    
    /* Merchant/User status report */
	public function merchant_status()
	{
        $this->load->library('lib_lokasi');        
        $this->load->model('merchant_m');        
        $data['title']      = 'Merchant Report';
		
        $data['sales']          = $this->merchant_m->get_sales();
        $data['sales_selected'] = $this->input->post('sales','');
        $data['status']         = $this->input->post('status','');
        $data['result']         = $this->report_m->get_merchant($data['status'],$data['sales_selected'],0,200);
            
		$this->load->view('admin/report/merchant_status_v', $data);        
	}
    
    /* Product Report */

    public function product()
    {
        $data['title'] = 'Product Report';

        $data['most_loved'] = $this->report_m->get_price_comparison('', '', 0, 5, 'loved', 'desc');

        $data['most_viewed'] = $this->report_m->get_price_comparison('', '', 0, 5, 'viewed', 'desc');
        $data['most_sale'] = $this->report_m->get_price_comparison('', '', 0, 5, 'terjual', 'desc');

        $this->load->view('admin/report/product_v', $data);
    }

    /* Revenue Report */
	public function revenue()
	{   
        $this->load->library('lib_lokasi');
        $data['title']          = 'Revenue Report';
		
        $data['period_start']   = $this->input->post('period_start');
        $data['period_end']     = $this->input->post('period_end');
        $data['status']         = $this->report_m->get_payment_delivery_statuses();
        $data['produk_item']    = $this->report_m->get_group_product_item($data['period_start'],$data['period_end']);
        $data['kota_item']      = $this->report_m->get_group_city_orders($data['period_start'],$data['period_end']);
        
		$this->load->view('admin/report/revenue_v', $data);        
	}
    
    /* Payment Report */
	public function payment()
	{   
        $data['title']          = 'Payment Report';
        
        $time_from  = strftime("%Y-%m-%d",strtotime($this->input->post('period_start')));
        $time_to    = strftime("%Y-%m-%d",strtotime($this->input->post('period_end')));
        $check      = $this->input->post('send');
        $emails     = $this->input->post('email');
		
        $data['result']         = $this->report_m->get_payment_buyer($time_from, $time_to, $this->input->post('status_payment'));
        
        $date_from = date_create($time_from);
        $date_to = date_create($time_to);
        
        if(!empty($check)&&!empty($emails)){
            $email = '<style>* {font-family:Arial, serif;font-size:12px} table tr td {padding:2px 5px;} tr.judul td {font-weight:bold;}</style>';
                $email .='<p>Hallo <br /><br />
					Berikut ini laporan pembayaran mulai tanggal '.date_format($date_from, 'd M Y').' sampai tanggal '.date_format($date_to, 'd M Y').'';
                $email .= '<table style="border-collapse:collapse; width:650px;">
				<tr style="background:#666666; color:white; text-align:center" class="judul">
                <td><div align="center">No</div></td><td><div align="center">Kode Order</div></td><td><div align="center">Buyer</div></td>
                <td><div align="center">Email</div></td><td><div align="center">Jumlah</div></td><td><div align="center">Status</div></td></tr>';
                $i = 1;
                foreach ($data['result'] as $items)
                {
                    echo $items->kode_order;
                    $email .= '<tr><td><div align="center">' . $i . '</div></td> 
						<td><div align="center">'.$items->kode_order.'</div></a></td>
						<td><div align="center">' . $items->username . '</div></strong></td>
						<td><div align="center">' . $items->email . '</div></td>
						<td><div align="center">' . $items->amount . '</div></td>						
						<td><div align="center">' . $items->status_payment . '</div></td>
					</tr>';
                    $i++;
                }
                $email.='</table><br><br>';
                
                $email .= '
					<div style="margin:10px 0 0;"><p>Jangan dibalas ( reply ) email ini karena tidak akan dibaca. email ini hanya untuk mengirim laporan pembayaran.<br/>
					Jika ada yang mau disampaikan hubungi Customer Service kami via YM, Telp / SMS pada hari &amp; jam Kerja.</p>
					<br/><p>Terima Kasih,<br/><br/><br/>
					<strong>Cipika Store &trade;</strong><br>
					<a href="' . base_url() . '">www.cipikastore.com</a>
					</p>
					</div>
					';
            $config = $this->_mail_win();
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('admin@cipikastore.com', 'Cipika Store');
            $this->email->to($emails);
            $this->email->subject('Laporan Pembayaran');
            $this->email->message($email);
            $send = $this->email->send();
        }
        $this->load->view('admin/report/payment_v', $data);
    }
        
    /*add function send email - ALIE*/    
    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }
}