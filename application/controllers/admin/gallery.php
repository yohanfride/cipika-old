<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function index()
	{
		$data=array();
		$this->load->view('admin/gallery_v', $data);
        $this->load->library('auth');
        $this->auth->check();
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */