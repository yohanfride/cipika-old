<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kategori_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}
		
		$data['title']='kategori';
		$data['datas']=$this->kategori_m->get_list_kategori();
		$this->load->view('admin/kategori_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';
		$data['title']='kategori';
		if($this->input->post('simpan')){
			$input=array(
				'id_kategori' => null,
				'nama_kategori' => $this->input->post('nama_kategori'),
				'keterangan' => $this->input->post('keterangan'),
				'id_parent' => $this->input->post('id_parent')
			);
			$insert=$this->kategori_m->insert('tbl_kategori', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['kategori']=$this->kategori_m->get_all_data('tbl_kategori', 'id_kategori', 'asc');
		$this->load->view('admin/kategori_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		$data['title']='kategori';
		if($this->input->post('simpan')){
			$input=array(
				'nama_kategori' => $this->input->post('nama_kategori'),
				'keterangan' => $this->input->post('keterangan'),
				'id_parent' => $this->input->post('id_parent')
			);
			$insert=$this->kategori_m->update('tbl_kategori', 'id_kategori', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['kategori']=$this->kategori_m->get_all_data('tbl_kategori', 'id_kategori', 'asc');
		$data['data']=$this->kategori_m->get_single('tbl_kategori', 'id_kategori', $id);
		$this->load->view('admin/kategori_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->kategori_m->delete('tbl_kategori', 'id_kategori', $id);
		if($del) redirect(admin_url().'kategori/?alert=success');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */