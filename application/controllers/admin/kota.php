<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kota extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kota_m');
		$data['title']='kota';
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		$data['alert']='';
		$data['title']='kota';
		$data['datas']=$this->kota_m->get_data_join('tbl_kota','tbl_provinsi','id_provinsi');
		$this->load->view('admin/kota_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['title']='add kota';
		$data['alert']='';
		$data['provinsi']=$this->kota_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
		if($this->input->post('simpan')){
			$input=array(
				'id_kota' => null,
				'nama_kota' => $this->input->post('nama_kota'),
				'id_provinsi' => $this->input->post('id_provinsi')
			);
			$insert=$this->kota_m->insert('tbl_kota', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$this->load->view('admin/kota_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['title']='edit kota';
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'nama_kota' => $this->input->post('nama_kota'),				
				'id_provinsi' => $this->input->post('id_provinsi')
			);
			$insert=$this->kota_m->update('tbl_kota', 'id_kota', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['data']=$this->kota_m->get_single('tbl_kota', 'id_kota', $id);
		$data['provinsi']=$this->kota_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
		$this->load->view('admin/kota_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->kota_m->delete('tbl_kota', 'id_kota', $id);
		if($del) redirect(admin_url().'kota');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */