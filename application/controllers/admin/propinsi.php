<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Propinsi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('propinsi_m');
        $this->load->library('auth');
        $this->auth->check();
    }
    
    public function index()
    {
        $data = array();
        if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
        else{$data['alert']='';}
        
        $data['title'] = 'propinsi';
        $data['datas'] = $this->propinsi_m->get_all_data('tbl_propinsi', 'id_propinsi', 'asc');
        $this->load->view('admin/propinsi_v', $data);
    }

    public function add()
    {
        $data = array();
        $data['alert'] = '';
        if ($this->input->post('simpan'))
        {
            $input = array(
                'id_propinsi' => null,
                'nama_propinsi' => $this->input->post('nama_propinsi')
            );
            $insert = $this->propinsi_m->insert('tbl_propinsi', $input);
            if ($insert)
                $data['alert'] = 'success';
            else
                $data['alert'] = 'failed';
        }
        $this->load->view('admin/propinsi_add_v', $data);
    }

    public function edit($id = '')
    {
        $data = array();
        $data['alert'] = '';
        if ($this->input->post('simpan'))
        {
            $input = array(
                'nama_propinsi' => $this->input->post('nama_propinsi')
            );
            $insert = $this->propinsi_m->update('tbl_propinsi', 'id_propinsi', $id, $input);
            if ($insert)
                $data['alert'] = 'success';
            else
                $data['alert'] = 'failed';
        }
        $data['data'] = $this->propinsi_m->get_single('tbl_propinsi', 'id_propinsi', $id);
        $this->load->view('admin/propinsi_edit_v', $data);
    }

    public function delete($id = '')
    {
        $del = $this->propinsi_m->delete('tbl_propinsi', 'id_propinsi', $id);
        if ($del)
            redirect(admin_url() . 'propinsi?alert=success');
    }

}

/* End of file  */
/* Location: ./application/controllers/ */