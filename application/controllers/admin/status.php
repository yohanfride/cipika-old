<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('status_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}
		$data['title']='status';
		$data['datas']=$this->status_m->get_all_data('tbl_status', 'id_status', 'asc');
		$this->load->view('admin/status_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'id_status' => null,
				'nama_status' => $this->input->post('nama_status')
			);
			$insert=$this->status_m->insert('tbl_status', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$this->load->view('admin/status_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'nama_status' => $this->input->post('nama_status')
			);
			$insert=$this->status_m->update('tbl_status', 'id_status', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['data']=$this->status_m->get_single('tbl_status', 'id_status', $id);
		$this->load->view('admin/status_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->status_m->delete('tbl_status', 'id_status', $id);
		if($del) redirect(admin_url().'status/?alert=success');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */