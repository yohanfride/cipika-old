<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ongkir extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ongkir_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		$data['alert']='';
		$data['title']='ongkir';
		$data['propinsi']=$this->ongkir_m->get_propinsi();
		$this->load->view('admin/ongkir/ongkir_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';
		$data['title']='ongkir';
		if($this->input->post('simpan')){
			$input=array(
				'id_ongkir' => null,
				'nama_ongkir' => $this->input->post('nama_ongkir'),
				'keterangan' => $this->input->post('keterangan'),
				'id_parent' => $this->input->post('id_parent')
			);
			$insert=$this->ongkir_m->insert('tbl_ongkir', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['ongkir']=$this->ongkir_m->get_all_data('tbl_ongkir', 'id_ongkir', 'asc');
		$this->load->view('admin/ongkir/ongkir_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		$data['title']='ongkir';
		if($this->input->post('simpan')){
			$input=array(
				'nama_ongkir' => $this->input->post('nama_ongkir'),
				'keterangan' => $this->input->post('keterangan'),
				'id_parent' => $this->input->post('id_parent')
			);
			$insert=$this->ongkir_m->update('tbl_ongkir', 'id_ongkir', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['ongkir']=$this->ongkir_m->get_all_data('tbl_ongkir', 'id_ongkir', 'asc');
		$data['data']=$this->ongkir_m->get_single('tbl_ongkir', 'id_ongkir', $id);
		$this->load->view('admin/ongkir/ongkir_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->ongkir_m->delete('tbl_ongkir', 'id_ongkir', $id);
		if($del) redirect(admin_url().'ongkir');		
	}

	public function ajax_kota(){
		if (!empty($_GET['q'])){
			if (ctype_digit($_GET['q'])) {
				
				$query = mysql_query("SELECT * FROM tbl_lokasi where lokasi_propinsi=$_GET[q] and lokasi_kecamatan=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota!=0 order by lokasi_nama");
				echo"<option selected value=''>Pilih Kota/Kab</option>";
				while($d = mysql_fetch_array($query)){
					echo "<option value='$d[lokasi_kabupatenkota]&prop=$_GET[q]'>$d[lokasi_nama]</option>";
				}


			}
		}

		if (empty($_GET['kel'])){

			if (!empty($_GET['kec']) and !empty($_GET['prop'])){
				if (ctype_digit($_GET['kec']) and ctype_digit($_GET['prop'])) {
				
					$query = mysql_query("SELECT * FROM tbl_lokasi where lokasi_propinsi=$_GET[prop] and lokasi_kecamatan!=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota=$_GET[kec] order by lokasi_nama");
					echo"<option selected value=''>Pilih Kecamatan</option>";
					while($d = mysql_fetch_array($query)){
						echo "<option value='$d[lokasi_kecamatan]&kec=$d[lokasi_kabupatenkota]&prop=$d[lokasi_propinsi]''>$d[lokasi_nama]</option>";
					}
				}
			}
		} else {
			if (!empty($_GET['kec']) and !empty($_GET['prop'])){
				if (ctype_digit($_GET['kec']) and ctype_digit($_GET['prop'])) {
				
					$query = mysql_query("SELECT * FROM tbl_lokasi where lokasi_propinsi=$_GET[prop] and lokasi_kecamatan=$_GET[kel] and lokasi_kelurahan!=0 and lokasi_kabupatenkota=$_GET[kec] order by lokasi_nama");
					echo"<option selected value=''>Pilih Kelurahan/Desa</option>";
					while($d = mysql_fetch_array($query)){
						echo "<option value='$d[lokasi_kode]'>$d[lokasi_nama]</option>";
					}
				}
			}
		}
	}

}

/* End of file  */
/* Location: ./application/controllers/ */