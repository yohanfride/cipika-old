<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}
		
		$data['title']='page';
		$data['datas']=$this->page_m->get_all_data_order('tbl_page', 'id_page', 'asc');
		$this->load->view('admin/page_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';		
		$data['title']='Add page';
		if($this->input->post('simpan')){
			$input=array(
				'id_page' => null,
				'title' => $this->input->post('title'),
				'content' => $this->input->post('content'),
				'publish' => $this->input->post('publish'),
				'date_added' => date('Y-m-d H:i:s'),
				'date_modified' => date('Y-m-d H:i:s'),
				'sorting' => $this->input->post('sorting')
			);
			$insert=$this->page_m->insert('tbl_page', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['sorting']=$this->page_m->urutan()+1;
		$this->load->view('admin/page_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		$data['title']='Edit page';
		if($this->input->post('simpan')){
			$input=array(
				'title' => $this->input->post('title'),
				'content' => $this->input->post('content'),
				'publish' => $this->input->post('publish'),
				'date_modified' => date('Y-m-d H:i:s'),
				'sorting' => $this->input->post('sorting')
			);
			$insert=$this->page_m->update('tbl_page', 'id_page', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['data']=$this->page_m->get_single('tbl_page', 'id_page', $id);
		$this->load->view('admin/page_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->page_m->delete('tbl_page', 'id_page', $id);
		if($del) redirect(admin_url().'page/?alert=success');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */