<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
        $this->load->library('auth');        
	}

	public function index()
	{
        
		
		$data['alert']      = '';
		$data['title']      = 'user';
		$data['datas']      = $this->user_m->view_all_admin(1000,0);
		$data['datas2']     = $this->user_m->view_all_member(1000,0);
		$data['merchant']   = $this->merchant_m->get_merchant(0,1000);
        
		$this->load->view('admin/user_v', $data);
	}

	public function admin(){
                $this->auth->check();
		$data=array();
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}

		$data['title'] = 'Admin';
		$data['datas'] = $this->user_m->view_all_admin(1000,0);
		$this->load->view('admin/user_admin_v', $data);
	}

	public function merchant(){
        $this->auth->check();
		$data=array();
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}

		$data['title'] = 'Merchant';
		$this->load->model('merchant_m');
		$data['datas'] = $this->merchant_m->get_merchant(0,1000);
		$this->load->view('admin/user_merchant_v', $data);
	}
        
        public function add_merchant() {
        $this->auth->check();
        $this->load->model('merchant_m');
        $this->load->library('lib_lokasi');
        $data = array();
        $data['add'] = 'member';
        $data['success'] = '';
        $data['error'] = '';
        $data['title'] = 'Add Merchant';
        $data['data_sales'] = $this->merchant_m->get_sales();
        if ($this->input->post('simpan')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('nama_store', 'Nama Merchant', 'required');
            $this->form_validation->set_rules('biodata', 'Biodata', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('kecamatan', 'Alamat Kecamatan', 'required');
            $this->form_validation->set_rules('nama_pemilik', 'Nama Pemilik', 'required');
            $this->form_validation->set_rules('tgl_lahir_pemilik', 'Tgl Lahir', 'required');
            $this->form_validation->set_rules('telpon', 'Telp', 'required');
            $this->form_validation->set_rules('hp', 'No HP', 'required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('bank_nama', 'Nama Bank', 'required');
            $this->form_validation->set_rules('bank_norek', 'No Rekening', 'required');
            $this->form_validation->set_rules('bank_pemilik', 'Pemegang Rekening', 'required');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {
                $cek_email = $this->user_m->get_email_user($this->input->post('email'));

                if (!empty($cek_email)) {
                    if ($cek_email->id_level != '7') {

                        $insert = array('id_store' => null,
                            'id_kota' => $this->input->post('kecamatan'),
                            'id_user' => (int) $cek_email->id_user,
                            'id_sales' => $this->input->post('merchant_sales'),
                            'negara' => "INDONESIA",
                            'nama_store' => $this->input->post('nama_store'),
                            'deskripsi' => $this->input->post('biodata'),
                            'nama_pemilik' => $this->input->post('nama_pemilik'),
                            'tgl_lahir_pemilik' => strftime("%Y-%m-%d", strtotime($this->input->post('tgl_lahir_pemilik'))),
                            'alamat' => $this->input->post('alamat'),
                            'telpon' => $this->input->post('telpon'),
                            'merchant_hp' => $this->input->post('hp'),
                            'merchant_gender' => $this->input->post('gender'),
                            'ym' => '',
                            'fb' => '',
                            'tw' => '',
                            'bb' => '',
                            'wa' => '',
                            'email' => $cek_email->email,
                            'bank_nama' => $this->input->post('bank_nama'),
                            'bank_norek' => $this->input->post('bank_norek'),
                            'bank_pemilik' => $this->input->post('bank_pemilik'),
                            'store_status' => 'approve',
                            'date_added' => date('Y-m-d H:i:s'),
                            'date_modified' => date('Y-m-d H:i:s')
                        );
                        $insert1 = $this->merchant_m->insert('tbl_store', $insert);
                
                        //# Update status level user ke merchant (7)
                        $update = array('id_level' => 7);
                        $this->merchant_m->update('tbl_user', 'id_user', (int) $cek_email->id_user, $update);

                        if ($insert1)
                            $data['success'] = 'success';
                        else
                            $data['error'] = 'failed';
                    } else {
                        $data['error'] = 'Email Merchant telah terdaftar';
                    }
                } else {
                    $data['error'] = 'Email User Tidak ada';
                }
            }
        }
        $data['kota'] = $this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');
        $this->load->view('admin/merchant/add_merchant_v', $data);
    }

    public function member(){
                $this->auth->check();
		$data=array();
		if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
		else{$data['alert']='';}

		$data['title'] = 'Member';
		$data['datas'] = $this->user_m->view_all_member(1000,0);
		$this->load->view('admin/user_member_v', $data);
	}

	public function profile($id=''){
                $this->auth->check();
		if($id==''){redirect(base_url('admin/dashboard'));}		
		$data=array();
		$data['success']='';
		$data['error']='';
		$data['title']='Profile';
		if($this->input->post('simpan')){
			$input=array(
				// 'username' => $this->input->post('username'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'bio' => $this->input->post('bio'),
				'alamat' => $this->input->post('alamat'),
				'telpon' => $this->input->post('telpon'),
				'id_kota' => $this->input->post('id_kota')
			);
			$update=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user, $input);
			// echo $update;exit;
			if($_FILES["user_image"]["name"]){
				$image=$this->user_m->get_image($this->session->userdata('admin_session')->id_user);
				if($image){
					unlink('asset/upload/profil/'.$image);
				}
				$nama_baru=date('YmdHis') . $_FILES["user_image"]["name"];
				move_uploaded_file($_FILES["user_image"]["tmp_name"], "asset/upload/profil/" . $nama_baru);
				$inp_pp=array(
					'image' => $nama_baru
				);
				$ganti=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user, $inp_pp);
			}
			if($update||isset($ganti)){$data['success']='success';}else{$data['error']='Failed';}
		}
		$data['data']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user);
		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');
		$this->load->view('admin/user_profil_v', $data);
	}

	public function setting(){
                $this->auth->check();
		$data=array();
		$data['success']='';
		$data['error']='';
		$data['title']      = 'Setting';
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('old_password', 'Old Password', 'required');
			$this->form_validation->set_rules('password', 'New Password', 'required|matches[passconf]|min_length[6]');
			$this->form_validation->set_rules('passconf', 'Konfirmasi password', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
		    	$cek_password=$this->user_m->cek_password($this->session->userdata('admin_session')->id_user, md5($this->input->post('old_password')));
		    	if($cek_password>0){
		    		$ganti=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user, array('email'=>$this->input->post('email'),  'password'=>md5($this->input->post('password'))));
		    		if($ganti){$data['success']='Email dan password berhasil diganti.';}else{$data['error']='Failed';}
		    	} else {
		    		$data['error']= 'Password Anda salah';
		    	}
		    }
		}
		$data['data']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user);		
		$this->load->view('admin/user_setting_v', $data);
	}

	public function password()
	{
                $this->auth->check();
		$data=array();
		$data['success']='';
		$data['error']='';
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('old_password', 'Old Password', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
		    	$cek_password=$this->user_m->cek_password($this->session->userdata('admin_session')->id_user, md5($this->input->post('old_password')));
		    	if($cek_password>0){
		    		$ganti=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user, array('password'=>md5($this->input->post('password'))));
		    		if($ganti){$data['success']='success';}else{$data['error']='Failed';}
		    	}
		    }
		}
		$data['data']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user);
		$this->load->view('admin/user_password_v', $data);
	}
	public function add()
	{
                $this->auth->check();
		$data=array();
		$data['add']='admin';
		$data['success']='';
		$data['error']='';
		$data['title']='Add User';
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|is_unique[tbl_user.username]');
			$this->form_validation->set_rules('firstname', 'Firstname', 'required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('id_kota', 'Kota', 'required');
			$this->form_validation->set_rules('id_level', 'Level', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
				$input=array(
					'id_user' => null,
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'email' => $this->input->post('email'),
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'id_kota' => $this->input->post('id_kota'),
					'id_level' => $this->input->post('id_level'),
					'date_added' => date('Y-m-d H:i:s'),
					'date_modified' => date('Y-m-d H:i:s')
				);
				$insert=$this->user_m->insert('tbl_user', $input);
				if($insert)	$data['success']='success';
				else $data['error']='failed';
		    }
		}
		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');
		$data['level']=$this->user_m->get_all_data('tbl_level', 'id_level', 'asc');
		$this->load->view('admin/user_add_v', $data);
	}

	public function edit($id='')
	{
                $this->auth->check();
		$data=array();
		$data['edit']='admin';
		$data['success']='';
		$data['error']='';
		$data['title']='Edit User';
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean');
			$this->form_validation->set_rules('firstname', 'Firstname', 'required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('id_kota', 'Kota', 'required');
			$this->form_validation->set_rules('id_level', 'Level', 'required');
			$this->form_validation->set_rules('id_status', 'Status', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
		    if($this->user_m->cek_username($id, $this->input->post('username'))==0){
			$input=array(
				// 'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'id_kota' => $this->input->post('id_kota'),
				'id_level' => $this->input->post('id_level'),
				'id_status' => $this->input->post('id_status'),
				'date_modified' => date('Y-m-d H:i:s')
			);
			$insert=$this->user_m->update('tbl_user', 'id_user', $id, $input);
			if($insert)	$data['success']='success';
			else $data['error']='failed';
			}else {
				$data['error']='Username already exist.';
			}
			}
		}
		$data['data']=$this->user_m->get_single('tbl_user', 'id_user', $id);
		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');
		$data['level']=$this->user_m->get_all_data('tbl_level', 'id_level', 'asc');
		$data['status']=$this->user_m->get_all_data('tbl_status', 'id_status', 'asc');	
		$this->load->view('admin/user_edit_v', $data);
	}
    
    public function edit_merchant($id='')
	{
        $this->auth->check();
        $this->load->library('lib_lokasi');
    
		$data['edit']       = 'admin';
		$data['success']    = '';
		$data['error']      = '';		
		$data['title']      = 'Edit Merchant';
        
		if($this->input->post('simpan')){
			$this->load->library('form_validation');	
			$this->form_validation->set_rules('nama_store', 'Nama Store', 'required');		
			$this->form_validation->set_rules('nama_pemilik', 'Nama Pemilik', 'required');		
			$this->form_validation->set_rules('alamat', 'Alamat Merchant', 'required');		
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');		
			$this->form_validation->set_rules('tgl_lahir_pemilik', 'Tanggal Lagir', 'required');		
			$this->form_validation->set_rules('telpon', 'Telpon', 'required');		
			$this->form_validation->set_rules('hp', 'No HP', 'required');		
			$this->form_validation->set_rules('bank_nama', 'Nama Bank', 'required');		
			$this->form_validation->set_rules('bank_norek', 'No Rekening', 'required');		
			$this->form_validation->set_rules('bank_pemilik', 'Pemilik Bank', 'required');		
			$this->form_validation->set_rules('store_status', 'Status', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}else{                
                $merchant   = $this->user_m->get_single('tbl_store', 'id_store', $id);
                
                if($this->input->post('store_status')!=$this->input->post('curr_status')){
                
                    $date_verified      = ($this->input->post('store_status')=='approve')?date('Y-m-d H:i:s'):$merchant->date_verified;
                    $date_unverified    = ($this->input->post('store_status')=='block')?date('Y-m-d H:i:s'):$merchant->date_unverified;
                
                    $update     = array( 'store_status'     => $this->input->post('store_status'),
                                     'id_kota'               => $this->input->post('kecamatan'),
                                     'nama_store'            => $this->input->post('nama_store'),
                                     'nama_pemilik'          => $this->input->post('nama_pemilik'),
                                     'tgl_lahir_pemilik'     => strftime("%Y-%m-%d",strtotime($this->input->post('tgl_lahir_pemilik'))),
                                     'alamat'                => $this->input->post('alamat'),
                                     'telpon'                => $this->input->post('telpon'),
                                     'merchant_hp'           => $this->input->post('hp'),
                                     'bank_nama'             => $this->input->post('bank_nama'),
                                     'bank_norek'            => $this->input->post('bank_norek'),
                                     'bank_pemilik'          => $this->input->post('bank_pemilik'),
                                     'date_verified'        => $date_verified,
                                     'date_unverified'      => $date_unverified
                                    );
                }else{
                    $update     = array( 'store_status'     => $this->input->post('store_status'),
                                     'id_kota'               => $this->input->post('kecamatan'),
                                     'nama_store'            => $this->input->post('nama_store'),
                                     'nama_pemilik'          => $this->input->post('nama_pemilik'),
                                     'tgl_lahir_pemilik'     => strftime("%Y-%m-%d",strtotime($this->input->post('tgl_lahir_pemilik'))),
                                     'alamat'                => $this->input->post('alamat'),
                                     'telpon'                => $this->input->post('telpon'),
                                     'merchant_hp'           => $this->input->post('hp'),
                                     'bank_nama'             => $this->input->post('bank_nama'),
                                     'bank_norek'            => $this->input->post('bank_norek'),
                                     'bank_pemilik'          => $this->input->post('bank_pemilik')
                                    );
                }
                
                
                $udpdate    = $this->user_m->update('tbl_store', 'id_store', (int)$id, $update);
                if($udpdate){
                	//kirim email disini
                	if($this->input->post('store_status')=='approve' && ($this->input->post('store_status')!=$this->input->post('curr_status'))){
						$data_email='<p>Selamat, Akun Anda telah terverifikasi sebagai Merchant di Cipika Store.</p>';
						$data_email.='<p>Anda dapat mengatur panel merchant Anda di <a href="'.base_url().'merchant">' . base_url() .'merchant' . '</a></p>';

						$config=$this->_mail_win();
						$this->load->library('email', $config);
						$this->email->set_newline("\r\n");
						$this->email->from($this->config->item('email_from'), 'Cipika Store');
						$this->email->to($merchant->email);
						// $this->email->bcc($this->config->item('email_tujuan'));
						$this->email->subject('Verifikasi Merchant Cipika');
						$this->email->message($data_email);
						$send = $this->email->send();
					}

                	$data['success']='success';
                }
                else $data['error']='failed';
			}
		}
		
        $data['data']   = $this->user_m->get_single('tbl_store', 'id_store', $id);
        $data['produk'] = $this->produk_m->get_all_produk_user($data['data']->id_user);
        $data['sales'] = $this->produk_m->get_single('tbl_user', 'id_user',$data['data']->id_sales);
        
		$this->load->view('admin/merchant/edit_merchant_v', $data);
	}

	public function delete($id='', $page='')
	{
                $this->auth->check();
		$delstore=$this->user_m->delete('tbl_store', 'id_user', $id);
		$del=$this->user_m->delete('tbl_user', 'id_user', $id);
		if($del){
			if($page=='admin') redirect(admin_url().'user/admin/?alert=success');
			if($page=='merchant') redirect(admin_url().'user/merchant/?alert=success');
			if($page=='member') redirect(admin_url().'user/member/?alert=success');
		}			
	}

	public function hapus($id='', $page='')
	{
		$delstore=$this->user_m->delete('tbl_store', 'id_user', $id);

		$del=$this->user_m->update('tbl_user', 'id_user', $id, array('deleted'=>1));
		$del2=$this->user_m->update('tbl_produk', 'id_user', $id, array('deleted'=>1));

		if($del){
			if($page=='admin') redirect(admin_url().'user/admin/?alert=success');
			if($page=='merchant') redirect(admin_url().'user/merchant/?alert=success');
			if($page=='member') redirect(admin_url().'user/member/?alert=success');
		}			
	}


	public function add_member() {
        $this->auth->check();
        $this->load->library('lib_lokasi');
        $data = array();
        $data['add'] = 'member';
        $data['success'] = '';
        $data['error'] = '';
        $data['title'] = 'Add User';
        if ($this->input->post('simpan')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_user.email]');
            $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|min_length[6]');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
            $this->form_validation->set_rules('username', 'Nama Tampilan', 'required');
            $this->form_validation->set_rules('firstname', 'Nama Depan', 'required');
            $this->form_validation->set_rules('lastname', 'Nama Belakang', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('telpon', 'Telpon', 'required|numeric');
            $this->form_validation->set_rules('hp', 'Hp', 'required|numeric');
            $this->form_validation->set_rules('id_propinsi', 'Propinsi', 'required');
            $this->form_validation->set_rules('id_kabupaten', 'Kabupaten', 'required');
            $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {

                $input = array('id_user' => null,
                    'password' => md5($this->input->post('password')),
                    'email' => $this->input->post('email'),
                    'id_status' => 1,
                    'active' => 1,
                    'username' => $this->input->post('username'),
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'birthdate' => strftime("%Y-%m-%d", strtotime($this->input->post('birthdate'))),
                    'gender' => $this->input->post('gender'),
                    'alamat' => $this->input->post('alamat'),
                    'telpon' => $this->input->post('telpon'),
                    'hp' => $this->input->post('hp'),
                    'id_propinsi' => $this->input->post('id_propinsi'),
                    'id_kabupaten' => $this->input->post('id_kabupaten'),
                    'id_kecamatan' => $this->input->post('kecamatan'),
                    'id_level' => 6
                );
                $insert1 = $this->user_m->insert('tbl_user', $input);
                if ($insert1)
                    $data['success'] = 'success';
                else
                    $data['error'] = 'failed';
            }
        }
        $this->load->view('admin/user_add_member_v', $data);
    }

    public function edit_member($id='')
	{
            $this->auth->check();
        echo "edit member";
    
		$data=array();
		$data['edit']='member';
		$data['success']='';
		$data['error']='';
		$data['title']='Add User';
        
        $this->load->model('merchant_m');
        $this->load->model('meta_m');
        $data['data_sales']     = $this->merchant_m->get_sales();
        $data['curr_sales']     = $this->meta_m->get_meta_data('tbl_user',$id,'merchant_sales');
        
		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Display name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('telpon', 'Phone', 'required');
			$this->form_validation->set_rules('id_kota', 'Kota', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
                //# Update sales
                $this->load->model('meta_m');
                $idmeta = $this->input->post('idmeta');
                if($idmeta!=''){
                    $this->meta_m->update_meta($idmeta,$this->input->post('merchant_sales'));
                }else{
                    $metas = array('merchant_sales' => $this->input->post('merchant_sales'));
                    $this->meta_m->save_meta('tbl_user',$id,$metas,'add');
                }                
                $data['curr_sales']     = $this->meta_m->get_meta_data('tbl_user',$id,'merchant_sales');
                
				$input1=array(
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'telpon' => $this->input->post('telpon'),
					'id_kota' => $this->input->post('id_kota'),
					'date_modified' => date('Y-m-d H:i:s')
				);
				$insert1=$this->user_m->update('tbl_user', 'id_user', $id, $input1);
				/*$input2=array(
					'nama_store' => $this->input->post('nama_store'),
					'telpon' => $this->input->post('telpon'),
					'email' => $this->input->post('email'),
					'date_modified' => date('Y-m-d H:i:s')
				);
				$insert2=$this->user_m->update('tbl_store', 'id_user', $id, $input2);*/
				if($insert1) $data['success']='success';
				else $data['error']='failed';
		    }
		}
		$data['data2']=$this->user_m->get_single('tbl_user', 'id_user', $id);
		$data['store']=$this->user_m->get_single('tbl_store', 'id_user', $id);
		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');
		$this->load->view('admin/user_edit_v', $data);
	}

	public function login() {
        if ($this->session->userdata('admin_session')) {

            redirect(base_url('home'));
        }

        $this->load->view('admin/user_login_v');
    }

    public function dologin() {
        $this->load->library('form_validation');
        $data['error'] = FALSE;

        //#1 Set Form Validation

        $this->form_validation->set_rules('username', 'Username', 'xss_clean|required');
        $this->form_validation->set_rules('password', 'Password', 'xss_clean|required');

        if ($this->form_validation->run($this) == FALSE) {
            //#2 Display Error Message
            $data['error'] = validation_errors();
        } else {
            $username = $this->input->post("username");
            $pass = md5($this->input->post('password'));

            $user = $this->user_m->get_user_login($username, $pass);
            if(!empty ($user)){
                $this->auth->save($user);
//                var_dump($_SESSION['admin_session']);
                redirect(base_url() . "admin/dashboard");
            }
            else{
                $data['error'] = "Check your Username & Password";
            }
        }

        $this->load->view('admin/user_login_v', $data);
    }

    public function destroy(){
    	 $this->auth->destroy();
    }
    
    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }
    
    
}

/* End of file  */
/* Location: ./application/controllers/ */
