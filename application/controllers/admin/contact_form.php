<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_form extends CI_Controller 
{

	public function __construct() {

        parent::__construct();
        $this->load->library('auth');        
        $this->load->model('contact_form_m');                 
        $this->auth->check();
    }
    
    /* List all contact form */
	public function index()
	{   
        $data['title']  = 'Contact Form';
		
        $data['data']   = $this->contact_form_m->get_all_data_order('contact_form', 'idcf', 'desc');
            
		$this->load->view('admin/contact_form/list_v', $data);        
	}
    
    public function delete($id='')
	{   
        if( ($id!='') && (is_numeric($id)) ){
            $this->contact_form_m->delete('contact_form', 'idcf', $id);
        }
		redirect(base_url('admin/contact_form'));
	}
    
}