<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct() {

        parent::__construct();
        $this->load->library('auth');
        $this->load->model('banner_m');
        $this->auth->check();
    }
    
    /* All Banner Data */
	public function index()
	{   
        $data['title']  = 'Banner';
        
        if(isset($_GET['alert'])){$data['alert']=$_GET['alert'];}
        else{$data['alert']='';}

		$data['datas']  = $this->banner_m->get_all_data('banner', 'idbanner', 'asc');
		
		$this->load->view('admin/banner/banner_v', $data);        
	}
    
    /* Add New Banner */
    public function add()
    {
    
        $data['title']  = 'Banner';
        
        $this->load->view('admin/banner/add_banner_v', $data);        
    }

    public function setting(){
        $data=array();
        $data['title']  = 'Setting banner';
        $data['success']='';
        $data['error']='';
        
        if($this->input->post('simpan')){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('banner_interval', 'Interval', 'required');
            
            if ($this->form_validation->run() == FALSE){
                $data['error'] = validation_errors();
            }else{
                $insert = array(
                    'option_name'   => 'banner_interval',
                    'option_value'  => $this->input->post('banner_interval')
                );

                $cek=$this->banner_m->cek_option($insert['option_name']);

                // echo $cek;exit;

                if($cek==0){
                    $this->banner_m->insert('tbl_options',$insert);
                } else {
                    $this->banner_m->update('tbl_options', 'option_name', $insert['option_name'], $insert);
                }
                // redirect(base_url('admin/banner'));
                $data['success']='success';
            }
        }

        $data['setting']=$this->banner_m->get_option('banner_interval');
        $this->load->view('admin/banner/banner_setting_v', $data);

    }
    
    /* Sav Banner to DB */
    public function save()
    {
        $data['title']  = 'Banner';
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('banner_title', 'Title', 'required|trim|xss_clean|is_unique[banner.banner_title]');
		$this->form_validation->set_rules('banner_url', 'Target URL', 'required|trim|xss_clean|prep_url');
		
        if ($this->form_validation->run() == FALSE){
            $data['error'] = validation_errors();
		}else{
            $insert = array('idbanner'      => null,
                            'banner_title'  => $this->input->post('banner_title'),
                            'banner_url'    => $this->input->post('banner_url'),
                            'banner_image'  => $this->_doupload_file('banner_image','asset/upload/banner/'),
                            'banner_status' => 'publish'
                           );
            $this->banner_m->insert('banner',$insert);
            redirect(base_url('admin/banner'));
        }
        
        $this->load->view('admin/banner/add_banner_v', $data);
    }
    
    /* Edit Banner */
    function edit()
    {
        $this->load->library('form_validation');
        $data['title']  = 'Banner';        
        $idbanner       = $this->uri->segment(4);
        $data['success']='';
        
        if($this->input->post('update')){
            $data['datas']   = $this->banner_m->get_single('banner','idbanner',$this->input->post('idbanner'));
            
            $is_unique = ($this->input->post('banner_title') != $data['datas']->banner_title)?'|is_unique[banner.banner_title]':'';
            $this->form_validation->set_rules('banner_title', 'Title', 'required|trim|xss_clean'.$is_unique);
            $this->form_validation->set_rules('banner_url', 'Target URL', 'required|trim|xss_clean|prep_url');
            
            if ($this->form_validation->run() == FALSE){
                $data['error'] = validation_errors();
            }else{
                $img = $this->input->post('curr_img');
                if($_FILES['banner_image']['name']!=''){
                    $file = explode('/',$this->input->post('curr_img'));
                    @unlink('asset/upload/banner/'.$file[9]);
                    $img = $this->_doupload_file('banner_image','asset/upload/banner/');
                }
            
                $update = array('banner_title'  => $this->input->post('banner_title'),
                                'banner_url'    => $this->input->post('banner_url'),
                                'banner_image'  => $img                                
                               );
                $this->banner_m->update('banner','idbanner',$this->input->post('idbanner'), $update);
                // redirect(base_url('admin/banner/?alert=success'));
                $data['success']='Data berhasil disimpan.';
            }
        }    
        $data['data']   = $this->banner_m->get_single('banner','idbanner',$idbanner);    
        $this->load->view('admin/banner/edit_banner_v', $data);
    }
    
    public function delete()
    {
        $idbanner = $this->uri->segment(4);
        
        $this->banner_m->delete('banner','idbanner',$idbanner);
        redirect(base_url('admin/banner/?alert=success'));
    }
    
    public function _doupload_file($name,$target)
	{
		$img						= $name;
		$config['file_name']  		= preg_replace("/[^0-9a-zA-Z ]/", "", $_FILES[$img]['name']).date('dmYHis');
		$config['upload_path'] 		= $target;
		$config['overwrite'] 		= FALSE;
		$config['allowed_types'] 	= '*';
		$config['max_size']			= '50060';
		$config['max_width']  		= '10000';
		$config['max_height']  		= '10000';			
		$config['remove_spaces']  	= TRUE; 

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
			
		if ( ! $this->upload->do_upload($img)){
			$return['error'] 	 = $this->upload->display_errors();
			$return['file_name'] = '';
		}else{
			$data = array('upload_data' => $this->upload->data());								
			$return['error'] 	 = '-';
			$return['file_name'] = $data['upload_data']['file_name'];					
		}
		$this->upload->file_name = '';
		
		if($return['file_name']==''){
			//return $return['error'];
			return '-';
		}else{
			return base_url($target.$return['file_name']);
		}
	}
    
    function tes()
    {
        $url = explode("/","http://127.0.0.1:8080/project/Cipika/cipika/asset/upload/banner/code7png11052014015050.png");
        print_r($url);
        //unlink('asset/upload/banner/1png11052014010005.png');
    }
}
