<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ekspedisi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ekspedisi_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		$data['alert']='';
		$data['title']='ekspedisi';
		$data['datas']=$this->ekspedisi_m->get_all_data('tbl_ekspedisi', 'id_ekspedisi', 'asc');
		$this->load->view('admin/ekspedisi_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'id_ekspedisi' => null,
				'nama_ekspidisi' => $this->input->post('nama_ekspidisi'),
				'telepon' => $this->input->post('telepon'),
				'alamat' => $this->input->post('alamat')
			);
			$insert=$this->ekspedisi_m->insert('tbl_ekspedisi', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$this->load->view('admin/ekspedisi_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		if($this->input->post('simpan')){
			$input=array(
				'nama_ekspidisi' => $this->input->post('nama_ekspidisi'),
				'telepon' => $this->input->post('telepon'),
				'alamat' => $this->input->post('alamat')
			);
			$insert=$this->ekspedisi_m->update('tbl_ekspedisi', 'id_ekspedisi', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['data']=$this->ekspedisi_m->get_single('tbl_ekspedisi', 'id_ekspedisi', $id);
		$this->load->view('admin/ekspedisi_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->ekspedisi_m->delete('tbl_ekspedisi', 'id_ekspedisi', $id);
		if($del) redirect(admin_url().'ekspedisi');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */