<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_m');
        $this->load->library('auth');
        $this->auth->check();
	}

	public function index()
	{
		$data=array();
		$data['alert']='';
		$data['title']='page';
		$data['datas']=$this->article_m->get_all_data('tbl_article', 'id_article', 'asc');
		$this->load->view('admin/article_v', $data);
	}

	public function add()
	{
		$data=array();
		$data['alert']='';		
		$data['title']='Add article';
		if($this->input->post('simpan')){
			$input=array(
				'id_article' => null,
				'article_slug' => $this->commonlib->generate_permalink($this->input->post('title'), 'tbl_article', 'id_article', 'article_slug'),
				'title' => $this->input->post('title'),
				'id_user' => $this->auth_m->get_user()->id_user,
				'publish' => $this->input->post('publish'),
				'content' => $this->input->post('content'),
				'date_added' => date('Y-m-d H:i:s'),
				'date_modified' => date('Y-m-d H:i:s'),
			);
			$insert=$this->article_m->insert('tbl_article', $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$this->load->view('admin/article_add_v', $data);
	}

	public function edit($id='')
	{
		$data=array();
		$data['alert']='';
		$data['title']='Edit article';
		if($this->input->post('simpan')){
			$input=array(
				'article_slug' => $this->commonlib->generate_permalink($this->input->post('title'), 'tbl_article', 'id_article', 'article_slug'),
				'title' => $this->input->post('title'),
				'publish' => $this->input->post('publish'),
				'content' => $this->input->post('content'),
				'date_modified' => date('Y-m-d H:i:s'),
			);
			$insert=$this->article_m->update('tbl_article', 'id_article', $id, $input);
			if($insert)	$data['alert']='success';
			else $data['alert']='failed';
		}
		$data['data']=$this->article_m->get_single('tbl_article', 'id_article', $id);
		$this->load->view('admin/article_edit_v', $data);
	}

	public function delete($id='')
	{
		$del=$this->article_m->delete('tbl_article', 'id_article', $id);
		if($del) redirect(admin_url().'article?delete=success');		
	}

}

/* End of file  */
/* Location: ./application/controllers/ */