<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('order_m');
		$this->load->model('auth_m');
                $this->load->model('payment_m');
                $this->load->model('produk_m');
                $this->load->model('user_m');
		$this->auth_m->check();
	}

	public function index($offset=0)
	{
		$data=array();
		$data['sidebar_member_order'] = "class='active'";
		$id_user=$this->session->userdata('member')->id_user;
		if(isset($_GET['hal'])){$hal=$_GET['hal'];}
		else {$hal='';}

		$dataPerhalaman=10;
		($hal=='')?$nohalaman = 1:$nohalaman = $hal;
		$offset = ($nohalaman - 1) * $dataPerhalaman;
		$off = abs( (int) $offset);
		$jmldata=$this->order_m->count_all_data_where('tbl_order', 'id_user', $id_user);
		$data['paginator']=$this->order_m->page($jmldata, $dataPerhalaman, $hal);

		$data['order']=$this->order_m->get_order_user($id_user, $dataPerhalaman, $off);
                foreach ($data['order'] as $v)
                {
                    $inv=$this->order_m->get_single("tbl_order", "kode_order" ,$v->kode_order);
                    $data['invoices'][$v->kode_order] = $inv->kode_invoice;
                }
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/order_v', $data);
	}
    
    public function view($id)
    {
        $this->load->library('lib_lokasi');
    
        $data['title'] = 'My Orders Single';
        $data['order'] = $this->cart_m->get_order($id);
        $data['order_item'] = $this->cart_m->get_order_item($id);
        $data['sidebar_member_order'] = "class='active'";
        foreach ($data['order_item'] as $v)
        {
            $data['prod'][$v->id_produk] = $this->produk_m->get_single('tbl_produk', 'id_produk', $v->id_produk);
        }
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/order_single_v', $data);
    }

    public function print_invoice($id)
    {
        $action = $this->uri->segment(4, 0);
        
        // $user = $this->session->userdata('member');
        // $data['user']=$user;
        // $data['kota']=$this->user_m->get_single("tbl_kota", 'id_kota', $user->id_kota);
        // $data['provinsi']=$this->user_m->get_single("tbl_propinsi", 'id_propinsi', $user->id_propinsi);
        
        $data['order'] = $this->cart_m->get_order_by_invoice($id);
        // var_dump($data['order']);exit;
        $data['user'] = $this->auth_m->get_user();
        $data['invoices'] = $this->order_m->get_single("tbl_invoices", 'md5(kode_invoice)', $id);
        $data['payment'] = $this->payment_m->get_payment_single($data['invoices']->kode_invoice, $data['invoices']->id_payment);
        $data['pay']=  $this->payment_m->get_det_payment($data['invoices']->id_payment);
        $data['action']=$action;
        
        foreach ($data['order'] as $v)
        {
            $data['order_item'][$v->id_order] = $this->cart_m->get_order_item(md5($v->id_order));

            foreach ($data['order_item'][$v->id_order] as $c)
            {
                $data['product'][$c->id_produk] = $this->produk_m->get_single("tbl_produk", 'id_produk', $c->id_produk);
            }
            $data['order_merchant'][$v->id_merchant]=$this->order_m->get_single("tbl_store", 'id_user', $v->id_merchant);
        }

        $this->load->view('publik/report/invoice', $data);
    }

}

/* End of file search.php */
/* Location: ./application/controllers/search.php */