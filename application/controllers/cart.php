<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('home_m');
        $this->load->model('payment_m');
        $this->load->model('order_m');
        $this->load->model('cart_m');
        $this->load->library('lib_rest');
    }

    public function index()
    {
        $data=array();
        // hapus cart submit
        if($this->input->post('btn-delete-cart-item')){
            if($this->input->post('delete-cart-item')){
                foreach ($this->input->post('delete-cart-item') as $rowid){
                    $item = array('rowid' => $rowid,'qty' => 0);
                    $this->cart->update($item);
                }
            }
        }
        // billing submit
        if($this->input->post('btn-goto-billing')){
            if ($this->cart->contents()) redirect(base_url('cart/billing'));
        }

        //cart group by merchant
        $cart=$this->cart->contents();
        $grouped = array();
        foreach($cart as $c) {
          if(!isset($grouped[$c['id_merchant']])) {
            $grouped[$c['id_merchant']] = array();
          }
          $grouped[$c['id_merchant']][] = $c;
        }

        $data['cart']=$grouped;
        // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/cart_v', $data);
    }
    
    public function alamat(){
        $sesionnya =$this->session->userdata('member');
        if (empty($sesionnya))
        {
            redirect(base_url());
        }
        $data=array();
        $this->load->library('lib_lokasi');
        $data['propinsi']       = $this->lib_lokasi->get_propinsi();
        $data['js_lokasi']      = TRUE;
        if($this->input->post('simpan')){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('firstname', 'Nama depan', 'required');
            $this->form_validation->set_rules('lastname', 'Nama belakang', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('hp', 'No Hp', 'required|numeric');
            $this->form_validation->set_rules('id_propinsi', 'Provinsi', 'required');
            $this->form_validation->set_rules('id_kabupaten', 'Kabupaten', 'required');
            $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
            $this->form_validation->set_message('required', '%s tidak boleh kosong');

            if ($this->form_validation->run() == FALSE){
                    $data['error'] = validation_errors();
            }else{
                $input=array(
                        'firstname'    => remove_contact($this->input->post('firstname')),
                        'lastname'    => remove_contact($this->input->post('lastname')),
                        'alamat'    => remove_contact($this->input->post('alamat')),
                        'hp'    => $this->input->post('hp'),
                        'id_propinsi'    => $this->input->post('id_propinsi'),
                        'id_kabupaten'    => $this->input->post('id_kabupaten'),
                        'id_kecamatan'    => $this->input->post('kecamatan')
                );
                $update=$this->user_m->update('tbl_user', 'id_user', $this->session->userdata('member')->id_user, $input);

                if($update){
                    redirect(base_url('cart/billing?update_alamat=1'));
                }
            }
        }
		$data['user']=$this->user_m->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
		$data['provinsi']=$this->produk_m->get_all_data_order('tbl_propinsi', 'nama_propinsi', 'asc');
                $data['nama_propinsi']=$this->produk_m->get_single('tbl_propinsi', 'id_propinsi', $data['user']->id_propinsi);
                $data['nama_kabupaten']=$this->produk_m->get_single('tbl_kabupaten', 'id_kabupaten', $data['user']->id_kabupaten);
                $data['nama_kecamatan']=$this->produk_m->get_single('tbl_kecamatan', 'id_kecamatan', $data['user']->id_kecamatan);
//		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');		
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/chekout_alamat_v', $data);
    }
    
    public function ganti_alamat($id=''){
        $sesionnya =$this->session->userdata('member');
        if (empty($sesionnya))
        {
            redirect(base_url());
        }
        
        $this->load->library('lib_lokasi');
        $data=array();
        $data['propinsi']       = $this->lib_lokasi->get_propinsi();
        $data['js_lokasi']      = TRUE;
        $data['id'] = $id;
        $data['ses'] = $this->session->userdata('alamat'.$id);
        if($this->input->post('simpan')){
                
                $this->load->library('form_validation');
                $this->form_validation->set_rules('nama', 'Nama', 'required');
                $this->form_validation->set_rules('alamat', 'Alamat', 'required');
                $this->form_validation->set_rules('telpon', 'No Hp', 'required|numeric');
                $this->form_validation->set_rules('email', 'Email', 'required');
                $this->form_validation->set_rules('id_propinsi', 'Provinsi', 'required');
                $this->form_validation->set_rules('id_kabupaten', 'Kabupaten', 'required');
                $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
                $this->form_validation->set_message('required', '%s tidak boleh kosong');

                if ($this->form_validation->run() == FALSE){
                        $data['error'] = validation_errors();
                }else{
                    $this->session->unset_userdata('alamat'.$id);
                    $propinsi = $this->produk_m->get_single('tbl_propinsi', 'id_propinsi', $this->input->post('id_propinsi'))->nama_propinsi;
                    $kota = $this->produk_m->get_single('tbl_kabupaten', 'id_kabupaten', $this->input->post('id_kabupaten'))->nama_kabupaten;
                    $kecamatan = $this->produk_m->get_single('tbl_kecamatan', 'id_kecamatan', $this->input->post('kecamatan'))->nama_kecamatan;
                    $array = array(
                            'nama' => $this->input->post('nama'),
                            'alamat' => $this->input->post('alamat'),
                            'telpon' => $this->input->post('telpon'),
                            'email' => $this->input->post('email'),
                            'id_provinsi' => $this->input->post('id_propinsi'),
                            'id_kota' => $this->input->post('id_kabupaten'),
                            'id_kecamatan' => $this->input->post('kecamatan'),
                            'propinsi' => $propinsi,
                            'kabupaten' => $kota,
                            'kecamatan' => $kecamatan
                       );
                   $this->session->set_userdata('alamat'.$id, $array);
                   redirect(base_url('cart/billing'));
                }
        }
        $data['provinsi']=$this->produk_m->get_all_data_order('tbl_propinsi', 'nama_propinsi', 'asc');
        $data['nama_propinsi']=$this->produk_m->get_single('tbl_propinsi', 'id_propinsi', $data['ses']['id_provinsi']);
        $data['nama_kabupaten']=$this->produk_m->get_single('tbl_kabupaten', 'id_kabupaten', $data['ses']['id_kota']);
//		$data['kota']=$this->user_m->get_all_data('tbl_kota', 'id_kota', 'asc');		
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
        $data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/chekout_ganti_alamat_v', $data);
    }

    public function billing_payment()
    {
        $nilai_admin = $this->payment_m->get_option('cost_payment');
        $sesionnya =$this->session->userdata('member');
        if (empty($sesionnya))
        {
            redirect(base_url());
        }
        $data['user'] = $this->cart_m->get_user($this->session->userdata('member')->id_user);
        $cart = $this->cart->contents();
//        $in = $_POST;
        $send = FALSE;
//        if (empty($_SESSION['post']))
//        {
//            $_SESSION['post'] = $in;
//        }
        $grouped = array();
        foreach ($cart as $c)
        {
            if (!isset($grouped[$c['id_merchant']]))
            {
                $grouped[$c['id_merchant']] = array();
            }
            $grouped[$c['id_merchant']][] = $c;
        }
        $cart = $grouped;
        $ses = array();
        foreach ($cart as $key => $value)
        {
            $ses[] = $this->session->userdata('alamat' . $key);
        }
        if ($this->input->post('submit'))
        {
//            $getss = $_SESSION['post'];

//            $ongkir_ses = $getss['ongkir_sementara'];
            $id_payment = $this->input->post('id_payment');
            $grouped = array();
            $cart = $this->cart->contents();
            foreach ($cart as $c)
            {
                if (!isset($grouped[$c['id_merchant']]))
                {
                    $grouped[$c['id_merchant']] = array();
                }
                $grouped[$c['id_merchant']][] = $c;
            }
            $cart = $grouped;
            $grand_total = 0;
            $ongkir = 0;
            foreach ($cart as $key => $value)
            {
                $subtotal = 0;
                foreach ($value as $row)
                {
                    $subtotal+=$row['subtotal'];
                }
                $grand_total += $subtotal;
            }
            $m = 0;
            $total = 0;
            $ongkirs = 0;
            $input = array();
            foreach ($cart as $kunci => $value)
            {
                $i = 0;
                $berat = 0;
                $subtotal = 0;
                foreach ($value as $row)
                {
                    $berat+=($row['berat'] * $row['qty']);
                    $subtotal+=$row['subtotal'];
                    $i++;
                }
                $m++;
                $ses_ongkir = $this->session->userdata('ongkir' . $kunci);
                $ongkirs+=$ses_ongkir['ongkir'];
            }

            $key = date('ymd');
            $id = $this->cart_m->get_new_id_invoice($key);
            $id_new = (int) $id->jumlah;
            $id_new++;
            $id_new = 100000 + $id_new;
            $invoices = strtoupper($key . '-' . substr($id_new, 1, 5));

            $invoices=$invoices."_".date("dmyhis");

            $bank = 0;
            $dompet = 0;
            
            if ((int) $nilai_admin['option_value'] > 0)
                if ($id_payment == 5)
                    $nilai_admin['option_value'] = 0;

            $total_kirim = $grand_total + $ongkirs + $nilai_admin['option_value'];

            if ($id_payment == 1)
            {
                $bank = 1;
                $data["bank"] = $bank;
                $url = 'merchant_id=' . USER_ID_PAYMENT_TRANFER;
                $url.='&transaction_id=' . $invoices;
                $url.='&amount=' . $total_kirim;
                $url.='&cust_email=' . $data['user']->email;
                $url.='&adm_email=' . ADMIN_EMAIL;
                $url.='&key=' . SECRET_ID_PAYMENT_TRANFER;
                $pay = $this->lib_rest->rest_get(base_url('api/banktranfer/get_payment_tranfer?' . $url));

                $res = json_decode($pay);

                if (!empty($res))
                {
                    if ($res->result == 1000)
                    {
                        $send = TRUE;
                    }
                    else
                    {
                        $data["error"] = "Terjadi kesalahan pada pembayaran.";
                    }
                }
                else
                {
                    $data["error"] = "Cek Kembali pembayaran Anda.";
                }
            }
            else
            {
                $dompet = 1;
                $tokennya = $this->input->post('token');
                $phonenya = $this->input->post('nomer_hp');
                if (!empty($tokennya) && !empty($phonenya))
                {
                    $data["dompetku"] = $dompet;
                    $url = 'amount=' . $total_kirim;
                    $url.='&trans_id=' . $invoices;
                    $url.='&coupon_id=' . $tokennya;
                    $url.='&msisdn=' . $phonenya;
                    $pay = $this->lib_rest->rest_get(base_url('api/dompetku/post_transfer?' . $url));

                    $res = json_decode($pay);

                    if (isset($res->status) && $res->status != 0)
                    {
                        if ($res->status == 1001)
                        {
                            $data["error"] = "Saldo Anda tidak mencukupi.";
                        }
                        elseif ($res->status == 542)
                        {
                            $data["error"] = "Token Anda salah";
                        }
                        elseif ($res->status == 1007)
                        {
                            $data["error"] = "Kode Transaksi Anda salah";
                        }
                        else
                        {
                            $data["error"] = "Telah Terjadi kesalahan.";
                        }
                    }
                    else
                    {
                        $send = TRUE;
                    }
                }
                else
                {
                    $data["error"] = "Mohon isi Field Nomer HP dan Token";
                }
            }
            $test = explode("_", $invoices);
            $invoices = $test[0];
            
            $ongkirs=0;
            if($send)
            {
//                if (!isset($ongkir_ses))
//                {
//                    redirect(base_url('cart/billing'));
//                }
                $m = 0;
                $total = 0;
                $ongkir = 0;
                $id_news = 0;
                $index = 1;
                $urut = 0;
                foreach ($cart as $kunci => $value)
                {
                    $key = substr($data['user']->firstname, 0, 3) . date('ymd');
                    $id = $this->cart_m->get_new_id_order($key);

                    $id_news = (int) $id->jumlah;
                    $id_news+=$index;
                    $id_news = 10000 + $id_news;
                    $kode_order = strtoupper($key . '-' . substr($id_news, 1, 4));
                    $i = 0;
                    $berat = 0;
                    $subtotal = 0;
                    foreach ($value as $row)
                    {
                        $berat+=($row['berat'] * $row['qty']);
                        $subtotal+=$row['subtotal'];
                        $i++;
                    }
//                    $pecah_ongkir = explode(" ", $ongkir_ses[$m]);
                    
                    $ses_ongkir = $this->session->userdata('ongkir' . $kunci);
                    $ongkirs+=$ses_ongkir['ongkir'];
                    $order = array(
                        'id_order' => null,
                        'kode_order' => $kode_order,
                        'total' => $subtotal,
                        'kode_invoice' => $invoices,
                        'id_user' => $data['user']->id_user,
                        'id_merchant' => $kunci,
                        'id_payment' => $id_payment,
                        'id_ongkir' => null,
                        'ongkir_sementara' => $ses_ongkir['ongkir'],
                        'paket_ongkir' => $ses_ongkir['paket'],
                        'keterangan' => $this->input->post('keterangan'),
                        'status_payment' => 'waiting',
                        'status_delivery' => 'persiapan pengiriman',
                        'view_notif' => 'N',
                        'date_added' => date('Y-m-d H:i:s'),
                        'date_modified' => date('Y-m-d H:i:s')
                    );
                    $insert = $this->cart_m->insert('tbl_order', $order);
                    if ($insert)
                    {
                        if ($this->session->unset_userdata('ongkir'.$kunci))
                        {
                        $this->session->unset_userdata('ongkir'.$kunci);
                        }
                        
                        $alamat = array(
                            'id_orderShipping' => null,
                            'nama' => $ses[$urut]['nama'],
                            'alamat' => $ses[$urut]['alamat'],
                            'telpon' => $ses[$urut]['telpon'],
                            'email' => $ses[$urut]['email'],
                            'id_provinsi' => $ses[$urut]['id_provinsi'],
                            'id_kota' => $ses[$urut]['id_kota'],
                            'id_kecamatan' => $ses[$urut]['id_kecamatan'],
                            'date_added' => date('Y-m-d H:i:s'),
                            'date_modified' => date('Y-m-d H:i:s'),
                            'id_order' => $insert
                        );
                        $urut++;
                        $insert_alamat = $this->cart_m->insert('tbl_ordershipping', $alamat);
                        if ($insert_alamat)
                            $this->session->unset_userdata('alamat' . $kunci);
                        $jml_barang = 0;
                        foreach ($value as $items)
                        {
                            $prod = $this->cart_m->get_single('tbl_produk', 'id_produk', $items['id']);
                            $order_detail = array(
                                'id_orderItem' => null,
                                'id_order' => $insert,
                                'id_produk' => $items['id'],
                                'harga' => $items['harga'],
                                'diskon' => $items['discount'],
                                'jml_produk' => $items['qty'],
                                'total' => $items['subtotal'],
                                'date_added' => date('Y-m-d H:i:s'),
                                'date_modified' => date('Y-m-d H:i:s')
                            );
                            $this->cart_m->insert('tbl_orderitem', $order_detail);
                            $stok_update = $prod->stok_produk - $items['qty'];
                            $this->cart_m->update('tbl_produk', 'id_produk', $items['id'], array('stok_produk' => $stok_update));
                        }
                    }
                    $m++;
                }
                $invoice_ins = array(
                    'id_invoice' => null,
                    'kode_invoice' => $invoices,
                    'id_user' => $data['user']->id_user,
                    'id_payment' => $id_payment,
                    'keterangan' => $this->input->post('keterangan'),
                    'total' => $grand_total,
                    'ongkir' => $ongkirs,
                    'payment_fee' => $nilai_admin['option_value'],
                    'status_payment' => 'waiting',
                    'view_notif' => 'N',
                    'date_added' => date('Y-m-d H:i:s'),
                    'date_modified' => date('Y-m-d H:i:s')
                );
                
                $insert_invo = $this->cart_m->insert('tbl_invoices', $invoice_ins);
                
                $test = explode("_", $invoices);
                $transid = $test[0];

                $single = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $transid);

                $user = $this->auth_m->get_user();
                $email = '<style>* {font-family:Arial, serif;font-size:12px} table tr td {padding:2px 5px;} tr.judul td {font-weight:bold;}</style>';
                
                $payment = $this->payment_m->get_payment($invoices, $id_payment);
                
                if ($id_payment == 5)
                {
                    $this->payment_m->update('tbl_payment_buyers', 'kode_order', $invoices, array('paid' => 1));
                    $this->order_m->update('tbl_invoices', 'kode_invoice', $transid, array('status_payment' => "Done"));
                    $this->order_m->update('tbl_order', 'kode_invoice', $single->kode_invoice, array('status_payment' => "Done"));
                }

                $email = "";
                $kabupaten = $this->user_m->get_single("tbl_kabupaten", 'id_kabupaten', $data['user']->id_kabupaten);
                $propinsi = $this->user_m->get_single("tbl_propinsi", 'id_propinsi', $user->id_propinsi);
                if ($id_payment == 1)
                {
                    $email .= "<h3>Bapak/Ibu " . $user->firstname . " " . $user->lastname . " Yth.</h3>
                <br />
                <p>
                    Terima kasih Anda telah berbelanja melalui CipikaStore. Silahkan lakukan pembayaran Biaya Transaksi sesuai nilai yang tertera dalam Invoice, ke rekening CipikaStore berikut:
                </p>
                <table style='margin: 2em 0;'>
                    <tr>
                        <td style='width: 200px'>Nama Bank</td>
                        <td style='width: px'>:</td>
                        <td><strong>Bank Permata (kode bank 013)</strong></td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>Nomor Rekening (VAN ID)</td>
                        <td style='width: 10px'>:</td>
                        <td><strong>" . $payment . "</strong></td>
                </tr>
                </table>";
                    $waktu_exp = date('d-m-Y H:i:s', mktime(date('H') + 4, date('i'), date('s'), date('m'), date('d'), date('Y')));
                    $email .= "
                <p>
                    Pembayaran dapat Anda lakukan melalui Jaringan <strong>ATM Bersama</strong>, <strong>PRIMA</strong>, dan <strong>ALTO</strong>. Anda juga diperkenankan memanfaatkan kemudahan fasilitas E-Banking yang dilengkapi menu <strong>Transfer Bank Online</strong>. <br>
                    Pastikan Anda menyelesaikan pembayaran sebelum " . $waktu_exp . ". Apabila melebihi periode tersebut, maka Virtual Account (VAN ID) secara otomatis ditutup dan transaksi tidak diproses lebih lanjut.
                </p>
                <table style='margin: 2em 0;'>
                    <tr>
                        <td style='width: 200px'>Invoice No</td>
                        <td style='width: px'>:</td>
                        <td><strong>" . $invoices . "</strong></td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>Nama</td>
                        <td style='width: px'>:</td>
                        <td>" . $user->firstname . " " . $user->lastname . "</td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>Alamat</td>
                        <td style='width: 10px'>:</td>
                        <td><strong>" . $user->alamat . " , ".  ucfirst(strtolower($kabupaten->nama_kabupaten)).", ".ucfirst($propinsi->nama_propinsi)."</strong></td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>No Telp</td>
                        <td style='width: 10px'>:</td>
                        <td><strong>" . $user->telpon . "</strong></td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>No Handphone</td>
                        <td style='width: 10px'>:</td>
                        <td><strong>" . $user->hp . "</strong></td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>Total Harga</td>
                        <td style='width: 4px'>:</td>
                        <td><strong>Rp " . $this->cart->format_number($single->total + $single->ongkir+ $single->payment_fee) . "</strong></td>
                    </tr>
                    <tr>
                        <td style='width: 200px'>Tanggal</td>
                        <td style='width: 4px'>:</td>
                        <td><strong>" . date('d-m-Y H:i:s') . "</strong></td>
                    </tr>

                    </table>";
                }
                else
                {
                    $email .= "
                        <h3>Terima Kasih Telah Berbelanja di Cipika Store</h3>
            <br />
            <p>
                <strong>Bapak/Ibu Yth.</strong><br />
                Terima kasih Anda telah berbelanja melalui Cipika Store. Apabila saldo yang ada pada Akun DompetKu anda mencukupi, maka secara otomatis pemesanan akan di proses lebih lanjut.
            </p>
            <table style='margin: 2em 0;'>
                <tr>
                    <td style='width: 200px'>Nomer Akun DompetKU</td>
                    <td style='width: px'> : </td>
                    <td>" . $phonenya . "</td>
                </tr>
                <tr>
                    <td style='width: 200px'>Jumlah yang sudah terbayar</td>
                    <td style='width: 4px'> : </td>
                    <td><strong> Rp " . $this->cart->format_number($single->total + $single->ongkir + $single->payment_fee) . "</strong></td>
                </tr>
            </table>";
                }
                $email .= "<hr />";
                $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $invoices);
                foreach ($orderdetail as $value)
                {
                    $shipping_info = $this->cart_m->get_order_shipping($value->id_order);
                    $email .= "<table width='100%'>
                            <tr>
                                <td width='40%'>Merchant: " . ucwords($this->cart_m->get_merchant($value->id_merchant)->nama_store) . "</td>
                                <td width='40%'>Nomor Transaksi: " . $value->kode_order . "</td>
                                <td width='20%'>Status: Success</td>
                            </tr>
                        </table>";
                    $email .= "<table width='100%'>
                            <tr>
                                <td width='40%'>
                                    <strong>Alamat Pengiriman</strong><br /><span></span>
                                    " . ucwords($shipping_info->nama) . "<br />
                                    " . ucwords($shipping_info->alamat) . "<br>
                                    " . ucwords($shipping_info->nama_kabupaten) . " - " . ucwords($shipping_info->nama_propinsi) . "<br>
                                    " . $shipping_info->telpon . "<br />
                                </td>
                                <td width='40%'>
                                    <strong>Metode Pengiriman</strong><br />
                                    " . ucwords($value->paket_ongkir) . "
                                </td>
                               <td width='20%'>
                                    <strong>Metode Pembayaran</strong><br>";
                    $id_paymentStr = $this->payment_m->get_det_payment($id_payment);
                    $email.= $id_paymentStr->nama_payment;
                    $email .= "</td>
                            </tr>
                            </table><br>";
                    $sub_total = 0;
                    $email .= '<table style="border-collapse:collapse; width:100%;">
                <tr style="background:#666666; color:white;" class="judul"><td width="10%" style="text-align:center">No</td><td>Nama Produk</td><td width="10%" style="text-align:right">Jumlah</td><td width="15%" style="text-align:right">Harga</td><td width="10%" style="text-align:right">Diskon</td><td width="15%" style="text-align:right">Harga setelah diskon</td><td width="20%" style="text-align:right">Subtotal</td></tr>';
                    $itemorder = $this->cart_m->get_order_item(md5($value->id_order));
                    $i = 0;
                    $totalAll=0;
                    foreach ($itemorder as $items)
                    {
                        if ($items->diskon > 0){
                            $harga_diskon = $items->harga - ($items->harga) * ($items->diskon / 100);
                        }else{
                            $harga_diskon = $items->harga;
                        }
                        $total_sub = $harga_diskon * $items->jml_produk;
                        $i++;
                        $email .= '<tr style="text-align:center"><td>' . $i . '</td> 
                        <td style="text-align:left"><a href="' . base_url() . $this->config->slash_item('index_page') . 'product/detail/' . $items->id_produk . '" >' . $items->nama_produk . '</a></td>
                        <td style="text-align:right">' . $items->jml_produk . '</td>
                        <td style="text-align:right">Rp. ' . $items->harga . '</td>                        
                        <td style="text-align:right">' . $items->diskon . '%</td>
                        <td style="text-align:right">Rp. ' . $harga_diskon . '</td>
                        <td style="text-align:right">Rp. ' . $this->cart->format_number($total_sub) . '</td>
                    </tr>';

                        $sub_total += $total_sub;
                    }
                    $email .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Sub Total</td><td >Rp. ' . $this->cart->format_number($sub_total) . '</td></tr>';
                    $email .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Ongkos Kirim</td><td >Rp. ' . $this->cart->format_number($value->ongkir_sementara) . '</td></tr>';
                    $total_harga = $sub_total + $value->ongkir_sementara;
                    $email .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Total</td><td >Rp. ' . $this->cart->format_number($total_harga) . '</td></tr>            
                </table><div style="height:10px;"></div>';
                    $totalAll+=$total_harga;
                }
                $totalPrint = $totalAll + $single->payment_fee;

                if((int)$single->payment_fee > 0) {

                    $email .= "<table width='100%'>
                <tr>
                    <td width='80%' style='text-align: right;'>Convenience Fee</td>
                    <td width='20%' style='text-align: right;'>Rp " . $this->cart->format_number($single->payment_fee) . "</td>
                </tr>
            </table>";
                }
                $email .= "<table width='100%'>
                <tr>
                    <td width='80%' style='text-align: right;'>Grand Total </td>
                    <td width='20%' style='text-align: right;'>Rp " . $this->cart->format_number($single->total+$single->ongkir+$single->payment_fee) . "</td>
                </tr>
            </table>";

                $email .= '<br/><p>Terima Kasih,<br/><br/><br/>
                    <strong>Cipika Store &trade;</strong><br>
                    <a href="' . base_url() . '">www.cipikastore.com</a>
                    </p>
                    </div>
                    ';
                $config = $this->_mail_win();
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->config->item('email_from'), 'Cipika Store');
                $this->email->to($user->email);
                $this->email->subject('Informasi order di Cipika Store');
                $this->email->message($email);
                $send = $this->email->send();

                redirect(base_url('cart/confirm/' . md5($insert_invo)));
            }
        }
        $data['payments'] = $this->payment_m->get_option('cost_payment');
        $data['propinsi'] = $this->cart_m->get_all_data_order('tbl_propinsi', 'id_propinsi', 'asc');
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');

        $data['cart'] = $cart;
//        $data['ongkir_sementara'] = $in['ongkir_sementara'];

        $this->load->view('publik/checkout_payment_v', $data);
    }

    public function billing(){
        $data = array();
        $data['user'] = $this->cart_m->get_user($this->session->userdata('member')->id_user);

        $data['payment'] = $this->cart_m->get_all_data('tbl_payment', 'id_payment', 'asc');
        
        $berat = 0;
        $cart = $this->cart->contents();
        $grouped = array();
        foreach ($cart as $c)
        {
            if (!isset($grouped[$c['id_merchant']]))
            {
                $grouped[$c['id_merchant']] = array();
            }
            $grouped[$c['id_merchant']][] = $c;
        }
        $cart = $grouped;
        $data['cart'] = $grouped;
        $grand_total = 0;
        foreach ($cart as $key => $value)
        {
            $subtotal = 0;
            foreach ($value as $row)
            {
                $subtotal+=$row['subtotal'];
            }
            $grand_total += $subtotal;
        }
        if ($grand_total < $this->config->item('minimal_order'))
            redirect(base_url() . 'cart?error=minimal_order');
        if ($data['user']->firstname == null || $data['user']->alamat == null || $data['user']->id_propinsi == null || $data['user']->id_kabupaten == null || $data['user']->id_kecamatan == null || $data['user']->hp == null || $data['user']->email == null)
            redirect(base_url() . 'cart/alamat');

//        $_SESSION['post'] = "";
        
        if($this->input->post('submit')){
            foreach ($cart as $key => $value)
            {
                 $this->session->unset_userdata('ongkir'.$key);
                 $ongkir = explode( " " , $this->input->post('ongkir_sementara'.$key) );
                 $array = array(
                    'ongkir' => $ongkir[0],
                    'paket' => "JNE ".$ongkir[1]
                );
                $this->session->set_userdata('ongkir' . $key, $array);
            }
            redirect(base_url() . 'cart/billing_payment');
        }
        
        $data['payments'] = $this->payment_m->get_option('cost_payment');
        $data['propinsi'] = $this->cart_m->get_all_data_order('tbl_propinsi', 'id_propinsi', 'asc');
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/checkout_v', $data);
    }

    public function confirm($id = '')
    {
        if ($this->cart->total_items() == 0 || $id == '')
        {
            redirect(base_url());
        }
        $data = array();
        $data['id'] = $id;

        //cart group by merchant
        $cart = $this->cart->contents();

        $grouped = array();
        foreach ($cart as $c)
        {
            if (!isset($grouped[$c['id_merchant']]))
            {
                $grouped[$c['id_merchant']] = array();
            }
            $grouped[$c['id_merchant']][] = $c;
        }
        $cart = $grouped;
        $data['cart'] = $grouped;

        $data['order'] = $this->cart_m->get_invoice($id);
        $data['orderdetail'] = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $data['order']->kode_invoice);
        $data['user'] = $this->user_m->get_single('tbl_user', 'id_user', $data['order']->id_user);
        $data['order_item'] = $this->cart_m->get_order_item($id);
        
        $data['kabupaten']=$this->user_m->get_single("tbl_kabupaten", 'id_kabupaten', $data['user']->id_kabupaten);
        $data['propinsi']=$this->user_m->get_single("tbl_propinsi", 'id_propinsi', $data['user']->id_propinsi);
      
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $data['nilai_admin'] = $this->payment_m->get_option('cost_payment');

        $data['payment'] = $this->payment_m->get_payment($data['order']->kode_invoice, $data['order']->id_payment);

        $single = $this->payment_m->get_single('tbl_invoices', 'kode_invoice', $data['order']->kode_invoice);
            
        $transid= $single->kode_invoice;
        
        if ($data['order']->id_payment == 5 && $data['payment']->payment == 'DOMPETKU' && $data['payment']->paid == 1)
        {
            $inputan_shipping=array();
            $inputan_merchant=array();
            $order_items=array();
            
            $this->load->database();
            $orderdetail = $this->cart_m->get_data_where('tbl_order', 'kode_invoice', $transid);
            foreach ($orderdetail as $v)
            {
                $nama_store = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                $email = $this->cart_m->get_merchant($v->id_merchant)->email;
                $order_item = $this->cart_m->get_order_item(md5($v->id_order));
                $shipping = $this->cart_m->get_order_shipping($v->id_order);

                $inputan_shipping[$v->id_order] = $this->cart_m->get_order_shipping($v->id_order);
                $inputan_merchant[$v->id_merchant] = $this->cart_m->get_merchant($v->id_merchant)->nama_store;
                $order_items[$v->id_order] = $this->cart_m->get_order_item(md5($v->id_order));

                $send = new \Cipika\View\Helper\EmailPoMerchant($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
                $sending = $send->sends($email, $v, $shipping, $nama_store, $order_item, $this->config->slash_item('index_page'));
            }

            $nama_payment = $this->payment_m->get_single('tbl_payment', 'id_payment', $single->id_payment);
            $user = $this->payment_m->get_single('tbl_user', 'id_user', $single->id_user);

            $send_to_buyer = new \Cipika\View\Helper\EmailBuyerPembayaran($this->db, $this->config->item('email_from'), $this->config->item('bcc_email_merchant_pembelian'));
            $sending_to_buyer = $send_to_buyer->sends($user->email, $single, $orderdetail, $inputan_shipping, $inputan_merchant, $order_items, $this->config->slash_item('index_page'), $nama_payment, $user);
        }
        $this->load->view('publik/cart_confirm_v', $data);
        $this->cart->destroy();
    }

    public function add(){
        $id=$this->input->post('id_produk');
        $data_lama=array();
        $sudah_ada=FALSE;

        $produk=$this->cart_m->get_produk($id);
        // var_dump($produk);exit;
        // cek sudah ada apa belum datanya
        foreach($this->cart->contents() as $items){
            if($items['id']==$id){
                $sudah_ada=TRUE;
                $data_lama=$items;
                if($data_lama['qty']==$produk->stok_produk) exit;
            }
        }

        if(!$sudah_ada){
            $nama=$produk->nama_produk;
            $id_merchant=$produk->id_user;
            $jml=1;
            $diskon=$produk->diskon;
            $image=$produk->image;
            $harga=$produk->harga_jual;         
            $stok=$produk->stok_produk;     
            $berat=$produk->jne_berat;
            $dimensi=$produk->panjang*$produk->lebar*$produk->tinggi;
            $price=$produk->harga_jual-($harga*$diskon/100);
            $data=array(
                'id'                => $id,
                'id_merchant'       => $id_merchant,
                'image'             => $image,
                'qty'               => $jml,
                'name'              => $nama,
                'price'             => $price,
                'harga'             => $harga,
                'harga_merchant'    => $produk->harga_produk,
                'berat'             => $berat,
                'stok'              => $stok,
                'dimensi'           => $dimensi,
                'discount'          => $diskon
            );  
            $result = $this->cart->insert($data);
        } else {
            $data=array(
                'rowid' => $data_lama['rowid'],
                'qty' => $data_lama['qty']+1,
                'berat' => $data_lama['berat']+$produk->jne_berat
            );
            $this->cart->update($data);
        }
        echo json_encode(array('qty'=>$this->cart->total_items()));
    }

    public function update(){
        $rowid=$this->input->post('rowid');
        $qty=$this->input->post('qty');

        $data=array(
            'rowid' => $rowid,
            'qty' => $qty
        );
        $this->cart->update($data);
        echo json_encode(array('qty'=>$this->cart->total_items()));
    }

    public function delete(){
        $this->cart->destroy();exit;
    }

    public function kota(){
        $id = $_GET['id'];
        $data = $this->cart_m->get_data_where('tbl_kota', 'id_provinsi', $id);
        echo json_encode($data);
    }
    
    public function kabupaten(){
        $id = $_GET['id'];
        $data = $this->cart_m->get_data_where('tbl_kabupaten', 'id_propinsi', $id);
        echo json_encode($data);
    }
    
    public function kecamatan(){
        $id = $_GET['id'];
        $data = $this->cart_m->get_data_where('tbl_kecamatan', 'id_kabupaten', $id);
        echo json_encode($data);
    }

    public function cekjne($from='', $tujuan=''){
        require_once 'jne.php';
        $rest = new Jne(array(
            'server' => 'http://api.ongkir.info/'
        ));   
        $result = $rest->post('cost/find', array(
            'from'  => strtoupper($from),
            'to'        => strtoupper($tujuan),
            'weight'    => 1000, 
            'courier'   => 'jne',
            'API-Key'   => 'b669d36c28c480da8117ec0f58752f84'
        ));
        $hasil = '';
        try   {
            $status = $result['status'];            
            if ($status AND $status->code == 0){
                $hasil = $result['price'];
            }
            else{
                $hasil = 'gagal';
            }
        }
        catch (Exception $e){
            $hasil = 'gagal';
        }
        return $hasil;
    }
    
    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
