<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_m');
        $this->load->model('user_m');
        $this->load->driver('cache');
	}

	//redirect if needed, otherwise display the user list
	function index()
	{

	}
	//log the user in
	function login()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email Or Phone Number', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			$remember = (bool) $this->input->post('remember');
			if ($this->auth_m->login($this->input->post('email'), $this->input->post('password'), $remember))
			{
				// redirect('/', 'refresh');
				$out=array('status'=>1, 'message'=>'Login successfully');
			}
                        // else if($this->auth_m->cek_email($this->input->post('email'))){
                        //     $out=array('status'=>0, 'message'=>'Password Salah!');
                        // }
			else
			{
				$out=array('status'=>0, 'message'=>'Username atau password yang anda masukkan salah!');
			}
		} else {
			$out=array('status'=>0, 'message'=>'Field tidak boleh kosong');
		}
		echo json_encode($out);
	}
        
    function login_fb() {
        $konek = $this->lib_facebook->connect();
        if (isset($konek['user_profile'])) {
            if ($konek['user']) {

                $user = $this->user_m->get_email_user($konek['user_profile']['email']);
                if (!empty($user)) { /* registered, force login */
//                    echo 'adsd';
//                    $remember = 1;
                    if ($this->auth_m->login_fb($user->email))
                          redirect(base_url());
                } else {
                    $password = random_password();
                    $email = $konek['user_profile']['email'];
                    if(empty($konek['user_profile']['username'])){
                        $username = $email;
                    }else{
                        $username = $konek['user_profile']['username'];
                    }
                    $input1 = array(
                        "username" => $username,
                        "email" => $email,
                        "password" => md5($password),
                        "firstname" => $konek['user_profile']['first_name'],
                        "lastname" => $konek['user_profile']['last_name'],
                        "fb_token" => $konek['token'],
                        "active" => 1
                    );

                    $this->user_m->insert('tbl_user', $input1);
                    if ($this->auth_m->login_fb($email)){
                           redirect(base_url());
                    }
                        
                }
            }
        } else {
            redirect(base_url());
        }
    }

    //log the user out
	public function logout(){
		$this->session->unset_userdata('member');
		$this->session->sess_destroy();	
        $this->lib_facebook->destroy_fb();
        $this->cache->clean();
     	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
     	header("Expires: Wed, 4 Jul 2012 05:00:00 GMT"); // Date in the past
    	redirect(base_url(), 'refresh');
	}

	//forgot password
	function forgot_password()
	{
		$data=array();
		$data['error']='';
		$data['success']='';
		if($this->input->post('submit')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Email', 'required');
			if ($this->form_validation->run() == false)
			{
				$data['error'] = validation_errors();
			}
			else
			{
				$email=$this->input->post('email');
				$identity = $this->auth_m->cek_email($email);
				
            	if($identity==0) {
		        	$data['error']='Email tidak terdaftar.';
        		} else {
					$new_pass=$this->randomPassword();
					$insert=array(
						'password'=>md5($new_pass)
					);
					$this->auth_m->update('tbl_user', 'email', $email, $insert);

					$data_email='<p>Password Anda telah direset, berikut informasi login Anda yang baru: </p>';
					$data_email.='<ul><li>Email: '.$email.'</li><li>Password: '.$new_pass.'</li></ul>';

					$config=$this->_mail_win();
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from($this->config->item('email_from'), 'Cipika Store');
					$this->email->to($email);
					// $this->email->bcc($this->config->item('email_tujuan'));
					$this->email->subject('Lupa Password');
					$this->email->message($data_email);
					$send = $this->email->send();

					if($send){$data['success']='Password Anda telah berhasil direset, silakan cek email Anda.';}
				}
			}
		}

		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/forgot_password_v', $data);
	}
    
    /* Kirim link untuk reset password */
    function reset_password()
    {
        $email              = $this->input->post('email');
        $data['error']      = '';
        $data['success']    = '';
        
        $this->session->unset_userdata('member');
		$this->session->sess_destroy();	
        $this->lib_facebook->destroy_fb();
        $this->cache->clean();
        
        if($email!=''){
            $this->load->library('lib_mailer');
            $user   = $this->auth_m->get_single('tbl_user','email',$email);
            if(!empty($user)){
                
                //# Save reset password request to meta table
                $now    = date('Y-m-d H:i:s');
                $meta   = array('idmeta'            => null,
                                'meta_master_table' => 'tbl_user',
                                'meta_idmaster'     => $user->id_user,
                                'meta_name'         => 'reset_password_link_expired',
                                'meta_value'        => $now
                                );
                $idmeta = $this->auth_m->insert('meta', $meta);
                
                $mail_data  = array('idmeta' => $idmeta,'expired' => time(),'user'  => $user);
                $message    = $this->load->view('email/lupa_password_v',$mail_data,TRUE);
                $mailer     = array('module'    => 'Forgot Password',
                                    'from'      => $this->config->item('email_from'),
                                    'to'        => $email,
                                    'subject'   => "Lupa Password Cipika Store",
                                    'message'   => $message
                                    );
                $idmailer = $this->lib_mailer->save($mailer);                
            
                $data['success']    = "Periksa email anda untuk mengganti password.";
            }else{
                $data['error'] = "Email tidak terdaftar.";
            }
        }else{
            $data['error'] = "Email tidak boleh kosong.";
        }
        
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/forgot_password_v', $data);
    }
    
    /* Form ganti password dari link lupa password email */
    function new_password($idmeta='',$time='')
    {
        $data['error']      = '';
        $data['success']    = '';
        $data['form']       = true;
        $data['idmeta']     = $idmeta;
        $data['time']       = $time;
        
        $this->session->unset_userdata('member');
		$this->session->sess_destroy();	
        $this->lib_facebook->destroy_fb();
        $this->cache->clean();
    
        if(($idmeta!='') && ($time!='')){
        
            $meta   = $this->auth_m->get_single('meta','idmeta',$idmeta);
            if(!empty($meta)){
                $today      = date('Y-m-d H:i:s');
                $expired    = $meta->meta_value;
                $hourdiff   = round((strtotime($today) - strtotime($expired))/3600, 1);
                if($hourdiff<24){
                    if($this->input->post('submit')){
                        
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('password', 'Password', 'required|matches[password2]|min_length[6]');
                        $this->form_validation->set_rules('password2', 'Konfirmasi Password', 'required');
                        
                        $this->form_validation->set_message('required', '%s tidak boleh kososng.');
                        $this->form_validation->set_message('matches', '%s tidak sama.');
                        $this->form_validation->set_message('min_length', '%s minimal 6 karakter.');
                        
                        if ($this->form_validation->run() == FALSE){
                            $data['error'] = validation_errors();
                        }else{
                            //# update password database
                            $user    = $this->auth_m->get_single('tbl_user','id_user',$meta->meta_idmaster);
                            $insert=array(
                                'password'=>md5($this->input->post('password'))
                            );
                            $this->auth_m->update('tbl_user', 'email', $user->email, $insert);
                            
                            //# Send konfirmasi email password sudah terganti                            
                            $this->load->library('lib_mailer');
                            $message    = $this->load->view('email/new_password_v',null,TRUE);
                            $mailer     = array('module'    => 'New Password',
                                                'from'      => $this->config->item('email_from'),
                                                'to'        => $user->email,
                                                'subject'   => "Password Baru Cipika Store",
                                                'message'   => $message
                                                );
                            $idmailer = $this->lib_mailer->save($mailer);                
                        
                            $data['form']       = false;
                            $data['success']    = "Password anda telah berhasil di ganti, periksa email anda.";
                        }
                        
                    }
                }else{
                    $data['form']  = false;
                    $data['error'] = "Maaf link ganti password sudah tidak aktif lagi. <a href='".base_url('auth/forgot_password')."'>Klik di sini jika ingin mengirim link lupa password yang baru.</a>";
                }
            }else{
                $data['error'] = "Permintaan reset password tidak valid.";
            }        
        }else{
            $data['error'] = "Permintaan reset password tidak valid.";
        }
        
        $data['page'] = $this->auth_m->get_data_where('tbl_page', 'publish', '1');
        $this->load->view('publik/new_password_v', $data);
    }

	function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}

	public function register()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|min_length[6]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_user.email]');
        
        $this->form_validation->set_message('matches', 'Password tidak sama.');
        $this->form_validation->set_message('required', '%s tidak boleh kosong');
        $this->form_validation->set_message('valid_email', 'Alamat email tidak valid');
        $this->form_validation->set_message('is_unique', 'Alamat email sudah terdaftar');
        $resp = $this->lib_recaptcha->auth_recaptcha($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        
        $data['error_captcha'] = false;
        if (!$resp->is_valid) {
                    // What happens when the CAPTCHA was entered incorrectly
                    //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
                    //"(reCAPTCHA said: " . $resp->error . ")");
                    //$data['error'] = "CAPTCHA Yang anda masukan salah. Reload CAPTCHA dan coba lagi.";
                    $data['error'] = "Kode captcha anda salah, coba lagi.";
                    $data['error_captcha'] = true;
                    $out=array('status'=>0, 'message'=>$data['error'] );
        }else{
                  // Your code here to handle a successful verification
            if ($this->form_validation->run() == FALSE){
                $data['error'] = validation_errors();
                $out=array('status'=>0, 'message'=>$data['error'] );
                // $this->load->view('publik/home_v', $data);
            }else{
                // $key = preg_replace('/([^@]*).*/', '$1', $this->input->post('email'));
                // $username=$this->user_m->get_username(substr($key,0,3));
                $kode_random = random_string('alnum', 21);
                $input=array(
                    'id_user' => null,
                    // 'username' => 'Anonymous',
                    'password' => md5($this->input->post('password')),
                    'email' => $this->input->post('email'),
                    'activation_code' => $kode_random,
                    'id_level' => 6,
                    'date_added' => date('Y-m-d H:i:s')
                );
                $hasil=$this->auth_m->insert('tbl_user', $input);
                $email_dihash = str_replace('@', md5('@'), $this->input->post('email'));
                // $data_email = '<p>Selamat Anda telah berhasil registrasi member Cipikastore.com. Untuk mengaktifkan registrasi Anda silahkan klik link berikut ini.<br>
                // 		<a href="'.base_url().$this->config->slash_item('index_page').'auth/activate/'.$hasil.'/'.$email_dihash.'/'.$kode_random.$this->config->item('url_suffix').'" >Aktivasi Cipikastore.com</a><br>
                // 		Jika link diatas tidak bisa diklik, silahkan copy url berikut ini : '.base_url().$this->config->slash_item('index_page').'auth/activate/'.$hasil.'/'.$email_dihash.'/'.$kode_random.$this->config->item('url_suffix').'
                // 	</p>';
                $data_email = '<strong>Member CipikaStore Yth,</strong>

                    <p>Selamat! Anda berhasil melakukan pendaftaran Member CipikaStore dengan detil informasi sebagai

                    berikut:</p>

                    <table style="border:none;">
                    <tr>
                    <td style="width:150px;">Username</td><td>:</td><td>'. $input['email'] .'</td>
                    </tr>
                    <tr>
                    <td>Link Validasi</td><td>:</td><td>'.base_url().$this->config->slash_item('index_page').'auth/activate/'.$hasil.'/'.$email_dihash.'/'.$kode_random.$this->config->item('url_suffix').'</td>
                    </tr>
                    </table>
                    <p>
                    Segera klik Link di atas untuk validasi akun. Terima kasih.</p>
                    <br>
                    <p>Cipika Store</p>;
                    <p>Semuanya Menjadi Mudah</p>';
                // $config = ($this->config->item('email_win')) ? $this->_mail_win() : $this->_mail_unix() ;
                $config=$this->_mail_win();
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->config->item('email_from'), 'Cipika Store');
                $this->email->to($input['email']);
                // $this->email->bcc($this->config->item('email_tujuan'));
                $this->email->subject('Aktivasi registrasi member cipikastore.com');
                $this->email->message($data_email);
                $send = $this->email->send();
                //if($send) {$out=array('status'=>1,'message'=>base_url().$this->config->slash_item('index_page').'auth/activate'.$this->config->item('url_suffix'));}
                if($send){$out=array('status'=>1,'message'=>base_url().$this->config->slash_item('index_page').'auth/activate/'.$hasil.'/'.$email_dihash.$this->config->item('url_suffix'));}
            }
        }
		
	    echo json_encode($out);
	}

	public function activate($id_user='', $email='', $kode_aktivasi=''){
		if($id_user=='' OR $email=='') redirect(base_url(), 'refresh');
		$data = array();		
		$data['aktivasi_berhasil'] = FALSE;
		if($kode_aktivasi==''){
			$data['belum_diaktifkan'] = TRUE;
			$data['email']=str_replace(md5('@'), '@', $email);
		}
		else {
			$data_update = array(
					'id_user'			=> (int)$id_user,
					'email'			=> $email,
					'kode_aktivasi'	=> $kode_aktivasi,
					'status_aktif'	=> 1,
				);
			$data['aktivasi_berhasil'] = $this->auth_m->update_registrasi($data_update);
			$data['aktivasi_berhasil']; 
            
            //# Set auto logged in
            $user   = $this->auth_m->get_single('tbl_user','id_user',(int)$id_user);
            $this->session->set_userdata('member', $user);
		}
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/user_confirm_v', $data);
	}
    
    function _mail_win()
    {
        $config = array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }

    /* AJAX check valid email */
    function check_email()
    {   
        $this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_user.email]');
        
        $this->form_validation->set_message('valid_email', 'Alamat email tidak valid');
        $this->form_validation->set_message('is_unique', 'Alamat email sudah terdaftar');
        
        if ($this->form_validation->run() == FALSE){
            echo validation_errors();
        }else{            
            echo 1;
        }
    }
    
    /* AJAX check valid password */
    function check_password()
    {   
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		
        $this->form_validation->set_message('matches', 'Password tidak sama.');
        $this->form_validation->set_message('required', '%s tidak boleh kosong');
        
        if ($this->form_validation->run() == FALSE){
            echo validation_errors();
        }else{            
            echo 1;
        }
    }
    
    function tes()
    {
        log_message('error', 'Some variable did not contain a value.');
    }
}
