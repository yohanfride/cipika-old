<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

    public function cancel_order(){
        $date=date('y-m-d H:i:s');
        // echo $date;exit;
        $q=mysql_query("SELECT * from tbl_order 
            WHERE TIMESTAMPDIFF(HOUR, date_added, '". $date ."') > 4 AND status_payment = 'waiting'");
        // var_dump($q);exit;

                //database transaction
        $this->db->trans_begin();
        while($a=mysql_fetch_array($q)){
            // print_r($a);
            $u=mysql_query("update tbl_order set status_payment='canceled' where id_order='". $a['id_order'] ."'");
            // echo $a['id_order'];
            $q2=mysql_query("select * from tbl_orderitem where id_order='". $a['id_order'] ."'");
            while($a2=mysql_fetch_array($q2)){
                $p=$this->cart_m->get_single('tbl_produk', 'id_produk', $a2['id_produk']);
                if($p){
                    $stok=$p->stok_produk + $a2['jml_produk'];
                    $u2=mysql_query("update tbl_produk set stok_produk='". $stok ."' where id_produk='". $a2['id_produk'] ."'");
                }
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            
        }
        else
        {
            $this->db->trans_commit();
        }

    }

	public function autoemail()
	{
            $this->load->model('order_m');
            
            $result = $this->order_m->get_order_status_deliver();
            foreach ($result as $row) {
                
                //kirim email konfirmasi
            $email ='<style>* {font-family:Arial, serif;font-size:12px} table tr td {padding:2px 5px;} tr.judul td {font-weight:bold;}</style>';
            $email .='<p>Hallo '.$row->username.'<br /><br />
                    Terima kasih telah melakukan order di Cipika Store, 
                    Proses order telah selesai.<br>
                    Berikut adalah informasi order pembelian Anda :</p><div style="height:10px;"></div><strong><h4>Kode Order : '.$row->kode_order.'</h4></strong><div style="height:10px;"></div>';

            $email .= '<p><a href="'.base_url('user/confirm_product/'.md5($row->id_order)).'">Konfirmasi Produk</a></p>';
            $email .= '<p><a href="'.base_url('user/confirm_product/'.md5($row->id_order)).'">'.base_url('user/confirm_product/'.md5($row->id_order)).'</a></p>';
            $email .= '<p>If clicking the link above does not work, please try copying and pasting the URL into a new browser window.</p>';
            $email .= '<p>jika menuju ke halaman Home, Harap Login Terlebih Dahulu</p>';
            $email .= '
                    <div style="margin:10px 0 0;"><p>Jangan dibalas ( reply ) email ini karena tidak akan dibaca.<br/>
                    Jika ada yang mau disampaikan hubungi Customer Service kami via YM, Telp / SMS pada hari &amp; jam Kerja.</p>
                    <br/><p>Terima Kasih,<br/><br/><br/>
                    <strong>Cipika Store &trade;</strong><br>
                    <a href="'.base_url().'">www.cipikastore.com</a>
                    </p>
                    </div>
                    ';
                
                // $config = ($this->config->item('email_win')) ? $this->_mail_win() : $this->_mail_unix() ;
                $config=$this->_mail_win();
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->config->item('email_from'), 'Cipika Store');
                $this->email->to($row->email);
                // $this->email->bcc($this->config->item('email_tujuan'));
                $this->email->subject('Product Confirmation');
                $this->email->message($email);
                $send = $this->email->send();
                
                 $input = array(
                    'email_confirm' => 1
                );
                $insert = $this->order_m->update('tbl_order', 'id_order', $row->id_order, $input);
//            echo $row->email;
            }
	}
        
        public function reminderemail()
	{
            $this->load->model('order_m');
            $this->load->model('payment_m');
            $nilai_admin = $this->payment_m->get_option('cost_payment');
            
            
            $result = $this->order_m->get_order_reminder();
            foreach ($result as $row) {
                $payment=$this->payment_m->get_single('tbl_payment_buyers', 'kode_order', $row->kode_order)->respond;
                
                $email = '<h3>Terima Kasih Telah Berbelanja di Cipika Store</h3>';
                $email .= '<br />';
                $email .= '<p>
                            <strong>Bapak/Ibu Yth.</strong><br />
                            Terima kasih Anda telah berbelanja melalui Cipika Store. Silahkan lakukan pembayaran Biaya Transaksi sesuai nilai yang tertera dalam Invoice, ke rekening Cipika Store berikut:
                        </p>';
                $email .= '<table style="margin: 2em 0;">
                            <tr>
                                <td style="width: 200px">Nama Bank</td>
                                <td style="width: px">:</td>
                                <td>Bank Permata (kode bank 013)</td>
                            </tr>
                            <tr>
                                <td style="width: 200px">Nomor Rekening (VAN ID)</td>
                                <td style="width: 10px">:</td>
                                <td><strong>'.$payment.'</strong></td>
                            </tr>
                            <tr>
                                <td style="width: 200px">Jumlah yang harus ditransfer</td>
                                <td style="width: 4px">:</td>
                                <td><strong>Rp '.$this->cart->format_number($row->total+$row->ongkir_sementara+$nilai_admin['option_value']).'</strong></td>
                            </tr>

                        </table>';
                $email .='<p>
                            Pembayaran dapat Anda lakukan melalui Jaringan ATM Bersama, PRIMA, dan ALTO. Anda juga diperkenankan memanfaatkan kemudahan fasilitas E-Banking yang dilengkapi menu Transfer Bank Online.
                        </p>
                        <p>
                            Pastikan Anda menyelesaikan pembayaran dalam waktu <strong>maksimal 4 (empat) jam</strong> terhitung dari anda bertansaksi. Apabila melebihi periode tersebut, maka Virtual Account (VAN ID) secara otomatis ditutup dan transaksi tidak diproses lebih lanjut.
                        </p>
                        <p>
                            Ketentuan pembayaran adalah:
                        </p>
                        <ol>
                            <li>Bila pembayaran kurang dari jumlah biaya yang ditentukan, maka transfer secara otomatis akan ditolak oleh system cipikastore.</li>
                            <li>Harap melakukan pembayaran tepat sesuai jumlah biaya (termasuk biaya pengiriman) yang ditentukan.</li>
                            <li>Pembayaran melebihi jumlah yang ditentukan akan dianggap sukses oleh system cipikastore and tidak bisa dilakukan REFUND.</li>
                        </ol>';
//                $data_email = '<p>Anda telah menerima email konfirmasi pengiriman
//                        </p>';
                // $config = ($this->config->item('email_win')) ? $this->_mail_win() : $this->_mail_unix() ;
                $config=$this->_mail_win();
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->config->item('email_from'), 'Cipika Store');
                $this->email->to($row->email);
                // $this->email->bcc($this->config->item('email_tujuan'));
                $this->email->subject('Email Reminder');
                $this->email->message($email);
                $send = $this->email->send();
                
                 $input = array(
                    'email_confirm' => $row->id_order
                );
                $insert = $this->order_m->update('tbl_order', 'id_order', $row->id_order, $input);
            }
	}
    
    function _mail_win()
    {
        $config = Array(
            'protocol' => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_port' => $this->config->item('smtp_port'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass'),
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset'),
            'smtp_crypto' => $this->config->item('smtp_crypto')
        );
        return $config;
    }

    function _mail_unix()
    {
        $config = array(
            'mailtype' => $this->config->item('mailtype'),
            'charset' => $this->config->item('charset')
        );
        return $config;
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */