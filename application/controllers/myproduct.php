<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myproduct extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_m');
		$this->load->model('auth_m');
		$this->load->model('myproduct_m');
		$this->auth_m->check();
	}

	public function index($offset=0)
	{
		$data=array();
		$data['menu_myproduct']="class='active'";
		$id_user=$this->session->userdata('member')->id_user;
		if(isset($_GET['hal'])){$hal=$_GET['hal'];}
		else {$hal='';}

		$dataPerhalaman=5;
		($hal=='')?$nohalaman = 1:$nohalaman = $hal;
		$offset = ($nohalaman - 1) * $dataPerhalaman;
		$off = abs( (int) $offset);
		$data['offset']=$offset;

		if(!isset($_GET['search'])){
			$jmldata=$this->produk_m->count_product_user('tbl_produk', 'id_user', $id_user);
			$data['paginator']=$this->produk_m->page($jmldata, $dataPerhalaman, $hal);
			$data['produk']=$this->produk_m->get_produk_user($id_user, $dataPerhalaman, $off);
            $data['kategori']=null;
		}else{

			$jmldata=$this->produk_m->count_produk_user_by_search($id_user, $_GET['search'], $_GET['cat'], $dataPerhalaman, $off);
			$data['paginator']=$this->produk_m->page($jmldata, $dataPerhalaman, $hal);
			$data['produk']=$this->produk_m->get_produk_user_by_search($id_user, $_GET['search'], $_GET['cat'], $dataPerhalaman, $off);
			$data['search']=$_GET['search'];
			$data['kategori']=$_GET['cat'];
		}

		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$this->load->view('publik/myproduct_v', $data);
	}

	public function upload(){
		$data=array();
		$data['success']='';
		$data['error']='';
        $data['menu_myproduct']="class='active'";
		$data['kategori']=$this->myproduct_m->get_data_where('tbl_kategori', 'id_parent', '0');
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['provinsi']=$this->produk_m->get_all_data_order('tbl_provinsi', 'nama_provinsi', 'asc');

		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_produk', 'Nama produk', 'required');
			$this->form_validation->set_rules('deskripsi', 'Deskripspi', '');
            $this->form_validation->set_rules('berat', 'Berat', 'required');
			// $this->form_validation->set_rules('panjang', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('lebar', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('tinggi', 'Panjang', 'numeric');
			$this->form_validation->set_rules('stok_produk', 'Stok', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'required');
			$this->form_validation->set_rules('diskon', 'Diskon', '');
			$this->form_validation->set_rules('id_kategori', 'Kategori', 'required|callback__check_category');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'numeric');
            $this->form_validation->set_rules('photo_img', 'Foto Produk', 'callback__check_foto_produk');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
                        $volume = $this->input->post('panjang')*$this->input->post('lebar')*$this->input->post('tinggl');
                        $volume_kon = $volume/6000;
                        if($volume_kon > $this->input->post('berat'))
                            $jne_berat = $volume_kon;
                        else {
                            $jne_berat = $this->input->post('berat');
                        }

                $berat = str_replace(',', '.', $this->input->post('berat'));
                if($berat < 0){
                	$berat = 0;
                }
                $panjang = str_replace(',', '.', $this->input->post('panjang'));
                if($panjang < 0){
                	$panjang = 0;
                }
                $lebar = str_replace(',', '.', $this->input->post('lebar'));
                if($lebar < 0){
                	$lebar = 0;
                }
                $tinggi = str_replace(',', '.', $this->input->post('tinggi'));
                if($tinggi < 0){
                	$tinggi = 0;
                }
                $stok = $this->input->post('stok_produk');
                if($stok < 0){
                	$stok = 0;
                }

				$input=array(
					'id_produk' => null,
					'nama_produk' => $this->input->post('nama_produk'),
					'deskripsi' => $this->input->post('deskripsi'),
					'berat' => $berat,
					'stok_produk' => $stok,
					'harga_produk' => $this->input->post('harga_produk'),
					'diskon' => $this->input->post('diskon'),
					'publish' => 0,
					'view_notif' => 'N',
					'id_user' => $this->session->userdata('member')->id_user,
					// 'id_kota' => $this->input->post('id_kota'),
					'panjang' => $panjang,
					'lebar' => $lebar,
					'tinggi' => $tinggi,
					'shipping_area' => $this->input->post('shipping_area'),
                                        'jne_berat' => $jne_berat,
					'date_added' => date('Y-m-d H:i:s'),
					'date_modified' => date('Y-m-d H:i:s')
				);
				//database transaction
				$this->db->trans_begin();

				$insert=$this->produk_m->insert('tbl_produk', $input);				
//				$inptag=array(
//					'nama_tag' => $this->input->post('nama_tag'),
//					'date_added' => date('Y-m-d H:i:s')
//				);
//		    	$id_tag=$this->produk_m->insert('tbl_tag', $inptag);
//		    	$prod_tag=$this->produk_m->insert('tbl_produk_tag', array('id_produk'=>$insert, 'id_tag'=>$id_tag, 'date_added'=>date('Y-m-d H:i:s')));
				
//                                start afandi
                                
                                if (!empty($_POST['nama_tag'])) {
                                $tag = explode(',', $_POST['nama_tag']);
                                foreach ($tag as $a){
                                    $b = trim($a);
                                    //select tag
                                    $select_tag = $this->db->query("select * from tbl_tag where nama_tag = '".$b."'");
                                    if ($select_tag->num_rows() > 0) {
                                        $result = $select_tag->row();
                                        $idtag = $result->id_tag;
                                    }else{
                                        $insert_tag = array('id_tag' => null,
                                            'nama_tag' => $b,
                                            'date_added' => date('Y-m-d H:i:s'),
                                            'date_modified' => date('Y-m-d H:i:s')
                                        );
                                        $idtag = $this->produk_m->insert('tbl_tag', $insert_tag);
                                    }
                                    //insert tag_relation
                                     $insert_relation = array('id_produk_tag' => null,
                                        'id_produk' => $insert,
                                        'id_tag' => $idtag,
                                        'date_added' => date('Y-m-d H:i:s'),
                                        'date_modified' => date('Y-m-d H:i:s')
                                    );
                                    $this->produk_m->insert('tbl_produk_tag', $insert_relation);
                                }
                            }
                           
//                                end afandi
                                
                if($insert){
                    if(!empty($_POST['photo_img'])){
                                
                        foreach ($_POST['photo_img'] as $foto) {
                            if($foto!=''){
                                $inpdet=array(
                                    'id_produk' => $insert,
                                    'image'	 => $foto,
                                    'date_added' => date('Y-m-d H:i:s'),
                                    'date_modified' => date('Y-m-d H:i:s')
                                );
                                $produk_foto=$this->produk_m->insert('tbl_produkfoto', $inpdet);
                            }
                        }
                    }
					foreach ($_POST['id_kategori'] as $id) {
                        if($id!=''){
                            $prod_kat=array('id_kategori'=>$id, 'id_produk'=>$insert, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
                            $ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
                        }
					}
				}
				// throw new Exception('ghghjgj');
				if ($this->db->trans_status() === FALSE)
				{
				    $this->db->trans_rollback();
				    $data['error']='Produk gagal disimpan.';
				    
				}
				else
				{
				    $this->db->trans_commit();
				    $data['success']='Produk berhasil disimpan. Kami akan melakukan verifikasi dalam waktu 1x24 jam.';
				}

				// if($insert)	$data['success']='Perubahan data telah disimpan. Kami akan melakukan verifikasi dalam waktu 1x24 jam.';
				// else $data['error']='Produk gagal disimpan.';
		    }
		}
		$this->load->view('publik/myproduct_add_v', $data);
	}

	public function edit($id_produk='', $id_user=''){
		$data=array();
		$data['success']='';
		$data['error']='';
        $data['menu_myproduct']="class='active'";

		if($this->input->post('simpan')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_produk', 'Nama produk', 'required');
			$this->form_validation->set_rules('deskripsi', 'Deskripspi', '');
            $this->form_validation->set_rules('berat', 'Berat', 'required');
			// $this->form_validation->set_rules('panjang', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('lebar', 'Panjang', 'numeric');
			// $this->form_validation->set_rules('tinggi', 'Panjang', 'numeric');
			$this->form_validation->set_rules('stok_produk', 'Stok', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'required');
			$this->form_validation->set_rules('diskon', 'Diskon', '');
			$this->form_validation->set_rules('id_kategori', 'Kategori', 'required');
			$this->form_validation->set_rules('harga_produk', 'Harga', 'numeric');
            $this->form_validation->set_rules('photo_img', 'Foto Produk', 'callback__check_foto_produk');
			if ($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}
		    else{
                        $volume = $this->input->post('panjang')*$this->input->post('lebar')*$this->input->post('tinggl');
                        $volume_kon = $volume/6000;
                        if($volume_kon > $this->input->post('berat'))
                            $jne_berat = $volume_kon;
                        else {
                            $jne_berat = $this->input->post('berat');
                        }

                $berat = str_replace(',', '.', $this->input->post('berat'));
                if($berat < 0){
                	$berat = 0;
                }
                $panjang = str_replace(',', '.', $this->input->post('panjang'));
                if($panjang < 0){
                	$panjang = 0;
                }
                $lebar = str_replace(',', '.', $this->input->post('lebar'));
                if($lebar < 0){
                	$lebar = 0;
                }
                $tinggi = str_replace(',', '.', $this->input->post('tinggi'));
                if($tinggi < 0){
                	$tinggi = 0;
                }
                $stok = $this->input->post('stok_produk');
                if($stok < 0){
                	$stok = 0;
                }

				$input=array(
					'nama_produk' => $this->input->post('nama_produk'),
					'deskripsi' => $this->input->post('deskripsi'),
					'berat' => $berat,
					'stok_produk' => $stok,
					'harga_produk' => $this->input->post('harga_produk'),
					'diskon' => $this->input->post('diskon'),
					'publish' => 0,
					'view_notif' => 'N',
					'id_user' => $this->session->userdata('member')->id_user,
					'panjang' => $panjang,
					'lebar' => $lebar,
					'tinggi' => $tinggi,
					'shipping_area' => $this->input->post('shipping_area'),
                                        'jne_berat' => $jne_berat,
					'date_modified' => date('Y-m-d H:i:s')
				);
				//database transaction
				$this->db->trans_begin();
				$insert=$this->produk_m->update('tbl_produk', 'md5(id_produk)', $id_produk, $input);
				$prod=$this->produk_m->get_single('tbl_produk', 'md5(id_produk)', $id_produk);
                                
//                                start afandi
                                
                                //clear tag
                                $this->produk_m->clear_tag($prod->id_produk);
                                
                                if (!empty($_POST['nama_tag'])) {
                                $tag = explode(',', $_POST['nama_tag']);
                                foreach ($tag as $a){
                                    $b = trim($a);
                                    //select tag
                                    $select_tag = $this->db->query("select * from tbl_tag where nama_tag = '".$b."'");
                                    if ($select_tag->num_rows() > 0) {
                                        $result = $select_tag->row();
                                        $idtag = $result->id_tag;
                                    }else{
                                        $insert_tag = array('id_tag' => null,
                                            'nama_tag' => $b,
                                            'date_added' => date('Y-m-d H:i:s'),
                                            'date_modified' => date('Y-m-d H:i:s')
                                        );
                                        $idtag = $this->produk_m->insert('tbl_tag', $insert_tag);
                                    }
                                    //insert tag_relation
                                     $insert_relation = array('id_produk_tag' => null,
                                        'id_produk' => $prod->id_produk,
                                        'id_tag' => $idtag,
                                        'date_added' => date('Y-m-d H:i:s'),
                                        'date_modified' => date('Y-m-d H:i:s')
                                    );
                                    $this->produk_m->insert('tbl_produk_tag', $insert_relation);
                                }
                            }
                           
//                                end afandi
                                
				if($insert){
                    if(!empty($_POST['photo_img'])){
                        foreach ($_POST['photo_img'] as $foto) {
                            $cek_foto=$this->myproduct_m->cek_foto($foto);
                            if($cek_foto<1){
                                $inpdet=array(
                                    'id_produk' => $this->input->post('id_produk'),
                                    'image'	 => $foto,
                                    'date_added' => date('Y-m-d H:i:s'),
                                    'date_modified' => date('Y-m-d H:i:s')
                                );
                                $produk_foto=$this->produk_m->insert('tbl_produkfoto', $inpdet);
                            }
                        }
                    }
					if($_POST['id_kategori'][0]!=''){
						$this->myproduct_m->delete_kategori($id_produk);
						$idp=$this->input->post('id_produk');
						foreach ($_POST['id_kategori'] as $id) {
                        	if($id!=''){
								$prod_kat=array('id_kategori'=>$id, 'id_produk'=>$idp, 'date_added'=>date('Y-m-d H:i:s'), 'date_modified'=>date('Y-m-d H:i:s'));
								$ins_prod_kat=$this->produk_m->insert('tbl_produk_kategori', $prod_kat);
							}
						}
					}
					if(isset($_POST['id_kota'])){
						$this->produk_m->update('tbl_produk', 'md5(id_produk)', $id_produk, array('id_kota'=> $_POST['id_kota']));
					}
				}
				// throw new Exception('ghghjgj');
				if ($this->db->trans_status() === FALSE)
				{
				    $this->db->trans_rollback();
				    $data['error']='Produk gagal disimpan.';
				    
				}
				else
				{
				    $this->db->trans_commit();
				    $data['success']='Perubahan data telah disimpan. Kami akan melakukan verifikasi dalam waktu 1x24 jam.';
				}
		    }
		}
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['provinsi']=$this->produk_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
		$data['produk']=$this->myproduct_m->get_produk_single($id_produk, $id_user);
		$data['tags']=$this->produk_m->get_tag($data['produk']->id_produk);
                $data['count_tag'] = $this->produk_m->count_tag($data['produk']->id_produk);
		$data['fotos']=$this->myproduct_m->get_foto($id_produk);
		$data['kategoris']=$this->myproduct_m->get_data_where('tbl_kategori', 'id_parent', '0');
		$data['kategorip']=$this->myproduct_m->get_kategori($id_produk);
		// var_dump($data['kategori']);exit;
		$this->load->view('publik/myproduct_edit_v', $data);
	}

	public function delete($id_produk='', $id_user=''){
		$data=array();
		$data['success']='';
		$data['error']='';
		// $foto=$this->myproduct_m->get_data_where('tbl_produkfoto', 'md5(id_produk)', $id_produk);
		// $del_produk=$this->myproduct_m->delete($id_produk, $id_user);
		// foreach ($foto as $f) {
		// 	$post = $f->image;
		// 	$len = strlen(base_url());
		// 	$new_path = substr($post, $len, strlen($post)-$len);
		// 	unlink($new_path);
		// }
		$del=$this->produk_m->update('tbl_produk', 'md5(id_produk)', $id_produk, array('deleted'=>1));
		redirect(base_url('myproduct'));
	}
    
    function _check_category()
    {
        if($_POST['id_kategori'][0]!=''){            
            return TRUE;
        }else{
            $this->form_validation->set_message('_check_category', 'Pilih salah satu Kategori');
            return FALSE;
        }
    }
    
    function _check_foto_produk()
    {
        if(!empty($_POST['photo_img'])){            
            return TRUE;
        }else{
            $this->form_validation->set_message('_check_foto_produk', 'Upload minimal 1 foto produk');
            return FALSE;
        }
    }

}

/* End of file search.php */
/* Location: ./application/controllers/search.php */