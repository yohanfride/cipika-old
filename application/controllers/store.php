<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('store_m');
		$this->load->model('produk_m');
	}

	public function index()
	{
		redirect(base_url());
	}

	public function id($id=""){
		if($id=='')redirect(base_url());
		$data=array();
		$data['id']=$id;
                if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
                else{$data['by_search'] = '';}
		//infinite scroll
		$limit=12;
		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
		else{$offset=0;}
		
		if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
	    else{$data['by_search'] = '';}
	    if(isset($_GET['cat'])){$data['by_category'] = $_GET['cat'];}
	    else{$data['by_category'] = '';}
	    if(isset($_GET['loc'])){$data['by_location'] = $_GET['loc'];}
	    else{$data['by_location'] = '';}
	    if(isset($_GET['pr'])){$data['by_price'] = $_GET['pr'];}
	    else{$data['by_price'] = '';}
	    if(isset($_GET['tag'])){$data['by_tag'] = $_GET['tag'];}
	    else{$data['by_tag'] = '';}
	    if(isset($_GET['sort'])){$data['by_sort'] = $_GET['sort'];}
		else{$data['by_sort'] = '';}

		$data['produk']=$this->store_m->get_listing($id, $limit, $offset);

		$data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
                
                if($this->session->userdata('member')){
			$id_user_followed=$id;
			$id_user_following=$this->auth_m->get_user()->id_user;
			$cek=$this->auth_m->cek('tbl_follow', 'id_user_following', 'id_user_followed', $id_user_following, $id_user_followed);
			if($cek>0){
				$data['btn_follow']='<a href="#" class="btn btn-sm btn-unfollow btn-primary">Unfollow</a>';
			}else{
				$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
			}
		}else
		{
			$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
		}
                
		$this->load->view('publik/store_v', $data);		
	}

	public function follower($id=""){
		if($id=='')redirect(base_url());
		$data=array();
		$data['id']=$id;
		$data['follower']=$this->store_m->get_follower($id);

		$data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
		$this->load->view('publik/store_follower_v', $data);		
	}

	public function following($id=""){
		if($id=='')redirect(base_url());
		$data=array();
		$data['id']=$id;
		$data['following']=$this->store_m->get_following($id);

		$data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
		$this->load->view('publik/store_following_v', $data);		
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */