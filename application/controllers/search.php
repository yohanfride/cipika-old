<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('search_m');
		$this->load->model('store_m');
	}

	public function index()
	{
		$data=array();
		$limit=12;
		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
		else{$offset=0;}
		// echo $offset;exit;
		if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
	    else{$data['by_search'] = '';}
	    if(isset($_GET['cat'])){$data['by_category'] = $_GET['cat'];}
	    else{$data['by_category'] = '';}
	    if(isset($_GET['loc'])){$data['by_location'] = $_GET['loc'];}
	    else{$data['by_location'] = '';}
	    if(isset($_GET['pr'])){$data['by_price'] = $_GET['pr'];}
	    else{$data['by_price'] = '';}
	    if(isset($_GET['tag'])){$data['by_tag'] = $_GET['tag'];}
	    else{$data['by_tag'] = '';}
	    if(isset($_GET['sort'])){$data['by_sort'] = $_GET['sort'];}
		else{$data['by_sort'] = '';}
	    $data['produk']=$this->search_m->search($data['by_search'], $data['by_category'], $data['by_location'], $data['by_price'], $data['by_tag'], $data['by_sort'], $limit, $offset);
		// $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
            if(empty($data['produk']) && $offset == 0){
                $data['empty'] = TRUE;
            }  else {
                $data['empty'] = FALSE;
            }
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['provinsi']=$this->search_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
		$data['tag']=$this->search_m->get_all_data('tbl_tag', 'nama_tag', 'asc');
		$this->load->view('publik/search_v', $data);
	}
        
    public function store($id = '')
	{
		$data=array();
                $data['id'] = $id;
		$limit=12;
		if(isset($_GET['sc'])){$offset=$limit*($_GET['sc']-1);}
		else{$offset=0;}
		// echo $offset;exit;
		if(isset($_GET['s'])){$data['by_search'] = $_GET['s'];}
	    else{$data['by_search'] = '';}
	    if(isset($_GET['cat'])){$data['by_category'] = $_GET['cat'];}
	    else{$data['by_category'] = '';}
	    if(isset($_GET['loc'])){$data['by_location'] = $_GET['loc'];}
	    else{$data['by_location'] = '';}
	    if(isset($_GET['pr'])){$data['by_price'] = $_GET['pr'];}
	    else{$data['by_price'] = '';}
	    if(isset($_GET['tag'])){$data['by_tag'] = $_GET['tag'];}
	    else{$data['by_tag'] = '';}
	    if(isset($_GET['sort'])){$data['by_sort'] = $_GET['sort'];}
		else{$data['by_sort'] = '';}
	    $data['produk']=$this->search_m->search_store($data['by_search'], $data['by_category'], $data['by_location'], $data['by_price'], $data['by_tag'], $data['by_sort'], $id, $limit, $offset);
            if(empty($data['produk']) && $offset == 0){
                $data['empty'] = TRUE;
            }  else {
                $data['empty'] = FALSE;
            }
            // $data['page']=$this->auth_m->get_all_data_order('tbl_page', 'sorting', 'asc');
		$data['page']=$this->auth_m->get_data_where('tbl_page', 'publish', '1');
		$data['provinsi']=$this->search_m->get_all_data('tbl_provinsi', 'id_provinsi', 'asc');
		$data['tag']=$this->search_m->get_all_data('tbl_tag', 'nama_tag', 'asc');
                
                $data['jml_list']=$this->produk_m->count_list($id);
		$data['jml_love_all']=$this->produk_m->count_love_all($id);
		$data['jml_follower']=$this->produk_m->count_follower($id);
		$data['jml_following']=$this->produk_m->count_following($id);
		$data['data']=$this->store_m->get_single('tbl_user', 'id_user', $id);
                
                if($this->session->userdata('member')){
			$id_user_followed=$id;
			$id_user_following=$this->auth_m->get_user()->id_user;
			$cek=$this->auth_m->cek('tbl_follow', 'id_user_following', 'id_user_followed', $id_user_following, $id_user_followed);
			if($cek>0){
				$data['btn_follow']='<a href="#" class="btn btn-sm btn-unfollow btn-primary">Unfollow</a>';
			}else{
				$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
			}
		}else
		{
			$data['btn_follow']='<a class="btn btn-sm btn-follow btn-warning" href="#">Follow</a>';
		}
		$this->load->view('publik/search_store_v', $data);
	}

}

/* End of file search.php */
/* Location: ./application/controllers/search.php */