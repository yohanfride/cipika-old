<?php
function checkLog($userID) {
	$file = "loguid.json";
	$data = @file_get_contents($file);
	$array = (array)json_decode($data, true);
	return ($array[$userID] == 1) ? false : true;
}

function updateLog($userID) {
	$file = "loguid.json";
	$data = @file_get_contents($file);
	$array = (array) json_decode($data, true);
    $array[$userID] = 1;
	$handle = fopen($file, "w");
	$json = json_encode($array);
	fwrite($handle, $json);
	fclose($handle);
}

function stringBetween($regex, $source, $i = 1, $type = '') {
	if($type == 'ALL') {
		return (preg_match_all('{'.$regex.'}s', $source, $res)) ? $res[$i] : '';
	}
	else return (preg_match('{'.$regex.'}s', $source, $res)) ? $res[$i] : '';
}

function quote($source) {
	return preg_quote($source);
}