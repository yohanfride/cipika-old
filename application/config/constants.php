<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define('DEV_PAYMENT', TRUE);
define('ADMIN_EMAIL', 'ali@firzil.co.id');

define('IP_SERVER', '54.254.229.60');

define('USER_ID_PAYMENT_TRANFER','36');
define('SECRET_ID_PAYMENT_TRANFER','d3v3l0pm3nt');

define('USER_ID_DOMPETKU','third_party_test');
define('SECRET_KEY_DOMPETKU','Th1s_0nLy_F0uR_t3sT1nG__');
define('INITIATOR_DOMPETKU','outlet_1');
define('INITIATOR_PIN_DOMPETKU','123456');

/** ID & API_KEY FACEBOOK APP**/
define('APPID_FB','549385521836660');
define('SECRETID_FB','319ef50712b73160a7121159a23b7856');

define('MERCHANT', TRUE);

/* IP & PORT SERVER MANDIRI CLICKPAY*/
//define('IP_SERVER_MANDIRI_CLICKPAY', '202.169.43.53');
//define('PORT_SERVER_MANDIRI_CLICKPAY', '18444');
/* End of file constants.php */
/* Location: ./application/config/constants.php */