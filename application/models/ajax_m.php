<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	
	function get_loved($id=''){
		$this->db->where('id_produk', $id);
		$q=$this->db->get('tbl_produk');
		$data=$q->row();
		$q->free_result();
		return $data->loved;
	}

	function delete_love($id_produk, $id_user){		
        $this->db->where('id_produk', $id_produk);
        $this->db->where('id_user', $id_user);
        $this->db->delete('tbl_love');
        return $this->db->affected_rows();
	}

	function get_data_produk_baru(){
		$this->db->where('view_notif', 'N');
		$this->db->where('deleted', '0');
		$q = $this->db->get('tbl_produk');
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_jumlah_produk_baru(){
		$this->db->where('view_notif', 'N');
		$this->db->where('deleted', '0');
		$this->db->from('tbl_produk');
		$data = $this->db->count_all_results();
		return $data;
	}

	function get_data_order_baru(){
		$this->db->where('view_notif', 'N');
		$q = $this->db->get('tbl_order');
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_jumlah_order_baru(){
		$this->db->where('view_notif', 'N');
		$this->db->from('tbl_order');
		$data = $this->db->count_all_results();
		return $data;
	}
        
        function get_data_merchant_baru(){
		$this->db->where('view_notif', 'N');
		$q = $this->db->get('tbl_store');
		$data=$q->result();
		$q->free_result();
		return $data;
	}
        
        function get_jumlah_merchant_baru(){
		$this->db->where('view_notif', 'N');
		$this->db->from('tbl_store');
		$data = $this->db->count_all_results();
		return $data;
	}

	function get_category_child($id=''){
		$this->db->where('id_parent', $id);
		$q=$this->db->get('tbl_kategori');
		$data=$q->result();
		$q->free_result();
		return $data;
	}

}

/* End of file  */
/* Location: ./application/models/ */