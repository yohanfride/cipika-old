<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settlement_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_tagihan(){
		$date=date('Y-m-d');
		$sql="SELECT a.*, b.nama_store, b.bank_nama, b.bank_norek, b.bank_pemilik 
			FROM tbl_order a
			LEFT JOIN tbl_store b ON a.id_merchant=b.id_user
			WHERE 
			TIMESTAMPDIFF(DAY, date(a.date_modified), NOW()) > 6 
			AND a.status_payment='paid' 
			AND a.status_delivery='proses pengiriman' 
			AND a.kode_order NOT IN (SELECT kode_order FROM tbl_settlement_item)";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_settlement_item($id=''){
		$date=date('Y-m-d');
		$sql="SELECT a.*, b.nama_store, b.bank_nama, b.bank_norek, b.bank_pemilik 
			FROM tbl_order a
			LEFT JOIN tbl_store b ON a.id_merchant=b.id_user
			WHERE 
			a.id_order='". $id ."'
			AND a.status_payment='paid' 
			AND a.status_delivery='proses pengiriman' 
			AND a.kode_order NOT IN (SELECT kode_order FROM tbl_settlement_item)";
		$q=$this->db->query($sql);
		$data=$q->row();
		$q->free_result();
		return $data;
	}

	function get_last_kode(){
		$sql="select max(kode_settlement) as id from tbl_settlement";
		$q=$this->db->query($sql);
		$data=$q->row();
		$q->free_result();
		return $data->id;
	}

	function check_kode($id=''){
		$sql="select count(*) as jumlah from tbl_settlement where (select left(kode_settlement, 7))='S" . $id. "' ";
		$q=$this->db->query($sql);
		$data=$q->row();
		$q->free_result();
		return $data->jumlah;
	}

	function get_order($id=''){
		$this->db->where('id_order', $id);
		$q=$this->db->get('tbl_order');
		$data=$q->row();
		$q->free_result();
		return $data;
	}

}

/* End of file  */
/* Location: ./application/models/ */