<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myproduct_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_produk_single($id_produk='', $id_user){
		$sql="select a.*, b.nama_kota from tbl_produk a, tbl_kota b where a.id_kota=b.id_kota and md5(id_produk)='".$id_produk."' and md5(id_user)='".$id_user."'";
		$q=$this->db->query($sql);
		$data=$q->row();
		$q->free_result();
		return $data;
	}

	function get_kategori($id=''){
		$sql="select a.id_kategori, b.nama_kategori from tbl_produk_kategori a, tbl_kategori b where a.id_kategori=b.id_kategori and md5(id_produk)='".$id."'";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_foto($id=''){
		$sql="select * from tbl_produkfoto where md5(id_produk)='".$id."'";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function delete($id_produk='', $id_user=''){		
        $this->db->where('md5(id_produk)', $id_produk);
        $this->db->where('md5(id_user)', $id_user);
        $this->db->delete('tbl_produk');
        return $this->db->affected_rows();
	}

	function delete_foto($id_produk=''){		
        $this->db->where('md5(id_produk)', $id_produk);
        $this->db->delete('tbl_produkfoto');
        return $this->db->affected_rows();
	}

	function delete_kategori($id_produk=''){		
        $this->db->where('md5(id_produk)', $id_produk);
        $this->db->delete('tbl_produk_kategori');
        return $this->db->affected_rows();
	}

	function delete_atribut($id_produk=''){		
        $this->db->where('md5(id_produk)', $id_produk);
        $this->db->delete('tbl_produkatribut');
        return $this->db->affected_rows();
	}

	function delete_tag($id_produk=''){		
        $this->db->where('md5(id_produk)', $id_produk);
        $this->db->delete('tbl_produk_tag');
        return $this->db->affected_rows();
	}

	function cek_foto($foto=''){
		$sql="select count(*) as jml from tbl_produkfoto where image='".$foto."'";
		$q=$this->db->query($sql);
		$data=$q->row();
		$q->free_result();
		return $data->jml;
	}

}

/* End of file  */
/* Location: ./application/models/ */