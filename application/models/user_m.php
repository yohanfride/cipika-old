<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function cek_username($id='', $username=''){
		$sql="SELECT COUNT(*) AS jumlah FROM tbl_user WHERE id_user!='".$id."' AND username='".$username."' ";
		$q=$this->db->query($sql);
		$data = $q->row();
		$q->free_result();
		return $data->jumlah;
	}

	function cek_password($id='', $pass=''){
		$sql="SELECT COUNT(*) AS jumlah FROM tbl_user WHERE id_user='".$id."' AND password='".$pass."' ";
		$q=$this->db->query($sql);
		$data = $q->row();
		$q->free_result();
		return $data->jumlah;
	}

	function view_all_admin($limit='', $offset=''){
		$sql=	"select a.*, b.nama_level, c.nama_kabupaten, d.nama_status, e.nama_group
				from tbl_user a
				left join tbl_level b on a.id_level=b.id_level
				left join tbl_kabupaten c on a.id_kabupaten=c.id_kabupaten
				left join tbl_status d on a.id_status=d.id_status
				left join tbl_group e on a.id_group=e.id_group
				where a.id_level!=6 and a.id_level!=7 and a.deleted=0
				order by a.id_user asc limit ".$offset.",".$limit."";
		$q=$this->db->query($sql);
		$data = $q->result();
		$q->free_result();
		return $data;
	}

	function view_all_member($limit='', $offset=''){
		$sql=	"select a.*, c.nama_kabupaten, d.nama_status, e.nama_group
				from tbl_user a
				left join tbl_kabupaten c on a.id_kabupaten=c.id_kabupaten
				left join tbl_status d on a.id_status=d.id_status
				left join tbl_group e on a.id_group=e.id_group
				where a.id_level=6 and a.deleted=0
				order by a.id_user desc limit ".$offset.",".$limit."";
		$q=$this->db->query($sql);
		$data = $q->result();
		$q->free_result();
		return $data;
	}

	function get_username($key){
		$sql="select max(username) as id from tbl_user where (select left(username, 3))='".$key."'";
		$q=$this->db->query($sql);
		$data = $q->row();
		$q->free_result();
		if($data->id!=null){
			$id_anyar = str_replace($key, '', $data->id);
			$id_anyar += 1000; $id_anyar++;
			$out = $key.substr($id_anyar, 1,3);	
		} else {
			$id_anyar = 1000; $id_anyar++;
			$out=$key.substr($id_anyar, 1,3);
		}
		return $out;	
	}

	function get_user_login($_username, $_password) {
        $sql = "select * from tbl_user where id_level !='6' AND (username='" . $_username . "' or email ='".$_username."') AND password = '" . $_password . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    function get_email_user($email){
    	$sql = "select * from tbl_user where email ='".$email."'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    function get_image($id_user=''){
    	$sql = "select image from tbl_user where id_user ='".$id_user."'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->image;
    }
    
    function delete_love($id_produk, $id_user){		
        $this->db->where('md5(id_produk)', $id_produk);
        $this->db->where('md5(id_user)', $id_user);
        $this->db->delete('tbl_love');
        return $this->db->affected_rows();
    }
    
    /* Check duplicate email */
    function duplicate_email($email)
    {
        $sql = "select email from tbl_user where email='".$email."'";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
	
}

/* End of file  */
/* Location: ./application/models/ */