<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produk_m extends MY_Model {

    public $variable;

    public function __construct()
    {
        parent::__construct();
    }

    function getSales()
    {
        $this->db->where('id_level', '3');
        $this->db->order_by("username", "asc");
        $q = $this->db->get('tbl_user');
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function getMerchant()
    {
        $this->db->order_by("nama_store", "asc");
        $q = $this->db->get('tbl_store');
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_all_produk($limit='', $offset='')
    {
        $sql = "select a.*, b.nama_kabupaten, c.*, a.date_modified as tgl_upload, d.*
                from tbl_produk a
                left join tbl_user c on a.id_user=c.id_user
                left join tbl_store d on a.id_user=d.id_user
                left join tbl_kabupaten b on b.id_kabupaten=c.id_kabupaten
                where a.deleted=0 order by a.date_modified desc LIMIT " . $offset . ", " . $limit . "";
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_kategori($id=''){
        $sql="select b.nama_kategori
            from tbl_produk_kategori a
            left join tbl_kategori b on a.id_kategori=b.id_kategori
            where a.id_produk='".$id."'";
        $q=$this->db->query($sql);
        $data=$q->result();
        $q->free_result();

        foreach ($data as $key => $value) {
            echo $value->nama_kategori;
            echo ", ";
        }
    }

    function get_produk($id = '')
    {
        $sql = "select a.*, c.nama_kabupaten, b.*, d.nama_store
				from tbl_produk a
                left join tbl_user b on a.id_user=b.id_user
                left join tbl_store d on a.id_user=d.id_user
                left join tbl_kecamatan e on d.id_kota=e.id_kecamatan
                left join tbl_kabupaten c on e.id_kabupaten=c.id_kabupaten
				where id_produk='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data;
    }

    function count_list($id)
    {
        $sql = "select count(*) as jml from tbl_produk where id_user='" . $id . "' and publish = '1' and deleted = '0'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jml;
    }

    function count_follower($id)
    {
        $sql = "select count(*) as jml from tbl_follow where id_user_followed='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jml;
    }

    function count_following($id)
    {
        $sql = "select count(*) as jml from tbl_follow where id_user_following='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jml;
    }

    function count_love($id)
    {
        $sql = "select count(*) as jml from tbl_love where id_produk='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jml;
    }

    function count_love_all($id)
    {
        $sql = "select count(*) as jml from tbl_love a, tbl_produk b, tbl_user c where a.id_produk=b.id_produk and b.id_user=c.id_user and b.id_user='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jml;
    }

    function count_comment($id)
    {
        $sql = "select count(*) as jml from tbl_comment where id_produk='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jml;
    }

    function get_comment($id = '', $limit = '', $offset = '')
    {
        $sql = "select a.*, b.username, b.id_user, b.image
			from tbl_comment a, tbl_user b
			where a.id_user=b.id_user and a.id_produk='".$id."' order by a.date_added asc limit " . $offset . "," . $limit . "";
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_produk_user($id = '', $limit = '', $offset = '')
    {
        $sql = "select a.*, c.image
				from tbl_produk a
				left join tbl_produkfoto c
				on a.id_produk=c.id_produk
				where a.id_user=" . $id . " and a.deleted=0
				group by a.id_produk order by id_produk desc limit " . $offset . "," . $limit . "";
                
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }
    
    function get_all_produk_user($id = '')
    {
        $sql = "select a.*, c.image
				from tbl_produk a
				left join tbl_produkfoto c
				on a.id_produk=c.id_produk
				where a.id_user=" . $id . " and a.deleted=0
				group by a.id_produk order by id_produk desc ";
                
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function count_produk_user_by_search($id = '', $by_search='', $by_category='')
    {
        $query = "select count(*) as jumlah from (select a.id_produk 
                from tbl_produk a
                left join tbl_produk_kategori b
                on a.id_produk=b.id_produk
                left join tbl_produkfoto c
                on a.id_produk=c.id_produk
                where a.id_user='" . $id . "' and deleted=0";

        $conditions = array();
        if($by_search !="") {
          $conditions[] = "a.nama_produk LIKE '%$by_search%'";
        }
        if($by_category !="") {
          $conditions[] = "b.id_kategori='$by_category'";
        }

        $sql = $query;
        if (count($conditions) > 0) {
          $sql .= " AND " . implode(' AND ', $conditions) . "";
        }
        $sql .= " group by a.id_produk) as x";
        // echo $sql;exit;

        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jumlah;
    }

    function count_all_product_search($by_search='', $by_category='', $by_sales='', $by_status='', 
    $by_merchant='', $by_stok='', $by_from='', $by_to='', $by_feature='', $by_price='', $by_location='', $limit = '', $offset = '')
    {
        $query = "select count(*) as jumlah from (select a.*, c.image
                from tbl_produk a
                left join tbl_produk_kategori b
                on a.id_produk=b.id_produk
                left join tbl_produkfoto c
                on a.id_produk=c.id_produk
                left join tbl_store d
                on a.id_user=d.id_user
                left join tbl_user f on a.id_user=f.id_user
                left join tbl_kabupaten e on f.id_kabupaten=e.id_kabupaten
                where a.deleted=0";

        $conditions = array();
        if($by_search !="") {
          $conditions[] = "a.nama_produk LIKE '%$by_search%'";
        }
        if($by_category !="") {
          $conditions[] = "b.id_kategori='$by_category'";
        }
        if($by_sales !="") {
          $conditions[] = "d.id_sales='$by_sales'";
        }
        if($by_status !="") {
          $conditions[] = "a.publish='$by_status'";
        }
        if($by_merchant !="") {
          $conditions[] = "a.id_user='$by_merchant'";
        }
        if($by_from !="" && $by_to !="") {
            $from = strftime("%Y-%m-%d",strtotime($by_from));
            $to = strftime("%Y-%m-%d",strtotime($by_to));
            $conditions[] = "(DATE(a.date_modified) BETWEEN '$from' AND '$to')";
        }
        if($by_feature !="") {
            if($by_feature=="1") {$conditions[] = "a.unggulan='1'";}
            else if($by_feature=="2") {$conditions[] = "a.pick='1'";}
            else {}
        }
        if($by_stok !="") {
          if($by_stok=='1'){$conditions[] = "a.stok_produk < 10";}
          else if($by_stok=='2'){$conditions[] = "a.stok_produk BETWEEN 10 AND 100";}
          else if($by_stok=='3'){$conditions[] = "a.stok_produk BETWEEN 100 AND 1000";}
          else {}
        }
        if($by_price !="") {
          if($by_price=='1'){$conditions[] = "a.harga_jual < 50000";}
          else if($by_price=='2'){$conditions[] = "a.harga_jual BETWEEN 50000 AND 100000";}
          else if($by_price=='3'){$conditions[] = "a.harga_jual BETWEEN 100000 AND 250000";}
          else if($by_price=='4'){$conditions[] = "a.harga_jual BETWEEN 250000 AND 500000";}
          else if($by_price=='5'){$conditions[] = "a.harga_jual > 500000";}
          else {}
        }
        if($by_location !="") {
          $conditions[] = "e.nama_kabupaten LIKE '%$by_location%'";
        }

        //(DATE(date) BETWEEN '".date('Y-m-d', strtotime("-1 month"))."' AND '".date('Y-m-d')."')

        $sql = $query;
        if (count($conditions) > 0) {
          $sql .= " AND " . implode(' AND ', $conditions) . "";
        }
        $sql .= " group by a.id_produk) as x";
        
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jumlah;
    }

    function get_produk_by_search($by_search='', $by_category='', $by_sales='', $by_status='', 
    $by_merchant='', $by_stok='', $by_from='', $by_to='', $by_feature='', $by_price='', $by_location='', $limit = '', $offset = '')
    {
        $query = "select a.*, a.date_modified as tgl_upload, d.id_store, d.nama_store
                from tbl_produk a
                left join tbl_produk_kategori b on a.id_produk=b.id_produk
                left join tbl_store d on a.id_user=d.id_user
                left join tbl_user f on a.id_user=f.id_user
                left join tbl_kabupaten e on f.id_kabupaten=e.id_kabupaten
                where a.deleted=0";

        $conditions = array();
        if($by_search !="") {
          $conditions[] = "a.nama_produk LIKE '%$by_search%'";
        }
        if($by_category !="") {
          $conditions[] = "b.id_kategori='$by_category'";
        }
        if($by_sales !="") {
          $conditions[] = "d.id_sales='$by_sales'";
        }
        if($by_status !="") {
          $conditions[] = "a.publish='$by_status'";
        }
        if($by_merchant !="") {
          $conditions[] = "a.id_user='$by_merchant'";
        }
        if($by_from !="" && $by_to !="") {
            $from = strftime("%Y-%m-%d",strtotime($by_from));
            $to = strftime("%Y-%m-%d",strtotime($by_to));
            $conditions[] = "(DATE(a.date_modified) BETWEEN '$from' AND '$to')";
        }
        if($by_feature !="") {
            if($by_feature=="1") {$conditions[] = "a.unggulan='1'";}
            else if($by_feature=="2") {$conditions[] = "a.pick='1'";}
            else {}
        }
        if($by_stok !="") {
          if($by_stok=='1'){$conditions[] = "a.stok_produk < 10";}
          else if($by_stok=='2'){$conditions[] = "a.stok_produk BETWEEN 10 AND 100";}
          else if($by_stok=='3'){$conditions[] = "a.stok_produk BETWEEN 100 AND 1000";}
          else {}
        }
        if($by_price !="") {
          if($by_price=='1'){$conditions[] = "a.harga_jual < 50000";}
          else if($by_price=='2'){$conditions[] = "a.harga_jual BETWEEN 50000 AND 100000";}
          else if($by_price=='3'){$conditions[] = "a.harga_jual BETWEEN 100000 AND 250000";}
          else if($by_price=='4'){$conditions[] = "a.harga_jual BETWEEN 250000 AND 500000";}
          else if($by_price=='5'){$conditions[] = "a.harga_jual > 500000";}
          else {}
        }
        if($by_location !="") {
          $conditions[] = "e.nama_kabupaten LIKE '%$by_location%'";
        }

        //(DATE(date) BETWEEN '".date('Y-m-d', strtotime("-1 month"))."' AND '".date('Y-m-d')."')

        $sql = $query;
        if (count($conditions) > 0) {
          $sql .= " AND " . implode(' AND ', $conditions) . "";
        }
        $sql .= " group by a.id_produk desc LIMIT " . $offset . ", " . $limit . "";
        // var_dump($sql);exit;
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_produk_user_by_search($id = '', $by_search='', $by_category='', $limit = '', $offset = '')
    {
        $query = "select a.*, c.image
                from tbl_produk a
                left join tbl_produk_kategori b
                on a.id_produk=b.id_produk
                left join tbl_produkfoto c
                on a.id_produk=c.id_produk
                where a.id_user='" . $id . "' and a.deleted=0";

        $conditions = array();
        if($by_search !="") {
          $conditions[] = "a.nama_produk LIKE '%$by_search%'";
        }
        if($by_category !="") {
          $conditions[] = "b.id_kategori='$by_category'";
        }

        $sql = $query;
        if (count($conditions) > 0) {
          $sql .= " AND " . implode(' AND ', $conditions) . "";
        }
        $sql .= " group by a.id_produk desc LIMIT " . $offset . ", " . $limit . "";

        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_produk_other($id = '', $offset = '', $limit = '')
    {
        $id_user = mysql_query("select id_user from tbl_produk where id_produk='" . $id . "'");
        $idu = mysql_fetch_array($id_user);
        $sql = "select a.*, b.image, c.username, d.count_comment
            from tbl_produk a
            join tbl_produkfoto b on a.id_produk=b.id_produk
            join tbl_user c on a.id_user=c.id_user
            left join view_comment d on a.id_produk=d.id_produk
            where a.id_produk!='" . $id . "' and a.id_user='" . $idu['id_user'] . "' and a.deleted=0 and a.publish='1'
            group by id_produk DESC LIMIT " . $offset . ", " . $limit . "";
        $q = $this->db->query($sql);
        $data = array();
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_foto($id = '')
    {
        $sql = "select image from tbl_produkfoto where id_produk='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function get_tag($id = '')
    {
        $sql = "select b.nama_tag from tbl_produk_tag a, tbl_tag b where a.id_tag = b.id_tag and id_produk='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function count_tag($id = '')
    {
        $sql = "select b.nama_tag from tbl_produk_tag a, tbl_tag b where a.id_tag = b.id_tag and id_produk='" . $id . "'";
        $q = $this->db->query($sql);
        $data = $q->num_rows();
        $q->free_result();
        return $data;
    }

    function clear_tag($id = '')
    {

        $sql = "DELETE FROM tbl_produk_tag WHERE id_produk='" . $id . "'";

        $this->db->query($sql);
    }

    function get_category($parentId = 0)
    {
        $catgArray = array();
        $sql = mysql_query("SELECT * FROM tbl_kategori where id_parent=" . $parentId . "");
        if (mysql_num_rows($sql) > 0)
        {
            echo '<ul>';
            while ($mainCatg = mysql_fetch_array($sql))
            {
                echo '<li>';
                echo $mainCatg['nama_kategori'];
                $this->get_category($mainCatg['id_kategori']);
                echo '</li>';
            }
            echo '</ul>';
        }
    }

    function get_select_category($parentId = 0, $level = 0, $activeCategoryId = null)
    {
        $catgArray = array();
        $sql = mysql_query("SELECT * FROM tbl_kategori where id_parent=" . $parentId . "");
        if (mysql_num_rows($sql) > 0)
        {
            while ($mainCatg = mysql_fetch_array($sql))
            {
                echo '<option ';
                echo ($activeCategoryId == $mainCatg['id_kategori']) ? 'selected ' : '';
                echo 'value="' . $mainCatg['id_kategori'] . '">';
                echo str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . ucwords($mainCatg['nama_kategori']);
                $this->get_select_category($mainCatg['id_kategori'], $level + 1, $activeCategoryId );
                echo '</option>';
            }
        }
    }

    function get_select_category_edit($parentId = 0, $level = 0, $key = '')
    {
        $catgArray = array();
        $sql = mysql_query("SELECT * FROM tbl_kategori where id_parent=" . $parentId . "");
        if (mysql_num_rows($sql) > 0)
        {
            while ($mainCatg = mysql_fetch_array($sql))
            {
                // if($mainCatg['id_kategori']==$key){
                // 	$select='selected';
                // } else {
                // 	$select='';
                // }
                echo '<option value="' . $mainCatg['id_kategori'] . '" ' . $key . '>';
                echo str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . ucwords($mainCatg['nama_kategori']);
                $this->get_select_category($mainCatg['id_kategori'], $level + 1, $key);
                echo '</option>';
            }
        }
    }

    function get_product_wishlist($id_user)
    {
        $sql = "SELECT p.*, pf.image FROM tbl_love l, tbl_produk p , tbl_produkfoto pf where l.id_user=" . $id_user . " and l.id_produk=p.id_produk and p.id_produk=pf.id_produk and p.deleted=0 group by p.id_produk ORDER by p.id_produk DESC;";
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function count_product_user($table='', $field='', $key=''){
        $sql = "select count(*) as jumlah from ".$table." WHERE ".$field."='".$key."' and deleted=0";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jumlah;
    }

    function count_all_product(){
        $sql = "select count(*) as jumlah from tbl_produk WHERE deleted=0";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jumlah;
    }

    function get_parent_category(){
        $sql="select * from tbl_kategori where id_parent=0";
        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

}

/* End of file  */
/* Location: ./application/models/ */
