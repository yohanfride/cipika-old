<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_following($id=''){
		$sql="select * from tbl_follow a
			join tbl_user b
			where a.id_user_followed=b.id_user
			and a.id_user_following='".$id."'";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_follower($id=''){
		$sql="select * from tbl_follow a
			join tbl_user b
			where a.id_user_following=b.id_user
			and a.id_user_followed='".$id."'";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_listing($id='', $limit='', $offset=''){
        $sql = "select a.*, b.image, c.username, d.count_comment,s.nama_store,s.store_status
            from tbl_produk a
            join tbl_produkfoto b on a.id_produk=b.id_produk
            join tbl_user c on a.id_user=c.id_user
            join tbl_store s on c.id_user=s.id_user
            left join view_comment d on a.id_produk=d.id_produk
            where a.id_user=" . $id . "  AND a.publish='1' and a.deleted=0 and s.store_status='approve' 
            group by id_produk DESC LIMIT " . $offset . ", " . $limit . "";
        $q = $this->db->query($sql);
        $data = array();
        $data = $q->result();
        $q->free_result();
        return $data;
	}

	function get_loved($id=''){

	}

}

/* End of file  */
/* Location: ./application/models/ */