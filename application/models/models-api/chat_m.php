<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chat_m extends MY_Model {

	public $variable;

	function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->helper('date');	
	}
	
	function cek_user ($iduser='' ){
		$sql = "SELECT COUNT(*) AS jumlah FROM tbl_user WHERE id_user = '$iduser'";
		$b = $this->db->query($sql);
		$row = $b->row();
		$b->free_result();
		return $row->jumlah;
	}
	
	function get_chat_id($id=''){
		$sql = "select a.*, b.* from tbl_user a left join tbl_private_messages b on (a.id_user = b.from_user_id) where b.id = '$id'";
		$b = $this->db->query($sql);
		$row = $b->row();
		$b->free_result();
		return $row;
	}
	
	function get_chat($iduser = '',$limit=20,$offset=0 ){
		$sql = "select a.*, b.* from tbl_user a left join tbl_private_messages b on (a.id_user = b.from_user_id) 
			where b.to_user_id = '$id' or b.from_user_id = '$id' ORDER id DESC GROUP BY b.from_user_id, b.to_user_id 
			LIMIT $offset,$limit";
		$b = $this->db->query($sql);
		$row = $b->row();
		$b->free_result();
		return $row;
	}
	
	function get_list_chat($id = '',$limit=20,$offset=0 ){
		$sql = "select * from tbl_user a left join tbl_private_messages b on (a.id_user = b.from_user_id) 
			where b.to_user_id = '$id' or b.from_user_id = '$id' GROUP BY b.from_user_id, b.to_user_id ORDER BY b.id DESC  
			LIMIT $offset,$limit";
		$b = $this->db->query($sql);
		$row = $b->result();
		$b->free_result();
		return $row;
	}
	
	function get_detail_chat($from='',$to='',$limit=20,$offset=0 ){
		$sql = "select * from tbl_user a left join tbl_private_messages b on (a.id_user = b.from_user_id) 
			where (b.to_user_id = '$from' and b.from_user_id = '$to' ) or (b.to_user_id = '$to' and b.from_user_id = '$from' )
			ORDER BY b.id DESC  
			LIMIT $offset,$limit";
		$b = $this->db->query($sql);
		$row = $b->result();
		$b->free_result();
		return $row;
	}
	
	function get_count_chat_unread($from='',$to=''){
		$sql = "select COUNT(*) as Jumlah from tbl_user a left join tbl_private_messages b on (a.id_user = b.to_user_id) where b.to_user_id = '$to'  and b.from_user_id='$from' AND b.status=0";
		$b = $this->db->query($sql);
		$row = $b->row();
		$b->free_result();
		return $row->Jumlah;
	}
	
	function get_count_unread($to=''){
		$sql = "select * from tbl_private_messages where to_user_id = '$to' AND status=0 GROUP BY from_user_id";
		$b = $this->db->query($sql);
		$row = $b->num_rows();
		$b->free_result();
		return $row;
	}
	
	function update_status($from='',$to=''){
		$sql = "UPDATE tbl_private_messages SET status = 1 WHERE to_user_id = '$to' and from_user_id='$from' AND status=0";
		$b = $this->db->query($sql);		
	}
}

/* End of file  */
/* Location: ./application/models/ */