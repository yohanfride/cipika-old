<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function search($by_search='', $by_category='', $by_location='', $by_price='', $by_tag='', $by_sort='', $limit='', $offset=''){
	   //  $query = "select distinct a.*, c.nama_provinsi, e.image, f.username
				// from tbl_produk a, tbl_kota b, tbl_provinsi c, tbl_produk_tag d, tbl_produkfoto e, tbl_user f, tbl_produk_kategori g
				// where a.publish='1' and a.id_kota=b.id_kota and b.id_provinsi=c.id_provinsi and a.id_produk=d.id_produk and a.id_produk=e.id_produk and a.id_user=f.id_user and a.id_produk=g.id_produk";
		$query = "select distinct a.*, c.nama_propinsi, e.image, f.username, h.count_comment,s.nama_store,s.store_status
				from tbl_produk a
				join tbl_produkfoto e on a.id_produk=e.id_produk 
				join tbl_user f on a.id_user=f.id_user
                join tbl_store s on f.id_user=s.id_user
				left join tbl_kabupaten b  on f.id_kabupaten=b.id_kabupaten 
				left join tbl_propinsi c on b.id_propinsi=c.id_propinsi
				join tbl_produk_kategori g on a.id_produk=g.id_produk
				left join view_comment h on a.id_produk=h.id_produk
				left join tbl_produk_tag  d on a.id_produk=d.id_produk WHERE a.publish='1' and a.deleted=0 and s.store_status='approve'";
	    
	    $conditions = array();
	    if($by_search !="") {
	      $conditions[] = "a.nama_produk LIKE '%$by_search%'";
	    }
	    if($by_category !="") {
	      $conditions[] = "g.id_kategori='$by_category'";
	    }
	    if($by_location !="") {
	      $conditions[] = "c.id_propinsi='$by_location'";
	    }
	    if($by_price !="") {
	      if($by_price=='1'){$conditions[] = "a.harga_jual < 50000";}
	      if($by_price=='2'){$conditions[] = "a.harga_jual BETWEEN 50000 AND 100000";}
	      if($by_price=='3'){$conditions[] = "a.harga_jual BETWEEN 100000 AND 250000";}
	      if($by_price=='4'){$conditions[] = "a.harga_jual BETWEEN 250000 AND 500000";}
	      if($by_price=='5'){$conditions[] = "a.harga_jual > 500000";}
	    }
	    /**
		Koding tag menyusul
	    **/
	    if($by_tag !="") {
	      $conditions[] = "d.id_tag='$by_tag'";
	    }

	    if($by_sort !="") {
	      if($by_sort =="1"){$sorting = " order by a.date_added desc";}
	      if($by_sort =="2"){$sorting = " order by a.loved desc";}
	      if($by_sort =="3"){$sorting = " order by a.viewed desc";}
	    }

	    $sql = $query;
	    if (count($conditions) > 0) {
	      $sql .= " AND " . implode(' AND ', $conditions) . "";
	    }
      	$sql .= " group by g.id_produk" . $sorting . " LIMIT " . $offset . ", " . $limit . "";

	    // print_r($sql);exit;
	    $q = $this->db->query($sql);
	    $data = $q->result();
	    $q->free_result();
	    return $data;
	}
        
        function search_store($by_search='', $by_category='', $by_location='', $by_price='', $by_tag='', $by_sort='', $id, $limit='', $offset=''){
	   //  $query = "select distinct a.*, c.nama_provinsi, e.image, f.username
				// from tbl_produk a, tbl_kota b, tbl_provinsi c, tbl_produk_tag d, tbl_produkfoto e, tbl_user f, tbl_produk_kategori g
				// where a.publish='1' and a.id_kota=b.id_kota and b.id_provinsi=c.id_provinsi and a.id_produk=d.id_produk and a.id_produk=e.id_produk and a.id_user=f.id_user and a.id_produk=g.id_produk";
		$query = "select distinct a.*, c.nama_propinsi, e.image, f.username, h.count_comment
				from tbl_produk a
				join tbl_produkfoto e on a.id_produk=e.id_produk 
				join tbl_user f on a.id_user=f.id_user
				left join tbl_kabupaten b  on f.id_kabupaten=b.id_kabupaten 
				left join tbl_propinsi c on b.id_propinsi=c.id_propinsi
				join tbl_produk_kategori g on a.id_produk=g.id_produk
				left join view_comment h on a.id_produk=h.id_produk
				left join tbl_produk_tag  d on a.id_produk=d.id_produk WHERE a.publish='1' AND a.id_user='".$id."' and a.deleted=0";
	    
	    $conditions = array();
	    if($by_search !="") {
	      $conditions[] = "a.nama_produk LIKE '%$by_search%'";
	    }
	    if($by_category !="") {
	      $conditions[] = "g.id_kategori='$by_category'";
	    }
	    if($by_location !="") {
	      $conditions[] = "c.id_propinsi='$by_location'";
	    }
	    if($by_price !="") {
	      if($by_price=='1'){$conditions[] = "a.harga_jual < 50000";}
	      if($by_price=='2'){$conditions[] = "a.harga_jual BETWEEN 50000 AND 100000";}
	      if($by_price=='3'){$conditions[] = "a.harga_jual BETWEEN 100000 AND 250000";}
	      if($by_price=='4'){$conditions[] = "a.harga_jual BETWEEN 250000 AND 500000";}
	      if($by_price=='5'){$conditions[] = "a.harga_jual > 500000";}
	    }
	    /**
		Koding tag menyusul
	    **/
	    if($by_tag !="") {
			$conditions[] = "d.id_tag='$by_tag'";
	    }

	    if($by_sort !="") {
	      if($by_sort =="1"){$sorting = " order by a.date_added desc";}
	      if($by_sort =="2"){$sorting = " order by a.loved desc";}
	      if($by_sort =="3"){$sorting = " order by a.viewed desc";}
	    } else {
			$sorting  = " order by a.date_added desc";
		}

	    $sql = $query;
	    if (count($conditions) > 0) {
	      $sql .= " AND " . implode(' AND ', $conditions) . "";
	    }
      	$sql .= " group by g.id_produk" . $sorting . " LIMIT " . $offset . ", " . $limit . "";

	    // print_r($sql);exit;
	    $q = $this->db->query($sql);
	    $data = $q->result();
	    $q->free_result();
	    return $data;
	}

}

/* End of file  */
/* Location: ./application/models/ */