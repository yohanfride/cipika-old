<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_m extends MY_Model 
{

	public function __construct()
	{
		parent::__construct();
		
	}
    
    function get_group_product_item($period_start='',$period_end='')
    {
        $sql = "select p.*,oi.* from tbl_produk p,tbl_orderitem oi where oi.id_produk=p.id_produk ";
        $where = "AND";
        
        if($period_start!=''){
            $date = explode('-',$period_start);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 00:01:01';
            $sql.="$where oi.date_added>='".$time."' ";
            $where = "AND";
        }
        
        if($period_end!=''){
            $date = explode('-',$period_end);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 23:59:59';
            $sql.="$where oi.date_added<='".$time."' ";
            $where = "AND";
        }
        
        $sql.="group by oi.id_produk";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function get_group_city_orders($period_start='',$period_end='')
    {
        $sql = "select u.*,o.*,k.* from tbl_user u,tbl_order o,tbl_kecamatan k where o.id_user=u.id_user and u.id_kecamatan=k.id_kecamatan ";
        
        $where = "AND";
        
        if($period_start!=''){
            $date = explode('-',$period_start);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 00:00:00';
            $sql.="$where o.date_added>='".$time."' ";
            $where = "AND";
        }
        
        if($period_end!=''){
            $date = explode('-',$period_end);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 23:59:59';
            $sql.="$where o.date_added<='".$time."' ";
            $where = "AND";
        }
        
        $sql.="group by o.id_user";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function get_revenue_city($idcity)
    {
        $sql = "SELECT sum(total) as `revenue` FROM tbl_order WHERE id_user = ANY(SELECT id_user FROM tbl_user WHERE id_kecamatan=".$idcity.") limit 0,1";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			$result = $query->row();
            return $result->revenue;
		}
    }
    
    /*
        Author  : mdhb2
        Desc    : Get Product Most sales
        
        $param int @start
        $param int @limit
        $param string @orderby - filed of tbl_order
        $param string @order - asc/des
        @return array - product data
    */
    function get_product_sales($status='',$start=0,$limit=10,$orderby='id_user',$order='desc')
    {
        $sql = "select * from tbl_user where id_level=6 ";
        
        $where = "AND";
      
        if($status!=''){
            $sql.="$where active=".$status;
            $where = "AND";
        }
        
        $sql.=" order by $orderby $order limit $start,$limit";        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    
    /*
        Author  : mdhb2
        Desc    : Get merchant by status (id_level 6 = member/merchant)
        
        $param int @status
        $param int @start
        $param int @limit
        $param string @orderby - filed of tbl_order
        $param string @order - asc/des
        @return array - merchant data
    */
    function get_merchant($status='',$start=0,$limit=10,$orderby='u.id_user',$order='desc')
    {
        $sql = "select u.*,s.* from tbl_user u,tbl_store s where u.id_user=s.id_user ";
        
        $where = "AND";
      
        if($status!=''){
            $sql.="$where s.store_status='".$status."' ";
            $where = "AND";
        }
        
        $sql.=" order by $orderby $order limit $start,$limit";                
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    /* 
        Author  : mdhb2
        Desc    : Get Product price comparison 
        
        $param int @category - id_kategori
        $param int @city - id_kota
        $param int @start
        $param int @limit
        $param string @orderby - filed of tbl_order
        $param string @order - asc/des
        @return array - product data
    */
    function get_price_comparison($category='',$city='',$start=0,$limit=10,$orderby='p.nama_produk',$order='desc')
    {
        $sql = "select p.*,pk.*,k.* from tbl_produk p,tbl_produk_kategori pk,tbl_kategori k where p.id_produk=pk.id_produk AND pk.id_kategori=k.id_kategori ";
        
        $where = "AND";
        
        if($category!=''){
            $sql.="$where pk.id_kategori='".$category."'";
            $where = "AND";
        }
        
        if($city!=''){
            $sql.="$where id_kota='".$city."' ";
            $where = "AND";
        }
        
        $sql.=" order by $orderby $order limit $start,$limit";
        //echo $sql;
        $query = $this->db->query($sql);
        $this->session->set_userdata('sql', $sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function get_category()
    {
        $sql = "select * from tbl_kategori order by nama_kategori asc";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    /* 
        Author  : mdhb2
        Desc    : Get order 
        
        $param string @status_delivery
        $param string @status_payment
        $param int @start
        $param int @limit
        $param string @orderby - filed of tbl_order
        $param string @order - asc/des
        @return array - order data
    */
    function get_orders($period_start='',$period_end='',$status_delivery='',$status_payment='',$start=0,$limit=10,$orderby='date_added',$order='desc')
    {
        $sql = "select * from tbl_order ";
        
        $where = "WHERE";
        
        if($period_start!=''){
            $date = explode('-',$period_start);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 00:01:01';
            $sql.="$where date_added>='".$time."' ";
            $where = "AND";
        }
        
        if($period_end!=''){
            $date = explode('-',$period_end);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 23:59:59';
            $sql.="$where date_added<='".$time."' ";
            $where = "AND";
        }
        
        if($status_delivery!=''){
            $sql.="$where status_delivery='".$status_delivery."'";
            $where = "AND";
        }
        
        if($status_payment!=''){
            $sql.="$where status_payment='".$status_payment."' ";
            $where = "AND";
        }
        
        $sql.=" order by $orderby $order limit $start,$limit";        
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
        
    }
    
    /* 
        Author  : mdhb2
        Desc    : Get total order (revenue)
        
        $param string @status_delivery
        $param string @status_payment                
        @return array - total revenue
    */
    function get_revenue($period_start='',$period_end='',$status_delivery='',$status_payment='',$id_city='')
    {
        $sql = "select sum(total) as 'revenue',sum(total_merchant) as 'revenue_merchant' from tbl_order ";
        
        $where = "WHERE";
        
        if($period_start!=''){
            $date = explode('-',$period_start);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 00:01:01';
            $sql.="$where date_added>='".$time."' ";
            $where = "AND";
        }
        
        if($period_end!=''){
            $date = explode('-',$period_end);
            $time = $date[2].'-'.$date[1].'-'.$date[0].' 23:59:59';
            $sql.="$where date_added<='".$time."' ";
            $where = "AND";
        }
        
        if($status_delivery!=''){
            $sql.="$where status_delivery='".$status_delivery."'";
            $where = "AND";
        }
        
        if($status_payment!=''){
            $sql.="$where status_payment='".$status_payment."' ";
            $where = "AND";
        }
        
        $query = $this->db->query($sql);        
        if($query->num_rows() > 0){
			return $query->row();
		}
        
    }
    
    
    /* 
        Author  : mdhb2
        Desc    : Get all payment & delivery status from order
        
        @return array - payment & delivery status        
    */
	function get_payment_delivery_statuses()
	{
		$sql = "select status_payment from tbl_order group by status_payment order by status_payment asc";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result['status_payment'] = $query->result();
		}
        
        $sql = "select status_delivery from tbl_order group by status_delivery order by status_delivery asc";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result['status_delivery'] = $query->result();
		}
        
        return $result;
	}
    
    function get_product_category($idproduct)
    {
        $sql = "SELECT pk . * , k . * FROM tbl_produk_kategori pk, tbl_kategori k WHERE pk.id_kategori = k.id_kategori AND pk.id_produk=".$idproduct;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function display_category($category)
    {
        $cat = '';
        if(!empty($category)){
            foreach($category as $row){
                $cat.=ucwords($row->nama_kategori).' > ';
            }
        }
        
        return substr($cat,0,(strlen($cat)-3));
    }
    
    function display_category_tree($id_kategori)
    {
        $cat = '';
        
        $sql = "select * from tbl_kategori where id_kategori=".$id_kategori." limit 0,1";        
        $query = $this->db->query($sql);
        
        if($query->num_rows() > 0){
            $data = $query->row();
			
            if($data->id_parent>0){
                $cat.=$this->display_category_tree($data->id_parent).' > ';                
            }
            
            $cat.=ucwords($data->nama_kategori).' > ';
            
            
		}
        
        return substr($cat,0,(strlen($cat)-3));
    }
    
    function list_category($id_parent='')
    {
        $sql = "select * from tbl_kategori ";
        
        if($id_parent!=''){
            $sql.=" where id_parent=".$id_parent;
        }
        
        $sql.=" order by nama_kategori asc";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result();
		}
    }   
    
    /*<!--4kuikialie*/
    function get_payment_buyer($time_from=NULL, $time_to=NULL, $paid=NULL)
    {
        $sql = "select pb.*,u.*,o.* from tbl_payment_buyers pb, tbl_order o, tbl_user u where ";
        if ($paid == 'paid')
            $sql .="paid=1 and ";
        elseif($paid=='unpaid')
            $sql .="paid=0 and ";
        else
            $sql .="";
        
        if (empty($time_from) AND empty($time_to))
            $sql .= "pb.kode_order=o.kode_order and o.id_user=u.id_user;";
        else
            $sql .= "pb.created >'" . $time_from . " 00:00:00' and pb.created <'" . $time_to . " 23:59:59' and pb.kode_order=o.kode_order and o.id_user=u.id_user;";

        // echo $sql;exit;
        $query = $this->db->query($sql);
        // if ($query->num_rows() > 0)
        // {
            return $query->result_object();
        // }
        // return $sql;
    }
    
    /* <!--4kuikialie */

    function get_detail_merchant($id_user)
    {
        $sql = "SELECT *  FROM `tbl_user` WHERE `id_user` = " . $id_user;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_object();
        else
            return "";
    }

}