<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_m extends MY_Model {

	public function __construct()
	{
		parent::__construct();		
	}
    
     /* 
        Author  : mdhb2
        Desc    : Get banner by banner title
        $param string - banner name
        @return array banner data
    */
	public function get_banner($name)
    {
        $sql = "select * from banner where banner_name='".$name."' limit 0,1";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->row(); 
		}
    }

    function cek_option($name=''){
        $sql = "SELECT COUNT(*) AS jumlah FROM tbl_options";
        $sql .= " WHERE option_name='".$name."'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jumlah;
    }

    function get_option($name=''){
        $sql="select * from tbl_options WHERE option_name='".$name."'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data;
    }
}
