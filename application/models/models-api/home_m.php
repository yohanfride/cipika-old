<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_produk($limit='', $offset=''){
		$sql="select a.*, b.image, c.username,s.nama_store,s.store_status, d.count_comment
			from tbl_produk a
			join tbl_produkfoto b on a.id_produk=b.id_produk
			join tbl_user c on a.id_user=c.id_user
            join tbl_store s on c.id_user=s.id_user
			left join view_comment d on a.id_produk=d.id_produk
			where a.publish='1' and a.deleted=0 and s.store_status='approve'
			group by id_produk DESC LIMIT ".$offset.", ".$limit."";
        $q = $this->db->query($sql);
        $data = array();
        $data =$q->result();
        $q->free_result();
        return $data;
	}

	function get_produk_love($offset, $limit){
		$sql="select a.*, c.image, a.loved,s.nama_store,s.store_status
			from tbl_produk as a
			join tbl_produkfoto as c on a.id_produk=c.id_produk
            join tbl_user u on a.id_user=u.id_user
            join tbl_store s on u.id_user=s.id_user
			left join tbl_comment as b on a.id_produk=b.id_produk
			where a.publish='1' and a.deleted=0 and s.store_status='approve'
			group by a.id_produk order by a.loved desc limit ".$offset." , ".$limit."";            
        $q = $this->db->query($sql);
        $data =$q->result();
        $q->free_result();
        return $data;
	}

	function get_produk_disqus(){
		$sql="select a.*, c.image, count(b.id_comment) as komen
			from tbl_produk as a
			join tbl_produkfoto as c on a.id_produk=c.id_produk
            join tbl_user u on a.id_user=u.id_user
            join tbl_store s on u.id_user=s.id_user
			left join tbl_comment as b on a.id_produk=b.id_produk
			where a.publish='1' and a.deleted=0 and s.store_status='approve'
			group by a.id_produk order by komen desc limit 8";
		$q = $this->db->query($sql);
        $data =$q->result();
        $q->free_result();
        return $data;
	}

	function get_produk_pick($offset, $limit){
		$sql="select a.*, c.image
			from tbl_produk as a
			join tbl_produkfoto as c on a.id_produk=c.id_produk
            join tbl_user u on a.id_user=u.id_user
            join tbl_store s on u.id_user=s.id_user
			where a.pick='1' and a.publish='1' and a.deleted=0 and s.store_status='approve'
			group by a.id_produk order by date_modified desc limit ".$offset." , ".$limit."";
		$q = $this->db->query($sql);
        $data =$q->result();
        $q->free_result();
        return $data;
	}

}

/* End of file  */
/* Location: ./application/models/ */