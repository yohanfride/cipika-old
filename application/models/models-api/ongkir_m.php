<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ongkir_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_propinsi(){
		$sql="SELECT * FROM tbl_lokasi 
			where lokasi_kabupatenkota=0 
			and lokasi_kecamatan=0 
			and lokasi_kelurahan=0 
			order by lokasi_nama";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

}

/* End of file  */
/* Location: ./application/models/ */