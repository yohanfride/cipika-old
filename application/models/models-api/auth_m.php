<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_m extends MY_Model {

	public $variable;

	function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->helper('date');
		session_start();
	}

	function cek_email($email=''){
        $sql = "SELECT COUNT(*) AS jumlah FROM tbl_user";
        $sql .= " WHERE email='".$email."'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        return $data->jumlah;
	}

	function update_registrasi($data=''){
		$berhasil = FALSE;
		$this->db->where('id_user', $data['id_user']);
		$this->db->where('email', str_replace(md5('@'), '@', $data['email']));
		$this->db->where('activation_code', $data['kode_aktivasi']);
		$this->db->from('tbl_user');
		if($this->db->count_all_results() > 0) {
			$this->db->set('active', $data['status_aktif']);
			$this->db->update('tbl_user');
			$berhasil = TRUE;
		}
		return $berhasil;
	}

	function login($email='', $password='', $remember=''){
		$pass=md5($password);
		$sql="select * from tbl_user where (telpon='" . $email . "' or email ='".$email."') and password='".$pass."' and active=1";
		$query=$this->db->query($sql);
		if ($query->num_rows() === 1){
			$user = $query->row();
			$this->session->set_userdata('member', $user);
			if ($remember){
				$this->remember_user($user->id_user);
			}
			return true;
		} else {
			return false;
		}
	}

	function login_fb($email){
        $remember = 1;
		$sql="select * from tbl_user where email='".$email."' and active=1";
		$query=$this->db->query($sql);
		if ($query->num_rows() === 1){
			$user = $query->row();
			$this->session->set_userdata('member', $user);
			if ($remember){
				$this->remember_user($user->id_user);
			}
			return true;
		} else {
			return false;
		}
    }

	function remember_user($id){
		if (!$id)
		{
			return FALSE;
		}
		$salt = $this->salt();
		$this->db->update('tbl_user', array('remember_code' => $salt), array('id_user' => $id));
		if ($this->db->affected_rows() > -1)
		{
			$expire = (60*60*24*365*2);
			set_cookie(array(
			    'name'   => 'remember_code',
			    'value'  => $salt,
			    'expire' => $expire
			));
			return TRUE;
		}
		return FALSE;
	}	

	function salt(){
		$raw_salt_len = 16;
 		$buffer = '';
        $buffer_valid = false;
        if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
            $buffer = mcrypt_create_iv($raw_salt_len, MCRYPT_DEV_URANDOM);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
            $buffer = openssl_random_pseudo_bytes($raw_salt_len);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && @is_readable('/dev/urandom')) {
            $f = fopen('/dev/urandom', 'r');
            $read = strlen($buffer);
            while ($read < $raw_salt_len) {
                $buffer .= fread($f, $raw_salt_len - $read);
                $read = strlen($buffer);
            }
            fclose($f);
            if ($read >= $raw_salt_len) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid || strlen($buffer) < $raw_salt_len) {
            $bl = strlen($buffer);
            for ($i = 0; $i < $raw_salt_len; $i++) {
                if ($i < $bl) {
                    $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
                } else {
                    $buffer .= chr(mt_rand(0, 255));
                }
            }
        }

        $salt = $buffer;

        // encode string with the Base64 variant used by crypt
        $base64_digits   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        $bcrypt64_digits = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $base64_string   = base64_encode($salt);
        $salt = strtr(rtrim($base64_string, '='), $base64_digits, $bcrypt64_digits);
	    $salt = substr($salt, 0, 22);
		return $salt;
	}

    function check() {
        if (!$this->session->userdata('member')) {
            redirect(base_url(), 'refresh');
        }
    }

    function get_user(){
    	if($this->user){
    		// $data=$this->get_single('tbl_user', 'id_user', $this->session->userdata('member')->id_user);
    		$sql="select a.*, b.nama_propinsi, c.nama_kabupaten, d.nama_kecamatan
    			from tbl_user a
    			left join tbl_propinsi b on a.id_propinsi=b.id_propinsi
    			left join tbl_kabupaten c on a.id_kabupaten=c.id_kabupaten
    			left join tbl_kecamatan d on a.id_kecamatan=d.id_kecamatan
    			where a.id_user='". $this->user->id_user ."'";
    		$q=$this->db->query($sql);
    		$data=$q->row();
    		$q->free_result();
    	} else if($this->session->userdata('admin_session')){
    		$data=$this->get_single('tbl_user', 'id_user', $this->session->userdata('admin_session')->id_user);
    	}
    	return $data;
    }

    function get_propinsi($id=''){
    	$this->db->where('id_propinsi', $id);
    	$q=$this->db->get('tbl_propinsi');
    	$data=$q->row();
    	$q->free_result();
    	return $data->nama_propinsi;
    }

    function get_kabupaten($id=''){
    	$this->db->where('id_kabupaten', $id);
    	$q=$this->db->get('tbl_kabupaten');
    	$data=$q->row();
    	$q->free_result();
    	return $data->nama_kabupaten;
    }

    function get_kecamatan($id=''){
    	$this->db->where('id_kecamatan', $id);
    	$q=$this->db->get('tbl_kecamatan');
    	$data=$q->row();
    	$q->free_result();
    	return $data->nama_kecamatan;
    }
}

/* End of file  */
/* Location: ./application/models/ */