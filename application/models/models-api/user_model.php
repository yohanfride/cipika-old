<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends MY_Model {

    protected $table = "tbl_user";

    /*function do_upgrade($id_user, $month, $note) 
    {
        $this->db->insert("history", array(
            "id_user"           => $id_user,
            "execution_time"    => date('Y-m-d H:i:s'),
            "range"             => $month,
            "note"              => $note
        ));

        $query_user = $this->db->get_where("users", array("id_user" => $id_user));
        $user = $query_user->row_array();        
        
        $this->db->where("id_user", $id_user);
        if(strtotime($user['premium_end']) < now())
            $this->db->set("premium_end", "'" . date('Y-m-d H:i:s') . "' + INTERVAL " . $month . " MONTH", FALSE);
        else
            $this->db->set("premium_end", "`premium_end` + INTERVAL " . $month . " MONTH", FALSE);
        
        $this->db->update("users");

        $query = $this->db->get_where("users", array("id_user" => $id_user));

        return $query->row_array();
    }*/
    
    function email_fb_exists($email)
    {
        $query = $this->db->get_where("users", array("fb_email" => $email));
        return $query->row_array();
    }

    /*function upgrade_by_phone_number($phone) 
    {
        $query = $this->phone_exists($phone);
        if ($query->num_rows() > 0) {
            $id = $query[0]->id_user;
            $run = $this->do_upgrade($id, 1, "Upgrade from paypal");
            return true;
            exit();
        }
    }*/

    function get_albumid($id_user) 
    {
        $query = $this->db->get_where("users", array("id_user" => $id_user));
        $result = $query->row_array();
        return $result['album_id'];
    }

    function set_albumid($id_user, $albumid) 
    {
        $this->db->where("id_user", $id_user);
        $this->db->update("users", array("album_id" => $albumid));

        return $this->db->affected_rows();
    }
    function cek_user_id($phone) 
    {
        $query = $this->db->get_where("users", array("phone" => $phone));
        $result = $query->row_array();
        return $result['album_id'];
    }

    public function get_user_by_key($key)
    {
        $this->db->select('tbl_user.*');
        $this->db->join('keys', 'keys.id = tbl_user.id_key');
        $this->db->where('keys.' . config_item('rest_key_column'), $key);

        $user = $this->db->get('tbl_user')->row();
		/*	
        if (!$user) {
            return false;
        }

        if(strtotime($user->premium_end) < now())
            $premium_active = 0;
        else
            $premium_active = 1;

        $user->premium_active = $premium_active;
        $user->status = 0;
		*/
        return $user;
    }

    public function get_user_by_id($id_user)
    {
        $user = $this->db->where('id_user', $id_user)->get('tbl_user')->row();
        $obj = new StdClass();
        if($user) {
            $obj->id_user       = $user->id_user;
            $obj->username		= $user->username;
            $obj->firstname     = $user->firstname;
            $obj->lastname      = $user->lastname;
            $obj->address       = $user->alamat;
            $obj->email         = $user->email;
            $obj->phone         = $user->telpon;
            $obj->hp	        = $user->telpon;
            $obj->id_propinsi	= $user->id_propinsi;
            $obj->id_kabupaten  = $user->id_kabupaten;
            $obj->avatar       	= $user->image;
            $obj->id_kecamatan  = $user->id_kecamatan;
            $obj->birthdate  	= $user->birthdate;
        } else {
            $obj = FALSE;
        }
        return $obj;
    }
}

/* End of file user_model.php */
/* Location: ./application/models/user_Model.php */