<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_order_item($field='', $key=''){
		$sql="select a.*, b.kode_order, c.nama_produk from tbl_orderitem a, tbl_order b, tbl_produk c
			where a.id_order=b.id_order and a.id_produk=c.id_produk and ".$field."='".$key."'";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_order_all($limit='', $offset=''){
		$sql="select * from tbl_order order by id_order desc limit ".$offset.",".$limit."";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

	function get_order_user($id_user='', $limit='', $offset=''){
		$sql="select * from tbl_order where id_user='".$id_user."' order by id_order desc limit ".$offset.",".$limit."";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}
    
    function get_invoice($kode, $id_user)
    {
        $sql = "select * from tbl_invoices where kode_order='" . $kode . "'";
        $q = $this->db->query($sql);
        $data = $q->row();
        $q->free_result();
        if (!$data)
        {
            $sql = "INSERT INTO `tbl_invoices` (`id_invoice` ,`kode_order` ";
            $sql .= ",`created` ,`creator`)VALUES (NULL , '".$kode."',CURRENT_TIMESTAMP , '".$id_user."');";
            $q = $this->db->query($sql);
            if ($q)
            {
                $sql = "SELECT * FROM `tbl_invoices` WHERE `id_invoice` = '" . $this->db->insert_id() . "';";
                $q = $this->db->query($sql);
                $data = $q->row();
                $q->free_result();
                return $data;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return $data;
        }
    }
    
    function get_order_status_deliver(){
        $date = date('Y-m-d', mktime(0,0,0, date('m'), date('d') - 3 , date('Y')));
        $sql="select b.email, a.id_order, b.username, a.kode_order from tbl_order a, tbl_user b where a.id_user = b.id_user AND a.status_payment = 'done' AND a.status_delivery = 'sent' AND email_confirm = '0' AND a.date_modified < '".$date."'";
            $q=$this->db->query($sql);
            $data=$q->result();
            $q->free_result();
            return $data;
    }
    
    function get_order_reminder(){
        $date = date('Y-m-d H-i-s', mktime(date('H') - 4 , 0,0, date('m'), date('d') , date('Y')));
        $sql="select b.email, a.id_order, b.username, a.kode_order, a.total, a.ongkir_sementara from tbl_order a, tbl_user b where a.id_user = b.id_user AND a.status_payment = 'waiting' AND a.date_modified > '".$date."'";
            $q=$this->db->query($sql);
            $data=$q->result();
            $q->free_result();
            return $data;
    }

    function get_order_by_invoice($kode_invoice)
    {
        $sql = "select * from tbl_order where kode_invoice='".$kode_invoice."' order by id_order asc ";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
    }
    
    function get_orderitem($idorder)
    {
        $sql = "select oi.*,p.* from tbl_orderitem oi,tbl_produk p where oi.id_produk=p.id_produk and oi.id_order=".$idorder." order by id_orderItem asc ";        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
    }
    
    function get_order_shipping($idorder)
    {
        $sql = "select * from tbl_ordershipping where id_order=".$idorder." limit 0,1";        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->row(); 
		}
    }
    
    function get_payment_detail($kode_order)
    {
        $sql = "select * from tbl_payment_buyers where kode_order='".$kode_order."' limit 0,1";        
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->row(); 
		}
    }
    
}

/* End of file  */
/* Location: ./application/models/ */