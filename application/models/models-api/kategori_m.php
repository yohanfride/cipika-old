<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function get_list_kategori(){
		$sql="select a.id_kategori, a.nama_kategori, b.nama_kategori as parent, a.keterangan
			from tbl_kategori as a
			left join tbl_kategori as b
			on a.id_parent=b.id_kategori
			order by a.id_kategori asc";
		$q=$this->db->query($sql);
		$data=$q->result();
		$q->free_result();
		return $data;
	}

}

/* End of file  */
/* Location: ./application/models/ */