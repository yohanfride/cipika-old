<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meta_m extends MY_Model 
{

	public function __construct()
	{
		parent::__construct();
		
	}
    
    /* 
        Author  : mdhb2
        Desc    : Get All Meta Data
        
        @param string $master - Table name of master meta
        @param int $idmaster - id value of master table
        @return array - All meta data
        
    */
	function get_meta($master,$idmaster)
	{
		$sql = "select * from meta where meta_master_table='".$master."' and meta_idmaster=".$idmaster." order by idmeta asc";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	/* 
        Author  : mdhb2
        Desc    : Get Meta Value by Meta Key
        @param string $master - Table name of master meta
        @param int $idmaster - id value of master table
        @param string $meta_name - name of meta
        @return string - Meta value
    */
	function get_meta_value($master,$idmaster,$meta_name)
	{
		$sql = "select * from meta where meta_master_table='".$master."' and meta_idmaster=".$idmaster." and meta_name='".$meta_name."' limit 0,1";
       
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$data = $query->row(); 			
			return $data->meta_value;
		}
	}
    
    /* 
        Author  : mdhb2
        Desc    : Get Meta Data by Meta Key
        @param string $master - Table name of master meta
        @param int $idmaster - id value of master table
        @param string $meta_name - name of meta
        @return string - Meta value
    */
	function get_meta_data($master,$idmaster,$meta_name)
	{
		$sql = "select * from meta where meta_master_table='".$master."' and meta_idmaster=".$idmaster." and meta_name='".$meta_name."' limit 0,1";
       
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			return $query->row();
		}
	}
    
    /* 
        Author  : mdhb2
        Desc    : Save meta to DB (for add new or edit)
        @param string $master - Table name of master meta
        @param int $idmaster - id value of master table        
        @param array meta_name => meta_value pairs of data
        @param string $mode - clear : delete all previous meta data, add : just add new without clear previous meta data
        @return void
    */
    function save_meta($master,$idmaster,$metas,$mode='clear')
    {   
        if(!empty($metas)){
            foreach($metas as $k=>$v){
                $insert_meta = array('idmeta'		        => null,
									 'meta_master_table'	=> $master,
									 'meta_idmaster'		=> $idmaster,
									 'meta_name'		    => trim($k),
									 'meta_value'	        => trim($v)
									);
                if($mode=='clear'){
                    $sql = "DELETE FROM meta WHERE meta_master_table='".$master."' and meta_idmaster=".$idmaster." and meta_name='".trim($k)."'";
                    $query = $this->db->query($sql);                    
                }

                $this->db->insert('meta',$insert_meta);
            }
        }
    }
    
	/* 
        Author  : mdhb2
        Desc    : delete meta by master
        @param string $master - Table name of master meta
        @param int $idmaster - id value of master table        
        @return void
    */
	function clear_meta($master,$idmaster)
	{
		$sql = "DELETE FROM meta WHERE meta_master_table='".$master."' and master_idmaster=".$idmaster;
		$query = $this->db->query($sql);
	}
    
    /* 
        Author  : mdhb2
        Desc    : delete meta by idmeta        
        @param int $idmaster - idmeta
        @return void
    */
	function delete_meta($idmeta)
	{
		$sql = "DELETE FROM meta WHERE idmeta=".$idmeta;        
		$query = $this->db->query($sql);
	}
    
    /* 
        Author  : mdhb2
        Desc    : Update meta by idmeta
        @param int $idmaster - idmeta
        @param string $value - new meta value
        @return void
    */
	function update_meta($idmeta,$value)
	{
		$sql = "UPDATE meta SET meta_value='".$value."' WHERE idmeta=".$idmeta;       
		$query = $this->db->query($sql);
	}
    
    /* 
        Author  : mdhb2
        Desc    : Get all sales (user level=3)        
        @return array all user with roles sales
    */
	function get_sales()
	{
		$sql = "select u.*,k.* from tbl_user u,tbl_kota k where u.id_level=3 and u.id_kota=k.id_kota order by u.first_name asc";
		$query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
	}
}