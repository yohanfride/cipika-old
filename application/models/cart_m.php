<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();		
	}

    function sum_qty($id=''){
        $sql="select sum(qty) as qty from tbl_cart where id_user='". $id ."'";
        $q=$this->db->query($sql);
        $data=$q->row();
        $q->free_result();
        return $data->qty;
    }

    function get_item($id_produk='', $id_user=''){
        $this->db->where('id_produk', $id_produk);
        $this->db->where('id_user', $id_user);
        $q=$this->db->get('tbl_cart');
        $data=$q->row();
        $q->free_result();
        return $data;
    }
    
    function get_invoice($id=''){
        $sql="select a.*, b.nama_payment as payment, c.nama, c.alamat, c.telpon, c.email, d.nama_propinsi, e.nama_kabupaten, f.*
            from tbl_order a, tbl_payment b, tbl_ordershipping c, tbl_propinsi d, tbl_kabupaten e, tbl_invoices f
            where a.id_payment=b.id_payment and a.id_order=c.id_order and c.id_provinsi=d.id_propinsi and c.id_kota=e.id_kabupaten and f.kode_invoice=a.kode_invoice and md5(f.id_invoice)='".$id."'";
        $q=$this->db->query($sql);
//        echo $sql;
        $data=$q->row();
        $q->free_result();
        return $data;
    }    
    
    function get_order($id=''){
        $sql="select a.*, b.nama_payment as payment, c.nama, c.alamat, c.telpon, c.email, c.id_kecamatan, d.nama_propinsi, e.nama_kabupaten, f.nama_kecamatan
            from tbl_order a
            left join tbl_payment b on a.id_payment=b.id_payment
            left join tbl_ordershipping c on a.id_order=c.id_order
            left join tbl_propinsi d on c.id_provinsi=d.id_propinsi
            left join tbl_kabupaten e on c.id_kota=e.id_kabupaten
            left join tbl_kecamatan f on c.id_kecamatan=f.id_kecamatan  
            where md5(a.id_order)='".$id."'";
        $q=$this->db->query($sql);
        $data=$q->row();
        $q->free_result();
        return $data;
    }    
    
    function get_order_by_invoice($id=''){
        $sql="select a.*, b.nama_payment as payment, c.nama, c.alamat, c.telpon, c.email, c.id_kecamatan, d.nama_propinsi, e.nama_kabupaten, f.nama_kecamatan
            from tbl_order a
            left join tbl_payment b on a.id_payment=b.id_payment
            left join tbl_ordershipping c on a.id_order=c.id_order
            left join tbl_propinsi d on c.id_provinsi=d.id_propinsi
            left join tbl_kabupaten e on c.id_kota=e.id_kabupaten
            left join tbl_kecamatan f on c.id_kecamatan=f.id_kecamatan  
            where md5(a.kode_invoice)='".$id."'";
        $q=$this->db->query($sql);
        $data=$q->result();
        $q->free_result();
        return $data;
    }     

    function get_order_shipping($id=''){
        $sql="select a.*, b.nama_propinsi, c.nama_kabupaten
            from tbl_ordershipping a, tbl_propinsi b, tbl_kabupaten c
            where a.id_provinsi=b.id_propinsi and a.id_kota = c.id_kabupaten and a.id_order='".$id."'";
        $q=$this->db->query($sql);
        $data=$q->row();
        $q->free_result();
        return $data;
    }

    function get_order_item($id=''){
        $sql="select a.*, b.nama_produk
            from tbl_orderitem a, tbl_produk b
            where a.id_produk=b.id_produk and md5(a.id_order)='".$id."'";            
        $q=$this->db->query($sql);
        $data=$q->result();
        $q->free_result();
        return $data;
    }

    function get_user($id=''){
        $sql="select a.*, b.nama_kabupaten as kabupaten, c.nama_propinsi as propinsi, d.nama_kecamatan as kecamatan
            from tbl_user a, tbl_kabupaten b, tbl_propinsi c, tbl_kecamatan d
            where a.id_kabupaten=b.id_kabupaten and a.id_propinsi=c.id_propinsi and a.id_kecamatan = d.id_kecamatan and a.id_user='".$id."'";
        $q=$this->db->query($sql);
        $data=$q->row();
        $q->free_result();
        return $data;
    }

    function get_cart_qty(){        
        if(isset($_SESSION['cart'])){
            $qty=count($_SESSION['cart']);
        } else {
            $qty=0;
        }
        return $qty;
    }

    function get_merchant($id=''){
        // $this->db->where('id_user', $id);
        // $q=$this->db->get('tbl_user');
        $sql="select a.*, b.nama_propinsi, c.nama_kabupaten from tbl_store a
            left join tbl_kecamatan d on a.id_kota=d.id_kecamatan
            left join tbl_kabupaten c on d.id_kabupaten=c.id_kabupaten
            left join tbl_propinsi b on c.id_propinsi=b.id_propinsi
            where a.id_user='".$id."'";
        $q=$this->db->query($sql);
        $data=$q->row();
        $q->free_result();
        return $data;
    }

    function get_produk($id=''){
        $sql="select a.*, b.image from tbl_produk a, tbl_produkfoto b
            where a.id_produk = b.id_produk and a.id_produk = '".$id."' group by a.id_produk";
        $q=$this->db->query($sql);
        $data=$q->row();
        $q->free_result();
        return $data;
    }
        
    function get_cart_item(){
        foreach ($_SESSION['cart'] as $key) {
            $id[]=$key['id_produk'];
        }
        $data = '\'' . implode('\', \'', $id) . '\'';
        $sql="select * from tbl_produk where id_produk in (".$data.")";
        $q=$this->db->query($sql);
        $data=$q->result();
        $q->free_result();
        return $data;
    }

    function get_new_id_order($key=''){
        $q = $this->db->query("select count(*) as jumlah from tbl_order where (select left(kode_order, 9))='" . $key. "' ");
        $data = $q->row();
        $q->free_result();
        return $data;
    }
    
    function get_new_id_invoice($key=''){
        $q = $this->db->query("select count(*) as jumlah from tbl_invoices where (select left(kode_invoice, 6))='" . $key. "' ");
        $data = $q->row();
        $q->free_result();
        return $data;
    }
    
    function check_kategori($id='', $id_kategori=''){
         $sql="select * from tbl_produk_kategori
            where id_produk = '".$id."' and id_kategori = '".$id_kategori."'";
        $q=$this->db->query($sql);
        $data=$q->num_rows();
        $q->free_result();
        if($data)
        return 1;
        else {
            return 0;
        }
    }

    function cek_jne($from, $to){
        $data = array(
            'transaction_id' => $this->get('transaction_id'),
            's_propinsi' => $this->get('s_propinsi'),
            's_kota_kab' => $this->get('s_kota_kab'),
            'd_kecamatan' => $this->get('d_kecamatan'),
            'd_kota_kab' => $this->get('d_kota_kab'),
            'd_propinsi' => $this->get('d_propinsi'),
            'weight' => $this->get('weight'),
            'volume' => $this->get('volume'),
        );
        $data_string = json_encode($data);

        $url = "http://124.81.102.190:543/shipment/page/api_jne.php?key=" . urlencode($data_string);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec($ch);
        curl_close($ch);

        $login = json_decode($login);
        
        if ($login)
        {
            return $login;
        }
        else
        {
            echo 'Maaf Ongkos kirim tidak ditemukan.';
        }
    }

}

/* End of file  */
/* Location: ./application/models/ */