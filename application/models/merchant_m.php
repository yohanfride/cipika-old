<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_m extends MY_Model 
{

	public function __construct()
	{
		parent::__construct();
		
	}
    
    /* 
        Author  : mdhb2
        Desc    : Get all sales (user level=3)        
        @return array all user with roles sales
    */
	function get_sales()
	{
		$sql = "select u.*,k.* from tbl_user u,tbl_kota k where u.id_level=3 and u.id_kota=k.id_kota and u.deleted=0 order by u.firstname asc";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
	}
    
    /* 
        Author  : mdhb2
        Desc    : Get all provinsi
        @return array all provinsi
    */
	function get_provinsi()
	{
		$sql = "select * from tbl_propinsi order by nama_propinsi asc";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
	}
    
    /* 
        Author  : mdhb2
        Desc    : Get kota by provinsi
        @param int idprovinsi
        @return array city
    */
	function get_city($id)
	{
		$sql = "select * from tbl_kabupaten where id_propinsi=".$id." order by nama_kabupaten asc";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
	}
    
    function get_merchant($start=0,$limit=50)
    {
        $sql = "select m.*,u.*,m.date_added as 'merchant_register_date', m.telpon as 'merchant_telp' from tbl_store m,tbl_user u where m.id_user=u.id_user and u.deleted=0 order by m.date_added DESC limit $start,$limit";
        
        $query = $this->db->query($sql);
        $data=$query->result();
        $query->free_result();
        return $data;
  //       if($query->num_rows() > 0){
		// 	return $query->result(); 
		// }
    }
    
    function get_single_merchant($iduser)
    {
        $sql = "select m.*,u.*,m.id_kota as 'merchant_kota',m.telpon as 'merchant_telpon', m.merchant_hp as 'merchant_hp' , m.email as 'merchant_email' from tbl_store m,tbl_user u where u.id_user=".(int)$iduser." AND m.id_user=u.id_user limit 0,1";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->row(); 
		}
    }
    
    function get_merchant_product($iduser,$start=0,$limit=20)
    {
        $sql = "select p.*,m.*,u.* from tbl_produk p,tbl_store m,tbl_user u where u.id_user=".(int)$iduser." AND m.id_user=u.id_user AND p.id_user=u.id_user order by p.id_produk desc limit $start,$limit";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
    }
    
    /* Order *********************************************************************************************/    
    
    /* 
        Author  : mdhb2
        Desc    : Get all order detail, buyer,shipping,order item
        @param int id_order
        @return array order detail
    */
    function get_order($idorder)
    {
        $sql = "select * from tbl_order o,tbl_user u,tbl_ordershipping os where os.id_order=o.id_order and o.id_user=u.id_user and o.id_order=".$idorder." limit 0,1";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			$result['order'] = $query->row(); 
            
            $sql2 = "select oi.*,p.* from tbl_orderitem oi,tbl_produk p where oi.id_order=".$idorder." and oi.id_produk=p.id_produk order by oi.id_orderitem asc";
            $query2 = $this->db->query($sql2);
            if($query2->num_rows() > 0){
                $result['items'] = $query2->result(); 
            }            
            
            return $result;
		}
    }
    
    
    function get_order_payment($kode_order)
    {
        $sql = "select * from tbl_payment_buyers where kode_order='".$kode_order."' limit 0,1";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->row(); 
		}
    }
    
    function get_merchant_order($idmerchant,$start=0,$limit=100,$paid_status='')
    {
        $sql = "select * from tbl_order where id_merchant=".$idmerchant;
        
        if($paid_status!=''){
            $sql.=" AND status_payment='".$paid_status."' ";
        }
        
        $sql.=" order by date_added desc limit $start,$limit";
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
			return $query->result(); 
		}
    }
}