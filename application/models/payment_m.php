<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_m extends MY_Model {

    public $variable;
    public $tbl_option = 'tbl_options';
    public $tbl_payment_buyer = 'tbl_payment_buyers';

    public function __construct()
    {
        parent::__construct();
    }

    function get_option($option_name, $option_value = NULL, $id = '')
    {
        if ($id == '')
        {
            $sql = "SELECT * FROM " . $this->tbl_option . " WHERE `option_name` = '" . $option_name . "';";
            $q = $this->db->query($sql);
            $data = $q->row_array();

            if ($data)
            {
                return $data;
            }
            else
            {
                $sql = "INSERT INTO `tbl_options` (`id_option` , ";
                $sql.="`option_name` ,`option_value`)VALUES (NULL , '" . $option_name . "', '" . $option_value . "');";
                $q = $this->db->query($sql);
                if ($q)
                {
                    $sql = "SELECT * FROM " . $this->tbl_option . " WHERE `id_option` = '" . $this->db->insert_id() . "';";
                    $q = $this->db->query($sql);
                    $data = $q->row_array();

                    return $data;
                }
                else
                {
                    return 0;
                }
            }
        }
        else
        {
            $sql = "UPDATE `tbl_options` SET `option_value` = '" . $option_value . "' WHERE `tbl_options`.`id_option` =" . $id . ";";
            $q = $this->db->query($sql);
            if ($q)
            {
                $sql = "SELECT * FROM " . $this->tbl_option . " WHERE `id_option` = '" . $id . "';";
                $q = $this->db->query($sql);
                $data = $q->row_array();

                return $data;
            }
            else
            {
                return 0;
            }
        }
    }

    function get_payment_buyer($limit = 100, $offset = 0, $search = NULL)
    {
        if (!empty($search))
            $sql = "SELECT * FROM " . $this->tbl_payment_buyer . " where kode_order like '%" . $search . "%' ORDER BY created DESC LIMIT " . $offset . "," . $limit . ";";
        else
            $sql = "SELECT * FROM " . $this->tbl_payment_buyer . " ORDER BY created DESC LIMIT " . $offset . "," . $limit . ";";

        $q = $this->db->query($sql);
        $data = $q->result_object();
        return $data;
    }
    
    function get_payment($kode_invoice, $id_payment)
    {
        if ($id_payment == 1)
        {
            $sql = "SELECT *  FROM `tbl_payment_buyers` WHERE `kode_order` LIKE '%" . $kode_invoice . "%' and `payment`='BANKTRANFER';";

            $q = $this->db->query($sql);
            $data = $q->row_object();
            return $data->respond;
        }
        else
        {
            $sql = "SELECT *  FROM `tbl_payment_buyers` WHERE `kode_order` LIKE '%" . $kode_invoice . "%' and `payment`='DOMPETKU' order by id_payments desc;";

            $q = $this->db->query($sql);
            $data = $q->row_object();
            
            return $data;
        }
    }
    
    function get_payment_single($kode_invoice, $id_payment)
    {
        if ($id_payment == 1)
        {
            $sql = "SELECT *  FROM `tbl_payment_buyers` WHERE `kode_order` LIKE '%" . $kode_invoice . "%' and `payment`='BANKTRANFER';";

            $q = $this->db->query($sql);
            $data = $q->row_object();
            return $data;
        }
        else
        {
            $sql = "SELECT *  FROM `tbl_payment_buyers` WHERE `kode_order` LIKE '%" . $kode_invoice . "%' and `payment`='DOMPETKU' order by id_payments desc;";

            $q = $this->db->query($sql);
            $data = $q->row_object();
            
            return $data;
        }
    }
    
    function get_det_payment($id_payment)
    {
        $sql = "SELECT *  FROM tbl_payment WHERE id_payment=" . $id_payment . ";";

        $q = $this->db->query($sql);
        $data = $q->row_object();
        return $data;
    }

}

/* End of file  */
/* Location: ./application/models/ */