<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_m extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	function urutan(){
		$sql="SELECT max(sorting) as jml FROM tbl_page";
		$q=$this->db->query($sql);
        $data =$q->row();
        $q->free_result();
        return $data->jml;
	}
}

/* End of file  */
/* Location: ./application/models/ */