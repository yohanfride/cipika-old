<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">Users</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <h3>Members</h3>
            <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
            <a href="<?=admin_url();?>user/add_member" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <br><br>
            <?php } ?>
            <div class="table-responsive">
              <table class="table datatable table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Kab/kota</th>
                    <th>Tgl Daftar</th>
                    <!-- <th>group</th>
                    <th>status</th> -->
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr class="<?php 
                    if($data->id_level==1) echo 'active';
                    else if($data->id_level==2) echo 'success';
                    else if($data->id_level==3) echo 'warning';
                    else if($data->id_level==4) echo 'danger';
                  ?>">
                    <td><?=$i;?></td>
                    <td><?=$data->username;?></td>
                    <td><?=$data->email;?></td>
                    <td><?=$data->nama_kabupaten;?></td>
                    <td><?=$data->date_added;?></td>
                    <!-- <td><?=$data->nama_group;?></td>
                    <td><?=$data->nama_status;?></td> -->
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <td>
                      <a href="<?=admin_url();?>user/edit_member/<?=$data->id_user;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>user/hapus/<?=$data->id_user;?>/member" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td> 
                    <?php } ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->        
        
      </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>