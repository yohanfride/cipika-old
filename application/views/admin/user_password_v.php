<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>user</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>user">user</a></li>
              <li class="active">password</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="<?=admin_url();?>user/password">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Old Password</label>
                    <div class="col-lg-4">
                      <input value="" name="old_password" type="password" class="form-control" id="old_password" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">New Password</label>
                    <div class="col-lg-4">
                      <input name="password" type="password" class="form-control" id="password" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">New Password Confirmation</label>
                    <div class="col-lg-4">
                      <input name="passconf" type="password" class="form-control" id="passconf" placeholder="" >
                    </div>                  
                  </div>
                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>