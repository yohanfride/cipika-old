<?php $this->load->view('admin/header.php'); ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Settlement</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>settlement">Settlement</a></li>
              <li class="active">List</li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <!-- <a href="<?=admin_url();?>settlement/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a> -->
            <!-- <br><br> -->
            <!-- <form method="post" action="<?=admin_url()?>settlement"> -->
            <div class="table-responsive">
              <table class="table table-hover table-striped datatable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Settlement</th>
                    <th>Total (Rp)</th>
                    <th>Admin</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr>
                    <td><?=$i?></td>                    
                    <td><?=$data->tanggal?></td>
                    <td><a href="<?=admin_url()?>settlement/detail/<?=$data->id_settlement?>"><?=$data->kode_settlement?></a></td>
                    <td><?=$this->cart->format_number($data->total)?></td>
                    <td><?=$data->admin?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <a href="<?=admin_url()?>settlement" class="btn btn-primary">Kembali</a>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer.php'); ?>