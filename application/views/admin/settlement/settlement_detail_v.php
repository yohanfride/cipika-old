<?php $this->load->view('admin/header.php'); ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Settlement</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>settlement">Settlement</a></li>
              <li class="active">Detail</li>
            </ol>
          </div>
        </div><!-- /.row -->


        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-striped">
                  <tr>
                    <td width="150">Tanggal</td>
                    <td width="5">:</td>
                    <td><?=$settlement->tanggal?></td>
                  </tr>
                  <tr>
                    <td>Kode Settlement</td>
                    <td>:</td>
                    <td><strong><?=$settlement->kode_settlement?></strong></td>

                  </tr>
                  <tr>
                    <td>Total (Rp)</td>
                    <td>:</td>
                    <td><?=$this->cart->format_number($settlement->total)?></td>
                  </tr>
                  <tr>
                    <td>Admin</td>
                    <td>:</td>
                    <td><?=$settlement->admin?></td>
                  </tr>
              </table>
          </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <!-- <p>Kode Settlement : <strong><?=$settlement->kode_settlement?></strong></p> -->
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Order</th>
                    <th>Nama Merchant</th>
                    <th>Bank</th>
                    <th>Nama Pemilik</th>
                    <th>No Rekening</th>
                    <th>Total yang diminta (Rp)</th>
                    <th>Total Transfer (Rp)</th> 
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $i=0;foreach ($datas as $data) { $i++;
                  ?>
                  <tr>
                    <td><?=$i?></th>
                    <td><?=$data->kode_order?></td>
                    <td><?=$data->nama_store?></td>
                    <td><?=$data->bank_nama?></td>
                    <td><?=$data->bank_norek?></td>
                    <td><?=$data->bank_pemilik?></td>
                    <td><?=$this->cart->format_number($data->total_request)?></td>
                    <td><?=$this->cart->format_number($data->total_transfer)?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <a href="<?=admin_url()?>settlement/view" class="btn btn-primary">Kembali</a>
            <a href="<?=admin_url()?>settlement/export/<?=$data->id_settlement?>" class="btn btn-primary"><i class="fa fa-file"> Export to Excel</i></a>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer.php'); ?>