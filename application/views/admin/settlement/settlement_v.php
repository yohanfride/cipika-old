<?php $this->load->view('admin/header.php'); ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Settlement</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">Settlement</li>
            </ol>
            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <!-- <a href="<?=admin_url();?>settlement/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a> -->
            <!-- <br><br> -->
            <form method="post" action="<?=admin_url()?>settlement">
            <div class="table-responsive">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th><input class="check_all" type="checkbox"></th>
                    <th>Kode Order</th>
                    <th>Nama Merchant</th>
                    <th>Bank</th>
                    <th>Nama Pemilik</th>
                    <th>No Rekening</th>
                    <th>Total yang diminta (Rp)</th>
                    <th>Total Transfer (Rp)</th> 
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $i=0;foreach ($datas as $data) { $i++;
                    $total_transfer = $data->total + $data->ongkir_sementara;
                  ?>
                  <tr>
                    <td><input name="id_order[]" class="check_item" type="checkbox" value="<?=$data->id_order?>"></td>
                    <td><a href="<?=admin_url()?>order/detail/<?=$data->id_order?>"><?=$data->kode_order?></a></td>
                    <td><?=$data->nama_store?></td>
                    <td><?=$data->bank_nama?></td>
                    <td><?=$data->bank_norek?></td>
                    <td><?=$data->bank_pemilik?></td>
                    <td><?=$this->cart->format_number($total_transfer)?></td>
                    <td><?=$this->cart->format_number($total_transfer)?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>

            <button name="simpan" value="1" type="submit" class="btn btn-primary">Simpan Settlement</button>
            <a name="simpan" href="<?=admin_url()?>settlement/view" class="btn btn-success pull-right">Lihat Settlement</a>
            </form>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer.php'); ?>