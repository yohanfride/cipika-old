<?php include 'header.php'; ?>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1><?=$title;?></h1>
            <ol class="breadcrumb">
                <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                <li class="active">payment</li>
            </ol>
            <?php
            if ($alert == 'success')
            {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Success
                </div>
            <?php } ?>

            <?php
            if ($alert == 'failed')
            {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Failed
                </div>
            <?php } ?>
            <h3>Daftar payment</h3>
            <a href="<?=admin_url();?>payment/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <br><br>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>id_payment</th>
                            <th>nama_payment</th>
                            <th>action</th>                    
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($datas as $data)
                        {
                            $i++;
                            ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td><?=$data->id_payment;?></td>
                                <td><?=$data->nama_payment;?></td>
                                <td>
                                    <a href="<?=admin_url();?>payment/edit/<?=$data->id_payment;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="<?=admin_url();?>payment/delete/<?=$data->id_payment;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                                </td>                    
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <h3>Setting payment</h3>
            <!--<div class="table-responsive">-->
                <?php if ($alert == 'success')
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Success
                    </div>
                <?php } ?>

                <?php if ($alert == 'failed')
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Failed
                    </div>
<?php } ?>
                <form class="form-horizontal" method="post" action="<?=admin_url();?>payment/update_option">
                    <div class="form-group">
                        <label for="payment" class="col-lg-2 control-label">Cost Payment</label>
                        <div class="col-lg-4">
                            <input value="<?=$option['option_value'];?>" name="cost_payment" type="text" class="form-control" id="cost_payment">
                            <input value="<?=$option['id_option'];?>" name="id_option" type="hidden" class="form-control" id="id_option">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <button class="btn btn-primary pull-right" type="submit" name="simpan">Simpan</button>                  
                        </div>
                    </div>
                </form>     
            </div>
        </div>
    </div><!-- /.row -->

</div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>