<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1></h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>produk">Produk</a></li>
              <li class="active">Add</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="<?=admin_url();?>produk/add">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Product Name</label>
                    <div class="col-lg-4">
                      <input value="<?=(isset($post['nama_produk']))? $post['nama_produk']:'';?>" name="nama_produk" type="text" class="form-control" id="nama_produk" placeholder="">
                    </div>
                    <div class="col-lg-2">
                      <input value="1" name="pick" type="checkbox" id="pick" placeholder="" <?=(isset($post['pick']))? 'checked':'';?>>
                      Curator picks<br>
                      <input value="1" name="unggulan" type="checkbox" id="pick" placeholder="" <?=(isset($post['unggulan']))? 'checked':'';?>>
                      Produk Unggulan
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Description</label>
                    <div class="col-lg-4">
                      <textarea name="deskripsi" class="form-control" id="deskripsi" placeholder="" rows=5><?=(isset($post['deskripsi']))? $post['deskripsi']:'';?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Weight</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <input onkeypress="return isNumberKey(event)" value="<?=(isset($post['berat']))? $post['berat']:'';?>" name="berat" type="text" class="form-control" id="berat" placeholder="">
                      <span class="input-group-addon">Kg</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Dimension</label>
                    <div class="col-lg-9">
                        <div class="input-group col-lg-3 pull-left">
                          <input value="<?=(isset($post['panjang']))? $post['panjang']:'';?>" onkeypress="return isNumberKey(event)" value="" name="panjang" type="text" class="form-control" id="panjang" placeholder="panjang">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input value="<?=(isset($post['lebar']))? $post['lebar']:'';?>" onkeypress="return isNumberKey(event)" value="" name="lebar" type="text" class="form-control" id="lebar" placeholder="lebar">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input value="<?=(isset($post['tinggi']))? $post['tinggi']:'';?>" onkeypress="return isNumberKey(event)" value="" name="tinggi" type="text" class="form-control" id="tinggi" placeholder="tinggi">
                          <span class="input-group-addon">cm</span>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Stock</label>
                    <div class="col-lg-4">
                      <input value="<?=(isset($post['stok_produk']))? $post['stok_produk']:'';?>" onkeypress="return isNumberKey(event)" name="stok_produk" type="text" class="form-control" id="stok_produk" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Price from merchant</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input value="<?=(isset($post['harga_produk']))? $post['harga_produk']:'';?>" onkeypress="return isNumberKey(event)" name="harga_produk" type="text" class="form-control" id="harga_produk" placeholder="">
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Price for public</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input value="<?=(isset($post['harga_jual']))? $post['harga_jual']:'';?>" onkeypress="return isNumberKey(event)" name="harga_jual" type="text" class="form-control" id="harga_jual" placeholder="">
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Discount</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input value="<?=(isset($post['diskon']))? $post['diskon']:'';?>" onkeypress="return isNumberKey(event)" name="diskon" type="text" class="form-control" id="diskon" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group" id="category-parent">
                    <label for="" class="col-lg-2 control-label">Category</label>
                    <div class="col-lg-4">
                      <select name="id_kategori[]" class="form-control parent_category" id="id_kategori" data-sub="0" placeholder="">
                        <option value="">-- Choose Category --</option>
                        <?php foreach($kategori as $kat){ ?>
                        <option value="<?=$kat->id_kategori;?>"><?=ucwords($kat->nama_kategori);?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="sub-category-1"></div>


            <!-- <div class="form-group">
                <label for="" class="col-lg-2 control-label">Location</label>                
                <div class="col-lg-4">
                    <select  class="form-control" onchange="fillKota($(this).val())" name="id_provinsi">
                        <?php foreach($provinsi as $p){ ?>
                        <option value="<?=$p->id_provinsi;?>"><?=$p->nama_provinsi;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Kota</label>
                <div class="col-lg-4">
                    <select  class="form-control" id="select-kota" name="id_kota">
                    </select>
                </div>
            </div> -->

            <div class="form-group">
                <label class="col-lg-2 control-label">Merchant</label>
                <div class="col-lg-4">
                    <select  class="form-control" id="id_user" name="id_user">                      
                  <?php foreach($member as $row) { ?>
                  <option value="<?=$row->id_user;?>"><?=ucwords($row->nama_store);?></option>
                  <?php } ?>
                    </select>
                </div>
            </div>  

             <!--      <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Tag</label>
                    <div class="col-lg-4">
                      <input name="nama_tag" type="text" class="form-control" id="nama_tag" placeholder=""> comma separated
                    </div>
                  </div> -->
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Image</label>
                    <div class="col-lg-8">
                      <div class="row" id="photo-container">
                        <div id="add-photo1" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <div id="add-photo2" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <div id="add-photo3" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <div id="add-photo4" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                      </div>
                      <div class="row">
                         <div class="col-lg-8" style="color: #888">
                           * Maksimal ukuran foto adalah : <?php echo ini_get('upload_max_filesize') ?><br />
                           * Foto harus square (panjang dan lebar harus sama)<br />
                         </div>
                     </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>
<?php include 'upload.php'; ?>
