<?php include 'header.php'; ?>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1><?=$title;?></h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>">Dashboard</a></li>
              <li><a href="<?=admin_url();?>page">Page</a></li>
              <li class="active"><?=ucwords($title);?></li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form method="post" action="<?=admin_url();?>page/add" class="form-horizontal" role="form">
              <div class="form-group">
                <div class="col-lg-6">
                  <input name="title" type="text" class="form-control" id="title" placeholder="Title">
                </div>
                <div class="col-sm-1 pull-right">
                  <input value="<?=$sorting;?>" name="sorting" type="text" class="form-control" id="sorting" placeholder="Urutan">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <textarea cols="80" id="editor1" name="content" rows="50">
                  </textarea>
                </div>
              </div> 
              <div class="form-group">
                <div class="col-lg-12">
                  <input value="1" type="checkbox" name="publish"> <span>Publish</span>
                </div>
              </div> 
              <div class="form-group">
                <div class="col-lg-12">
                  <button class="btn btn-primary" type="submit" name="simpan" value="1">Simpan</button>
                </div>
              </div>
            </form>
          </div>         
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    <script type="text/javascript">
      var cktext='editor1';
    </script>    
    <?php include 'footer.php'; ?>
    <?php include 'tinymce.php'; ?>