<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Produk</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>produk">Produk</a></li>
              <li class="active">Setting</li>
            </ol>
            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success;?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            
            <form class="form-horizontal" method="post" action="<?=admin_url();?>produk/setting" enctype="multipart/form-data">
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <img src="<?=base_url()?>asset/img/watermark.png" class="img-thumbnail">
                </div>
              </div>
              <div class="form-group">
                <label for="watermark" class="col-lg-2 control-label">Watermark</label>
                <div class="col-lg-4">
                  <input name="watermark" type="file" class="form-control" id="watermark" placeholder="">
                  <small style="color:red;">*menggunakan file .png</small>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>