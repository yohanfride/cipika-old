<?php include 'header.php'; ?>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Orders</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
              <li class="active">Orders</li>
            </ol>
           <!--  <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              We're using <a class="alert-link" href="http://tablesorter.com/docs/">Tablesorter 2.0</a> for the sort function on the tables. Read the documentation for more customization options or feel free to use something else!
            </div> -->
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="search">
              <form method="GET" action="" class="form-inline" role="form">
                <div class="form-group">
                  <input value="<?=$fr?>" style="width:100px;" name="fr" type="text" class="form-control tanggal" id="dari_tanggal" placeholder="Dari Tgl">
                </div>
                <div class="form-group">
                  <input value="<?=$to?>" style="width:100px;" name="to" type="text" class="form-control tanggal" id="sampai_tanggal" placeholder="Sampai Tgl">
                </div>
                <div class="form-group">
                  <select style="width:130px;" onchange="this.form.submit()" name="mer" class="form-control">
                    <option value="">[merchant]</option>
                    <?php foreach ($merchant as $key => $value) { ?>
                    <option value="<?=$value->id_user?>" <?=($mer==$value->id_user)?'selected':'';?>><?=$value->nama_store?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <select style="width:100px;" onchange="this.form.submit()" name="sal" class="form-control">
                    <option value="">[sales]</option>
                    <?php foreach ($sales as $key => $value) { ?>
                    <option value="<?=$value->id_user?>" <?=($sal==$value->id_user)?'selected':'';?>><?=$value->username?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <select onchange="this.form.submit()" name="pay" class="form-control">
                    <option value="" selected>[payment]</option>
                    <option value="1" <?=($pay=='1')?'selected':'';?>>Paid</option>
                    <option value="2" <?=($pay=='2')?'selected':'';?>>Waiting</option>
                    <option value="3" <?=($pay=='3')?'selected':'';?>>Canceled</option>
                  </select>
                </div>
                <div class="form-group">
                  <select onchange="this.form.submit()" name="shi" class="form-control">
                    <option value="" selected>[shipping]</option>
                    <option value="1" <?=($shi=='1')?'selected':'';?>>Persiapan</option>
                    <option value="2" <?=($shi=='2')?'selected':'';?>>Proses</option>
                    <option value="3" <?=($shi=='3')?'selected':'';?>>Telah diterima</option>
                    <option value="4" <?=($shi=='4')?'selected':'';?>>Retur</option>
                  </select>
                </div>
                <div class="form-group">
                  <input style="width:130px;" value="<?=$loc_mer?>" name="loc_mer" type="text" class="form-control city_auto" id="cari_kota" placeholder="Kota Merchant">
                </div>
                <div class="form-group">
                  <input style="width:130px;" value="<?=$loc_buy?>" name="loc_buy" type="text" class="form-control city_auto" id="cari_kota" placeholder="Kota Buyer">
                </div>
                <br><br>
                <div class="form-group">
                  <input style="width:140px;" value="<?=$s?>" name="s" type="text" class="form-control" id="cari_produk" placeholder="Kode Order">
                </div>
                <button type="submit" class="btn btn-primary">Cari</button>
              </form>
            </div>
            <br>
            <div class="table-responsive">              
              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Kode Order</th>
                    <th>Kode Invoice</th>
                    <th>Total</th>
                    <th>payment</th>
                    <th>delivery</th>
                    <?php if($this->session->userdata('admin_session')->id_level=='1') { ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $i=0;foreach ($datas as $data) { 
                    $i++;
                    $payment = $this->order_m->get_payment_detail($data->kode_order);
                    $payment = (!empty($payment))?' ['.$payment->payment.']':'';
                  ?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$data->date_added;?></td>
                    <td><a href="<?=admin_url();?>order/detail/<?=$data->id_order;?>"><?=$data->kode_order;?></a></td>
                    <td><?php echo "<a href='".base_url('admin/order/detail_invoice/'.$invoices[$data->kode_order])."' target='_blank'>".$invoices[$data->kode_order]."</a>";?></td>
                    <td><?=$data->total;?></td>
                    <td><a href="<?=admin_url();?>order/detail_payment/<?=$data->id_order;?>"><?=status_payment_label($data->status_payment).$payment;?></a></td>
                    <!--<td><a href="<?=admin_url();?>order/detail_delivery/<?=$data->id_order;?>"><?=$data->status_delivery;?></a></td>--> 
                    <td>
                    <?php
                        echo "<a href='".base_url('admin/order/detail_delivery/'.$data->id_order)."'>".status_delivery_label($data->status_delivery)."</a>";
                    ?>
                    </td> 
                    <?php if($this->session->userdata('admin_session')->id_level=='1') { ?>
                    <td>
                      <?php if($data->status_payment!='canceled'){ ?>
                      <!--<a href="<?=admin_url();?>order/edit/<?=$data->id_order;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>-->
                      <a href="<?=admin_url();?>order/edit/<?=$data->id_order;?>/shipping" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit Shipping</a>
                      <a href="<?=admin_url();?>order/edit/<?=$data->id_order;?>/payment" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit Payment</a>
                      <!--<a href="<?=admin_url();?>order/delete/<?=$data->id_order;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>-->
                      <?php } else { echo 'Canceled'; } ?>
                    </td> 
                    <?php } ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          <?=$paginator;?>
          </div>
        </div><!-- /.row -->     

      </div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>