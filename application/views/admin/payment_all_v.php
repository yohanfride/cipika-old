<?php include 'header.php'; ?>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1><?= $title; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url(); ?>dashboard">Dashboard</a></li>
                <li class="active">payment</li>
            </ol>
            <?php
            if ($alert == 'success')
            {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Success
                </div>
            <?php } ?>

            <?php
            if ($alert == 'failed')
            {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Failed
                </div>
            <?php } ?>
            <h3>Daftar Pembayaran</h3>
            <form method="get" action="">
                <label>Search</label>
                <input type="text" id="search" name="search" style="padding: 5px 33px 6px 6px;" placeholder="Kode Order">
                <input type="submit" value="Search" style="padding: 5px 33px 6px 6px;">
                <a href="<?= admin_url(); ?>payment/all_payment"><input type="button" value="Refresh" style="padding: 5px 33px 6px 6px;"></a>
            </form>
            <br>
            <br>
            <div class="table-responsive">
                <form method="post" action="<?= admin_url(); ?>payment/release_all">
                    <table class="table table-bordered table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><div align='center'></div></th>
                        <th><div align='center'>No.</div></th>
                        <th><div align='center'>Nomor Invoice</div></th>
                        <th><div align='center'>Nominal</div></th>
                        <th><div align='center'>Jenis Pembayaran</div></th>
                        <th><div align='center'>VA</div></th>
                        <th><div align='center'>Status</div></th>
                        <th><div align='center'>Time</div></th>
                        <!-- <th><div align='center'>Action</div></th>                     -->
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($datas as $data)
                            {
                                $i++;
                                ?>
                                <tr>
                                    <td><div align='center'>
                                            <?php
                                            if ($data->paid != 0)
                                            {
                                                ?>
                                                <input type="checkbox" id="check[]" name="check[]" value="<?= $data->id_payments; ?>"></div></td>
                                        <?php
                                    }
                                    ?>
                                    <td><div align='center'><?= $i; ?></div></td>
                                    <td><div align='center'><?= $data->kode_order; ?></div></td>
                                    <td><div align='center'><?= $data->amount; ?></div></td>
                                    <td><div align='center'><?= $data->payment; ?></div></td>
                                    <td><div align='center'><?= $data->respond; ?></div></td>
                                    <td><div align='center'><?= $data->paid == 0 ? "Unpaid" : "Paid"; ?></div></td>
                                    <td><div align='center'><?= $data->created; ?></div></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <label>Action </label>
                    <select id="action" name="action" style="padding: 5px 33px 6px 6px;">
                        <option value="release">Release</option>
                        <option value="delete">Delete</option>
                    </select>
                    <input type="submit" id="submit" name="submit" value="Proses" style="padding: 5px 33px 6px 6px;">
                </form>
            </div>
        </div>
        <div style="margin-right:auto;margin-left: auto;width: 30%"><?php echo $paginator; ?></div>
    </div>
</div><!-- /.row -->

</div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>