<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>kota</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>kota">kota</a></li>
              <li class="active">Add</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>kota/add">
              <div class="form-group">
                <label for="kota" class="col-lg-2 control-label">Nama kota</label>
                <div class="col-lg-4">
                  <input name="nama_kota" type="text" class="form-control" id="nama_kota" placeholder="Kota Surabaya">
                </div>
              </div>
              <div class="form-group">
                <label for="kota" class="col-lg-2 control-label">provinsi</label>
                <div class="col-lg-4">
                  <select name="id_provinsi" type="text" class="form-control" id="id_provinsi">
                    <option value="">-- pilih provinsi --</option>
                    <?php foreach ($provinsi as $p) { ?>
                    <option value="<?=$p->id_provinsi;?>"><?=ucwords($p->nama_provinsi);?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>