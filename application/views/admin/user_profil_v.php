<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>user</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <!-- <li><a href="<?=admin_url();?>user">user</a></li> -->
              <li class="active">Profile</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

          <div class="row">
              <div class="col-lg-12"> 
                <form class="form-horizontal" method="post" action="<?=base_url();?>admin/user/profile/<?=md5($data->id_user);?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label"><a href="#" id="change_pp">Change Picture</a></label>
                    <div class="col-lg-6" id="img_pp">
                      <img id="previewHolder" src="<?=($data->image)? base_url().'asset/upload/profil/'.$data->image:base_url().'asset/img/no-avatar-single.png'; ?>">
                    </div>
                    <input type="file" id="filePhoto" class="hidden" name="user_image">
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Username</label>
                    <div class="col-lg-4">
                      <input value="<?=$data->username;?>" name="username" type="text" class="form-control" id="username" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">First Name</label>
                    <div class="col-lg-4">
                      <input value="<?=$data->firstname;?>" name="firstname" type="text" class="form-control" id="firstname" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Last Name</label>
                    <div class="col-lg-4">
                      <input value="<?=$data->lastname;?>" name="lastname" type="text" class="form-control" id="lastname" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Bio</label>
                    <div class="col-lg-4">
                      <textarea name="bio" class="form-control" id="bio" placeholder="" rows=5><?=$data->bio;?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Address</label>
                    <div class="col-lg-4">
                      <textarea name="alamat" class="form-control" id="alamat" placeholder=""><?=$data->alamat;?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Phone</label>
                    <div class="col-lg-4">
                      <input value="<?=$data->telpon;?>"name="telpon" type="number" class="form-control" id="telpon" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">City</label>
                    <div class="col-lg-4">
                      <select name="id_kota" type="text" class="form-control" id="id_kota" placeholder="">
                        <?php foreach ($kota as $k) { ?>
                        <option value="<?=$k->id_kota;?>" <?=($data->id_kota==$k->id_kota)?'selected':'';?>><?=$k->nama_kota;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                  </div>                      
                  
                  </div>
                </form>

              </div>
            </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>