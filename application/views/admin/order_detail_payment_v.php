<?php include 'header.php'; ?>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Orders</h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Orders</li>
            </ol>
            <!--  <div class="alert alert-info alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               We're using <a class="alert-link" href="http://tablesorter.com/docs/">Tablesorter 2.0</a> for the sort function on the tables. Read the documentation for more customization options or feel free to use something else!
             </div> -->
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <div align="center">

                    <label>Kode Order</label> :
                    <input type="text" disabled="" value="<?= $order->kode_order; ?>"><br>                    
                    <?php if(!empty($payment)):?>
                        <label>Payment</label> : <?= $payment->payment; ?><br>
                        <label>VAN ID</label> : <?= $payment->respond; ?><br>
                        <label>AMOUNT</label> : <?= $payment->amount; ?><br>
                        <label>Created</label> : <?= $payment->created; ?><br>
                        <label>Status</label> : <?= $payment->paid == 0 ? "Not Paid" : "Paid"; ?><br>                                       
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div><!-- /.row -->     

</div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>