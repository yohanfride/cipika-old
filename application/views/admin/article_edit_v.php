<?php include 'header.php'; ?>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1><?=$title;?></h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>">Dashboard</a></li>
              <li><a href="<?=admin_url();?>article">Article</a></li>
              <li class="active"><?=ucwords($title);?></li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form method="post" action="<?=admin_url();?>article/edit/<?=$data->id_article?>" class="form-horizontal" role="form">
              <div class="form-group">
                <div class="col-lg-6">
                  <input value="<?=$data->title;?>" name="title" type="text" class="form-control" id="title" placeholder="Title">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <textarea cols="80" id="content" name="content" rows="50"><?=$data->content;?>
                  </textarea>
                </div>
              </div>
              <div class="form-group">              
                <div class="col-lg-4">
                    <select  class="form-control" name="publish">                        
                      <option value="1" <?=($data->publish==1)?'selected':'';?>>Publish</option>
                      <option value="0" <?=($data->publish==0)?'selected':'';?>>Moderasi</option>                      
                      <option value="2" <?=($data->publish==2)?'selected':'';?>>Unpublish</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <button class="btn btn-primary" type="submit" name="simpan" value="1">Simpan</button>
                </div>
              </div>
            </form>
          </div>         
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->  
    <?php include 'footer.php'; ?>
    <?php include 'tinymce_1.php'; ?>