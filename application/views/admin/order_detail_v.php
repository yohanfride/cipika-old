<?php include 'header.php'; ?>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Orders</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
              <li><a href="<?=base_url('admin/order/');?>"><i class="fa"></i> Order</a></li>
              <li class="active">Order Detail</li>
            </ol>
           <!--  <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              We're using <a class="alert-link" href="http://tablesorter.com/docs/">Tablesorter 2.0</a> for the sort function on the tables. Read the documentation for more customization options or feel free to use something else!
            </div> -->
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
            
            <?php if(!empty($merchant)):?>
                <h4>Merchant</h4>
                <table class="table table-hover">                    
                    <tr>
                        <td width='20%'>Nama Merchant</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $merchant->nama_store ?></strong></td>
                    </tr>                    
                    <tr>
                        <td width='20%'>Nama Pemilik</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $merchant->nama_pemilik ?></strong></td>
                    </tr> 
                    <tr>
                        <td width='20%'>Alamat</td>
                        <td width='1%'>:</td>
                        <td><strong>
                            <?php 
                                
                                $lokasi = $this->lib_lokasi->get_lokasi($merchant->id_kecamatan_seller);
                                $lokasi = (!empty($lokasi))?ucwords($lokasi->nama_kecamatan).','.ucwords($lokasi->nama_kabupaten).','.ucwords($lokasi->nama_propinsi):'';
                                
                                echo $merchant->alamat.'<br/>'.$lokasi;
                            ?>
                            </strong>
                        </td>
                    </tr> 
                    <tr>
                        <td width='20%'>Telpon</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $merchant->telpon ?></strong></td>
                    </tr> 
                    <tr>
                        <td width='20%'>Email</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $merchant->email ?></strong></td>
                    </tr> 
                    <tr>
                        <td width='20%'>Bank</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $merchant->bank_nama.' :'.$merchant->bank_norek.' ('.$merchant->bank_pemilik.')' ?></strong></td>
                    </tr> 
                </table>
            <?php endif;?>
            
            <?php if(!empty($buyer)):?>
                <h4>Buyer Info</h4>
                <table class="table table-hover">                    
                    <tr>
                        <td width='20%'>Nama</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $buyer->firstname.' '.$buyer->lastname ?></strong></td>
                    </tr>
                    <tr>
                        <td width='20%'>Email</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $buyer->email ?></strong></td>
                    </tr>
                    <tr>
                        <td width='20%'>Telpon</td>
                        <td width='1%'>:</td>
                        <td><strong><?php echo $buyer->telpon.' '.$buyer->hp ?></strong></td>
                    </tr>
                    <tr>
                        <td width='20%'>Alamat</td>
                        <td width='1%'>:</td>
                        <td><strong>
                            <?php 
                                $lokasi   = $this->lib_lokasi->get_lokasi($buyer->id_kecamatan);
                                $lokasi = (!empty($lokasi))?ucwords($lokasi->nama_kecamatan).','.ucwords($lokasi->nama_kabupaten).','.ucwords($lokasi->nama_propinsi):'';
                                
                                echo $buyer->alamat.'<br/>'.$lokasi;
                            ?>
                            </strong>
                        </td>
                    </tr>
                </table>
            <?php endif;?>
                
                <?php if(!empty($shipping)):?>
                    <h4>Shipping Info</h4>
                    <table class="table table-hover">                    
                        <tr>
                            <td width='20%'>Nama</td>
                            <td width='1%'>:</td>
                            <td><strong><?php echo $shipping->nama ?></strong></td>
                        </tr>
                        <tr>
                            <td width='20%'>Alamat</td>
                            <td width='1%'>:</td>
                            <td><strong>
                                <?php                                     
                                    $lokasi = $this->lib_lokasi->get_lokasi($shipping->id_kecamatan);
                                    $lokasi = (!empty($lokasi))?ucwords($lokasi->nama_kecamatan).','.ucwords($lokasi->nama_kabupaten).','.ucwords($lokasi->nama_propinsi):'-';

                                    echo $shipping->alamat.'<br/>'.$lokasi; 
                                ?>
                            </strong></td>
                        </tr>
                        <tr>
                            <td width='20%'>Telpon</td>
                            <td width='1%'>:</td>
                            <td><strong><?php echo $shipping->telpon ?></strong></td>
                        </tr>
                        <tr>
                            <td width='20%'>Email</td>
                            <td width='1%'>:</td>
                            <td><strong><?php echo $shipping->email ?></strong></td>
                        </tr>                        
                    </table>   
                    <table class="table table-hover">                    
                        <tr>
                            <td width='20%'>Paket</td>
                            <td width='1%'>:</td>
                            <td><strong><?php echo $order->paket_ongkir ?></strong></td>
                        </tr>
                        <tr>
                            <td width='20%'>Harga</td>
                            <td width='1%'>:</td>
                            <td><strong>Rp <?php echo $order->ongkir_sementara ?></strong></td>
                        </tr>
                    </table> 
                <?php endif;?>
                
                <h4>Payment Info</h4>                
                    <table class="table table-hover">                        
                        <?php if(!empty($payment)):?>                        
                            <tr>
                                <td width='20%'>Status</td>
                                <td width='1%'>:</td>
                                <td><strong><?php echo $payment->paid == 0 ? "Not Paid" : "Paid"; ?></strong></td>
                            </tr>
                            <tr>
                                <td width='20%'>Payment</td>
                                <td width='1%'>:</td>
                                <td><strong><?php echo ucwords($payment->payment) ?></strong></td>
                            </tr>
                            <tr>
                                <td width='20%'>VAN ID</td>
                                <td width='1%'>:</td>
                                <td><strong><?php echo $payment->respond ?></strong></td>
                            </tr>
                            <tr>
                                <td width='20%'>Jumlah</td>
                                <td width='1%'>:</td>
                                <td><strong>Rp. <?php echo format_uang($payment->amount) ?></strong></td>
                            </tr>                             
                            <tr>
                                <td width='20%'>Tgl Dibuat</td>
                                <td width='1%'>:</td>
                                <td><strong><?php echo strftime("%d-%m-%Y",strtotime($payment->created)) ?></strong></td>
                            </tr>    
                            <tr>
                                <td width='20%'>Tgl Expired</td>
                                <td width='1%'>:</td>
                                <td><strong><?php echo ($payment->expired!='0000-00-00 00:00:00')?strftime("%d-%m-%Y",strtotime($payment->expired)):'-' ?></strong></td>
                            </tr>                            
                        <?php endif;?>                        
                    </table>
            
              <table class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama Produk</th>
                    <th>Berat</th>
                    <th>Volume</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;
                  $volume=$prod[$data->id_produk]->panjang*$prod[$data->id_produk]->lebar*$prod[$data->id_produk]->tinggi;
                  ?>
                  <tr>
                    <td><?=$i;?></td>                    
                    <td><?=$data->nama_produk;?></td>
                    <td><?=$prod[$data->id_produk]->berat;?></td>
                    <td><?=$volume;?></td>
                    <td><?=$data->harga;?></td>
                    <td><?=$data->jml_produk;?></td>
                    <td><?=$data->total;?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->     

      </div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>