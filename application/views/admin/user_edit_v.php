<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>user</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <?php if($edit=='admin'){ ?>
              <li><a href="<?=admin_url();?>user/admin">Admin</a></li>
              <?php } else { ?>
              <li><a href="<?=admin_url();?>user/member">Member</a></li>
              <?php } ?>
              <li class="active">Edit</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

<?php if($edit=='admin'){ ?>
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/edit/<?=$data->id_user;?>">
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">username</label>
                <div class="col-lg-4">
                  <input value="<?=$data->username;?>" name="username" type="text" class="form-control" id="username" placeholder="" disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">email</label>
                <div class="col-lg-4">
                  <input value="<?=$data->email;?>" name="email" type="email" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">firstname</label>
                <div class="col-lg-4">
                  <input value="<?=$data->firstname;?>" name="firstname" type="text" class="form-control" id="firstname" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">lastname</label>
                <div class="col-lg-4">
                  <input value="<?=$data->lastname;?>" name="lastname" type="text" class="form-control" id="lastname" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">kab/kota</label>
                <div class="col-lg-4">
                  <select name="id_kota" type="text" class="form-control" id="id_kota" placeholder="">
                    <option value="">-- pilih kota --</option>
                    <?php foreach ($kota as $k) { ?>
                    <option value="<?=$k->id_kota;?>" <?=($data->id_kota==$k->id_kota)?'selected':'';?>><?=$k->nama_kota;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">level</label>
                <div class="col-lg-4">
                  <select name="id_level" type="text" class="form-control" id="id_level" placeholder="">
                    <?php foreach ($level as $l) { ?>
                    <option value="<?=$l->id_level;?>" <?=($data->id_level==$l->id_level)?'selected':'';?>><?=$l->nama_level;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">status</label>
                <div class="col-lg-4">
                  <select name="id_status" type="text" class="form-control" id="id_status" placeholder="">
                    <?php foreach ($status as $s) { ?>
                    <option value="<?=$s->id_status;?>" <?=($data->id_status==$s->id_status)?'selected':'';?>><?=$s->nama_status;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->
<?php } ?>
<?php if($edit=='member'){ ?>
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/edit_member/<?=$data2->id_user;?>">
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Display Name</label>
                <div class="col-lg-4">
                  <input value="<?=$data2->username;?>" name="username" type="text" class="form-control" id="nama_store" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-4">
                  <input value="<?=$data2->email;?>" name="email" type="email" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Kab/kota</label>
                <div class="col-lg-4">
                  <select name="id_kota" type="text" class="form-control" id="id_kota" placeholder="">
                    <option value=""></option>
                    <?php foreach ($kota as $k) { ?>
                    <option value="<?=$k->id_kota;?>" <?=($data2->id_kota==$k->id_kota)?'selected':'';?>><?=$k->nama_kota;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Phone</label>
                <div class="col-lg-4">
                  <input value="<?=$data2->telpon;?>" name="telpon" type="text" class="form-control" id="telpon" placeholder="">
                </div>
              </div>
              
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Sales</label>
                <div class="col-lg-4">                
                   <select name="merchant_sales">
                                        <?php
                                            if(!empty($data_sales)){
                                                echo "<option value='-'>-- Select Sales --</option>";
                                                foreach($data_sales as $row){
                                                    $selected = ($curr_sales->meta_value==$row->id_user)?'selected':'';
                                                    echo "<option value='".$row->id_user."' ".$selected.">".$row->firstname." ".$row->lastname."</option>";
                                                }
                                            }
                                        ?>    
                    </select>
                </div>
              </div>
              
              
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
              
              <input type="hidden" name="idmeta" value="<?php echo (!empty($curr_sales))?$curr_sales->idmeta:''?>" readonly>
              
            </form>            
          </div>
        </div><!-- /.row -->
<?php } ?>

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>