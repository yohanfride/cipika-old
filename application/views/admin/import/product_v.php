<?php $this->load->view('admin/header') ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Merchant</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>              
              <li class="active">Import Product</li>
            </ol>
            
            <?php if(isset($_GET['s'])){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>
            
            <?php if(isset($_GET['e'])){ ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Upload File Error : <?php echo $_GET['e']?>
            </div>
            <?php } ?>

          </div>
        </div><!-- /.row -->


        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?php echo base_url('admin/import/import_product')?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">File Excel</label>
                    <div class="col-lg-4"><input type="file" name="file_excel"><br/>Format file harus xls, xlsx tidak support</div>
                </div>                
                <div class="form-group">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                      <button class="btn btn-primary pull-right" type="submit" name="simpan">Import</button>                  
                    </div>
                </div>
            </form>            
          </div>
        </div><!-- /.row -->



      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer') ?>