<?php $this->load->view('admin/header') ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Merchant</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>user/merchant">Merchant</a></li>
              <li class="active">Edit</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->


        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/edit_merchant/<?=$data->id_store;?>">
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Nama Merchant</label>
                    <div class="col-lg-4">
                    <input value="<?=$data->nama_store;?>" name="nama_store" type="text" class="form-control" id="nama_store" placeholder="Nama Toko">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Nama Pemilik</label>
                    <div class="col-lg-4">
                    <input value="<?=$data->nama_pemilik;?>" name="nama_pemilik" type="text" class="form-control" id="nama_pemilik" placeholder="Nama Toko">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Alamat</label>
                    <div class="col-lg-4">
                        <textarea name="alamat" class="form-control"><?=$data->alamat?></textarea>
                        <?php
                        $propinsi = $this->lib_lokasi->get_propinsi();
                        $lokasi = $this->lib_lokasi->get_lokasi($data->id_kota);
                        ?>

                            <select class="form-control" name="id_propinsi" id="propinsi" onchange="load_kabupaten('propinsi', 'div_kabupaten', 'id_kabupaten', 'id_kecamatan')">
                                <?php
                                if (!empty($propinsi))
                                {
                                    foreach ($propinsi as $row)
                                    {
                                        $selected = ($lokasi->id_propinsi == $row->id_propinsi) ? 'selected' : '';
                                        echo "<option value='" . $row->id_propinsi . "' " . $selected . ">" . ucwords($row->nama_propinsi) . "</option>";
                                    }
                                }
                                ?>
                            </select>  
                            <div id="div_kabupaten">                                         
                                <?php
                                if (!empty($lokasi))
                                {
                                    $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);
                                    if (!empty($kabupaten))
                                    {
                                        echo "<select class='form-control' name='id_kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                                        foreach ($kabupaten as $row)
                                        {
                                            $selected = ($lokasi->id_kabupaten == $row->id_kabupaten) ? 'selected' : '';
                                            echo "<option value='" . $row->id_kabupaten . "' " . $selected . ">" . ucwords($row->nama_kabupaten) . "</option>";
                                        }
                                        echo "</select>";
                                    }
                                }
                                ?>
                            </div>  
                            <div id="div_kecamatan">
                                <?php
                                if (!empty($lokasi))
                                {
                                    $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
                                    if (!empty($kecamatan))
                                    {
                                        echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                                        foreach ($kecamatan as $row)
                                        {
                                            $selected = ($lokasi->id_kecamatan == $row->id_kecamatan) ? 'selected' : '';
                                            echo "<option value='" . $row->id_kecamatan . "' " . $selected . ">" . ucwords($row->nama_kecamatan) . "</option>";
                                        }
                                        echo "</select>";
                                    }
                                }
                                ?>
                            </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Tgl Lahir</label>
                    <div class="col-lg-4">
                        <input value="<?=strftime("%d-%m-%Y",strtotime(substr($data->tgl_lahir_pemilik,0,10)));?>" name="tgl_lahir_pemilik" type="text" class="form-control" id="tgl_lahir">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Telp</label>
                    <div class="col-lg-4">
                        <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="telpon" value="<?=$data->telpon;?>" placeholder="0312281672">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Hp</label>
                    <div class="col-lg-4">
                        <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="hp" value="<?=$data->merchant_hp;?>" placeholder="085732897719">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Email</label>
                    <div class="col-lg-4"><?php echo $data->email?></div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Sales</label>
                    <div class="col-lg-4"><?php echo $sales->username?></div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Bank</label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="bank_nama" value="<?= $data->bank_nama ?>" placeholder="Mandiri">
                        <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="bank_norek" value="<?= $data->bank_norek ?>" placeholder="1234567890123">
                        <input type="text" class="form-control" name="bank_pemilik" value="<?= $data->bank_pemilik?>" placeholder="Pemegang Rekening">
                        
                    </div>
                </div>
                
                
             
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">status</label>
                    <div class="col-lg-4">                        
                        <select name="store_status" type="text" class="form-control" id="store_status" placeholder="">                        
                            <option value="pending" <?php echo ($data->store_status=='pending')?'selected':''?>>Pending</option>
                            <!-- <option value="review" <?php echo ($data->store_status=='review')?'selected':''?>>Proses Verifikasi</option> -->
                            <option value="approve" <?php echo ($data->store_status=='approve')?'selected':''?>>Verified</option>                        
                            <option value="block" <?php echo ($data->store_status=='block')?'selected':''?>>Unverified</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">                    
                    <h4>List Produk</h4>
                    
                    <div class="table-responsive">
                      <table class="table datatable table-hover table-striped tablesorter">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>nama</th>
                            <th>stok</th>
                            <th>harga kulak</th>
                            <th>harga jual</th>
                            <th>diskon</th>
                            <th>kategori</th>                            
                            
                            <th>status</th>
                            <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                            <th>action</th>
                            <?php } ?>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=0;foreach ($produk as $datap) { $i++;?>
                            <td><?=$i;?></td>
                            <td><a href="<?=admin_url();?>produk/edit/<?=$datap->id_produk;?>"><?=$datap->nama_produk;?></a></td>
                            <td><?=$datap->stok_produk;?></td>
                            <td><?=$datap->harga_produk;?></td>
                            <td><?=$datap->harga_jual;?></td>
                            <td><?=$datap->diskon;?></td>
                            <td><?=$this->produk_m->get_kategori($datap->id_produk);?></td>                            
                            
                            <td><?php echo status_product_label($datap->publish)?></td>
                            <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                            <td>
                              <a href="<?=admin_url();?>produk/edit/<?=$datap->id_produk;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                              <a href="<?=admin_url();?>produk/delete/<?=$datap->id_produk;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                            </td> 
                            <?php } ?>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                   
                   
                </div>
                
                <hr/>
                <div class="form-group">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                      <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                    </div>
                </div>
                <input type="hidden" name="curr_status" value="<?php echo $data->store_status?>" readonly>
            </form>            
          </div>
        </div><!-- /.row -->



      </div><!-- /#page-wrapper -->   
      <script type="text/javascript">

    function load_kabupaten(idpropinsi,container_kabupaten,nama_input_kabupaten)
    {
        var propinsi             = $('#'+idpropinsi).val();        
        $('#'+container_kabupaten).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kabupaten')?>",
                data    : "id_propinsi="+propinsi+"&nama_input_kabupaten="+nama_input_kabupaten,
                success : function(result){
                    $('#'+container_kabupaten).html(result);
                }
            });
    }
    
    function load_kecamatan(idkabupaten,container_kecamatan,nama_input_kecamatan)
    {
        var kabupaten = $('#'+idkabupaten).val();        
        $('#'+container_kecamatan).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kecamatan')?>",
                data    : "id_kabupaten="+kabupaten+"&nama_input_kecamatan="+nama_input_kecamatan,
                success : function(result){
                    $('#'+container_kecamatan).html(result);
                }
            });
    }
</script>
<?php $this->load->view('admin/footer') ?>
