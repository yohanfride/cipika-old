<?php $this->load->view('admin/header') ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Merchant</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>user/merchant">Merchant</a></li>
              <li class="active">Add</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/add_merchant">
              <h4>Profil Toko</h4>
              <hr>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-4">
                  <input name="email" id="email" type="email" class="form-control" placeholder="" value="<?php echo set_value('email')?>">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Nama Merchant</label>
                <div class="col-lg-4">
                  <input name="nama_store" type="text" class="form-control" id="nama_store" placeholder="" value="<?php echo set_value('nama_store')?>">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Biodata</label>
                <div class="col-lg-4">
                    <textarea name="biodata" class="form-control" id="biodata" placeholder="Biodata"><?php echo set_value('biodata')?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Alamat</label>
                <div class="col-lg-4">
                    <textarea name="alamat" class="form-control" id="alamat" placeholder="Alamat"><?php echo set_value('alamat')?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Lokasi</label>
                <div class="col-md-4">
                        <select class="form-control" name="id_propinsi" id="propinsi" onchange="load_kabupaten('propinsi', 'div_kabupaten', 'id_kabupaten', 'id_kecamatan')">
                            <?php
                            $propinsi = $this->lib_lokasi->get_propinsi();
                            if (!empty($propinsi))
                            {
                                foreach ($propinsi as $row)
                                {
                                    $selected = ($lokasi->id_propinsi == $row->id_propinsi) ? 'selected' : '';
                                    echo "<option value='" . $row->id_propinsi . "' " . $selected . ">" . ucwords($row->nama_propinsi) . "</option>";
                                }
                            }
                            ?>
                        </select>  
                        <div id="div_kabupaten">                                         
                            <?php
                            if (!empty($lokasi))
                            {
                                $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);
                                if (!empty($kabupaten))
                                {
                                    echo "<select class='form-control' name='id_kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                                    foreach ($kabupaten as $row)
                                    {
                                        $selected = ($lokasi->id_kabupaten == $row->id_kabupaten) ? 'selected' : '';
                                        echo "<option value='" . $row->id_kabupaten . "' " . $selected . ">" . ucwords($row->nama_kabupaten) . "</option>";
                                    }
                                    echo "</select>";
                                }
                            }
                            ?>
                        </div>  
                        <div id="div_kecamatan">
                            <?php
                            if (!empty($lokasi))
                            {
                                $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
                                if (!empty($kecamatan))
                                {
                                    echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                                    foreach ($kecamatan as $row)
                                    {
                                        $selected = ($lokasi->id_kecamatan == $row->id_kecamatan) ? 'selected' : '';
                                        echo "<option value='" . $row->id_kecamatan . "' " . $selected . ">" . ucwords($row->nama_kecamatan) . "</option>";
                                    }
                                    echo "</select>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
              <h4>Profil Pemilik</h4>
              <hr>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Nama Pemilik</label>
                <div class="col-lg-4">
                    <input name="nama_pemilik" type="text" class="form-control" id="nama_pemilik" placeholder="" value="<?php echo set_value('nama_pemilik')?>">
                </div>
              </div>
              <div class="form-group">
                  <label for="user" class="col-lg-2 control-label">Tgl Lahir</label>
                  <div class="col-lg-4">
                      <input value="<?php echo set_value('tgl_lahir_pemilik')?>" name="tgl_lahir_pemilik" type="text" class="form-control" id="tgl_lahir">
                  </div>
              </div>
              
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Telp</label>
                    <div class="col-lg-4">
                        <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="telpon" value="<?php echo set_value('telpon')?>" placeholder="0312281672">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Hp</label>
                    <div class="col-lg-4">
                        <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="hp" value="<?php echo set_value('hp')?>" placeholder="085732897719">
                    </div>
                </div>
                <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Gender</label>
                    <div class="col-lg-4">
                        <input type="radio" value="1" name="gender"> Pria
                        <input type="radio" value="2" name="gender"> Wanita
                    </div>
                </div>
              <h4>Rekening Pembayaran</h4>
              <hr>
              <div class="form-group">
                    <label for="user" class="col-lg-2 control-label">Bank</label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="bank_nama" value="<?php echo set_value('bank_nama')?>" placeholder="Mandiri">
                        <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="bank_norek" value="<?php echo set_value('bank_norek')?>" placeholder="1234567890123">
                        <input type="text" class="form-control" name="bank_pemilik" value="<?php echo set_value('bank_pemilik')?>" placeholder="Pemegang Rekening">
                        
                    </div>
                </div>
              
              <h4>Sales Pendamping</h4>
              <hr>
              <div class="form-group">
                  <label for="user" class="col-lg-2 control-label">Nama Sales</label>
                  <div class="col-lg-4">
                      <select class="form-control" name="merchant_sales">
                        <?php
                            if(!empty($data_sales)){
                                echo "<option value=''>-- Select Sales --</option>";
                                foreach($data_sales as $row){
                                    //$selected = ($curr_sales==$form-group->id_user)?'selected':'';
                                    echo "<option value='".$row->id_user."'>".$row->firstname." ".$row->lastname."</option>";
                                }
                            }
                        ?>    
                    </select>
                  </div>
              </div>
              
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    <script type="text/javascript">

    function load_kabupaten(idpropinsi,container_kabupaten,nama_input_kabupaten)
    {
        var propinsi             = $('#'+idpropinsi).val();        
        $('#'+container_kabupaten).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kabupaten')?>",
                data    : "id_propinsi="+propinsi+"&nama_input_kabupaten="+nama_input_kabupaten,
                success : function(result){
                    $('#'+container_kabupaten).html(result);
                }
            });
    }
    
    function load_kecamatan(idkabupaten,container_kecamatan,nama_input_kecamatan)
    {
        var kabupaten = $('#'+idkabupaten).val();        
        $('#'+container_kecamatan).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kecamatan')?>",
                data    : "id_kabupaten="+kabupaten+"&nama_input_kecamatan="+nama_input_kecamatan,
                success : function(result){
                    $('#'+container_kecamatan).html(result);
                }
            });
    }
</script>
<?php $this->load->view('admin/footer') ?>