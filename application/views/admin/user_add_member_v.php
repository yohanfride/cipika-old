
<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>user</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <?php if($add=='admin'){ ?>
              <li><a href="<?=admin_url();?>user/admin">Admin</a></li>
              <?php } else { ?>
              <li><a href="<?=admin_url();?>user/member">Member</a></li>
              <?php } ?>
              <li class="active">Add</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/add_member">
<!--                <div class="form-group">
                    <label for="" class="col-lg-2 control-label"><a href="#" id="change_pp">Ganti Logo</a></label>
                    <div class="col-lg-6" id="img_pp">
                        <img id="previewHolder" src="<?= base_url().'asset/img/no-avatar-single.png'; ?>">
                    </div>
                    <input type="file" id="filePhoto" class="hidden" name="user_image">
                </div>-->
                <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-4">
                  <input name="email" id="email" type="email" class="form-control" placeholder="" value="<?php echo set_value('email')?>">
                </div>
              </div>
                <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-4">
                  <input name="password" id="email" type="password" class="form-control" placeholder="">
                </div>
              </div>
                <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Password Confirm</label>
                <div class="col-lg-4">
                  <input name="passconf" id="email" type="password" class="form-control" placeholder="">
                </div>
              </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Nama Tampilan</label>
                    <div class="col-lg-4">
                        <input value="<?php echo set_value('username')?>" name="username" type="text" class="form-control" id="username" placeholder="Cipika Store">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Nama Depan</label>
                    <div class="col-lg-4">
                        <input value="<?php echo set_value('firstname')?>" name="firstname" type="text" class="form-control" id="firstname" placeholder="George">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Nama Belakang</label>
                    <div class="col-lg-4">
                        <input value="<?php echo set_value('lastname')?>" name="lastname" type="text" class="form-control" id="lastname" placeholder="Walker">
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Bio</label>
                    <div class="col-lg-4">
                        <textarea name="bio" class="form-control" id="bio" placeholder="" rows=5><?=$data->bio;?></textarea>
                    </div>
                </div> -->
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Tanggal Lahir</label>
                    <div class="col-lg-4">
                        <input value="<?php echo set_value('birthdate')?>" name="birthdate" type="text" class="form-control" id="tgl_lahir" placeholder="dd-mm-yyyy">
                    </div>
                </div>
<!--                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Gender</label>
                    <div class="col-lg-4">
                         <select name="gender" class="form-control" id="gender" placeholder="">
                            <option>Laki-laki</option>
                            <option>Perempuan</option>
                        </select> 
                        <input type="radio" value="man" name="gender" > Pria
                        <input type="radio" value="woman" name="gender" > Wanita
                    </div>
                </div>-->
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Alamat</label>
                    <div class="col-lg-4">
                        <textarea name="alamat" class="form-control" id="alamat" placeholder=""><?php echo set_value('alamat')?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Telepon</label>
                    <div class="col-lg-4">
                        <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('telpon')?>" name="telpon" type="text" class="form-control" id="telpon" placeholder="0312281672">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Hp</label>
                    <div class="col-lg-4">
                        <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('hp')?>" name="hp" type="text" class="form-control" id="hp" placeholder="085711234560">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-lg-2 control-label">Lokasi</label>
                <div class="col-md-4">
                        <select class="form-control" name="id_propinsi" id="propinsi" onchange="load_kabupaten('propinsi', 'div_kabupaten', 'id_kabupaten', 'id_kecamatan')">
                            <?php
                            $propinsi = $this->lib_lokasi->get_propinsi();
                            if (!empty($propinsi))
                            {
                                foreach ($propinsi as $row)
                                {
                                    $selected = ($lokasi->id_propinsi == $row->id_propinsi) ? 'selected' : '';
                                    echo "<option value='" . $row->id_propinsi . "' " . $selected . ">" . ucwords($row->nama_propinsi) . "</option>";
                                }
                            }
                            ?>
                        </select>  
                        <div id="div_kabupaten">                                         
                            <?php
                            if (!empty($lokasi))
                            {
                                $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);
                                if (!empty($kabupaten))
                                {
                                    echo "<select class='form-control' name='id_kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                                    foreach ($kabupaten as $row)
                                    {
                                        $selected = ($lokasi->id_kabupaten == $row->id_kabupaten) ? 'selected' : '';
                                        echo "<option value='" . $row->id_kabupaten . "' " . $selected . ">" . ucwords($row->nama_kabupaten) . "</option>";
                                    }
                                    echo "</select>";
                                }
                            }
                            ?>
                        </div>  
                        <div id="div_kecamatan">
                            <?php
                            if (!empty($lokasi))
                            {
                                $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
                                if (!empty($kecamatan))
                                {
                                    echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                                    foreach ($kecamatan as $row)
                                    {
                                        $selected = ($lokasi->id_kecamatan == $row->id_kecamatan) ? 'selected' : '';
                                        echo "<option value='" . $row->id_kecamatan . "' " . $selected . ">" . ucwords($row->nama_kecamatan) . "</option>";
                                    }
                                    echo "</select>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div> 
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->
<script type="text/javascript">

    function load_kabupaten(idpropinsi,container_kabupaten,nama_input_kabupaten)
    {
        var propinsi             = $('#'+idpropinsi).val();        
        $('#'+container_kabupaten).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kabupaten')?>",
                data    : "id_propinsi="+propinsi+"&nama_input_kabupaten="+nama_input_kabupaten,
                success : function(result){
                    $('#'+container_kabupaten).html(result);
                }
            });
    }
    
    function load_kecamatan(idkabupaten,container_kecamatan,nama_input_kecamatan)
    {
        var kabupaten = $('#'+idkabupaten).val();        
        $('#'+container_kecamatan).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kecamatan')?>",
                data    : "id_kabupaten="+kabupaten+"&nama_input_kecamatan="+nama_input_kecamatan,
                success : function(result){
                    $('#'+container_kecamatan).html(result);
                }
            });
    }
</script>
<?php include 'footer.php'; ?>
<?php $this->load->view('publik/upload') ?>