<?php include 'header.php'; ?>
<div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Daftar Produk</h1>
            <ol class="breadcrumb" style="height:30px;">
              <li><a href="<?=admin_url()?>">Dashboard</a></li>
              <li class="active">Produk</li>
              <div class="pull-right">
              <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
              <a href="<?=admin_url();?>produk/add" type="button" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Tambah</button></a>
              <a href="<?=admin_url();?>produk/setting" type="button" class="btn btn-sm btn-default"><i class="fa fa-cog"></i> Setting</button></a>
              <?php } ?>
              </div>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Data berhasil dihapus.
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="search">
              <form method="GET" action="" class="form-inline" role="form">
                <div class="form-group">
                  <input value="<?=$fr?>" style="width:100px;" name="fr" type="text" class="form-control tanggal" id="dari_tanggal" placeholder="Dari Tgl">
                </div>
                <div class="form-group">
                  <input value="<?=$to?>" style="width:100px;" name="to" type="text" class="form-control tanggal" id="sampai_tanggal" placeholder="Sampai Tgl">
                </div>
                <div class="form-group">
                  <select style="width:130px;" onchange="this.form.submit()" name="cat" class="form-control">
                    <option value="">[category]</option>
                      <?php $this->produk_m->get_select_category(0, 0 , $cat); ?> 
                  </select>
                </div>
                <div class="form-group">
                  <select style="width:130px;" onchange="this.form.submit()" name="mer" class="form-control">
                    <option value="">[merchant]</option>
                    <?php foreach ($merchant as $key => $value) { ?>
                    <option value="<?=$value->id_user?>" <?=($mer==$value->id_user)?'selected':'';?>><?=$value->nama_store?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <select style="width:100px;" onchange="this.form.submit()" name="sal" class="form-control">
                    <option value="">[sales]</option>
                    <?php foreach ($sales as $key => $value) { ?>
                    <option value="<?=$value->id_user?>" <?=($sal==$value->id_user)?'selected':'';?>><?=$value->username?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <select onchange="this.form.submit()" name="fe" class="form-control">
                    <option value="">[feature]</option>
                    <option value="1" <?=($fe==1)?'selected':'';?>>Unggulan</option>
                    <option value="2" <?=($fe==2)?'selected':'';?>>Curator Pick</option>
                  </select>
                </div>
                <div class="form-group">
                  <select onchange="this.form.submit()" name="stat" class="form-control">
                    <option value="" selected>[status]</option>
                    <option value="1" <?=($stat==1)?'selected':'';?>>Verified</option>
                    <!-- <option value="2">Unverified</option> -->
                    <option value="0" <?=($stat=='0')?'selected':'';?>>moderasi</option>
                  </select>
                </div>
                <div class="form-group">
                  <select style="width:100px;" onchange="this.form.submit()" name="pri" class="form-control">
                    <option value="">[price]</option>
                    <option value="1" <?=($pri==1)?'selected':'';?>>50.000 kurang</option>
                    <option value="2" <?=($pri==2)?'selected':'';?>>50.000 - 100.000</option>
                    <option value="3" <?=($pri==3)?'selected':'';?>>100.000 - 250.000</option>
                    <option value="4" <?=($pri==4)?'selected':'';?>>250.000 - 500.000</option>
                    <option value="5" <?=($pri==5)?'selected':'';?>>500.000 lebih</option>
                  </select>
                </div>
                <div class="form-group">
                  <select style="width:120px;" onchange="this.form.submit()" name="qty" class="form-control">
                    <option value="">[stock]</option>
                    <option value="1" <?=($qty==1)?'selected':'';?>>kurang 10</option>
                    <option value="2" <?=($qty==2)?'selected':'';?>>10 - 100</option>
                    <option value="3" <?=($qty==3)?'selected':'';?>>100 - 1000</option>
                  </select>
                </div>
                <br><br>
                <div class="form-group">
                  <input value="<?=$loc?>" name="loc" type="text" class="form-control city_auto" id="cari_kota" placeholder="Nama Kota/Kab">
                </div>
                <div class="form-group">
                  <input value="<?=$s?>" name="s" type="text" class="form-control" id="cari_produk" placeholder="Nama Produk">
                </div>
                <button type="submit" class="btn btn-primary">Cari</button>
              </form>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>nama</th>
                    <th>stok</th>
                    <th>harga merchant</th>
                    <th>harga publik</th>
                    <th>diskon</th>
                    <th>kategori</th>
                    <th>store</th>
                    <th>tgl upload</th>
                    <th>status</th>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=$offset;foreach ($datas as $data) { $i++;?>
                    <td><?=$i;?></td>
                    <td><a href="<?=admin_url();?>produk/edit/<?=$data->id_produk;?>"><?=$data->nama_produk;?></a></td>
                    <td><?=$data->stok_produk;?></td>
                    <td><?=$data->harga_produk;?></td>
                    <td><?=$data->harga_jual;?></td>
                    <td><?=$data->diskon;?></td>
                    <td><?=$this->produk_m->get_kategori($data->id_produk);?></td>
                    <td><a href="<?=admin_url();?>user/edit_merchant/<?=$data->id_store;?>"><?=$data->nama_store;?><a></td>
                    <td><?=$data->tgl_upload;?></td>
                    <td><?php echo status_product_label($data->publish)?></td>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <td>
                      <a href="<?=admin_url();?>produk/edit/<?=$data->id_produk;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>produk/delete/<?=$data->id_produk;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td> 
                    <?php } ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?=$paginator?>
          </div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>