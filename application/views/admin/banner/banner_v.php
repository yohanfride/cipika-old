<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Banner</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Banner</li>
                </ol> 
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Data berhasil dihapus.
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <a href="<?php echo base_url('admin/banner/add');?>" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                    <a href="<?php echo base_url('admin/banner/setting');?>" type="button" class="btn btn-sm btn-primary"><i class="fa fa-cogs"></i> Setting</a>
                <?php } ?>
                <br><br>
                <div class="table-responsive">
                  <table class="table datatable table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Banner URL</th>                        
                        <th>Image</th>                        
                        <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                            <th>action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                    <tbody>                        
                        <?php $i=0;foreach ($datas as $data) { $i++;?>
                            <tr>
                                <td><?=$i;?></td>
                                <td><?=$data->banner_title;?></td>                                
                                <td><?=$data->banner_url;?></td>
                                <td><img src='<?=$data->banner_image;?>' width='90' height='80'></td>
                                <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                                    <td>
                                        <a href="<?=admin_url();?>banner/edit/<?=$data->idbanner;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                        <a href="<?=admin_url();?>banner/delete/<?=$data->idbanner;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                                    </td>
                                <?php } ?>                    
                            </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>
