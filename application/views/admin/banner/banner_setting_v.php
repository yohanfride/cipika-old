<?php $this->load->view('admin/header'); ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Banner Setting</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>banner">Banner</a></li>
              <li class="active">Add</li>
            </ol>
            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success;?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>banner/setting">
              <div class="form-group">
                <label for="banner_interval" class="col-lg-2 control-label">Interval</label>
                <div class="col-lg-4">
                  <div class="input-group">
                  <input value="<?=(isset($setting->option_value))?$setting->option_value:'';?>" name="banner_interval" type="text" class="form-control" id="banner_interval" placeholder="3000">
                  <span class="input-group-addon">milisecond</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer'); ?>