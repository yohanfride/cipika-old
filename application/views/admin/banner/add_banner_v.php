<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1><?=$title;?></h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>">Dashboard</a></li>
                    <li><a href="<?=admin_url();?>banner">Banner</a></li>
                    <li class="active"><?=ucwords($title);?></li>
                </ol>           
                
                <?php if(isset($error)): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $error?>
                    </div>
                <?php endif; ?>
                
            </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form method="post" action="<?php echo base_url('admin/banner/save');?>" class="form-horizontal" role="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="banner_title" class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-4">
                        <input name="banner_title" type="text" class="form-control" id="banner_title" placeholder="">
                    </div>                   
                </div>
                <div class="form-group">
                    <label for="banner_url" class="col-lg-2 control-label">Target URL</label>
                    <div class="col-lg-4">
                        <input name="banner_url" type="url" class="form-control" id="banner_url" placeholder="">
                    </div>                   
                </div>
                <div class="form-group">
                    <label for="banner_image" class="col-lg-2 control-label">Image</label>
                    <div class="col-lg-4">
                        <!--<input name="banner_image" type="url" class="form-control" id="banner_image" placeholder="Image URL">-->
                        <input type="file" id="banner_image" name="banner_image">
                    </div>                   
                </div>                
                <div class="form-group">
                    <label for="banner_image" class="col-lg-2 control-label"></label>
                    <div class="col-lg-4">
                        <button class="btn btn-primary" type="submit" name="simpan" value="1">Simpan</button>
                    </div>
                </div>
            </form>
          </div>         
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    <script type="text/javascript">
      var cktext='editor1';
    </script>    
    
    <?php $this->load->view('admin/footer') ?>  
