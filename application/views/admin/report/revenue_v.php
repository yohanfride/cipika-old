<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Revenue Report</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Revenue Report</li>
                </ol> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">  

                <br><br>                
                <div id="filter">
                    <form name="f1" method="post" action="<?php echo base_url('admin/report/revenue')?>">
                        <b>Filter</b>
                        
                        <table>
                             <tr>
                                <td width='30%'>Period</td>
                                <td><input id="period_start" type="text" name="period_start" size="10" class='dtpicker'> to <input id="period_end" type="text" name="period_end" size="10" class='dtpicker'></td>
                            </tr>                                                  
                        </table><br/>
                        <input type="submit" name="submit" value="Submit">
                    </form>
                </div>
            
                <br><br>                
                <h1 align='center'>Revenue Report <?php echo $period_start.' - '.$period_end?></h1>                
                
                <div class="table-responsive uitabs">
                    <ul>
                        <li><a href="#shipping_status">Shipping Status</a></li>
                        <li><a href="#payment_status">Payment Status</a></li>
                        <li><a href="#product">Product</a></li>
                        <li><a href="#city">City</a></li>
                        <li><a href="#source">Source</a></li>
                    </ul>
                
                    <div id='shipping_status'>
                        <h3>Base on Shipping Status</h3>                    
                        <table class="table table-hover table-striped">                       
                             <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Merchant Price</th>
                                    <th>Cipika Price</th>
                                    <th>Revenue</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($status['status_delivery'])){
                                        foreach($status['status_delivery'] as $row){
                                            $revenue = $this->report_m->get_revenue($period_start,$period_end,$row->status_delivery);
                                        
                                            echo    "<tr>".
                                                        "<td>".status_delivery_label($row->status_delivery)."</td>".
                                                        "<td>Rp. ".format_uang($revenue->revenue_merchant)."</td>".
                                                        "<td>Rp. ".format_uang($revenue->revenue)."</td>".
                                                        "<td>Rp. ".format_uang($revenue->revenue-$revenue->revenue_merchant)."</td>".
                                                    "</tr>";
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <div id='payment_status'>
                        <h3>Base on Payment Status</h3>                    
                        <table class="table table-hover table-striped">                       
                             <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Merchant Price</th>
                                    <th>Cipika Price</th>
                                    <th>Revenue</th>                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($status['status_payment'])){
                                        foreach($status['status_payment'] as $row){
                                            $revenue = $this->report_m->get_revenue($period_start,$period_end,'',$row->status_payment);
                                        
                                            echo    "<tr>".
                                                        "<td>".status_payment_label($row->status_payment)."</td>".
                                                        "<td>Rp. ".format_uang($revenue->revenue_merchant)."</td>".
                                                        "<td>Rp. ".format_uang($revenue->revenue)."</td>".
                                                        "<td>Rp. ".format_uang($revenue->revenue-$revenue->revenue_merchant)."</td>".
                                                    "</tr>";
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                  
                    <div id='product'>
                        <h3>Base on Product</h3>                    
                        <table class="table table-hover table-striped">                       
                             <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Merchant Price</th>
                                    <th>Cipika Price</th>
                                    <th>Sold</th>
                                    <th>Revenue</th>                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($produk_item)){
                                        foreach($produk_item as $row){                                        
                                            $category       = $this->report_m->get_product_category($row->id_produk);
                                            $kategori       = $this->report_m->display_category($category);
                                            $revenue        = ($row->harga_jual-$row->harga_produk);
                                            
                                            echo    "<tr>".
                                                        "<td>".ucwords($row->nama_produk)."</td>".
                                                        "<td>".$kategori."</td>".
                                                        "<td>Rp. ".format_uang($row->harga_produk)."</td>".
                                                        "<td>Rp. ".format_uang($row->harga_jual)."</td>".
                                                        "<td>".format_uang($row->terjual)."</td>".
                                                        "<td>Rp. ".format_uang($revenue*$row->terjual)."</td>".
                                                    "</tr>";
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <div id='city'>
                        <h3>Base on City</h3>                    
                        <table class="table table-hover table-striped">                       
                             <thead>
                                <tr>
                                    <th>City</th>       
                                    <th>Merchant Price</th>
                                    <th>Cipika Price</th>
                                    <th>Revenue</th>                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    
                                    $total_harga_cipika         = 0;
                                    $total_harga_merchant       = 0;
                                    $total_ongkir               = 0;
                                    $total_ongkir_merchant      = 0;
                                
                                    if(!empty($kota_item)){
                                       
                                        foreach($kota_item as $row){
                                            $total_harga_cipika         = $total_harga_cipika + $row->total;
                                            $total_harga_merchant       = $total_harga_merchant + $row->total_merchant;
                                            $total_ongkir               = $total_ongkir + $row->ongkir_sementara;
                                            $total_ongkir_merchant      = $total_ongkir_merchant + $row->ongkir_merchant;
                                            
                                            $lokasi                     = $this->lib_lokasi->get_lokasi($row->id_kecamatan);
                                            $lokasi                     = (!empty($lokasi))?$lokasi->nama_kecamatan.','.$lokasi->nama_kabupaten.','.$lokasi->nama_propinsi:'-';
                                        
                                            $revenue = $this->report_m->get_revenue_city($row->id_kecamatan);
                    
                                            echo    "<tr>".
                                                        "<td>".$lokasi."</td>".
                                                        "<td>Rp. ".format_uang($row->total_merchant)."</td>".
                                                        "<td>Rp. ".format_uang($row->total)."</td>".
                                                        "<td>Rp. ".format_uang($row->total-$row->total_merchant)."</td>".
                                                    "</tr>";
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <div id='source'>
                        <h3>Base on Source</h3>  
                        <table class="table table-hover table-striped">                       
                             <thead>
                                <tr>
                                    <th>Source</th>       
                                    <th>Merchant</th>
                                    <th>Cipika</th>
                                    <th>Revenue</th>                                
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Price Gap</td>
                                    <td>Rp. <?php echo format_uang($total_harga_merchant)?></td>
                                    <td>Rp. <?php echo format_uang($total_harga_cipika)?></td>
                                    <td>Rp. <?php echo format_uang($total_harga_cipika-$total_harga_merchant)?></td>
                                </tr>
                                <tr>
                                    <td>Different Shipping Cost</td>
                                    <td>Rp. <?php echo format_uang($total_ongkir_merchant)?></td>
                                    <td>Rp. <?php echo format_uang($total_ongkir)?></td>
                                    <td>Rp. <?php echo format_uang($total_ongkir-$total_ongkir_merchant)?></td>
                                </tr>
                                <tr>
                                    <td colspan='3'><b>Total</b></td>                                
                                    <td><b>Rp. <?php echo format_uang( ($total_harga_cipika-$total_harga_merchant) + ($total_ongkir-$total_ongkir_merchant) )?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                  
                </div>
                <br/><br/><br/><br/>
                
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>
