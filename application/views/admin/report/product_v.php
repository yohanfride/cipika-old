<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Product Report</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Product Report</li>
                </ol> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">                
                <br><br>                                
                <div class="table-responsive">
                
                    <br/><br/>
                    <h3>Top 5 Most Sale Product</h3>                    
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Merchant</th>
                                <th>Category</th>
                                <th>Sales</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($most_sale)){
                                    $no = 0;
                                    foreach($most_sale as $row){                                                                                
                                        $no++;
                                        //$category   = $this->report_m->get_single('tbl_kategori','id_kategori',$row->id_kategori);
                                        //$kategori       = (isset($category->nama_kategori))?$category->nama_kategori:'-';
                                        
                                        $category       = $this->report_m->get_product_category($row->id_produk);
                                        $kategori       = $this->report_m->display_category($category);
                                        $users = $this->report_m->get_detail_merchant($row->id_user);
                                        echo    "<tr>".
                                                    "<td>".$no."</td>".
                                                    "<td>".$row->nama_produk."</td>".
                                                    "<td>".$users->username."</td>".
                                                    "<td>".$kategori."</td>".
                                                    "<td>".$row->terjual."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                    </table>
                
                    <h3>Top 5 Loved Product</h3>                    
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Merchant</th>
                                <th>Category</th>
                                <th>Loved</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($most_loved)){
                                    $no = 0;
                                    foreach($most_loved as $row){                                                                                
                                        $no++;
                                         $category       = $this->report_m->get_product_category($row->id_produk);
                                        $kategori       = $this->report_m->display_category($category);
                                        $users = $this->report_m->get_detail_merchant($row->id_user);
                                        
                                        echo    "<tr>".
                                                    "<td>".$no."</td>".
                                                    "<td>".$row->nama_produk."</td>".
                                                "<td>".$users->username."</td>".
                                                    "<td>".$kategori."</td>".                                                    
                                                    "<td>".$row->loved."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                    </table>
                    
                    <br/><br/>
                    <h3>Top 5 Viewed Product</h3>                    
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Merchant</th>
                                <th>Category</th>
                                <th>Viewed</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($most_viewed)){
                                    $no = 0;
                                    foreach($most_viewed as $row){                                                                                
                                        $no++;
                                        $category       = $this->report_m->get_product_category($row->id_produk);
                                        $kategori       = $this->report_m->display_category($category);
                                        $users = $this->report_m->get_detail_merchant($row->id_user);
                                        
                                        echo    "<tr>".
                                                    "<td>".$no."</td>".
                                                    "<td>".$row->nama_produk."</td>".
                                                "<td>".$users->username."</td>".
                                                    "<td>".$kategori."</td>".                                                    
                                                    "<td>".$row->viewed."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                    </table>
                    
                </div>
                
                
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>
