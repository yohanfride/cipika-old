<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Merchant Status</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Merchant Status</li>
                </ol> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">                
                <br><br>                
                <div id="filter">
                    <form name="f1" method="post" action="<?php echo base_url('admin/report/merchant_status')?>">
                        <b>Filter</b>                      
                        <table>                           
                            <tr>
                                <td width='30%'>Status</td>
                                <td>
                                    <select name="status">
                                        <option value=''>-- All --</option>
                                        <option value="pending" <?php echo ($status=='pending')?'selected':''?>>Pending</option>
                                        <option value="approve" <?php echo ($status=='approve')?'selected':''?>>Verified</option>
                                        <option value="block" <?php echo ($status=='block')?'selected':''?>>Unverified</option>                                        
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width='30%'>Sales</td>
                                <td>
                                    <select name="sales">
                                        <option value=''>-- All --</option>
                                        <?php
                                            if(!empty($sales)){
                                                foreach($sales as $row){
                                                    $selected = ($sales_selected==$row->id_user)?'selected':'';
                                                    echo "<option value='".$row->id_user."' ".$selected.">".$row->firstname." ".$row->lastname."</option>";
                                                }
                                            }
                                        ?>
                                        
                                    </select>
                                </td>
                            </tr>                                  
                        </table><br/>
                        <input type="submit" name="submit" value="Submit">
                    </form>
                </div>
                
                <br><br>
                <div class="table-responsive">
                    
                    <table class="table datatable table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th width='10%'>Merchant</th>
                                <th>Sales</th>
                                <th>Lokasi</th>
                                <th>Email</th>
                                <th>Tgl Verified</th>
                                <th>Tgl Unverified</th>
                                <th>Status</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($result)){
                                    foreach($result as $row){                                                                                
                                        $lokasi         = $this->lib_lokasi->get_lokasi($row->id_kota);                                        
                                        $lokasi         = (!empty($lokasi))?ucwords($lokasi->nama_propinsi).",".ucwords($lokasi->nama_kabupaten).",".ucwords($lokasi->nama_kecamatan):'-';
                                        
                                        $tgl_verified       = ($row->date_verified!='')?strftime("%d-%m-%Y",strtotime($row->date_verified)):'-';
                                        $tgl_unverified     = ($row->date_unverified!='')?strftime("%d-%m-%Y",strtotime($row->date_unverified)):'-';
                                        
                                        $sales              = $this->report_m->get_single('tbl_user','id_user',$row->id_sales);
                                        $sales              = (!empty($sales))?$sales->firstname.' '.$sales->lastname:'-';
                                        
                                        echo    "<tr>".
                                                    "<td>".$row->nama_store."</td>".                                                    
                                                    "<td>".$sales."</td>".
                                                    "<td>".$lokasi."</td>".
                                                    "<td>".$row->email."</td>".
                                                    "<td>".$tgl_verified."</td>".
                                                    "<td>".$tgl_unverified."</td>".
                                                    "<td>".status_merchant_label($row->store_status)."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                  </table>
                </div>
                
                
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>
