<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Category Report</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Category Report</li>
                </ol> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">                
                <br><br>                                
                <div class="table-responsive">
                    <h3>Top 5 Loved Category</h3>
                    <?php //echo $this->session->userdata('sql');?>
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>                                
                                <th>Loved</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($most_loved)){
                                    $no = 0;
                                    foreach($most_loved as $row){                                                                                
                                        $no++;
                                        $category   = $this->report_m->get_single('tbl_kategori','id_kategori',$row->id_kategori);
                                        
                                        echo    "<tr>".
                                                    "<td>".$no."</td>".
                                                    "<td>".$row->nama_produk."</td>".                                                    
                                                    "<td>".$row->loved."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                    </table>
                    
                    <br/><br/>
                    <h3>Top 5 Viewed Category</h3>
                    <?php //echo $this->session->userdata('sql');?>
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>                                
                                <th>Viewed</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($most_viewed)){
                                    $no = 0;
                                    foreach($most_viewed as $row){                                                                                
                                        $no++;
                                        $category   = $this->report_m->get_single('tbl_kategori','id_kategori',$row->id_kategori);
                                        
                                        echo    "<tr>".
                                                    "<td>".$no."</td>".
                                                    "<td>".$row->nama_produk."</td>".
                                                    "<td>".$category->nama_kategori."</td>".                                                    
                                                    "<td>".$row->viewed."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                    </table>
                    
                </div>
                
                
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>
