<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Price Comparison</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Price Comparison</li>
                </ol> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">                
                <br><br>
                <?php //echo $this->session->userdata('sql');?>
                <div id="filter">
                    <form name="f1" method="post" action="<?php echo base_url('admin/report/price_comparison')?>">
                        <b>Filter</b>                      
                        <table>                           
                            <tr>
                                <td width='30%'>Category</td>
                                <td>
                                    <select name="id_kategori">
                                        <option value=''>-- All --</option>
                                        <?php
                                            if(!empty($kategori)){
                                                foreach($kategori as $row){
                                                    //echo "<option value='".$row->id_kategori."'>".ucwords($row->nama_kategori)."</option>";
                                                    echo " <optgroup label='".ucwords($row->nama_kategori)."'>";
                                                    
                                                    $child = $this->report_m->list_category($row->id_kategori);
                                                    if(!empty($child)){
                                                        foreach($child as $row2){
                                                            $selected = ($id_kategori==$row2->id_kategori)?'selected':'';
                                                        
                                                            echo "<option $selected value='".$row2->id_kategori."'>-- ".ucwords($row2->nama_kategori)."</option>";
                                                            
                                                            $child2 = $this->report_m->list_category($row2->id_kategori);
                                                            if(!empty($child2)){
                                                                foreach($child2 as $row3){
                                                                    $selected = ($id_kategori==$row3->id_kategori)?'selected':'';
                                                                    
                                                                    echo "<option $selected value='".$row3->id_kategori."'>---- ".ucwords($row3->nama_kategori)."</option>";
                                                                }
                                                            }                                                            
                                                        }
                                                    }
                                                    
                                                    echo "</optgroup>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>
                                    <select name="id_kecamatan">
                                        <option value=''>-- All --</option>
                                        <?php
                                            if(!empty($kecamatan)){
                                                foreach($kecamatan as $row){      
                                                    $selected = ($id_kecamatan==$row->id_kecamatan)?'selected':'';
                                                    echo "<option $selected value='".$row->id_kecamatan."'>".ucwords($row->nama_kecamatan)."</option>";                                                    
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table><br/>
                        <input type="submit" name="submit" value="Submit">
                    </form>
                </div>
                
                <br><br>
                <div class="table-responsive">
                    
                    <table class="table datatable table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Product</th>
                                <th>Lokasi</th>                        
                                <th>Merchant Price</th>
                                <th>Cipika Price</th>
                                <th>Price Gap</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($result)){
                                    foreach($result as $row){                                        
                                        //$category       = $this->report_m->get_product_category($row->id_kategori);
                                        //$kategori       = $this->report_m->display_category($category);
                                        $kategori       = $this->report_m->display_category_tree($row->id_kategori);
                                        $lokasi         = $this->lib_lokasi->get_lokasi($row->id_kota);
                                        $user           = $this->report_m->get_single('tbl_user','id_user',$row->id_user);
                                        $persen         = $this->meta_m->get_meta_value('tbl_user',$row->id_user,'merchant_commercial_agreement')/100;
                                        $harga_cipika   = $row->harga_produk + ($row->harga_produk*$persen);
                                        $selisih        = $harga_cipika - $row->harga_produk;
                                        
                                        if(!empty($lokasi)){
                                            $lokasi         = ucwords($lokasi->nama_propinsi).",".ucwords($lokasi->nama_kabupaten).",".ucwords($lokasi->nama_kecamatan);
                                        }else{
                                            $lokasi = '-';
                                        }
                                        
                                        echo    "<tr>".
                                                    "<td>".$kategori."</td>".
                                                    "<td>".$row->nama_produk."</td>".
                                                    "<td>".$lokasi."</td>".                                                    
                                                    "<td>Rp. ".format_uang($row->harga_produk)."</td>".
                                                    "<td>Rp. ".format_uang($harga_cipika)."</td>".
                                                    "<td>Rp. ".format_uang($selisih)."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                  </table>
                </div>
                
                
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>
