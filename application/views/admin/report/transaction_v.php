<?php $this->load->view('admin/header') ?>

      <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1>Transaction Report</h1>
                <ol class="breadcrumb">
                    <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                    <li class="active">Transaction Report</li>
                </ol> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">                
                <br><br>                
                <div id="filter">
                    <form name="f1" method="post" action="<?php echo base_url('admin/report/transaction')?>">
                        <b>Filter</b>
                        
                        <table>
                             <tr>
                                <td width='30%'>Period</td>
                                <td><input id="period_start" type="text" name="period_start" size="10"> to <input id="period_end" type="text" name="period_end" size="10"> (date:dd-mm-yyyy)</td>
                            </tr>
                            <tr>
                                <td width='30%'>Payment</td>
                                <td>
                                    <select name="status_payment">
                                        <option value=''>-- All --</option>
                                        <?php
                                            if(!empty($statuses['status_payment'])){
                                                foreach($statuses['status_payment'] as $row){
                                                    if($row->status_payment!=''){
                                                        echo "<option value='".$row->status_payment."'>".ucwords($row->status_payment)."</option>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Delivery</td>
                                <td>
                                    <select name="status_delivery">
                                        <option value=''>-- All --</option>
                                        <?php
                                            if(!empty($statuses['status_delivery'])){
                                                foreach($statuses['status_delivery'] as $row){
                                                    if($row->status_delivery!=''){
                                                        echo "<option value='".$row->status_delivery."'>".ucwords($row->status_delivery)."</option>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table><br/>
                        <input type="submit" name="submit" value="Submit">
                    </form>
                </div>
                
                <br><br>
                <div class="table-responsive">
                    
                    <table class="table datatable table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th size='20%'>Date</th>
                                <th size='20%'>Invoice</th>
                                <th size='20%'>Kode Transaksi</th>
                                <th size='20%'>Merchant</th>                        
                                <th size='20%'>Buyer</th>                        
                                <th size='10%'>Payment Status</th>
                                <th size='10%'>Delivery Status</th>
                                <th size='20%'>Total Ongkir</th>
                                <th size='20%'>Total Produk</th>
                                <th size='20%'>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($result)){
                                    foreach($result as $row){
                                        $user           = $this->report_m->get_single('tbl_user','id_user',$row->id_user);
                                        $merchant       = $this->commonlib->get_seller($row->id_merchant);
                                        $seller         = (!empty($merchant))?$merchant->nama_store:'-';
                                        $invoice        = ($row->kode_invoice!='')?"<a href='".base_url('admin/order/detail_invoice/'.$row->kode_invoice)."'>".$row->kode_invoice."</a>":'-';
                                        
                                        echo    "<tr style='font-size:11px !important'>".
                                                    "<td>".$row->date_added."</td>".
                                                    "<td>".$invoice."</td>".
                                                    "<td><a href='".base_url('admin/order/detail/'.$row->id_order)."'>".$row->kode_order."</a></td>".
                                                    "<td>".$seller."</td>".
                                                    "<td>".$user->firstname." ".$user->lastname."</td>".
                                                    "<td><a href='".base_url('admin/order/detail_payment/'.$row->id_order)."'>".status_payment_label($row->status_payment)."</a></td>".
                                                    "<td><a href='".base_url('admin/order/detail_delivery/'.$row->id_order)."'>".status_delivery_label($row->status_delivery)."</a></td>".
                                                    "<td>Rp. ".format_uang($row->ongkir_sementara)."</td>".
                                                    "<td>Rp. ".format_uang($row->total)."</td>".
                                                    "<td>Rp. ".format_uang($row->total+$row->ongkir_sementara)."</td>".
                                                "</tr>";
                                    }
                                }else{
                                    echo "No Result.";
                                }
                            ?>
                        </tbody>
                  </table>
                </div>
                
                
            </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   

<?php $this->load->view('admin/footer') ?>