<?php $this->load->view('admin/header') ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1>Payment Report</h1>
            <ol class="breadcrumb">
                <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
                <li class="active">Payment Report</li>
            </ol> 
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-12">                
            <div id="filter">
                <form name="f1" method="post" action="">
                    <b>Filter</b>
                    <table>
                        <tr>
                            <td width='20%'>Period</td>
                            <td><input id="period_starts" type="text" name="period_start" size="10" style="padding: 2px 33px 6px 6px;" class='dtpicker'> to <input id="period_ends" type="text" name="period_end" size="10" style="padding: 2px 33px 6px 6px;" class='dtpicker'></td>
                        </tr>
                        <tr>
                            <td width='20%'>Payment</td>
                            <td>
                                <select name="status_payment" style="padding: 2px 33px 3px 6px;margin-top: 5px">
                                    <option value=''>-- All --</option>
                                    <option value='paid'>Paid</option>
                                    <option value='unpaid'>Unpaid</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width='20%'>Send Email</td>
                            <td>
                                <input type="checkbox" id="send" name="send" style="padding: 2px 33px 6px 6px;margin-top: 5px">
                                <label style="font-style: normal">To Email :</label>
                                <input type="text" id="email" name="email" placeholder="Send To Email" style="padding: 3px 33px 3px 6px;margin-top: 5px"> 
                            </td>
                        </tr>
                    </table>
                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped tablesorter">
                    <tr>
                        <td><div align='center'>Code Order</div></td>
                        <td><div align='center'>Invoice</div></td>
                        <td><div align='center'>Date</div></td>
                        <td><div align='center'>Buyer</div></td>
                        <td><div align='center'>Metode</div></td>
                        <td><div align='center'>Amount</div></td>
                        <td><div align='center'>Payment Status</div></td>
                    </tr>
                    <?php
                    if (!empty($result))
                    {
                        // print_r($result);exit;
                        foreach ($result as $row)
                        {
                            ?>
                            <tr>
                                <td><div align='center'><?=$row->kode_order;?></div></td>
                                <td><div align='center'><?=$row->kode_invoice;?></div></td>
                                <td><div align='center'><?=$row->created;?></div></td>
                                <td><div align='center'><?=$row->email;?></div></td>
                                <td><div align='center'><?=$row->payment;?></div></td>
                                <td><div align='center'><?=$row->total;?></div></td>
                                <td><div align='center'><?=$row->paid==0?"Unpaid":"Paid";?></div></td>
                            </tr>
                            <?php
                        }
                    }
                    else
                    {
                        ?>
                        <tr>
                            <td colspan="6"><div align='center'>No Result</div></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div><!-- /.row -->
</div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer') ?>
