<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>article</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">article</li>
            </ol>
            <?php if($this->input->get('delete')=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
            <a href="<?=admin_url();?>article/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <?php } ?>
            <br><br>
            <div class="table-responsive">
              <table class="table datatable table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>title</th>
                    <th>content</th>
                    <th>tanggal dibuat</th>
                    <th>tanggal publish</th>
                    <th>Status</th>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$data->title;?></td>
                    <td><?=strip_tags(substr($data->content, 0, 50));?>...</td>
                    <td><?=$data->date_added;?></td>
                    <td><?=$data->date_modified;?></td>
                    <?php
                        if(1 == $data->publish)
                            $status = "Publish";
                        else if(2 == $data->publish)
                            $status = "Unpublish";
                        else
                            $status = "Moderasi";
                    ?>
                    <td><?=$status;?></td>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <td>
                      <a href="<?=admin_url();?>article/edit/<?=$data->id_article;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>article/delete/<?=$data->id_article;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td>
                    <?php } ?>                    
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>