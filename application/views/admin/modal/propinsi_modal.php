<!-- Modal -->
<div class="modal fade" id="propinsi_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add <?=ucwords($title);?></h4>
      </div>  
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="<?=admin_url();?>propinsi" role="form" id="form_add">
          <div class="form-group">
            <label for="propinsi" class="col-sm-3 control-label">Nama Propinsi</label>
            <div class="col-sm-6">
              <input name="nama_propinsi" type="text" class="form-control" id="nama_propinsi" placeholder="Jawa Barat">
            </div>
          </div>
          <input value=1 type="text" name="form_add" class="hidden">
        </form>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="add();">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="propinsi_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit <?=ucwords($title);?></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="<?=admin_url();?>propinsi?id=<?=$data->id_propinsi;?>" role="form" id="form_edit">
          <div class="form-group">
            <label for="propinsi" class="col-sm-3 control-label">Nama Propinsi</label>
            <div class="col-sm-6">
              <input name="nama_propinsi" value="<$data->nama_propinsi;>" type="text" class="form-control" id="nama_propinsi">
            </div>
          </div>
          <input value=1 type="text" name="form_edit" class="hidden">
        </form>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function add(){
  validate("#form_add");
}
function validate(form){
  var nameReg = /^[A-Za-z]+$/;
  var nama_propinsi = $('#nama_propinsi').val();
  var inputVal = new Array(nama_propinsi);
  var inputMessage = new Array("nama propinsi");
  $('.error').hide();
  if(inputVal[0] == ""){
    $('#nama_propinsi').after('<span class="error"> Please enter ' + inputMessage[0] + '</span>');
  } 
  else if(!nameReg.test(nama_propinsi)){
    $('#nama_propinsi').after('<span class="error"> Letters only</span>');
  } 
  else{
    $(form).submit();
  }
}
</script>