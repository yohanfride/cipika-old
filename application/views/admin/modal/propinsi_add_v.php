<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Propinsi</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>">Dashboard</a></li>
              <li><a href="<?=admin_url();?>propinsi">Propinsi</a></li>
              <li class="active">add</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>propinsi" role="form" id="form_add">
              <div class="form-group">
                <label for="propinsi" class="col-sm-3 control-label">Nama Propinsi</label>
                <div class="col-sm-6">
                  <input name="nama_propinsi" type="text" class="form-control" id="nama_propinsi" placeholder="Jawa Barat">
                </div>
              </div>
              <button type="submit" name="simpan" value=1>Simpan</button>
            </form>            
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>