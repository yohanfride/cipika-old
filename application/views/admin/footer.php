    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="<?=base_url();?>asset/admin/js/jquery-1.10.2.js"></script>
    <script src="<?=base_url();?>asset/admin/js/bootstrap.js"></script>
    <script src="<?=base_url();?>asset/admin/js/jquery-ui.min.js"></script>
    <script src="<?=base_url();?>asset/admin/js/jquery.dataTables.js"></script>

    <!-- Page Specific Plugins -->
    <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
    <!--
    <script src="<?=base_url();?>asset/admin/js/morris.js"></script>
    <script src="<?=base_url();?>asset/admin/js/morris/chart-data-morris.js"></script>
    <script src="<?=base_url();?>asset/admin/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?=base_url();?>asset/admin/js/tablesorter/tables.js"></script>

    <script src="<?=base_url();?>asset/admin/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url();?>asset/admin/ckeditor/adapters/jquery.js"></script>
    -->
    <script type="text/javascript">
var base_url = '<?=base_url();?>';
var admin_url = '<?=admin_url();?>';
</script>
<script src="<?=base_url();?>asset/admin/js/notifikasi.js"></script>
<!--<script type="text/javascript" src="<?=base_url();?>asset/js/gus_cart.js"></script>-->
<script type="text/javascript">
//cek number field
  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if ((charCode > 47 && charCode < 58 ) || charCode == 44 || charCode == 46 || charCode == 8)
        return true;

     return false;
     // alert(charCode);
  }
</script>
<script type="text/javascript">
// cek all
$('.check_all').click(function () {    
  $('input:checkbox').prop('checked', this.checked);    
});
</script>
<script type="text/javascript">
    /**
  AJAX CATEGORY
    **/
    $(document).on('change', '.parent_category, .sub_category', function(){
      var id=$(this).val();
      var i=$(this).data().sub;
      var j=i+1;
      // alert(i);
      $.ajax({
          url: base_url + 'ajax/category',
          type: 'POST',
          data: {id: id, i: i, j: j},
          dataType: 'html',
          success: function (data) {
            // alert(data); 
            $('.sub-category-'+j).empty();
            $('.sub-category-'+j).append(data);
            $('.sub-category-'+j).append('<div class="sub-category-' + (j+1) +'"></div>');
          }
        });
    });

    /**
    END AJAX CATEGORY
    **/
</script>

  <script>
  function fillKota(id) {
    $.getJSON(base_url + "cart/kota?id=" + id,
      "", function (json) {
          $("#select-kota").html("");
          $.each(json, function (index, value) {
              $("#select-kota").append("<option value='" + value.id_kota + "'>" + value.nama_kota + "</option>");
          })
      })
  }

  (function( $ ) { 

    fillKota(1);

    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "" )
          .insertAfter( this.element );

        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },

      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";

        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "form-control" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });

        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },

          autocompletechange: "_removeIfInvalid"
        });
      },
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },

      _removeIfInvalid: function( event, ui ) {
        if ( ui.item ) {
          return;
        }
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
        if ( valid ) {
          return;
        }
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.data( "ui-autocomplete" ).term = "";
      },

      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );

  $(function() {
    $( "#id_kota" ).combobox();
  });
  </script>
  
<script type="text/javascript">
$('.btn-danger').click(function(){
  return confirm('Are you sure delete this item?');
});
</script>
<script>
   function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               $('#previewHolder').attr('src', e.target.result);
           }

           reader.readAsDataURL(input.files[0]);
       }
   }
   $('#change_pp').click(function(){
      $("#filePhoto").click();
   });
   $("#filePhoto").change(function() {
      readURL(this);
   });
</script>
    <script type="text/javascript">
        $(function() {
            $('.dtpicker').datepicker({changeYear:true, changeMonth:true, showMonthAfterYear:true,dateFormat: 'dd-mm-yy'});
            $(".uitabs").tabs();
            
            oTable = $('.datatable').dataTable({
                "bJQueryUI": false,
                "sPaginationType": "full_numbers"
                });

                $("#change").click(function(){
                    $("#foto").click();
                  });

                $(".pilih").click(function(){
                    $(this).prev().attr('checked',true);
                    $('.pilih').removeClass('selected');
                    $(this).addClass('selected');
                });
        });
    </script>

    <!--
    untuk kalkulasi harga jual otomatis
    -->
    <script type="text/javascript">
    $('#harga_produk').blur(function(){
      var val=parseInt($(this).val());
      $('#harga_jual').val(val+(10/100*val));
    });
    </script>
    <script type="text/javascript">
      $( "#delivery_date" ).datepicker({
        dateFormat: 'dd-mm-yy'
      });
    </script>
    <!--
    untuk ajax kota di ongkir
    -->
    <script type="text/javascript">
    var ajaxku;
    function ajaxkota(id){
        ajaxku = buatajax();
        var url= admin_url + "ongkir/ajax_kota";
        url=url+"?q="+id;
        url=url+"&sid="+Math.random();
        ajaxku.onreadystatechange=stateChanged;
        ajaxku.open("GET",url,true);
        ajaxku.send(null);
    }

    function ajaxkec(id){
        ajaxku = buatajax();
        var url= admin_url + "ongkir/ajax_kota";
        url=url+"?kec="+id;
        url=url+"&sid="+Math.random();
        ajaxku.onreadystatechange=stateChangedKec;
        ajaxku.open("GET",url,true);
        ajaxku.send(null);
    }

    function ajaxkel(id){
        ajaxku = buatajax();
        var url= admin_url + "ongkir/ajax_kota";
        url=url+"?kel="+id;
        url=url+"&sid="+Math.random();
        ajaxku.onreadystatechange=stateChangedKel;
        ajaxku.open("GET",url,true);
        ajaxku.send(null);
    }

    function buatajax(){
        if (window.XMLHttpRequest){
        return new XMLHttpRequest();
        }
        if (window.ActiveXObject){
        return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }
    function stateChanged(){
        var data;
        if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>=0){
        document.getElementById("kota").innerHTML = data
        }else{
        document.getElementById("kota").value = "<option selected>Pilih Kota/Kab</option>";
        }
        }
    }

    function stateChangedKec(){
        var data;
        if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>=0){
        document.getElementById("kec").innerHTML = data
        }else{
        document.getElementById("kec").value = "<option selected>Pilih Kecamatan</option>";
        }
        }
    }

    function stateChangedKel(){
        var data;
        if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>=0){
        document.getElementById("kel").innerHTML = data
        }else{
        document.getElementById("kel").value = "<option selected>Pilih Kelurahan/Desa</option>";
        }
        }
    }
    </script>
    
<script type="text/javascript">
$('.edit_cat').click(function () {
  $('.select_cat').toggle();
  $('#category-parent').toggle();
});
</script>
<script type="text/javascript">
$('.merchant_toggle').click(function(){
  $('.merchant_box').slideToggle();
  if($(this).html()=='Hide Merchant') $(this).html('Show Merchant');
    else $(this).html('Hide Merchant');
});
</script>

<script type="text/javascript">
  // $( "#tgl_lahir" ).datepicker({dateFormat: 'yy-mm-dd'});
  $( "#tgl_lahir" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange:'-90:+0',
    dateFormat: 'dd-mm-yy'
  });
  $( "#delivery_date" ).datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $( ".tanggal" ).datepicker({
    dateFormat: 'dd-mm-yy'
  });
</script>
<script type="text/javascript">
$( ".city_auto" ).autocomplete({
  source: <?=$this->auth_m->getCity();?>
});
</script>

  </body>
</html>