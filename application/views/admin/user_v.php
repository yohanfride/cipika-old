<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">Users</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              We're using <a class="alert-link" href="http://tablesorter.com/docs/">Tablesorter 2.0</a> for the sort function on the tables. Read the documentation for more customization options or feel free to use something else!
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->
<?php if($this->auth_m->get_user()->id_level!='5'){ ?>
        <div class="row">
          <div class="col-lg-12">
            <h3>Admins</h3>
            <a href="<?=admin_url();?>user/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <br><br>
            <div class="table-responsive">
              <table class="table table-hover table-striped tablesorter datatable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>username</th>
                    <th>email</th>                    
                    <th>group</th>
                    <th>kab/kota</th>
                    <th>level</th>
                    <th>status</th>
                    <th>action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr class="<?php 
                    if($data->id_level==1) echo 'active';
                    else if($data->id_level==2) echo 'success';
                    else if($data->id_level==3) echo 'warning';
                    else if($data->id_level==4) echo 'danger';
                  ?>">
                    <td><?=$i;?></td>
                    <td><?= "[".$data->id_user."] ".$data->username;?></td>
                    <td><?=$data->email;?></td>
                    <td><?=$data->nama_group;?></td>
                    <td><?=$data->nama_kota;?></td>
                    <td><?=$data->nama_level;?></td>
                    <td><?=$data->nama_status;?></td>
                    <td>
                      <a href="<?=admin_url();?>user/edit/<?=$data->id_user;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>user/delete/<?=$data->id_user;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td> 
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          
            <div class="col-lg-12">
                <h3>Merchant</h3>
                <div class="table-responsive">
                    <table class="table table-hover table-striped tablesorter datatable">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Merchant</th>
                                <th>Pemilik</th>                    
                                <th>Telp</th>                    
                                <th>Tanggal Registrasi</th>                                
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0;foreach ($merchant as $data) { $i++;?>                            
                            <tr class="<?php 
                                if($data->store_status=='pending') echo 'warning';
                                else if($data->store_status=='approve') echo 'success';
                                else if($data->store_status=='block') echo 'danger';                                
                            ?>">
                                <td><?=$i;?></td>
                                <td><?="[".$data->id_user."] ". $data->nama_store;?></td>
                                <td><?=$data->nama_pemilik;?></td>
                                <td><?=$data->telpon;?></td>
                                <td><?=$data->date_added;?></td>
                                <td><?=$data->store_status;?></td>                            
                                <td>
                                    <a href="<?=admin_url();?>user/edit_merchant/<?=$data->id_store;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>                              
                                </td> 
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
          
        </div><!-- /.row -->
<?php } ?>
        <div class="row">
          <div class="col-lg-12">
            <h3>Members</h3>
            <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
            <a href="<?=admin_url();?>user/add_member" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <br><br>
            <?php } ?>
            <div class="table-responsive">
              <table class="table datatable table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Kab/kota</th>
                    <th>group</th>
                    <th>status</th>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas2 as $data) { $i++;?>
                  <tr class="<?php 
                    if($data->id_level==1) echo 'active';
                    else if($data->id_level==2) echo 'success';
                    else if($data->id_level==3) echo 'warning';
                    else if($data->id_level==4) echo 'danger';
                  ?>">
                    <td><?=$i;?></td>
                    <td><?=$data->username;?></td>
                    <td><?=$data->email;?></td>
                    <td><?=$data->nama_kota;?></td>
                    <td><?=$data->nama_group;?></td>
                    <td><?=$data->nama_status;?></td>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <td>
                      <a href="<?=admin_url();?>user/edit_member/<?=$data->id_user;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>user/delete/<?=$data->id_user;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td> 
                    <?php } ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->        
        
      </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>