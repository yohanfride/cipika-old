<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">Merchants</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->
<?php if($this->auth_m->get_user()->id_level!='5'){ ?>
        <div class="row">
          
            <div class="col-lg-12">
                <h3>Merchant</h3>
                <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                <a href="<?=admin_url();?>user/add_merchant" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                <br><br>
                <?php } ?>
                <div class="table-responsive">
                    <table class="table table-hover table-striped tablesorter datatable">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Merchant</th>
                                <th>Pemilik</th>                    
                                <th>Telp</th>                    
                                <th>Tanggal Daftar</th>                                
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 

                            $storeStatusDisplay = array(
                                'pending' => 'Pending',
                                'approve' => 'Verified',
                                'block'   => 'Unverified',
                            );

                            $i=0;foreach ($datas as $data) { $i++;?>                            
                            <tr class="<?php 
                                if($data->store_status=='pending') echo 'warning';
                                else if($data->store_status=='approve') echo 'success';
                                else if($data->store_status=='block') echo 'danger';                                
                            ?>">
                                <td><?=$i;?></td>
                                <td><?="[".$data->id_user."] ". $data->nama_store;?></td>
                                <td><?=$data->nama_pemilik;?></td>
                                <td><?=$data->merchant_telp;?></td>
                                <td><?= strftime("%d-%m-%Y %H:%M:%S",strtotime($data->merchant_register_date));?></td>
                                <td><?= isset($storeStatusDisplay[$data->store_status]) ? $storeStatusDisplay[$data->store_status] : $data->store_status;?></td>
                                <td>
                                    <a href="<?=admin_url();?>user/edit_merchant/<?=$data->id_store;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>                              
                                </td> 
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
          
        </div><!-- /.row -->
<?php } ?> 
        
      </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>
