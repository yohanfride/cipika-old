<?php include 'header.php'; ?>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Orders</h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?= base_url('admin/order'); ?>"><i class="fa"></i> Orders</a></li>
                <li class="active">Status Delivery</li>
            </ol>            
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <td width='30%'>Status</td>
                        <td>: <?php echo status_delivery_label($order->status_delivery)?></td>
                    </tr>
                    <tr>
                        <td>Tgl Kirim</td>
                        <td>: <?php echo ($order->delivery_date!='')?strftime("%d-%m-%Y",strtotime($order->delivery_date)):'-'?></td>
                    </tr>
                    <tr>
                        <td>No Resi</td>
                        <td>: <?php echo $order->noresi?></td>
                    </tr>
                    <!--
                    <tr>
                        <td>Ongkir Cipika</td>
                        <td>: Rp. <?php echo format_uang($order->ongkir_sementara)?></td>
                    </tr>
                    <tr>
                        <td>Ongkir Merchant</td>
                        <td>: Rp. <?php echo format_uang($order->ongkir_merchant)?></td>
                    </tr>
                   
                    <tr>
                        <td>Paket Pengiriman</td>
                        <td>: <?php echo $order->paket_ongkir?></td>
                    </tr>
                    -->
                </table>
                
                <br/><br/>
                <div style="float:left;width:100%">
                    <div style="float:left;width:50%">
                        <table border='1' width='300'>
                            <tr>
                                <td colspan='2' align='center'><b>Metode Pengiriman</b></td>                                
                            </tr>
                            <tr>
                                <td width='50%'><?php echo $order->paket_ongkir?></td>                                
                                <td width='50%'><?php echo $order->paket_ongkir_merchant?></td>                                
                            </tr>
                        </table>
                    </div>
                    
                     <div style="float:left">
                        <table border='1' width='300'>
                            <tr>
                                <td colspan='2' align='center'><b>Biaya Pengiriman</b></td>                                
                            </tr>
                            <tr>
                                <td width='50%'>Rp. <?php echo format_uang($order->ongkir_sementara)?></td>                                
                                <td width='50%'>Rp. <?php echo format_uang($order->ongkir_merchant)?></td>                                
                            </tr>
                        </table>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div><!-- /.row -->     

</div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>