<!DOCTYPE html>               
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?=$title;?> - Cipika Store</title>
    <link rel="shortcut icon" href="<?=base_url();?>asset/img/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>asset/admin/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?=base_url();?>asset/admin/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url();?>asset/admin/font-awesome/css/font-awesome.min.css">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="<?=base_url();?>asset/admin/css/morris-0.4.3.min.css">    
    <link rel="stylesheet" href="<?=base_url();?>asset/admin/css/jquery-ui.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=admin_url();?>dashboard">Cipika Store</a>
        </div>
        <?php $current = $this->uri->segment(2); ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li class="<?=($current=='dashboard')?'active':''?>"><a href="<?=admin_url();?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>            
            <li class="<?=($current=='settlement')?'active':''?>"><a href="<?=admin_url();?>settlement"><i class="fa fa-money"></i> Settlement</a></li>            
            <li class="<?=($current=='order')?'active':''?>"><a href="<?=admin_url();?>order"><i class="fa fa-shopping-cart"></i> Orders</a></li>

<?php if($this->session->userdata('admin_session')->id_level==1 || $this->session->userdata('admin_session')->id_level==2 || $this->auth_m->get_user()->id_level==5) { ?>            
            <!-- <li class="<?=($current=='user')?'active':''?>"><a href="<?=admin_url();?>user"><i class="fa fa-users"></i> Users</a></li> -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Users<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class=""><a href="<?php echo base_url('admin/user/admin')?>"><i class="fa fa-wrench"></i> Admin</a></li>
                    <li class=""><a href="<?php echo base_url('admin/user/merchant')?>"><i class="fa fa-wrench"></i> Merchant</a></li>
                    <li class=""><a href="<?php echo base_url('admin/user/member')?>"><i class="fa fa-wrench"></i> Member</a></li>
                </ul>
            </li>
            <li><a href="<?=base_url();?>admin/article"><i class="fa fa-file"></i> Article</a></li>
            <li><a href="<?=base_url();?>admin/page"><i class="fa fa-file"></i> Page</a></li>
            <li><a href="<?=base_url();?>admin/produk"><i class="fa fa-file"></i> Produk</a></li>
            <li><a href="<?=base_url();?>admin/banner"><i class="fa fa-file"></i> Banner</a></li>
            <li><a href="<?=base_url();?>admin/ongkir"><i class="fa fa-file"></i> Ongkir</a></li>
            <li><a href="<?=base_url();?>admin/contact_form"><i class="fa fa-file"></i> Contact Form</a></li>
            <!--<li><a href="<?=base_url();?>admin/confirm"><i class="fa fa-file"></i> Confirm Produk</a></li>-->
            
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Import<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo ($current=='import-sales')?'active':''?>"><a href="<?php echo base_url('admin/import/sales')?>"><i class="fa fa-wrench"></i> Sales</a></li>
                    <li class="<?php echo ($current=='import-merchant')?'active':''?>"><a href="<?php echo base_url('admin/import/merchant')?>"><i class="fa fa-wrench"></i> Merchant</a></li>
                    <li class="<?php echo ($current=='import-product')?'active':''?>"><a href="<?php echo base_url('admin/import/product')?>"><i class="fa fa-wrench"></i> Product</a></li>
                </ul>
            </li>
            
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Report<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="<?=($current=='report-transaction')?'active':''?>"><a href="<?php echo base_url('admin/report/transaction')?>"><i class="fa fa-wrench"></i> Transaction</a></li>
                    <li class="<?=($current=='report-transaction')?'active':''?>"><a href="<?php echo base_url('admin/report/price_comparison')?>"><i class="fa fa-wrench"></i> Price Comparison</a></li>
                    <li class="<?=($current=='report-transaction')?'active':''?>"><a href="<?php echo base_url('admin/report/merchant_status')?>"><i class="fa fa-wrench"></i> Merchant Status</a></li>
                    <li class="<?=($current=='report-transaction')?'active':''?>"><a href="<?php echo base_url('admin/report/product')?>"><i class="fa fa-wrench"></i> Product</a></li>                    
                    <li class="<?=($current=='report-transaction')?'active':''?>"><a href="<?php echo base_url('admin/report/revenue')?>"><i class="fa fa-wrench"></i> Revenue</a></li>
                    <li class="<?=($current=='report-transaction')?'active':''?>"><a href="<?php echo base_url('admin/report/payment')?>"><i class="fa fa-wrench"></i> Payment</a></li>
                </ul>
            </li>
            
<?php } ?>
          
<?php if($this->session->userdata('admin_session')->id_level==1) { ?>           
            <li><a href="<?=base_url();?>admin/payment/all_payment"><i class="fa fa-file"></i> Payment</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Reference <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="<?=($current=='provinsi')?'active':''?>"><a href="<?=admin_url();?>propinsi"><i class="fa fa-wrench"></i> provinsi</a></li>
                <!-- <li class="<?=($current=='kota')?'active':''?>"><a href="<?=admin_url();?>kota"><i class="fa fa-wrench"></i> Kota</a></li> -->
                <li class="<?=($current=='status')?'active':''?>"><a href="<?=admin_url();?>status"><i class="fa fa-wrench"></i> Status</a></li>
                <li class="<?=($current=='level')?'active':''?>"><a href="<?=admin_url();?>level"><i class="fa fa-wrench"></i> Level</a></li>
                <li class="<?=($current=='kategori')?'active':''?>"><a href="<?=admin_url();?>kategori"><i class="fa fa-wrench"></i> Kategori</a></li>
                <li class="<?=($current=='payment')?'active':''?>"><a href="<?=admin_url();?>payment"><i class="fa fa-wrench"></i> Payment</a></li>
                <li class="<?=($current=='ekspedisi')?'active':''?>"><a href="<?=admin_url();?>ekspedisi"><i class="fa fa-wrench"></i> Ekspedisi</a></li>
              </ul>
            </li>
<?php } ?>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
          <!--
            <li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Messages <span class="badge">7</span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">7 New Messages</li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li><a href="#">View Inbox <span class="badge">7</span></a></li>
              </ul>
            </li>
          -->
            
            <li class="dropdown alerts-dropdown">
              <a id="merchant" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Merchant <span id="count-notifikasi-merchant" class="badge"></span> <b class="caret"></b></a>                              
              <ul id="merchant-notif" class="dropdown-menu">
                <div id="loading" style="display:none;"><br>Loading...<img src="<?=base_url();?>asset/admin/img/loading.gif"></div>                
              </ul>
            </li>
            <li class="dropdown alerts-dropdown">
              <a id="order" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Orders <span id="count-notifikasi-order" class="badge"></span> <b class="caret"></b></a>                              
              <ul id="order-notif" class="dropdown-menu">
                <div id="loading" style="display:none;"><br>Loading...<img src="<?=base_url();?>asset/admin/img/loading.gif"></div>                
              </ul>
            </li>
            <li class="dropdown alerts-dropdown">
              <a id="product" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Products <span id="count-notifikasi-produk" class="badge"></span> <b class="caret"></b></a>                              
              <ul id="product-notif" class="dropdown-menu">
                <div id="loading" style="display:none;"><br>Loading...<img src="<?=base_url();?>asset/admin/img/loading.gif"></div>                
              </ul>
            </li>
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=ucfirst($this->session->userdata('admin_session')->email);?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?=admin_url();?>user/profile/<?=md5($this->auth_m->get_user()->id_user);?>"><i class="fa fa-user"></i> Profile</a></li>
                <!-- <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li> -->
                <li><a href="<?=admin_url();?>user/setting/<?=md5($this->auth_m->get_user()->id_user);?>"><i class="fa fa-gear"></i> Settings</a></li>
                <li class="divider"></li>
                <li><a href="<?= base_url('admin/user/destroy') ?>"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
