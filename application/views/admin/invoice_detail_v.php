<?php include 'header.php'; ?>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Invoice</h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?= base_url('admin/order'); ?>"><i class="fa"></i> Orders</a></li>
                <li class="active">Invoice Detail</li>
            </ol>           
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <div style='float:left;width:100%'>
                    <div style='float:left;width:60%'>
                        <table border='0' width="600">                    
                            <tr>
                                <td style="width: 200px">Nomor Invoice</td>
                                <td style="width: px">:</td>
                                <td><strong><?= $invoice->kode_invoice ?></strong></td>
                            </tr>
                            <tr>
                                <td style="width: 200px">Nama</td>
                                <td style="width: px">:</td>
                                <td><?= $buyer->firstname ?> <?= $buyer->lastname ?></td>
                            </tr>
                            <tr>
                                <td style="width: 200px">Alamat</td>
                                <td style="width: 10px">:</td>
                                <td>
                                    <?php
                                        $lokasi = $this->lib_lokasi->get_lokasi($buyer->id_kecamatan);
                                        $lokasi = (!empty($lokasi))?$lokasi->nama_kecamatan.','.$lokasi->nama_kabupaten.','.$lokasi->nama_propinsi:'';
                                        echo $buyer->alamat.'<br/>'.$lokasi;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">No Telp</td>
                                <td style="width: 10px">:</td>
                                <td><?=$buyer->telpon;?></td>
                            </tr>
                            <tr>
                                <td style="width: 200px">No Handphone</td>
                                <td style="width: 10px">:</td>
                                <td><?=$buyer->hp;?></td>
                            </tr>
                            <tr>
                                <td style="width: 200px">Total Harga</td>
                                <td style="width: 4px">:</td>
                                <td>Rp <?= format_uang($invoice->total+$invoice->ongkir+$invoice->payment_fee);?></td>
                            </tr>
                            <tr>
                                <td style="width: 200px">Tanggal</td>
                                <td style="width: 4px">:</td>
                                <td><?= strftime("%d-%m-%Y %H:%M:%S",strtotime($invoice->date_added)) ?></td>
                            </tr>
                        </table>
                    </div>
                    
                    <div style="float:right">
                        <?php
                            if(!empty($payment)):
                                $pay    = $this->payment_m->get_det_payment($invoice->id_payment);
                                $pid    = ($payment->payment == "DOMPETKU")?$payment->msisdn:$payment->respond;
                        ?>
                        <table border='0' width="250">                    
                            <tr>
                                <td><b>Pembayaran</b></td>                            
                            </tr>
                            <tr>
                                <td>Metode : <?php echo $pay->nama_payment?></td>                            
                            </tr>
                            <tr>
                                <td>VAN ID : <?php echo $pid?></td>                            
                            </tr>
                        </table>
                        <?php endif;?>
                    </div>
                </div>
                <div style='clear:both'></div>
                
                <br/><br/>
                
                <?php
                    if(!empty($orders)){
                        $grant_total = 0;
                        
                        echo    "<h3>Data Transaksi</h3>";
                        foreach($orders as $row){
                            $seller   = $this->commonlib->get_seller($row->id_merchant);                            
                            $merchant = (!empty($seller))?$seller->nama_store:'';
                            
                            $lokasi   = $this->lib_lokasi->get_lokasi($seller->id_kecamatan_seller);
                            $lokasi = (!empty($lokasi))?ucwords($lokasi->nama_kecamatan).','.ucwords($lokasi->nama_kabupaten).','.ucwords($lokasi->nama_propinsi):'';
                                                    
                            //# Order Info
                            
                            echo    "<table width='100%' border='0'>".
                                        "<tr valign='top'>".
                                            "<td width='35%'>Merchant: <b>".$merchant."</b>".$seller->alamat_seller."<br/>".$lokasi."</td>".
                                            "<td width='35%'>Order: <b>".$row->kode_order."</b></td>".                                            
                                            "<td width='35%'>Status: <b>Sukses</b></td>".                                            
                                        "</tr>".                                        
                                    "</table>";
                            
                            //# Shipping Info
                            $shipping_info = $this->cart_m->get_order_shipping($row->id_order);
                            if(!empty($shipping_info)){
                                
                                $resi = '';
                                if($row->noresi!=''){
                                    $resi =     "<br/><strong>AWB Number</strong><br/>".
                                                $row->noresi."<br/>".
                                                "<strong>Tgl Kirim</strong><br/>".
                                                $row->delivery_date."<br/>";
                                }
                            
                                echo "<div style='margin-top: 1em;'>".
                                            "<table width='100%' border='0'>".
                                                "<tr>".
                                                    "<td width='35%'><strong>Alamat Pengiriman</strong></td>".
                                                    "<td width='35%'><strong>Metode Pengiriman</strong></td>".
                                                    "<td width='35%'><strong></strong></td>".
                                                "</tr>".
                                                "<tr valign='top'>".
                                                    "<td>".ucwords($shipping_info->nama)."<br/>".ucwords($shipping_info->alamat)."<br/>".ucwords($shipping_info->nama_kabupaten)." - ". ucwords($shipping_info->nama_propinsi)."<br/>".ucwords($shipping_info->telpon)."</td>".
                                                    "<td>".ucwords($row->paket_ongkir)." ".$resi."</td>".
                                                    "<td></td>".
                                                "</tr>".                                               
                                            "</table>".
                                        "</div>";
                            }
                                    
                            //# Order Item Info
                            $items  = $this->order_m->get_orderitem($row->id_order);
                            if(!empty($items)){
                                echo    "<br/><br/><table class='table table table-bordered'>".
                                            "<thead>".
                                                "<tr>".
                                                    "<th style='text-align: center' width='10%'>No.</th>".
                                                    "<th style='text-align: center'>Nama Produk</th>".
                                                    "<th style='text-align: center' width='10%'>Jumlah</th>".
                                                    "<th style='text-align: center' width='15%'>Harga</th>".
                                                    "<th style='text-align: center' width='15%'>Diskon</th>".
                                                    "<th style='text-align: center' width='20%'>Subtotal</th>".
                                                "</tr>".
                                            "</thead>".
                                            "<tbody>";
                                            
                                $no = 0;$berat = 0;$subtotal = 0;
                                foreach($items as $item){
                                    $no++;
                                    
                                
                                    if($item->diskon > 0)
                                        $total = ($item->harga*$item->jml_produk)*($item->diskon/100);
                                    else
                                        $total = $item->harga*$item->jml_produk;
                                    
                                    $subtotal+=$total;
                                    $total_harga = $subtotal+$row->ongkir_sementara;
                                    
                                
                                    echo    "<tr>".
                                                "<td style='text-align: center'>".$no."</td>".
                                                "<td>".$item->nama_produk."</td>".
                                                "<td style='text-align: right;'>".$item->jml_produk."</td>".
                                                "<td style='text-align: right;'>Rp ".format_uang($item->harga)."</td>".
                                                "<td style='text-align: right;'>".$item->diskon."</td>".                                                
                                                "<td style='text-align: right;'>Rp ".format_uang($total)."</td>".
                                            "</tr>";
                                }
                                
                                $grant_total = $grant_total + $total_harga;
                                echo    "<tr>".
                                            "<td style='text-align: right; font-weight: bold' colspan='5'>Subtotal</td>".
                                            "<td style='text-align: right'>Rp ".format_uang($subtotal)."</td>".
                                        "</tr>".
                                        "<tr>".
                                            "<td style='text-align: right; font-weight: bold' colspan='5'>Ongkos Kirim</td>".
                                            "<td style='text-align: right'>Rp ".format_uang($row->ongkir_sementara)."</td>".
                                        "</tr>".
                                        "<tr>".
                                            "<td style='text-align: right; font-weight: bold' colspan='5'>Total</td>".
                                            "<td style='text-align: right'>Rp ".format_uang($total_harga)."</td>".
                                        "</tr>";
                                
                                echo "</tbody></table><br/><hr/>";
                            }
                        }
                        
                        echo    "<table class='table table table-bordered' width='100%'>".                            
                                    //"<tr>".
                                    //    "<td width='80%' style='text-align: right; font-weight: bold'>Biaya Admin </td>".
                                    //    "<td width='20%' style='text-align: right;'>Rp ".format_uang($nilai_admin['option_value'])."</td>".
                                    //"</tr>".
                                    "<tr>".
                                        "<td width='80%' style='text-align: right; font-weight: bold'>Grand Total </td>".
                                        //"<td width='20%' style='text-align: right;'>Rp ".format_uang($grant_total+$nilai_admin['option_value'])."</td>".
                                        "<td width='20%' style='text-align: right;'>Rp ".format_uang($grant_total)."</td>".
                                    "</tr>".
                                "</table>";
                        
                    }
                ?>
                
            </div>
        </div>
    </div><!-- /.row -->     

</div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>