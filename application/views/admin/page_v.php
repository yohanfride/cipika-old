<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>page</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">page</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
            <a href="<?=admin_url();?>page/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <?php } ?>
            <br><br>
            <div class="table-responsive">
              <table class="table datatable table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>title</th>
                    <th>content</th>
                    <th>sort</th>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$data->title;?></td>
                    <td><?=strip_tags(substr($data->content, 0, 50));?>...</td>
                    <td><?=$data->sorting;?></td>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <td>
                      <a href="<?=admin_url();?>page/edit/<?=$data->id_page;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>page/delete/<?=$data->id_page;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td>
                    <?php } ?>                    
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>