
<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>user</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <?php if($add=='admin'){ ?>
              <li><a href="<?=admin_url();?>user/admin">Admin</a></li>
              <?php } else { ?>
              <li><a href="<?=admin_url();?>user/member">Member</a></li>
              <?php } ?>
              <li class="active">Add</li>
            </ol>
            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

<?php if($add=='admin'){ ?>
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/add">
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">username</label>
                <div class="col-lg-4">
                  <input name="username" type="text" class="form-control" id="username" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">password</label>
                <div class="col-lg-4">
                  <input name="password" type="password" class="form-control" id="password" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">password</label>
                <div class="col-lg-4">
                  <input name="passconf" type="password" class="form-control" id="passconf" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">email</label>
                <div class="col-lg-4">
                  <input name="email" type="email" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">firstname</label>
                <div class="col-lg-4">
                  <input name="firstname" type="text" class="form-control" id="firstname" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">lastname</label>
                <div class="col-lg-4">
                  <input name="lastname" type="text" class="form-control" id="lastname" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">kab/kota</label>
                <div class="col-lg-4">
                  <select name="id_kota" type="text" class="form-control" id="id_kota" placeholder="">
                    <option value=""></option>
                    <?php foreach ($kota as $k) { ?>
                    <option value="<?=$k->id_kota;?>"><?=$k->nama_kota;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">level</label>
                <div class="col-lg-4">
                  <select name="id_level" type="text" class="form-control" id="id_level" placeholder="">
                    <option value="">-- pilih level --</option>
                    <?php foreach ($level as $l) { ?>
                    <option value="<?=$l->id_level;?>"><?=$l->nama_level;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->
<?php } ?>

<?php if($add=='member'){ ?>
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>user/add_member">
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Display Name</label>
                <div class="col-lg-4">
                  <input name="username" type="text" class="form-control" id="username" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-4">
                  <input name="email" type="email" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-4">
                  <input name="password" type="password" class="form-control" id="password" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Password Confirmation</label>
                <div class="col-lg-4">
                  <input name="passconf" type="password" class="form-control" id="passconf" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Kab/kota</label>
                <div class="col-lg-4">
                  <select name="id_kota" type="text" class="form-control" id="id_kota" placeholder="">
                    <option value=""></option>
                    <?php foreach ($kota as $k) { ?>
                    <option value="<?=$k->id_kota;?>"><?=$k->nama_kota;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="user" class="col-lg-2 control-label">Phone</label>
                <div class="col-lg-4">
                  <input name="telpon" type="text" class="form-control" id="telpon" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>                  
                </div>
              </div>
            </form>            
          </div>
        </div><!-- /.row -->
<?php } ?>

      </div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>