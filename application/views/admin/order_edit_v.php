<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Order</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li><a href="<?=admin_url();?>order">order</a></li>
              <li class="active">Edit <?php echo ucwords($mode)?></li>
            </ol>
            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error?>
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=admin_url();?>order/edit/<?=$data->id_order;?>">              
              <div class="form-group">
                <label for="order" class="col-lg-2 control-label">Kode Invoice</label>
                <div class="col-lg-4">
                  <span><?=$invoice->kode_invoice;?></span>
                </div>
              </div>
                <div class="form-group">
                <label for="order" class="col-lg-2 control-label">Kode order</label>
                <div class="col-lg-4">
                  <span><?=$data->kode_order;?></span>
                </div>
              </div>
              
              <div class="form-group" style="<?php echo ($mode=='payment')?'display:block':'display:none'?>">
                <label for="order" class="col-lg-2 control-label">Status payment</label>
                <div class="col-lg-4">
                  <select name="status_payment" type="text" class="form-control" id="status_payment">
                    <option value="waiting" <?=($data->status_payment=='waiting')?'selected':'';?>>Waiting</option>
                    <option value="paid" <?=($data->status_payment=='paid')?'selected':'';?>>Paid</option>                   
                  </select>
                  <?php if($data->status_payment=='paid'){ ?>
                  <!-- <input type="text" value="paid" class="hidden" name="status_payment"> -->
                  <?php } ?>
                </div>
              </div>
              
              <div class="form-group" style="<?php echo ($mode=='shipping')?'display:block':'display:none'?>">
                <label for="order" class="col-lg-2 control-label">Status delivery</label>
                <div class="col-lg-4">
                  <?php if($data->status_payment=='waiting'){ ?>
                  <input type="text" value="persiapan pengiriman" class="hidden" name="status_delivery">
                  <?php } ?>
                  <select name="status_delivery" type="text" class="form-control" id="status_delivery" <?=($data->status_payment=='waiting')?'disabled':'';?>>
                    <option value='persiapan pengiriman' <?=($data->status_delivery=='persiapan pengiriman')?'selected':''?>>Persiapan Pengiriman</option>
                    <option value='proses pengiriman' <?=($data->status_delivery=='proses pengiriman')?'selected':''?>>Proses Pengiriman</option>
                    <option value='produk telah diterima' <?=($data->status_delivery=='produk telah diterima')?'selected':''?>>Produk Telah Diterima</option>                 
                    <option value='proses retur' <?=($data->status_delivery=='proses retur')?'selected':''?>>Proses Retur</option>                 
                  </select>
                </div>
              </div>
              
              <div class="form-group" style="<?php echo ($mode=='shipping')?'display:block':'display:none'?>">
                <label for="order" class="col-lg-2 control-label">Paket Ongkir</label>
                <div class="col-lg-4">
                  <select name="paket_ongkir_merchant" type="text" class="form-control" id="paket_ongkir_merchant">                                              
                    <option value=''>-- Pilih Paket Pengiriman --</option>
                    <option value='YES' <?=($order['order']->paket_ongkir_merchant=='YES')?'selected':'';?>>YES</option>
                    <option value='REG' <?=($order['order']->paket_ongkir_merchant=='REG')?'selected':'';?>>REG</option>
                    <option value='OKE' <?=($order['order']->paket_ongkir_merchant=='OKE')?'selected':'';?>>OKE</option>
                    <option value='CTYES' <?=($order['order']->paket_ongkir_merchant=='CTYES')?'selected':'';?>>CTYES</option>
                  </select>   
                </div>
              </div>
              <div class="form-group" style="<?php echo ($mode=='shipping')?'display:block':'display:none'?>">
                <label for="order" class="col-lg-2 control-label">Ongkir (Rp)</label>
                <div class="col-lg-4">
                  <input value="<?php echo set_value('ongkir_merchant',$order['order']->ongkir_merchant)?>" name="ongkir_merchant" type="text" class="form-control" id="ongkir_merchant">
                </div>
              </div>
              <div class="form-group" style="<?php echo ($mode=='shipping')?'display:block':'display:none'?>">
                <label for="order" class="col-lg-2 control-label">AWB Number</label>
                <div class="col-lg-4">
                  <input value="<?php echo set_value('noresi',$order['order']->noresi)?>" name="noresi" type="text" class="form-control" id="noresi">
                </div>
              </div>
              <div class="form-group" style="<?php echo ($mode=='shipping')?'display:block':'display:none'?>">
                <label for="order" class="col-lg-2 control-label">Tgl Kirim</label>
                <div class="col-lg-4">
                  <input value="<?=($order['order']->delivery_date)?set_value('delivery_date',strftime("%d-%m-%Y",strtotime($order['order']->delivery_date))):'';?>" name="delivery_date" type="text" class="form-control" id="delivery_date">
                </div>
              </div>

              <div class="form-group">
                  <div class="col-lg-3" style="width: 500px;">
                      <button class="btn btn-primary pull-right" type="submit" name="simpan" value=1>Simpan</button>
                      <?php
                      if ($act == 1)
                      {
                          ?>
                          <button class="btn btn-cancel pull-right" type="submit" name="send_po" value=1 style="margin-left: 2px;margin-right: 2px;">Kirim PO Merchant</button>                  
                          <button class="btn btn-cancel pull-right" type="submit" name="send_confirm" value=1 style="margin-left: 2px;margin-right: 2px">Kirim Payment Thankyou</button>                  
                          <?php
                      }
                      ?>
                  </div>
              </div>

              <input type="hidden" name='mode' value='<?php echo $mode ?>'>
            </form>            
              <div>Keterangan : <br>
                  <p>Bila anda sudah mengganti Status menjadi Paid, anda bisa mengirimkan PO Merchant dan Payment Thankyou.</p>
                  <p>Perubahan status payment berlaku untuk semua order pada invoice  :<?=$invoice->kode_invoice;?></p>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php include 'footer.php'; ?>