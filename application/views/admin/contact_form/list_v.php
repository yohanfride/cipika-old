<?php $this->load->view('admin/header') ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Contact Form</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">Contact Form</li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
           
            <div class="table-responsive">
              <table class="table datatable table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>Tgl Kirim</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Pesan</th>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <th>action</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($data as $row) { $i++;?>
                  <tr valign='top'>                    
                    <td width='15%'><?php echo strftime("%d-%m-%Y %H:%M:%S",strtotime($row->tgl_kirim));?></td>
                    <td width='15%'><?php echo $row->nama;?></td>
                    <td width='15%'><?php echo $row->email;?></td>
                    <td width='50%'><?php echo nl2br($row->pesan);?></td>
                    <?php if($this->auth_m->get_user()->id_level!='5'){ ?>
                    <td>                      
                      <a href="<?=admin_url();?>contact_form/delete/<?php echo $row->idcf;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td>
                    <?php } ?>                    
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer') ?>