<?php include 'header.php'; ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">Users</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->
<?php if($this->auth_m->get_user()->id_level!='5'){ ?>
        <div class="row">
          <div class="col-lg-12">
            <h3>Admins</h3>
            <a href="<?=admin_url();?>user/add" type="button" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
            <br><br>
            <div class="table-responsive">
              <table class="table table-hover table-striped tablesorter datatable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>username</th>
                    <th>email</th>                    
                    <!-- <th>group</th> -->
                    <th>kab/kota</th>
                    <th>level</th>
                    <th>status</th>
                    <th>action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr class="<?php 
                    if($data->id_level==1) echo 'active';
                    else if($data->id_level==2) echo 'success';
                    else if($data->id_level==3) echo 'warning';
                    else if($data->id_level==4) echo 'danger';
                  ?>">
                    <td><?=$i;?></td>
                    <td><?= "[".$data->id_user."] ".$data->username;?></td>
                    <td><?=$data->email;?></td>
                    <!-- <td><?=$data->nama_group;?></td> -->
                    <td><?=$data->nama_kabupaten;?></td>
                    <td><?=$data->nama_level;?></td>
                    <td><?=$data->nama_status;?></td>
                    <td>
                      <a href="<?=admin_url();?>user/edit/<?=$data->id_user;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>user/hapus/<?=$data->id_user;?>/admin" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td> 
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          
        </div><!-- /.row -->
<?php } ?>    
        
      </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>