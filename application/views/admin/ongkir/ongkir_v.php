<?php $this->load->view('admin/header') ?>
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Ongkir</h1>
            <ol class="breadcrumb">
              <li><a href="<?=admin_url();?>dashboard">Dashboard</a></li>
              <li class="active">ongkir</li>
            </ol>
            <?php if($alert=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($alert=='failed'){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Failed
            </div>
            <?php } ?>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="filter-ongkir">
              <form class="form-inline" role="form">
                <div class="form-group">
                  <label>From: </label>
                </div>
                <div class="form-group">
                  <select name="prop" class="form-control" id="provinsi" onchange="ajaxkota(this.value)">
                    <option value="">Pilih Provinsi</option>
                    <?php foreach($propinsi as $row){ ?>
                    <option value="<?=$row->lokasi_propinsi?>"><?=$row->lokasi_nama?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <select name="kota" class="form-control" id="kota" onchange="ajaxkec(this.value)">
                    <option selected value=''>Pilih Kota/Kab</option>
                  </select>
                </div>
                <div class="form-group">
                  <select name="kec" class="form-control" id="kec">
                    <option selected value=''>Pilih Kecamatan</option>
                  </select>
                </div>

                <button type="submit" class="btn btn-default">Go</button>

              </form>

            </div>
            <div class="clearfix">
              <label>To: </label>
            </div>
            <div class="table-responsive">
              <table class="table datatable table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama ongkir</th>
                    <th>Parent</th>                    
                    <th>Keterangan</th>
                    <th>Action</th>                    
                  </tr>
                </thead>
                <tbody><!--
                  <?php $i=0;foreach ($datas as $data) { $i++;?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=ucwords($data->nama_ongkir);?></td>
                    <td><?=ucwords($data->parent);?></td>                      
                    <td><?=$data->keterangan;?></td>
                    <td>
                      <a href="<?=admin_url();?>ongkir/edit/<?=$data->id_ongkir;?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                      <a href="<?=admin_url();?>ongkir/delete/<?=$data->id_ongkir;?>" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</a>
                    </td>                    
                  </tr>
                  <?php } ?>-->
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->   
<?php $this->load->view('admin/footer') ?>