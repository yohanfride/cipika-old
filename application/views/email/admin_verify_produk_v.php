Merchant Cipika Store Yth.<br/><br/>

Selamat! Produk Anda telah melalui tahap verifikasi dan tampil etalase produk Cipikastore.com. Adapun detil produk sbb:<br/><br/>


<table border='0' width='500'> 
    <tr>
        <td width='30%'>Nama Produk</td>
        <td>: <?php echo $produk->nama_produk?></td>
    </tr>
    <tr>
        <td>Stok</td>
        <td>: <?php echo $produk->stok_produk?></td>
    </tr>
    <tr>
        <td>Harga</td>
        <td>: Rp. <?php echo format_uang($produk->harga_produk)?></td>
    </tr>
    <tr>
        <td>Diskon</td>
        <td>: <?php echo format_uang($produk->diskon)?></td>
    </tr>
    <tr>
        <td>Kategori</td>
        <td>: <?php echo $this->produk_m->get_kategori($produk->id_produk)?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>: <?php echo status_product_label($produk->publish)?></td>
    </tr>
</table><br/><br/>

Salam, Cipika Store<br/><br/>

Semuanya Menjadi Mudah