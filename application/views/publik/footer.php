 <!-- FOOTER -->
 <style>
    .inline_error{
        margin:3px 0 3px 3px;
        color:#D7121D !important;
    }
    
    .inline_error p{
        margin:0 0 0 -3px !important
    }
    
    .text_error{
        border-color:#D7121D !important;
    }
    
    .text_pass{
        border-color:#1F9612 !important;
    }
 </style>
  <div class="block-footer">
    <div class="container-fluid">
      <!-- <nav class="pull-right"> -->
        <div class="nav nav-pills">
        <div class="row">
          <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div class="row">
          <?php foreach ($page as $page_key) { ?>
          <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
          <a class="link" href="<?=base_url().'page/index/'.$page_key->id_page.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$page_key->title));?>"><?=ucwords($page_key->title);?></a>
          </div>
          <?php } ?>
          
            </div>
          </div>
        </div>
        </div>
      <!-- </nav> -->
      <div class="row" style="margin-top: 2em"></div>
      <footer style="text-align: center">
        <strong>Powered by</strong><span class="logo_power"><img src="<?=base_url()?>asset/img/logoindosat.png"><img src="<?=base_url()?>asset/img/logoshoop.png"></span></p>
        <p>&copy; 2014 Cipika Store.</p>
      </footer>
    </div>
  </div>
  <!-- /FOOTER -->

  <!-- POPUPS -->
  <div id="register-login-form" class="modal fade">
      <?php
        $facebook = $this->lib_facebook->connect();
      ?>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <ul id="tab-register-login" class="nav nav-tabs">
            <li><a href="#login" data-toggle="tab">Masuk</a></li>
            <li class="active"><a href="#register" data-toggle="tab">Daftar</a></li>
          </ul>
        </div>
        <div class="tab-content">
          <div class="tab-pane" id="login">
            <div class="modal-body">           
              <div class="with-facebook">
                <a href="<?=$facebook['loginUrl']?>" class="fb-login-button btn btn-lg btn-facebook blocken"><i class="shoopicon-facebook"></i> Masuk dengan Facebook</a> 
              </div>
              <p class="line-sparator">
                <span class="sparator-inner">atau daftar dengan email</span>
              </p>
              <form id="form-login" class="form" method="post" action="<?=base_url();?>auth/login"> <!-- login form -->
                <div class="login_error" style="color:red;"></div>
                <div class="account-form">
                  <p><input name="email" class="form-control" type="text" placeholder="Email"></p>
                  <p><input name="password" class="form-control" type="password" placeholder="Password"></p>
                  
                  <!--<p><label><input name="remember" value="1" type="checkbox"> Tetap masuk</label></p>-->
                </div>
                <div class="submit-box">
                  <p><input type="submit" class="btn btn-lg btn-primary form-control" value="Masuk" /></p>
                  <p><a href="<?=base_url()?>auth/forgot_password">Lupa password?</a></p>
                </div>
              </form>


            </div>
          </div>
          <div class="tab-pane active" id="register">
            <div class="modal-body">           
              <div class="with-facebook">
                <a href="<?=$facebook['loginUrl'];?>" class="btn btn-lg btn-facebook blocken"><i class="shoopicon-facebook"></i> Daftar dengan Facebook</a> 
              </div>
              <p class="line-sparator">
                <span class="sparator-inner">atau daftar dengan email</span>
              </p>
              
              <div>                
                <div class='inline_error' id='email_register_error' style='display:none'></div>
                <div class='inline_error' id='passconf_register_error' style='display:none'></div>
              </div>
              
              <div class="reg_error" style="color:#D7121D !important;">                
              </div>
              <form id="form-reg" class="form" method="POST" action="<?= base_url('auth/register') ?>"> <!-- register form -->
                <div class="account-form">
                    <!-- <table border='0' width='100%'> -->
                        <!-- <tr>
                            <td width='90%'> --><p>
                              <input class="form-control" type="text" name="email" id="email_register" placeholder="Email" onblur="check_email()">
                            </p><!-- </td>
                            <td align='center'> --><img style='display:none' id='warning_email' src="<?php echo base_url('asset/img/dialog-warning.png')?>"><!-- </td>
                        </tr>
                        <tr>
                            <td> --><p>
                              <input class="form-control" type="password" name="password" placeholder="Password" id="password_register"><small style="color:red;">&nbsp;*panjang minimal 6 karakter.</small>
                            </p><!-- </td>
                            <td align='center'> -->
                              <img style='display:none' id='warning_password' src="<?php echo base_url('asset/img/dialog-warning.png')?>">
                            <!-- </td>
                        </tr>
                        <tr>
                            <td> --><p>
                              <input class="form-control" type="password" name="passconf" placeholder="Konfirmasi Password" id="passconf_register" onblur="check_password()">
                            </p><!-- </td>
                            <td align='center'> -->
                              <img style='display:none' id='warning_passconf' src="<?php echo base_url('asset/img/dialog-warning.png')?>">
                            <!-- </td>
                        </tr>
                        <tr>
                            <td> -->
                                <div class="recaptcha">
                                    <?= $this->lib_recaptcha->recaptcha(); ?>
                                </div>
                            <!-- </td>
                            <td align='center'> -->
                              <img style='display:<?php echo ($error_captcha)?'block':'none'?>' id='warning_captcha' src="<?php echo base_url('asset/img/dialog-warning.png')?>">
                           <!--  </td>
                        </tr> -->
                    <!-- </table> -->
                    <!--<p><?= $image ?></p>
                    <p><input class="form-control" type="text" name="captcha" placeholder="Chaptcha"></p>-->
                  
                  <p><small>Dengan klik tombol daftar, Anda telah menyetujui <a href="<?=base_url().'page/index/8';?>" target='_blank'>Syarat dan Ketentuan Berlaku</a>.</small></p>
                </div>
                <div class="submit-box">
                  <p><input type="submit" class="btn btn-lg btn-primary form-control register-submit" value="Daftar" /></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- MAIL ICON -->
  <div class="mail_icon" style="display:none;">
    <a href="mailto:e-care.store@cipika.co.id" title="Hubungi kami"><img src="<?=base_url()?>asset/img/mail3.png"><!--  <span>Contact Us</span> --></a>
  </div>
  <!-- END MAIL ICON -->

<!--<script src="<?=base_url();?>asset/js/vendor/jquery-1.11.0.min.js"></script>-->
<script src="<?=base_url();?>asset/admin/js/jquery-ui.min.js"></script>
<script src="<?=base_url();?>asset/js/vendor/bootstrap.min.js"></script>
<script src="<?=base_url();?>asset/js/jquery.nivo.slider.js" type="text/javascript"></script> 

<?php echo $this->config->item('google_analytics_script');?>

<?php if($this->uri->segment(1)=='') { ?>
<script type="text/javascript">
$('#nivo').nivoSlider({
    effect: 'fade',               // Specify sets like: 'fold,fade,sliceDown'
    slices: 15,                     // For slice animations
    boxCols: 8,                     // For box animations
    boxRows: 4,                     // For box animations
    animSpeed: 500,                 // Slide transition speed
    pauseTime: <?=$this->auth_m->get_single('tbl_options', 'option_name', 'banner_interval')->option_value;?>,                // How long each slide will show
    startSlide: 0,                  // Set starting Slide (0 index)
    directionNav: false,             // Next & Prev navigation
    controlNav: false,               // 1,2,3... navigation
    controlNavThumbs: false,        // Use thumbnails for Control Nav
    pauseOnHover: false,             // Stop animation while hovering
    manualAdvance: false,           // Force manual transitions
    // prevText: 'Prev',               // Prev directionNav text
    // nextText: 'Next',               // Next directionNav text
    randomStart: false,             // Start on a random slide
    beforeChange: function(){},     // Triggers before a slide transition
    afterChange: function(){},      // Triggers after a slide transition
    slideshowEnd: function(){},     // Triggers after all slides have been shown
    lastSlide: function(){},        // Triggers when last slide is shown
    afterLoad: function(){}         // Triggers when slider has loaded
});
</script>
<?php } ?>
<script src="<?=base_url();?>asset/js/main.js"></script>
<script type="text/javascript">
  // $( "#tgl_lahir" ).datepicker({dateFormat: 'yy-mm-dd'});
  $( "#tgl_lahir" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange:'-90:+0',
    dateFormat: 'dd-mm-yy'
  });
  $( "#delivery_date" ).datepicker({
    dateFormat: 'dd-mm-yy'
  });
</script>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. 
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>
-->
<?php 
if(isset($empty)){
    $infinite = $empty;
}else{
    $infinite = 0;
}
?>
<script type="text/javascript">
var base_url = '<?=base_url();?>';
var index_page = '<?=$this->config->slash_item('index_page');?>';
var url_suffix = '<?=$this->config->item('url_suffix');?>';
var empty_infinite = '<?= $infinite ?>';
</script>

<script type="text/javascript" src="<?=base_url();?>asset/js/nprogress.js"></script>
<script type="text/javascript">
  $('.link').click(function(){
    NProgress.start();
    NProgress.done();
  });
</script>
<script type="text/javascript" src="<?=base_url();?>asset/js/gus_cart.js"></script>
<script>
    function check_email()
    {
        var email = $('#email_register').val();
        if(email!=''){
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('auth/check_email')?>",                    
                data    : "email="+email,
                success : function(result){
                    
                    if(result!=1){
                        $('#warning_email').show();                        
                        $('#email_register_error').fadeIn();                        
                        $('#email_register_error').html(result);                        
                        $('#email_register').removeClass('text_pass').addClass('text_error');
                    }else{
                        $('#warning_email').hide();                        
                        $('#email_register_error').fadeOut();
                        $('#email_register').removeClass('text_error').addClass('text_pass');
                    }
                }
            });
        }
        
    }
    
    function check_password()
    {
        var pass    = $('#password_register').val();
        var passc   = $('#passconf_register').val();
        
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('auth/check_password')?>",                    
                data    : "password="+pass+"&passconf="+passc,
                success : function(result){
                    
                    if(result!=1){
                        $('#warning_password').show();                        
                        $('#warning_passconf').show();                        
                        $('#passconf_register_error').fadeIn();                        
                        $('#passconf_register_error').html(result);                        
                        $('#passconf_register').removeClass('text_pass').addClass('text_error');
                    }else{
                        $('#warning_password').hide();                        
                        $('#warning_passconf').hide();                        
                        $('#passconf_register_error').fadeOut();
                        $('#passconf_register').removeClass('text_error').addClass('text_pass');
                    }
                }
            });
        
    }

   function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               $('#previewHolder').attr('src', e.target.result);
           }

           reader.readAsDataURL(input.files[0]);
       }
   }
   $('#change_pp').click(function(){
      $("#filePhoto").click();
   });
   $("#filePhoto").change(function() {
      readURL(this);
   });
</script>
<script type="text/javascript">
$('.edit_cat').click(function () {
  $('.select_cat').toggle();
  $('#category-parent').toggle();
});
</script>
<script>
/**
START AJAX SEARCH
**/
// var search_url;
$(document).on('change', '.select-sort, .select-tag, .select-category, .select-location, .select-price', function(e) {
    $(this).closest('form').trigger('submit');
});

<?php if($this->uri->segment(1)=='search') { ?>
  var frmsearh = $('#form-search');
  frmsearh.submit(function (ev) {
      $.ajax({
          type: frmsearh.attr('method'),
          url: frmsearh.attr('action'),
          data: frmsearh.serialize(),
          dataType: 'html',
          beforeSend: function() {
          NProgress.start();
          },
          success: function (data) {
            // $(".product-thumb-row").html(data);
            $('.product-thumb-row').html($('.product-thumb-row', data).html());
            var pageurl = this.url;
            if(pageurl!=window.location.href){
              window.history.pushState({path:pageurl},'',pageurl);
            }

            NProgress.done();
            return false;
          }
      });

      ev.preventDefault();
  });

<?php } ?>
/**
END AJAX SEARCH
**/
</script>
<script type="text/javascript">
//cek number field
  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if ((charCode > 47 && charCode < 58 ) || charCode == 44 || charCode == 46 || charCode == 8)
        return true;

     return false;
  }
</script>
<script>
    $(document).on('click', '.product-thumb-love', function(e) {
        e.preventDefault();
        
        var $productTile = $(this).parents('.product-tile');
        var productId = $productTile.attr('data-product-id');
        var $this = $(this);

        $.ajax({
            url: base_url + "ajax/love",
            method: "POST",
            data: {id: productId},
            dataType: "json",
            success: function(str) {
              // alert('ok');
              if (str.status=='success') {
                var $loveCount = $this.children('.love-count');
                if (str.value == '+1') {
                    $loveCount.html(parseInt($loveCount.html()) + 1);
                    $('.count-love-all').html(parseInt($('.count-love-all').html())+1);
                    $this.addClass('user-loved');
                } else if (str.value == '-1') {
                    $loveCount.html(parseInt($loveCount.html()) - 1);
                    $('.count-love-all').html(parseInt($('.count-love-all').html())-1);
                    $this.removeClass('user-loved');
                }
              }
            }
        })
    });
</script>

<script type="text/javascript">
  $(document).ready(function () {
      $('.dtpicker').datepicker({changeYear:true, changeMonth:true, showMonthAfterYear:true,dateFormat: 'dd-mm-yy'});

      $('.ganti_alamat').click(function () {
          $('.alamat_lain').toggle();
          $('.alamat_profil').toggle();           
          $('.alamat_check').click();

//        $.ajax({
//            url: base_url + 'ajax/cek_jne/JAKARTA/' + $('.kota_awal').html(),
//            type: 'get',
//            beforeSend:
//            function(){
//              $('.list_ongkir').html('<div class="loading_ongkir"></div>');
//            },
//            success: function (data) {
//              $('.list_ongkir').html(data);
//            }
//          });

      });

      $('.lanjut_step2').click(function () {
        if(! $('.alamat_check').is(':checked')){
          var nm=$('.name').val();
          var phone=$('.phone').val();
          var email=$('.email').val();
          var address=$('.address').val();
          var propinsi=$('.propinsi').val();
          var kota=$('.kota').val();
          if(nm==''||phone==''||address==''){
            alert('Lengkapi data terlebih dahulu!');
            exit;
          }
        }

        // if($('.ongkir_sementara').is(':checked')){
          $('.checkout_step2').toggle();
          $('.checkout_step1').toggle();
          $('.progtrckr-done').next().removeClass().addClass('progtrckr-done');
        // } 
        // else{alert('Pilih ongkir terlebih dahulu!');}
      });

      $('.kembali_step1').click(function () {
        $('.checkout_step2').toggle();
        $('.checkout_step1').toggle();
        $('.progtrckr-todo').prev().removeClass().addClass('progtrckr-todo');
      });

      // $('.ongkir_sementara').change(function(){
      //   var val=$(this).val();
      //   var weight=$('.berat_total').html();
      //   $('.pilih_service').html(val);
      //   $('.total_ongkir').html(parseInt(val)*parseInt(weight));

      // });

      $('#select-kota').change(function(){
        $.ajax({
            url: base_url + 'ajax/cekjne/JAKARTA/' + $(this).val(),
            type: 'get',
            beforeSend:
            function(){
              $('.list_ongkir').html('<div class="loading_ongkir"></div>');
            },
            success: function (data) {
              $('.list_ongkir').html(data);
            }
          });
      });

      $('.alamat-radio').change(function () {
          $('.alamat-lain').toggle('fast');
      });

      // fillKota(17);
  });

  $('#id_propinsi').change(function(){
    fillKabupaten($(this).val());
    fillKecamatan($('#select_kabupaten').val());
  });

  $('#select_kabupaten').change(function(){
    fillKecamatan($(this).val());
  });

  function fillKota(id) {
    $.getJSON(base_url + "cart/kota?id=" + id,
      "", function (json) {
          $("#select-kota").html("");
          $.each(json, function (index, value) {
              $("#select-kota").append("<option value='" + value.id_kota + "'>" + value.nama_kota + "</option>");
          })
      })
  }
  
  function fillKabupaten(id) {
    $.getJSON(base_url + "cart/kabupaten?id=" + id,
      "", function (json) {
          $("#select_kabupaten").html("");
          $("#select_kabupaten").append("<option value=''>-- pilih kabupaten --</option>");
          $.each(json, function (index, value) {
              $("#select_kabupaten").append("<option value='" + value.id_kabupaten + "'>" + value.nama_kabupaten.toUpperCase() + "</option>");
          })
      })
  }
  
  function fillKecamatan(id) {
    $.getJSON(base_url + "cart/kecamatan?id=" + id,
      "", function (json) {
          $("#select_kecamatan").html("");
          $("#select_kecamatan").append("<option value=''>-- pilih kecamatan --</option>");
          $.each(json, function (index, value) {
              $("#select_kecamatan").append("<option value='" + value.id_kecamatan + "'>" + value.nama_kecamatan.toUpperCase() + "</option>");
          })
      })
  }
  
</script>

<script type="text/javascript">
    var frm = $('#form-login');
    frm.submit(function (ev) {
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            beforeSend: function () {
              NProgress.start();
            },
            success: function (data) {
              if(data.status==0){
                $(".login_error").empty();
                $(".login_error").append("<p>" + data.message + "</p>");
                $(".login_error").empty();
                $(".login_error").append("<p>" + data.message + "</p>");
              } else if(data.status==1){
                document.location = window.location.href;
              }
              NProgress.done();
            }
        });

        ev.preventDefault();
    });
</script>
<script type="text/javascript">
  var page = <?=$this->uri->segment(2)?>
</script>
<script type="text/javascript">
    var frmreg = $('#form-reg');
    frmreg.submit(function (ev) {
        $.ajax({
            type: frmreg.attr('method'),
            url: frmreg.attr('action'),
            data: frmreg.serialize(),
            dataType: 'json',
            beforeSend: function () {
              $('.register-submit').val('Mengirim ...');
              NProgress.start();
            },
            success: function (data) {
              $('.register-submit').val('Daftar');
              if(data.status==0){
                $("#recaptcha_reload").click();
                $(".reg_error").empty();
                $(".reg_error").append("<p>" + data.message + "</p>");

              } else if(data.status==1){
                document.location = data.message;
              }
              NProgress.done();
            }
        });

        ev.preventDefault();
    });
</script>
<script type="text/javascript">
    var frmkom = $('#form-komen');
      frmkom.submit(function (ev) {
      var komen = $('.input-comment').val();
      if(komen){
          $.ajax({
              type: frmkom.attr('method'),
              url: base_url + 'ajax/comment',
              data: frmkom.serialize(),
              dataType: 'json',
              beforeSend: function () {
                $('.loading-comment').show();
              },
              success: function (data) {
                var id=$('.product-tile').attr('data-product-id');
                $('#comment-list').load(base_url + 'ajax/comment_list/'+ id);
                $('.count-comment').html(parseInt($('.count-comment').html()) + 1);
                $('.input-comment').val('');
              }
          });
        }else{alert('Komentar tidak boleh kosong!')}
          ev.preventDefault();
      });
</script>

<script type="text/javascript">

        $(document).on('click', '.btn-follow', function(e) {
            e.preventDefault();
            
            var userId = $('.user-overview').attr('data-user-id');
            var $this = $(this);

            $.ajax({
                url: base_url + "ajax/follow",
                method: "POST",
                data: {'id_user': userId},
                dataType: "json",
                success: function(str) {
                    if (str.status && str.status == 1) {
                        $this.removeClass('btn-warning');
                        $this.removeClass('btn-follow');
                        $this.addClass('btn-unfollow');
                        $this.addClass('btn-primary');
                        $this.html('Unfollow');
                        $('.count-follower').html(parseInt($('.count-follower').html())+1);
                    }
                }
            });
        });

        $(document).on('click', '.btn-unfollow', function(e) {
            e.preventDefault();
            
            var userId = $('.user-overview').attr('data-user-id');
            var $this = $(this);

            $.ajax({
                url: base_url + "ajax/unfollow",
                method: "POST",
                data: {'id_user': userId},
                dataType: "json",
                success: function(str) {
                    if (str.status && str.status == 1) {
                        $this.removeClass('btn-primary');
                        $this.removeClass('btn-unfollow');
                        $this.addClass('btn-follow');
                        $this.addClass('btn-warning');
                        $this.html('Follow');
                        $('.count-follower').html(parseInt($('.count-follower').html())-1);
                    }
                }
            });
        });
</script>
<script type="text/javascript">
  $('.product-comments').click(function(){
    $('html, body').animate({
                        scrollTop: $(".comment-form").offset().top
                    }, 900);
  });
</script>
<script type="text/javascript">
  if ( $(window).width() > 450) {
    // $(".mobile").css( "display", "none" );
    $(window).scroll(function(){
       if($(document).scrollTop() > 60) {
        $(".navbar").addClass("navbar-fixed-top");
        $("#logo-cipika").css( "height", "50px" );
        $(".head-nav").css( "margin-top", "0" );
        $("#main-category").css( "margin-top", "3px" );
        $("#main-category").css( "left", "135px" );
        $(".navbar").css( "height", "auto" );
        $(".mail_icon").show('slide', {direction: 'right'}, 1000);
        // $('.mail_icon').popover('show');
      } else {
        $(".navbar").removeClass("navbar-fixed-top");
        $("#logo-cipika").css( "height", "100px" );
        $(".head-nav").css( "margin-top", "26px" );
        $("#main-category").css( "margin-top", "30px" );
        $("#main-category").css( "left", "235px" );
        $(".navbar").css( "height", "106px" ); 
        $(".mail_icon").hide();
      }
    });
  } else {
    $(".navbar").addClass("navbar-fixed-top");    
  }
</script>

<?php if($this->uri->segment(2)=='food_delivery'):?>
    <script type="text/javascript">
        $(document).ready(function () {
            display_delivery_city();
        });
    
        function get_city()
        {
            var provinsi = $('#provinsi').val();
            $('#city').html("Loading...");
            $.ajax({
                    type    : "POST",
                    url     : "<?php echo base_url('merchant/get_city')?>",
                    data    : "provinsi="+provinsi,
                    success : function(result){
                        $('#city').html(result);
                    }
                });
        }
        
        function add_city()
        {
            var idcity = $('#idcity').val();
            
            $('#message').fadeIn().html('Adding new city, please wait...');
            $('#simpan').attr('disabled','disabled');
            $("#f1").prop( "disabled", true );
            $.ajax({
                    type    : "POST",
                    url     : "<?php echo base_url('merchant/add_city')?>",
                    data    : "idcity="+idcity,
                    success : function(result){
                        $('#message').fadeOut();
                        $('#simpan').removeAttr('disabled');
                        $("#f1").prop( "disabled", false);
                        display_delivery_city()
                    }
                });
        }
        
        function display_delivery_city()
        {
            $('#delivery_city').html("Loading...");
            $.ajax({
                    type    : "POST",
                    url     : "<?php echo base_url('merchant/get_delivery_city')?>",                    
                    success : function(result){
                        $('#delivery_city').html(result);
                    }
                });
        }
        
        function remove_city(idmeta)
        {
            $.ajax({
                    type    : "POST",
                    url     : "<?php echo base_url('merchant/remove_delivery_city')?>",                    
                    data    : "idmeta="+idmeta,
                    success : function(result){
                        $('#delivery_city_'+idmeta).fadeOut();
                    }
                });
        }
    </script>
<?php endif;?>

<?php if($this->uri->segment(1)=='merchant'):?>
    <script>
        function get_city()
        {
            var provinsi = $('#provinsi').val();
            $('#city').html("Loading...");
            $.ajax({
                    type    : "POST",
                    url     : "<?php echo base_url('merchant/get_city')?>",
                    data    : "provinsi="+provinsi,
                    success : function(result){
                        $('#city').html(result);
                    }
                });
        }
    </script>
<?php endif;?>

<script type="text/javascript">
$(window).load(function(){
    $("ol.progtrckr").each(function(){
        $(this).attr("data-progtrckr-steps", 
                     $(this).children("li").length);
    });
})
</script>

<?php if(isset($js_lokasi)):?>
<script type="text/javascript">

    function load_kabupaten(idpropinsi,container_kabupaten,nama_input_kabupaten)
    {
        var propinsi             = $('#'+idpropinsi).val();        
        $('#'+container_kabupaten).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kabupaten')?>",
                data    : "id_propinsi="+propinsi+"&nama_input_kabupaten="+nama_input_kabupaten,
                success : function(result){
                    $('#'+container_kabupaten).html(result);
                }
            });
    }
    
    function load_kecamatan(idkabupaten,container_kecamatan,nama_input_kecamatan)
    {
        var kabupaten = $('#'+idkabupaten).val();        
        $('#'+container_kecamatan).html("Loading...");
        $.ajax({
                type    : "POST",
                url     : "<?php echo base_url('lokasi/dropdown_kecamatan')?>",
                data    : "id_kabupaten="+kabupaten+"&nama_input_kecamatan="+nama_input_kecamatan,
                success : function(result){
                    $('#'+container_kecamatan).html(result);
                }
            });
    }
</script>
<?php endif;?>

    </body>
</html>
