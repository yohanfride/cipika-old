<?php include 'header.php'; ?>  

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>

    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <?php 
      foreach ($article as $row) {
          ?>
      <div class="block-white common-box common-page col-xs-12 col-md-8 col-md-offset-2">
          <h1 class="single-title"><a href="<?= base_url('experttalk/single/'.$row->article_slug) ?>"><?= $row->title?></a></h1>

        <div class="content" style="float:right">by <a href="<?= base_url('store/id/'.$row->id_user) ?>"><?=$row->username?></a> <?= date("d F Y", strtotime($row->date_modified)); ?>
        </div>

      </div>
          <?php
      }
      ?>
      
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>