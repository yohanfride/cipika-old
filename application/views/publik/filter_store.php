
    <div class="navbar-collapse collapse container-fluid"><!-- important box for mobile panel -->
      <form id="form-search" method="GET" action="<?=base_url();?>search/store/<?= $id ?>">

      <section class="filter-header">
        <button type="button" class="opened-navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <h3><span class="font-shoopicon">=</span> <small>Filter</small> <span class="close"aria-hidden="true">&times;</span></h3>
        </button>
      </section>
      <!-- FILTER PANEL -->
      <div class=" block-container filter-panel">
        <header class="row filter_box">
          <div class="col-lg-4 filter-search"  method="GET" action="<?=base_url();?>search" ><!-- search -->
            <!-- <form class="search"> -->
              <label class="label-filter">CARI PRODUK</label>
              <div class="search-box">
                <!-- <form> -->
                  <input value="<?=$by_search;?>" type="search" class="form-control" name="s" placeholder="Cari produk, seperti: keripik">
                  <input type="submit" class="font-shoopicon" value="&gt;">
                <!-- </form> -->
              </div>
            <!-- </form> -->
          </div>
          <div class="col-lg-8 filter-parameter"><!-- filter -->
            <div class="row">
              <div class="col-lg-2 col-sm-4 filter-sort">
                <label class="label-filter">URUTKAN</label>
                <select class="form-control select-sort" name="sort">
                  <option value="1" <?=($by_sort=='1')?'selected':'';?>>Terbaru</option>
                  <option value="2" <?=($by_sort=='2')?'selected':'';?>>Terpopuler</option>
                  <option value="3" <?=($by_sort=='3')?'selected':'';?>>Banyak dilihat</option>
                  <!-- <option value="3">Top picks</option> -->
                </select>
              </div>
              <div class="col-lg-10 col-sm-8 filter-more">
                <table class="form">
                  <thead>
                    <tr>
                      <th class="label-filter">KATEGORI</th>
<!--                      <th class="label-filter">TAG</th>
                      <th class="label-filter">LOKASI</th>-->
                      <th class="label-filter">HARGA (Rp)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="form-control-group">
                      <td>
                        <select class="has-option-to-link form-control select-category" name="cat">
                          <option value="">[Semua]</option>
                          <?php $this->produk_m->get_select_category(0, 0, $by_category); ?>                       
                        </select>
                      </td>
                      <td>
                        <select class="form-control select-price" name="pr">
                          <option value="" <?=($by_price=='')?'selected':'';?>>[Semua]</option>
                          <option value="1" <?=($by_price=='1')?'selected':'';?>>50.000 kurang</option>
                          <option value="2" <?=($by_price=='2')?'selected':'';?>>50.000 - 100.000</option>
                          <option value="3" <?=($by_price=='3')?'selected':'';?>>100.000 - 250.000</option>
                          <option value="4" <?=($by_price=='4')?'selected':'';?>>250.000 - 500.000</option>
                          <option value="5" <?=($by_price=='5')?'selected':'';?>>500.000 lebih</option>
                        </select>
                      </td>
                    </tr>
                  </tbody>
                </table>              
              </div>
            </div>
          </div>
        </header>
      </div>

    </form>
      <!-- /FILTER PANEL -->

    </div>