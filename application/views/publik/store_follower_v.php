<?php include 'header.php'; ?>

  <!-- MAIN AREA -->
  <div class="content-area">
    <!-- PRODUCT SINGLE -->
    <div class="container-fluid block-container">
      <!-- user overview -->
      <header class="row user-overview">
        <div class="col-md-8 col-md-offset-2">
          <div class="block-white single-head">
            <div class="row">
              <div class="col-xs-5">
                <table>
                  <tbody>
                    <tr>
                      <th rowspan="2">
                        <a href="#">
                          <img class="avatar-round" src="<?=base_url();?>asset/img/no-avatar-single.png">
                        </a>
                      </th>
                      <td>
                        <h4><a href="#"><?=$data->username;?></a>
                          <!-- <span class="badge-verified"><i class="glyphicon glyphicon-ok-sign"></i></span> -->
                        </h4>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p><a href="#" class="btn btn-sm btn-primary">Follow</a></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-xs-7">
                <table class="table table-user-stats">
                  <tbody>
                    <tr>
                      <th>Listing</th>
                      <th>Loved</th>
                      <th>Following</th>
                      <th>Followers</th>
                    </tr>
                    <tr class="table-user-stats-data">
                      <td><a href="<?=base_url();?>store/id/<?=$id;?>"><?=$jml_list;?></a></td>
                      <td><a href="#"><span class="count-love-all"><?=$jml_love_all;?></span></a></td>
                      <td><a href="<?=base_url();?>store/following/<?=$id;?>"><span class="count-following"><?=$jml_following;?></span></a></td>
                      <td><a href="<?=base_url();?>store/follower/<?=$id;?>"><span class="count-follower"><?=$jml_follower;?></span></a></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- /user overview -->

      <!-- PEOPLE INDEX -->
    <div class="block-container index">
      <div class="row people-thumb-row">

<?php foreach ($follower as $row) { ?>
        <div id="people-123" class="col-lg-3 col-sm-4 col-xs-6 people-thumb-item"><!-- Item -->
          <div class="people-thumb-inner img-responsive">
            <div class="people-avatar">
              <a href="#">
                <img class="avatar-round" src="<?=base_url();?>asset/img/no-avatar-single.png">
              </a>
            </div>
            <div class="people-detail">
              <h4><a href="<?=base_url();?>store/id/<?=$row->id_user;?>"><?=$row->username;?></a> <span class="badge-verified"><!-- <i class="glyphicon glyphicon-ok-sign"></i> --></span></h4>
              <p><a href="#" class="btn btn-sm btn-warning following-status">Follow</a></p>
            </div>
          </div>
        </div>
<?php } ?>        

      </div>

<!--       <div class="loadmore">
        <img src="<?=base_url();?>asset/img/shoop-content-loader-icon.gif">
      </div> -->
    </div>
    <!-- PEOPLE INDEX -->

    </div>
    
  </div>

<?php include 'footer.php'; ?>