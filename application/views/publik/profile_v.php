<?php include 'header.php'; ?>
<style type="text/css">
    .form-control{
        border: 1px solid #eee;
    }

</style>
<!-- MAIN AREA -->
<div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">

                <?php $this->load->view('publik/sidebar_member_v')?>

                <div class="block-white common-box common-page col-xs-12 col-md-9">
                    <h1 class="single-title">Profilku</h1>
                    <?php
                    if ($success != '')
                    {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?=$success?>
                        </div>
                    <?php } ?>
                    <?php
                    if ($error != '')
                    {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?=$error;?>
                        </div>
                    <?php } ?>

                    <div class="content">
                        <div class="col-lg-12"> 
                            <form class="form-horizontal" method="post" action="<?=base_url();?>user/profile/?<?=$continue;?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label"><a href="#" id="change_pp">Ganti Logo</a></label>
                                    <div class="col-lg-6" id="img_pp">
                                        <img id="previewHolder" src="<?=($data->image)? base_url().'asset/upload/profil/'.$data->image:base_url().'asset/img/no-avatar-single.png'; ?>">
                                    </div>
                                    <input type="file" id="filePhoto" class="hidden" name="user_image">
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Nama Tampilan</label>
                                    <div class="col-lg-4">
                                        <input value="<?=$data->username;?>" name="username" type="text" class="form-control" id="username" placeholder="Cipika Store">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Nama Depan</label>
                                    <div class="col-lg-4">
                                        <input value="<?=$data->firstname;?>" name="firstname" type="text" class="form-control" id="firstname" placeholder="George">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Nama Belakang</label>
                                    <div class="col-lg-4">
                                        <input value="<?=$data->lastname;?>" name="lastname" type="text" class="form-control" id="lastname" placeholder="Walker">
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Bio</label>
                                    <div class="col-lg-4">
                                        <textarea name="bio" class="form-control" id="bio" placeholder="" rows=5><?=$data->bio;?></textarea>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Tanggal Lahir</label>
                                    <div class="col-lg-4">
                                        <input value="<?=strftime("%d-%m-%Y",strtotime(substr($data->birthdate,0,10)));?>" name="birthdate" type="text" class="form-control" id="tgl_lahir" placeholder="dd-mm-yyyy">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Gender</label>
                                    <div class="col-lg-4">
                                        <!-- <select name="gender" class="form-control" id="gender" placeholder="">
                                            <option>Laki-laki</option>
                                            <option>Perempuan</option>
                                        </select> -->
                                        <input type="radio" value="man" name="gender" <?=($data->gender=='man')?'checked':'';?>> Pria
                                        <input type="radio" value="woman" name="gender" <?=($data->gender=='woman')?'checked':'';?>> Wanita
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Alamat</label>
                                    <div class="col-lg-4">
                                        <textarea name="alamat" class="form-control" id="alamat" placeholder=""><?=$data->alamat;?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Telepon</label>
                                    <div class="col-lg-4">
                                        <input onkeypress="return isNumberKey(event)" value="<?=$data->telpon;?>"name="telpon" type="text" class="form-control" id="telpon" placeholder="0312281672">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-lg-2 control-label">Hp</label>
                                    <div class="col-lg-4">
                                        <input onkeypress="return isNumberKey(event)" value="<?=$data->hp;?>"name="hp" type="text" class="form-control" id="hp" placeholder="085711234560">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user" class="col-lg-2 control-label">Provinsi</label>
                                    <div class="col-lg-4">
                                        <select name="id_propinsi" type="text" class="form-control" id="id_propinsi" placeholder="">
                                            <option value="" selected>Pilih Provinsi</option>
                                            <?php if($data->id_propinsi != null){ ?>
                                            <option value="<?=$data->id_propinsi;?>" selected><?= strtoupper($this->auth_m->get_propinsi($data->id_propinsi));?></option>
                                            <?php } ?>
                                            <?php
                                            foreach ($provinsi as $k)
                                            {
                                                ?>
                                            <option value="<?=$k->id_propinsi;?>"><?= strtoupper($k->nama_propinsi);?></option>
                                            
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Kabupaten / Kota</label>
                                    <div class="col-lg-4">
                                        <select  class="form-control" id="select_kabupaten" name="id_kabupaten">
                                        <?php if(isset($data->id_kabupaten)){ ?>
                                            <option value="<?=$data->id_kabupaten;?>" selected><?= strtoupper($this->auth_m->get_kabupaten($data->id_kabupaten));?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Kecamatan</label>
                                    <div class="col-lg-4">
                                        <select  class="form-control" id="select_kecamatan" name="id_kecamatan">
                                            <?php if(isset($data->id_kecamatan)){ ?>
                                            <option value="<?=$data->id_kecamatan;?>" selected><?= strtoupper($this->auth_m->get_kecamatan($data->id_kecamatan));?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div> 
                                
                                <div class="clearfix"></div>
                                <br><br>
                                <div style="text-align:center;">
                                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Simpan</button>
                                </div>                      

                        </div>
                        </form>

                    </div>
                </div><!-- /.row -->
            </div>
        </div>

    </div>
</div>
<?php include 'footer.php'; ?>
<?php include 'upload.php'; ?>
