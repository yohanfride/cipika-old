<div class="visible-xs">
    <div class="navbar-collapse collapse container-fluid"><!-- important box for mobile panel -->
      <form id="form-search" method="GET" action="<?=base_url();?>search/">

      <section class="filter-header">
        <button type="button" class="opened-navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <h3><span class="font-shoopicon">=</span> <small>Filter</small> <span class="close"aria-hidden="true">&times;</span></h3>
        </button>
      </section>
      <!-- FILTER PANEL -->
      <div class=" block-container filter-panel">
        <header class="row filter_box">
          <div class="col-lg-4 filter-search"  method="GET" action="<?=base_url();?>search" ><!-- search -->
            <!-- <form class="search"> -->
              <label class="label-filter">CARI PRODUK</label>
              <div class="search-box">
                <!-- <form> -->
                  <input value="" type="search" class="form-control" name="s" placeholder="Cari produk, seperti: keripik">
                  <input type="submit" class="link font-shoopicon" value="&gt;">
                <!-- </form> -->
              </div>
            <!-- </form> -->
          </div>
          <div class="col-lg-8 filter-parameter"><!-- filter -->
            <div class="row">
              <div class="col-lg-2 col-sm-4 filter-sort">
                <label class="label-filter">URUTKAN</label>
                <select class="form-control select-sort" name="sort">
                  <option value="1">Terbaru</option>
                  <option value="2">Terpopuler</option>
                  <option value="3">Banyak dilihat</option>
                  <!-- <option value="3">Top picks</option> -->
                </select>
              </div>
              <div class="col-lg-10 col-sm-8 filter-more">
                <table class="form">
                  <thead>
                    <tr>
                      <th class="label-filter">KATEGORI</th>
<!--                      <th class="label-filter">TAG</th>
                      <th class="label-filter">LOKASI</th>-->
                      <th class="label-filter">HARGA (Rp)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="form-control-group">
                      <td>
                        <select class="has-option-to-link form-control select-category" name="cat">
                          <option value="">[Semua]</option>
                          <?php $this->produk_m->get_select_category(0, 0, null); ?>                       
                        </select>
                      </td>
                      <td>
                        <select class="form-control select-price" name="pr">
                          <option value="">[Semua]</option>
                          <option value="1">50.000 kurang</option>
                          <option value="2">50.000 - 100.000</option>
                          <option value="3">100.000 - 250.000</option>
                          <option value="4">250.000 - 500.000</option>
                          <option value="5">500.000 lebih</option>
                        </select>
                      </td>
                    </tr>
                  </tbody>
                </table>              
              </div>
            </div>
          </div>
        </header>
      </div>

    </form>
      <!-- /FILTER PANEL -->

    </div>
</div>