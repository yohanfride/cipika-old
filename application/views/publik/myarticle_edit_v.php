<?php include 'header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
}

</style>
  <!-- MAIN AREA -->
  <div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">
        <?php $this->load->view('publik/merchant/sidebar_v')?>
        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Artikel Baru</h1>

            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>

          <div class="content">
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="">
                  <input type="text" name="id_produk" value="<?=$article->id_article;?>" class="hidden">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Product Name</label>
                    <div class="col-lg-4">
                      <input value="<?=$article->title;?>" name="title" type="text" class="form-control" id="nama_produk" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Content</label>
                    <div class="col-lg-4">
                      <textarea name="content" class="form-control" id="deskripsi" placeholder="" rows=5><?=$article->content?></textarea>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->
          </div>
        </div>

        </div>
      </div>
    </div>
    
  </div>

<?php include 'footer.php'; ?>

<script type="text/javascript">
      var cktext='editor1';
    </script> 
    
    <?php include 'tinymce.php'; ?>
