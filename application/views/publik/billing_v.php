<?php include 'header.php'; ?>

<div class="content-area container">

<ol class="progtrckr" data-progtrckr-steps="3">
    <li class="progtrckr-todo">Metode Pengiriman</li>
    <li class="progtrckr-todo">Metode Pembayaran</li>
    <li class="progtrckr-todo">Konfirmasi Order</li>
</ol>

<div class="content">
<form method="POST" action="" class="form-horizontal">
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Metode Pembayaran</label>
        </div>
        <div class="col-md-6">
            <select class="form-control" name="payment">
                <?php foreach ($payment as $pay) { ?>
                <option value="<?=$pay->id_payment;?>"><?=$pay->nama_payment;?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <br />
    <h4>Alamat Pengiriman</h4>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Nama Lengkap</label>
        </div>
        <div class="col-md-6">
            <span><?=ucwords($user->firstname.' '.$user->lastname);?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Alamat Penagihan</label>
        </div>
        <div class="col-md-6">
            <span><?=ucwords($user->alamat);?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Propinsi</label>
        </div>
        <div class="col-md-6">
            <span><?=$user->provinsi;?>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Kota</label>
        </div>
        <div class="col-md-6">
            <span><?=$user->kota;?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Biaya Pengiriman</label>
        </div>
        <div class="col-md-6">
            <span><?=$ongkir;?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">No Handphone</label>
        </div>
        <div class="col-md-6">
            <span><?=$user->telpon;?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Email</label>
        </div>
        <div class="col-md-6">
            <span><?=$user->email;?></span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <label class="control-label">Pilih alamat pengiriman barang?</label>
        </div>
        <div class="col-md-6">
            <input class="alamat-radio" checked type="radio" name="alamat" value="sama"/> <span>Ya, saya pilih alamat sesuai dengan alamat di atas.</span><br />
            <input class="alamat-radio" type="radio" name="alamat" value="alamat-lain"/> <span>Pengiriman ke alamat lain.</span>
        </div>
    </div>

    <div class="row alamat-lain" style="display: none;">
        <div class="container" style="margin-left: 2em; margin-top: 2em;">
            <div class="row">
                <div class="col-md-3">
                    <label class="control-label">Nama Lengkap</label><span style="color:#F00000; font-weight:bold; font-size:15px">*</span>
                </div>
                <div class="col-md-3">
                    <input name="name"class="" value="" maxlength="255" id="alamat-lain-name" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"> 
                    <label class="control-label">No. Handphone</label><span style="color:#F00000; font-weight:bold; font-size:15px">*</span>
                </div>
                <div class="col-md-3">
                    <input name="phone" class="" value="" maxlength="255" id="alamat-lain-phone" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"> 
                    <label class="control-label">Email</label><span style="color:#F00000; font-weight:bold; font-size:15px">*</span>
                </div>
                <div class="col-md-3">
                    <input name="email" class="" value="" maxlength="255" id="alamat-lain-email" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"> 
                    <label class="control-label">Alamat Pengiriman</label><span style="color:#F00000; font-weight:bold; font-size:15px">*</span>
                </div>
                <div class="col-md-3">
                    <textarea name="address" class="user1 textshoppingcart"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"> 
                    <label class="control-label">Propinsi</label><span style="color:#F00000; font-weight:bold; font-size:15px">*</span>
                </div>
                <div class="col-md-3">
                    <select style="width: 100%; margin-top: 0px" onchange="fillKota($(this).val())" name="propinsi">
                        <?php foreach($provinsi as $p){ ?>
                        <option value="<?=$p->id_provinsi;?>"><?=$p->nama_provinsi;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"> 
                    <label class="control-label">Kota</label><span style="color:#F00000; font-weight:bold; font-size:15px">*</span>
                </div>
                <div class="col-md-3">
                    <select id="select-kota" style="width: 100%; margin-top: 0px" name="kota">
                    </select>
                </div>
            </div> 
        </div>
    </div>

    <br />
    <h4>Biaya Pengiriman</h4>
    <hr />
    <table>
        <?php 
            if($berat_total < 1) {
                $berat_total = 1;
            }
            if($ongkir !='gagal') { 
                foreach($ongkir->item as $item) {
                    if($item->service=='Reguler (REG)') { 
                        $jne = $item->value; 
                    }
                }
            } else {
                $jne = 0;
            } 
        ?>
        <tr>
            <td style="width:80%;"><?php if($ongkir =='gagal'){echo 'Maaf untuk biaya kirim ke kota Anda akan kami informasikan setelah Anda kirim order ini.';} else {echo 'Perkiraan biaya kirim JNE Reguler Rp '.$this->cart->format_number($jne).' x '.$berat_total.' kg' ;} ?></td>
            <td style="text-align:right;"><?php if($ongkir !='gagal') {  echo 'Rp '. $this->cart->format_number($jne * $berat_total);  } else {  echo '-'; }?></td>
            <input type="text" value="<?=($jne * $berat_total);?>" name="ongkir" class="hidden">
        </tr>
        <tr>
            <td>Sub Total(order+ongkir)</td>
            <td style="font-weight:bold;">Rp. <?php echo $this->cart->format_number($this->cart->total() + ($jne * $berat_total)); ?></td>
         </tr>
         <tr>
            <td>Administrasi Bank</td>
            <td style="font-weight:bold;">Rp. <?php echo $payments['option_value']; ?></td>
        </tr>
        <tr>
            <td><strong>Total</strong></td>
            <td style="font-weight:bold;">Rp. <?php echo $this->cart->format_number($this->cart->total() + ($jne * $berat_total)+$payments['option_value']); ?></td>
        </tr>
    </table>
    <br>
    
    <br />
    <br />

    <div class="row">
        <div class="col-md-3">
            <label>Instruksi Khusus (optional)</label>
        </div>
        <div class="col-md-6">
            <textarea name="keterangan" class="form-control" rows="3" col="25"></textarea>
        </div>
    </div>

    <br />
    <br />

    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-danger" type="submit" name="kembali" value="kembali">Kembali</button>
            <button class="btn btn-warning link" type="submit" name="submit" value="submit">Lanjutkan</button>
        </div>
    </div>
</form>
</div>
</div>
<?php include 'footer.php'; ?>