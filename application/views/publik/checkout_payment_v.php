<?php include 'header.php'; ?>

<div class="content-area container">
<?php include 'filter_mobile.php'; ?>
    <ol class="progtrckr" data-progtrckr-steps="3">
        <li class="progtrckr-done">Metode Pengiriman</li>
        <li class="progtrckr-done">Metode Pembayaran</li>
        <li class="progtrckr-todo">Konfirmasi Order</li>
    </ol>

    <div class="content">
        <h4>Metode Pembayaran</h4>
        <hr />
        <?php
        if (isset($error))
        {
            if ($error != '')
            {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $error; ?>
                </div>
                <?php
            }
        }
        ?>
        <form method="POST" action="<?= base_url() ?>cart/billing_payment" class="form-horizontal">
            <div class="row">
                <div class="col-md-3">
                    <label class="control-label">Metode Pembayaran</label>
                </div>
                <div class="col-md-6">
                    <select class="form-control" name="id_payment" onchange="changeFunc()" id="id_payment">
                        <option value="0" selected="selected">Pilih metode pembayaran</option>
                        <option value="1">Bank Transfer</option>
                        <option value="5">DompetKu</option>
                    </select>
                </div>
            </div>
            <div id="message1"></div>
            <div id="message2"></div>
            <script>
                function changeFunc() {
                    var selectBox = document.getElementById("id_payment");
                    var selectedValue = selectBox.options[selectBox.selectedIndex].value;

                    if (selectedValue == 1) {
                        document.getElementById('lanjutkan').removeAttribute('disabled');
                        document.getElementById("message1").innerHTML = '<h4>Detail</h4><div class="row"><img class="col-md-3" src="<?= base_url(); ?>asset/img/permata.jpg"><div class="colo-md-6">Bank Permata <br>(kode bank 013)<br><br><br><br><br><br></div></div>';
                        document.getElementById("message2").innerHTML = '';
                    } else {
                        document.getElementById('lanjutkan').removeAttribute("disabled");
                        document.getElementById("message2").innerHTML = '<h4>Detail</h4><div class="row"><div class="colo-md-3" style="float: left"><label style="margin-left: 25px">Nomer Indosat Anda</label><br><input type="text" name="nomer_hp" id="nomer_hp" placeholder="Masukkan Nomer Handphone Anda" style="padding: 4px 5px 4px 4px; margin-left: 25px"><br><label style="margin-left: 25px">Token Anda :</label><br><input type="text" name="token" id="token" placeholder="Masukkan Token Anda" style="padding: 4px 5px 4px 4px; margin-left: 25px"><br></div><div style="border: 2px solid black;float: left;margin-left: 80px; padding: 10px 10px 10px 10px"><p>Ketik TOKEN(spasi)PIN ke 789 atau telepon *789*2*3#</p><p>Token akan dikirimkan via SMS.</p></div></div>';
                        document.getElementById("message1").innerHTML = '';
                    }

                    if (selectedValue == 0) {
                        document.getElementById("message1").innerHTML = '';
                        document.getElementById("message2").innerHTML = '';
                        document.getElementById('lanjutkan').setAttribute("disabled", "disabled");
                    }
                }
            </script>

            <br>
            <a href="<?= base_url(); ?>cart/billing" class="btn btn-danger">Kembali</a>
            <!--<textarea style="display:none" name="cart"><?php echo serialize($cart) ?></textarea>-->
<!--            <input type="hidden" name="ongkir_sementara" id="ongkir_sementara" value="
            <?php
                echo serialize($ongkir_sementara);
            ?>">-->

            <button value="1" type="submit" class="btn btn-warning lanjut_step3 pull-right link" name="submit" id="lanjutkan" disabled="disabled">Lanjutkan</button>
        </form>
    </div>
</div>
<?php include 'footer.php'; ?>