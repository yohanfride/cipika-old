<?php include __DIR__ . '/../header.php'; ?>

<style>
    .error{
        background:#F7CFCF;
        border:1px solid #BA1F1F;
        color:#BA1F1F;
        padding:5px 10px;
        margin:0 0 10px 0;
    }
    
    .error p{
        margin:0;
    }
</style>

<div class="content-area container">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
<ol class="progtrckr" data-progtrckr-steps="3">
    <li class="progtrckr-done">Merchant Profile</li>
    <li class="progtrckr-done">Upload Produk</li>
    <li class="progtrckr-todo">Verifikasi</li>
</ol>

<div class="content">

<form method="POST" action="<?php echo base_url('merchant/get_started')?>" class="form-horizontal" id="f1">

    <div class="checkout_step1">
        <h4>Upload Produk</h4>
       
        <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
        <?php } ?>
       
        <div class="alamat_profil">
            
            <form class="form-horizontal" method="post" action="<?php echo base_url('get_started')?>">
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Nama Produk</label>
                    <div class="col-lg-4">
                      <input name="nama_produk" type="text" class="form-control" id="nama_produk" placeholder="" value="<?php echo set_value('nama_produk')?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Deskripsi</label>
                    <div class="col-lg-4">
                      <textarea name="deskripsi" class="form-control" id="deskripsi" placeholder="" rows=5><?php echo set_value('deskripsi')?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Berat</label>
                    <div class="input-group col-lg-4" style="padding: 0 10px;">
                      <input name="berat" type="text" class="form-control" id="berat" placeholder="0.00" value="<?php echo set_value('berat')?>">
                      <span class="input-group-addon">Kg</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Dimension</label>
                    <div class="col-lg-9">
                        <div class="input-group col-lg-3 pull-left">
                          <input name="panjang" type="text" class="form-control" id="panjang" placeholder="panjang" value="<?php echo set_value('panjang')?>">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input name="lebar" type="text" class="form-control" id="lebar" placeholder="lebar" value="<?php echo set_value('lebar')?>">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input name="tinggi" type="text" class="form-control" id="tinggi" placeholder="tinggi" value="<?php echo set_value('tinggi')?>">
                          <span class="input-group-addon">cm</span>
                        </div>
                      </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Stok</label>
                    <div class="col-lg-4">
                      <input onkeypress="return isNumberKey(event)" name="stok_produk" type="text" class="form-control" id="stok_produk" placeholder="" value="<?php echo set_value('stok_produk')?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Harga</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input onkeypress="return isNumberKey(event)" name="harga_produk" type="text" class="form-control" id="harga_produk" placeholder="" value="<?php echo set_value('harga_produk')?>">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Diskon</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <input onkeypress="return isNumberKey(event)" name="diskon" type="text" class="form-control" id="diskon" placeholder="" value="<?php echo set_value('diskon')?>">
                      <span class="input-group-addon">%</span>
                      </div>
                    </div>
                </div>
                <div class="form-group" id="category-parent">
                    <label for="" class="col-lg-2 control-label">Kategori</label>
                    <div class="col-lg-4">
                      <select name="id_kategori[]" class="form-control parent_category" id="id_kategori" data-sub="0" placeholder="">
                        <option value="">-- Pilih Kategori --</option>
                        <?php foreach($kategori as $kat){ ?>
                        <option value="<?=$kat->id_kategori;?>"><?=ucwords($kat->nama_kategori);?></option>
                        <?php } ?>
                      </select>
                    </div>
                </div>
                <div class="sub-category-1"></div>
                
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Gambar</label>
                    <div class="col-lg-8">
                      <div class="row" id="photo-container">
                        <div id="add-photo1" class="col-lg-2 add-foto">Upload Foto</div>
                        <div id="add-photo2" class="col-lg-2 add-foto">Upload Foto</div>
                        <div id="add-photo3" class="col-lg-2 add-foto">Upload Foto</div>
                        <div id="add-photo4" class="col-lg-2 add-foto">Upload Foto</div>
                      </div>
                      <div class="row">
                         <div class="col-lg-8" style="color: #888">
                           * Maksimal ukuran foto adalah : <?php echo ini_get('upload_max_filesize') ?><br />
                           * Foto harus square (panjang dan lebar harus sama)<br />
                         </div>
                     </div>
                    </div>
                </div>
                
                <div class="clearfix"></div>
                <br><br>
                <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Simpan</button>
                </div>
            
            </form>  
            
        </div>        
        <hr />

        <a href="<?php echo base_url('merchant/verification?notif=new')?>" class="btn btn-warning pull-right">Lanjutkan</a>
        <br />
        <br>
    </div>

</form>
</div>
</div>
<?php include __DIR__ . '/../footer.php'; ?>
<?php include __DIR__ . '/../upload.php'; ?>
