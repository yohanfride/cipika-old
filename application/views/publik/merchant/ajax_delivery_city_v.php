<style>
    #list_city{
        float:left;
    }
    
    .item_city{
        float:left;
        width:200px;
        height:50px;
        margin:0 10px 10px 0;
        border:1px solid #E8E8E8;
        padding:10x;
        overflow:hidden;
    }
</style>

<div id="list_city">
    <?php
        if(!empty($delivery_city)){
            foreach($delivery_city as $row){
                if($row->meta_name=='merchant_food_delivery_city'){
                    $kota   = $this->merchant_m->get_single('tbl_kabupaten', 'id_kabupaten', $row->meta_value);
                    echo    "<div class='item_city' align='center' id='delivery_city_".$row->idmeta."'>".
                                $kota->nama_kabupaten.
                                "<br/><a href='javascript:void(0)' onclick='remove_city(\"".$row->idmeta."\")'>remove</a>".
                            "</div>";
                }
            }
        }
    ?>
</div>