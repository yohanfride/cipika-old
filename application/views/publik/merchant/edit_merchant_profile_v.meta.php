<?php $this->load->view('publik/header.php') ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
  display:inline;
}

</style>
<!-- MAIN AREA -->
<div class="content-area">

    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v.php') ?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Profile</h1>

            <?php if(isset($success)): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Success
                </div>
            <?php endif ?>
            
            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('merchant')?>">
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Nama Merchant</label>
                                <div class="col-lg-4">
                                    <input type="text" name="merchant_name" id="merchant_name" class="form-control" placeholder="" value="<?php echo $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_name')?>">
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Alamat Lengkap</label>
                                <div class="col-lg-4">
                                    <input type="text" name="merchant_address" id="merchant_address" class="form-control" placeholder="" value="<?php echo $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_address')?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Provinsi\Kota</label>
                                <div class="col-lg-4">                                    
                                    <select name="merchant_state" id="merchant_state" onchange="get_city()">
                                        <?php
                                            if(!empty($provinsi)){
                                                $curr_provinsi = $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_state');
                                                foreach($provinsi as $row){
                                                    $selected = ($curr_provinsi==$row->id_provinsi)?'selected':'';
                                                    echo "<option value='".$row->id_provinsi."' ".$selected.">".$row->nama_provinsi."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                    <div id="city">
                                        <?php
                                            if($curr_provinsi!=''){
                                                $kota           = $this->merchant_m->get_city($curr_provinsi);
                                                $curr_provinsi  = $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_state');
                                                if(!empty($kota)){
                                                    echo "<br/><select name='idcity' name='idcity'>";
                                                    foreach($kota as $row){
                                                        $curr_kota = $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_city');
                                                        $selected = ($curr_kota==$row->id_kota)?'selected':'';
                                                        echo "<option value='".$row->id_kota."' ".$selected.">".$row->nama_kota."</option>";
                                                    }
                                                    echo "<select>";
                                                }
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Telp</label>
                                <div class="col-lg-4">
                                    <input type="text" name="merchant_phone" id="merchant_phone" class="form-control" placeholder="" style="width:100px" value="<?php echo $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_phone')?>">
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Nama Bank</label>
                                <div class="col-lg-4">                                                                        
                                    <input type="text" name="merchant_bank_name" id="merchant_bank_name" class="form-control" placeholder="" value="<?php echo $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_bank_name')?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">No Rekening</label>
                                <div class="col-lg-4">                                                                        
                                    <input type="text" name="merchant_bank_number" id="merchant_bank_number" class="form-control" placeholder="" value="<?php echo $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_bank_number')?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Nama Pemegang Rekening</label>
                                <div class="col-lg-4">                                                                        
                                    <input type="text" name="merchant_bank_holder" id="merchant_bank_holder" class="form-control" placeholder="" value="<?php echo $this->meta_m->get_meta_value('tbl_user',$this->session->userdata('member')->id_user,'merchant_bank_holder')?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Sales</label>
                                <div class="col-lg-4">                                               
                                    <select name="merchant_sales">
                                        <?php
                                            if(!empty($data_sales)){
                                                echo "<option value='-'>-- Select Sales --</option>";
                                                foreach($data_sales as $row){
                                                    $selected = ($curr_sales==$row->id_user)?'selected':'';
                                                    echo "<option value='".$row->id_user."' ".$selected.">".$row->firstname." ".$row->lastname."</option>";
                                                }
                                            }
                                        ?>    
                                    </select>
                                </div>
                            </div>
                            
                            
                         
                            <div class="clearfix"></div>
                            <br><br>
                            <div style="text-align:center;">
                                <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                            </div>
                        </form>            
                    </div>
                </div><!-- /.row -->
            </div>
        </div>

        </div>
      </div>
    </div>
</div>

<?php $this->load->view('publik/footer.php') ?>