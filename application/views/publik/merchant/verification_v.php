<?php include __DIR__ . '/../header.php'; ?>
<style>
    .pending{
        border:1px solid #BFBC15;
        background:#F2F091;
        padding:5px 10px;
        font-size:22px;
        color:#BFBC15;
        margin:0 0 25px 0;
        font-weight:bold;
    }
    
    .review{
        border:1px solid #196D93;
        background:#B5DFF2;
        padding:5px 10px;
        font-size:22px;
        color:#196D93;
        margin:0 0 25px 0;
        font-weight:bold;
    }
    
    .approve{
        border:1px solid #559618;
        background:#C1F293;
        padding:5px 10px;
        font-size:22px;
        color:#559618;
        margin:0 0 25px 0;
        font-weight:bold;
    }
</style>

<div class="content-area container">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
<ol class="progtrckr" data-progtrckr-steps="3">
    <li class="progtrckr-todo">Merchant Profile</li>
    <li class="progtrckr-todo">Upload Produk</li>
    <li class="progtrckr-done">Verifikasi</li>
</ol>

<div class="content">

<form method="POST" action="<?php echo base_url('merchant/register')?>" class="form-horizontal" id="f1">

    <div class="checkout_step1">
        <h4>Verifikasi Merchant</h4>
        <hr/><br/>
        <div class="alamat_profil">       
            
            <div class='<?php echo $merchant->store_status?>' align="center">
                <?php 
                if($this->input->get('notif')=="new"){
                    echo "Anda telah terdaftar menjadi merchant, tim Cipika Store akan segera menghubungi anda untuk verifikasi";
                }else{
                    switch($merchant->store_status){
                        case 'pending':
                            echo "Pending";
                            break;
                        case 'review':
                            echo "Dalam proses verifikasi";
                            break;
                        case 'approve':
                            echo "Verified";
                            break;
                        case 'block':
                            echo "Unverified";
                            break;
                    }
                }
                ?>
            </div>
        
            <div class="table-responsive">
                <table class="table table-hover table-striped tablesorter datatable">
                    <tr>
                        <td>Nama Merchant</td>
                        <td><?php echo $merchant->nama_store?></td>
                    </tr>
                    <tr>
                        <td>Nama Pemilik</td>
                        <td><?php echo $merchant->nama_pemilik?></td>
                    </tr>
                    <tr>
                        <td>Tgl Lahir</td>
                        <td><?php echo $merchant->tgl_lahir_pemilik?></td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td><?php echo $merchant->merchant_telpon?></td>
                    </tr>
                    <tr>
                        <td>Hp</td>
                        <td><?php echo $merchant->merchant_hp?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><?php echo $merchant->email?></td>
                    </tr>
                    <tr>
                        <td>Bank</td>
                        <td><?php echo $merchant->bank_nama.'<br/>'.$merchant->bank_norek.'<br/>'.$merchant->bank_pemilik?></td>
                    </tr>
                </table>
            </div>
            
            
        </div>        
        <hr />
        <!--<a href="<?php echo base_url('merchant/get_started')?>" class="btn btn-warning pull-right">Upload Produk</a>-->
        <br />
        <br>
    </div>

</form>
</div>
</div>
<?php include __DIR__ . '/../footer.php'; ?>