<?php include __DIR__ . '/../header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
  display:inline;
}

#city{
    margin:-10px 0 20px 0;
}

</style>
<!-- MAIN AREA -->
<div class="content-area">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v.php') ?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Profile</h1>

            <?php if(isset($_GET['p'])): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Data berhasil disimpan.
                </div>
            <?php endif ?>
            
            <?php
                if (isset($error))
                    {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?=$error;?>
                        </div>
            <?php } ?>
            
            <div class="content">
            <form name="f1" method="post" action="<?php echo base_url('merchant')?>">    
                <div class="alamat_profil">
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-md-6">
                            <span><?php echo $merchant->email?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Nama Merchant</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" type="text" name="nama_store" value="<?php echo set_value('nama_store',$merchant->nama_store)?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Bio</label>
                        </div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="deskripsi" rows=5><?php echo set_value('deskripsi',$merchant->deskripsi)?></textarea>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Lokasi</label>
                        </div>
                        
                        <div class="col-md-6">                                                   
                            <?php $lokasi = $this->lib_lokasi->get_lokasi($merchant->merchant_kota);?>
                            <select class="form-control" name="propinsi" id="propinsi" onchange="load_kabupaten('propinsi','div_kabupaten','kabupaten')">
                                <?php
                                    
                                    if(!empty($propinsi)){
                                        foreach($propinsi as $row){
                                            $selected = ($lokasi->id_propinsi==$row->id_propinsi)?'selected':'';
                                            echo "<option value='".$row->id_propinsi."' ".$selected.">".ucwords($row->nama_propinsi)."</option>";
                                        }
                                    }
                                ?>
                            </select>
                            <br><br>
                            <div id="div_kabupaten" class="form-group">                                         
                                <?php                                            
                                    $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);                                    
                                    if(!empty($kabupaten)){
                                        echo "<select class='form-control' name='kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                                        foreach($kabupaten as $row){
                                            $selected = ($lokasi->id_kabupaten==$row->id_kabupaten)?'selected':'';
                                            echo "<option value='".$row->id_kabupaten."' ".$selected.">".ucwords($row->nama_kabupaten)."</option>";
                                        }
                                        echo "</select>";
                                    }
                                ?>
                            </div>
                            <div id="div_kecamatan" class="form-group">
                                <?php                                            
                                    $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
                                    if(!empty($kecamatan)){
                                        echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                                        foreach($kecamatan as $row){
                                            $selected = ($lokasi->id_kecamatan==$row->id_kecamatan)?'selected':'';
                                            echo "<option value='".$row->id_kecamatan."' ".$selected.">".ucwords($row->nama_kecamatan)."</option>";
                                        }
                                        echo "</select>";
                                    }
                                ?>
                            </div><br/>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Nama Pemilik</label>
                        </div>
                        <div class="col-md-6">
                            <input class='form-control' type="text" name="nama_pemilik" value="<?php echo set_value('nama_pemilik',$merchant->nama_pemilik)?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Tanggal Lahir</label>
                        </div>
                        <div class="col-md-6">
                            <input id="tgl_lahir" class='form-control' type="text" name="tgl_lahir_pemilik" value="<?php echo set_value('tgl_lahir_pemilik',strftime("%d-%m-%Y",strtotime($merchant->tgl_lahir_pemilik)))?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Telp</label>
                        </div>
                        <div class="col-md-6">
                            <input onkeypress="return isNumberKey(event)" class='form-control' type="text" name="telpon" value="<?php echo set_value('telpon',$merchant->merchant_telpon)?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">No HP</label>
                        </div>
                        <div class="col-md-6">
                            <input onkeypress="return isNumberKey(event)" class='form-control' type="text" name="merchant_hp" value="<?php echo set_value('merchant_hp',$merchant->merchant_hp)?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Nama Bank</label>
                        </div>
                        <div class="col-md-6">
                            <input class='form-control' type="text" name="bank_nama" value="<?php echo set_value('bank_nama',$merchant->bank_nama)?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">No Rekening</label>
                        </div>
                        <div class="col-md-6">
                            <input onkeypress="return isNumberKey(event)" class='form-control' type="text" name="bank_norek" value="<?php echo set_value('bank_norek',$merchant->bank_norek)?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Pemegang Rekening</label>
                        </div>
                        <div class="col-md-6">
                            <input class='form-control' type="text" name="bank_pemilik" value="<?php echo set_value('bank_pemilik',$merchant->bank_pemilik)?>">
                        </div>
                    </div>
                     <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">Nama Sales</label>
                        </div>
                        <div class="col-md-6">                            
                            <select class='form-control' name="merchant_sales">
                                <?php
                                    if(!empty($data_sales)){
                                        echo "<option value=''>-- Select Sales --</option>";
                                        foreach($data_sales as $row){
                                            $selected = ($merchant->id_sales==$row->id_user)?'selected':'';
                                            echo "<option ".$selected." value='".$row->id_user."'>".$row->firstname." ".$row->lastname."</option>";
                                        }
                                    }
                                ?>    
                            </select>
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>
                            <br><br>
                            <div style="text-align:center;">
                                <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                            </div>
            </div>
        </div>

        </div>
      </div>
    </div>
</div>
</div>

<?php include __DIR__ . '/../footer.php'; ?>