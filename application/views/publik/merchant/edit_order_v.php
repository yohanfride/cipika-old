<?php include __DIR__ . '/../header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
  display:inline;
}

.readonly{
    background:#E2E2E2;
}

</style>
<!-- MAIN AREA -->
<div class="content-area">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v.php') ?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Edit Orders</h1>

            <?php if(isset($error)): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $error?>
                </div>
            <?php endif ?>

            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        
                        <div class="table-responsive">
                            <table class="table datatable table-hover table-striped tablesorter">                            
                                <tbody>
                                    <tr>
                                        <td width="25%">Tgl Order</td>
                                        <td><?php echo strftime("%d-%m-%Y %H:%M:%S",strtotime($order['order']->date_added))?></td>
                                    </tr>
                                    <tr>
                                        <td>Kode Order</td>
                                        <td><?php echo $order['order']->kode_order?></td>
                                    </tr>                                    
                                    <tr>
                                        <td>Status Pembayaran</td>
                                        <td><?php echo status_payment_label($order['order']->status_payment)?></td>
                                    </tr>                                                                        
                                    <tr>
                                        <td>Status Pengiriman</td>
                                        <td><?php echo status_delivery_label($order['order']->status_delivery)?></td>
                                    </tr>
                                    <tr>
                                        <td>Biaya Pengiriman Maximum</td>
                                        <td>Rp. <?php echo format_uang($order['order']->ongkir_sementara)?></td>
                                    </tr>
                                    <tr>
                                        <td>Paket Pengiriman</td>
                                        <td><?php echo $order['order']->paket_ongkir;?></td>
                                    </tr>
                                    <tr>
                                        <td>Pembeli</td>
                                        <td>
                                            <?php 
                                                
                                            
                                                $lokasi     = $this->lib_lokasi->get_lokasi($order['order']->id_kecamatan);
                                                //$provinsi   = $this->merchant_m->get_single('tbl_propinsi','id_propinsi',$order['order']->id_propinsi);
                                                //$kota       = $this->merchant_m->get_single('tbl_kota','id_kota',$order['order']->id_kota);
                                                
                                                if(!empty($lokasi)){
                                                    $lokasi = $lokasi->nama_kecamatan.','.$lokasi->nama_kabupaten.','.$lokasi->nama_propinsi;
                                                }else{
                                                    $lokasi = '-';
                                                }
                                            
                                                echo $order['order']->nama.'<br/>'.
                                                     $order['order']->alamat.'<br/>'.$lokasi.'<br/>'.
                                                     $order['order']->telpon.'<br/>'.
                                                     $order['order']->email.'<br/>';
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Barang</td>
                                        <td>
                                            <?php 
                                                if(!empty($order['items'])){
                                                    $no = 0;
                                                    echo "<table border='0' width='100%'>".
                                                            "<tr>".
                                                                "<th width='5%'>No</th>".
                                                                "<th width='45%'>Nama</th>".
                                                                "<th width='20%'>Harga</th>".
                                                                "<th width='10%'>Jumlah</th>".
                                                                "<th width='20%'>Total</th>".
                                                            "</tr>";
                                                    foreach($order['items'] as $row){
                                                        $no++;
                                                        echo "<tr>".
                                                                "<td>".$no."</td>".
                                                                "<td>".$row->nama_produk."</td>".
                                                                "<td>Rp. ".format_uang($row->harga)."</td>".
                                                                "<td>".$row->jml_produk."</td>".
                                                                "<td>Rp. ".format_uang($row->total)."</td>".
                                                             "</tr>";   
                                                    }
                                                    echo "</table>";
                                                }                                                
                                            ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/><br/>
                            
                            <?php
                                $readonly = "class='readonly' readonly";
                            ?>
                            <form name="f1" method="post" action="<?php echo base_url('merchant/update_order')?>">
                                <table class="table datatable table-hover table-striped tablesorter">                                    
                                    <!--
                                    <tr>
                                        <td>Status Delivery</td>
                                        <td>
                                            <select name='status_delivery'>
                                                <option value='persiapan pengiriman' <?php echo ($order['order']->status_delivery=='persiapan pengiriman')?'selected':''?>>Persiapan pengiriman</option>
                                                <option value='proses pengiriman' <?php echo ($order['order']->status_delivery=='proses pengiriman')?'selected':''?>>Proses pengiriman</option>
                                                <option value='delivery' <?php echo ($order['order']->status_delivery=='delivery')?'selected':''?>>Delivey</option>
                                            </select>
                                        </td>
                                    </tr>
                                    -->
                                    <tr>
                                        <td>Paket Ongkir</td>
                                        <td>
                                            <?php 
                                                $pk = set_value('paket_ongkir_merchant',$order['order']->paket_ongkir_merchant);                                                
                                            ?>
                                            <select name='paket_ongkir_merchant' <?php echo ($pk!='')?'disabled '.$readonly:'' ?>>                                                
                                                <option value=''>-- Pilih Paket Pengiriman --</option>
                                                <option value='YES' <?php echo ($pk=='YES')?'selected':''?>>YES</option>
                                                <option value='REG' <?php echo ($pk=='REG')?'selected':''?>>REG</option>
                                                <option value='OKE' <?php echo ($pk=='OKE')?'selected':''?>>OKE</option>
                                                <option value='CTYES' <?php echo ($pk=='CTYES')?'selected':''?>>CTYES</option>
                                            </select>
                                            <?php
                                                if($pk!=''){
                                                    echo "<input type='hidden' name='paket_ongkir_merchant' value='".$pk."'>";
                                                }
                                            ?>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ongkir (Rp)</td>
                                        <td>
                                            <input <?php echo ($order['order']->ongkir_merchant>0)?$readonly:'' ?> type="text" name="ongkir_merchant" id="ongkir_merchant" value="<?php echo set_value('ongkir_merchant',$order['order']->ongkir_merchant)?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>AWB Number</td>
                                        <td>
                                            <input <?php echo ($order['order']->noresi!='')?$readonly:'' ?> type="text" name="noresi" id="noresi" value="<?php echo set_value('noresi',$order['order']->noresi)?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tgl Kirim</td>
                                        <td>                                            
                                            <?php                                                
                                                switch($order['order']->delivery_date){
                                                    case '':
                                                        $delivery_date = '';
                                                        break;
                                                    case '0000-00-00':
                                                        $delivery_date = '';
                                                        break;
                                                    default:
                                                        $delivery_date = strftime("%d-%m-%Y",strtotime($order['order']->delivery_date));
                                                        break;
                                                }
                                            ?>
                                            <input <?php echo ($delivery_date!='')?$readonly:'' ?> type="text" name="delivery_date" id="delivery_date" value="<?php echo set_value('delivery_date',$delivery_date)?>" class='dtpicker'> (tgl-bulan-tahun)
                                        </td>
                                    </tr>
                                </table>
                                <button value="1" name="simpan" type="submit" class="btn btn-primary pull-right">Update</button>
                                <input type="hidden" name="id_order" value="<?php echo $order['order']->id_order?>">
                                <input type="hidden" name="status_delivery" value="<?php echo $order['order']->status_delivery?>">
                            </form>
                            
                        </div>
                       
                    </div>
                </div><!-- /.row -->
            </div>
        </div>

        </div>
      </div>
    </div>
</div>

<?php include __DIR__ . '/../footer.php'; ?>