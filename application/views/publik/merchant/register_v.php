<?php include __DIR__ . '/../header.php'; ?>

<style>
    .error{
        background:#F7CFCF;
        border:1px solid #BA1F1F;
        color:#BA1F1F;
        padding:5px 10px;
        margin:0 0 10px 0;
    }
    
    .error p{
        margin:0;
    }
    
    #city{
        margin:-10px 0 20px 0;
    }
</style>

<div class="content-area container">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
<ol class="progtrckr" data-progtrckr-steps="3">
    <li class="progtrckr-done">Merchant Profile</li>
    <li class="progtrckr-todo">Upload Produk</li>
    <li class="progtrckr-todo">Verifikasi</li>
</ol>

<div class="content">

<form method="POST" action="<?php echo base_url('merchant/register')?>" class="form-horizontal" id="f1">

    <div class="checkout_step1">
        <h3>Merchant Profile</h3>
        <?php
            if(isset($error)){
                echo "<div class='error'>".$error."</div>";
            }
        ?>
        <h4>Profil Toko</h4>
        <hr>
        <div class="alamat_profil">
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Email</label>
                </div>
                <div class="col-md-6">
                    <span><?php echo $this->session->userdata('member')->email?></span>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Nama Merchant</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="nama_store" value="<?php echo set_value('nama_store')?>" placeholder="Nama Store">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Biodata</label>
                </div>
                <div class="col-md-6">
                    <textarea name="deskripsi" class="form-control" placeholder="Toko bunga murah daerah Pasar Senen" rows=5><?php echo set_value('deskripsi')?></textarea>
                </div>
            </div>
            
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Alamat</label>
                </div>
                <div class="col-md-6">
                    <textarea name="alamat" class="form-control" placeholder="Jl. Singosari Blok I No 9" rows=5><?=$data->alamat;?></textarea>
                </div>
            </div>
            
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Lokasi</label>
                </div>
                <div class="col-md-6">                                                 
                    <?php $lokasi = $this->lib_lokasi->get_lokasi($data->id_kecamatan); ?>

                            <select class="form-control" name="id_propinsi" id="propinsi" onchange="load_kabupaten('propinsi', 'div_kabupaten', 'id_kabupaten', 'id_kecamatan')">
                                <?php
                                if (!empty($propinsi))
                                {
                                    foreach ($propinsi as $row)
                                    {
                                        $selected = ($lokasi->id_propinsi == $row->id_propinsi) ? 'selected' : '';
                                        echo "<option value='" . $row->id_propinsi . "' " . $selected . ">" . ucwords($row->nama_propinsi) . "</option>";
                                    }
                                }
                                ?>
                            </select>  
                            <div id="div_kabupaten">                                         
                                <?php
                                if (!empty($lokasi))
                                {
                                    $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);
                                    if (!empty($kabupaten))
                                    {
                                        echo "<select class='form-control' name='id_kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                                        foreach ($kabupaten as $row)
                                        {
                                            $selected = ($lokasi->id_kabupaten == $row->id_kabupaten) ? 'selected' : '';
                                            echo "<option value='" . $row->id_kabupaten . "' " . $selected . ">" . ucwords($row->nama_kabupaten) . "</option>";
                                        }
                                        echo "</select>";
                                    }
                                }
                                ?>
                            </div>  
                            <div id="div_kecamatan">
                                <?php
                                if (!empty($lokasi))
                                {
                                    $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
                                    if (!empty($kecamatan))
                                    {
                                        echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                                        foreach ($kecamatan as $row)
                                        {
                                            $selected = ($lokasi->id_kecamatan == $row->id_kecamatan) ? 'selected' : '';
                                            echo "<option value='" . $row->id_kecamatan . "' " . $selected . ">" . ucwords($row->nama_kecamatan) . "</option>";
                                        }
                                        echo "</select>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
            
            <h4>Profil Pemilik</h4>
            <hr>
            
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Nama Pemilik</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="nama_pemilik" value="<?php echo set_value('nama_pemilik')?>" placeholder="Nama Pemilik">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Tanggal Lahir</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="birthdate" id="tgl_lahir" value="<?=strftime("%d-%m-%Y",strtotime(substr($data->birthdate,0,10)));?>" placeholder="29-12-1994">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Telepon</label>
                </div>
                <div class="col-md-6">
                    <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="telpon" value="<?=$data->telpon;?>" placeholder="0312281672">
                </div>
            </div>
            
            
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Hp</label>
                </div>
                <div class="col-md-6">
                    <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="hp" value="<?=$data->hp;?>" placeholder="085726891789">
                </div>
            </div>
            
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Gender</label>
                </div>
                <div class="col-lg-6">
                    <!-- <select name="gender" class="form-control" id="gender" placeholder="">
                        <option>Laki-laki</option>
                        <option>Perempuan</option>
                    </select> -->
                    <input type="radio" value="1" name="gender" <?=($data->gender=='man')?'checked':'';?>> Pria
                    <input type="radio" value="2" name="gender" <?=($data->gender=='woman')?'checked':'';?>> Wanita
                </div>
            </div>
            <h4>Rekening Pembayaran</h4>
            <hr>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Nama Bank</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="bank_nama" value="<?php echo set_value('bank_nama')?>" placeholder="Mandiri">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">No Rekening</label>
                </div>
                <div class="col-md-6">
                    <input onkeypress="return isNumberKey(event)" type="text" class="form-control" name="bank_norek" value="<?php echo set_value('bank_norek')?>" placeholder="1234567890123">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Pemegang Rekening</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="bank_pemilik" value="<?php echo set_value('bank_pemilik')?>" placeholder="Pemegang Rekening">
                </div>
            </div>
            
            <h4>Sales Pendamping</h4>
            <hr>
            
             <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">Nama Sales</label>
                </div>
                <div class="col-md-6">
                    
                    <select class="form-control" name="merchant_sales">
                        <?php
                            if(!empty($data_sales)){
                                echo "<option value=''>-- Select Sales --</option>";
                                foreach($data_sales as $row){
                                    //$selected = ($curr_sales==$form-group->id_user)?'selected':'';
                                    echo "<option value='".$row->id_user."'>".$row->firstname." ".$row->lastname."</option>";
                                }
                            }
                        ?>    
                    </select>
                    
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label"></label>
                </div>
                <div class="col-md-6">
                    <input type="checkbox" name="syarat_ketentuan"> Saya setuju dengan <a target="_blank" href='<?=base_url();?>page/index/10/Perjanjian-Kerjasama-Merchant'>Syarat & Kententuan</a>.
                </div>
            </div>
            
            
        </div>        
        <hr />

        <input type="submit" class="btn btn-warning pull-right" name="button_register" value="Lanjutkan">
        <br />
        <br>
    </div>

</form>
</div>
</div>
<?php include __DIR__ . '/../footer.php'; ?>
