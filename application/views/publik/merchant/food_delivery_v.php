<?php include __DIR__ . '/../header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
  display:inline;
}

</style>
<!-- MAIN AREA -->
<div class="content-area">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v.php') ?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Food Delivery Cities</h1>
          
            <div id="message" class="alert alert-success alert-dismissable" style="display:none"></div>

            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('merchant')?>" name="f1" id="f1">
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Propinsi</label>
                                <div class="col-lg-4">
                                    <select class="form-control" name="provinsi" id="provinsi" onchange="get_city()">
                                        <option value="0">-- Pilih Propinsi --</option>
                                        <?php
                                            if(!empty($propinsi)){
                                                foreach($propinsi as $row){
                                                    echo "<option value='".$row->id_propinsi."'>".$row->nama_propinsi."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                    <div id="city"></div>
                                </div>
                            </div>
                            <br/><br/>
                            
                            <div id="delivery_city"></div>
                         
                            <div class="clearfix"></div>
                            <br><br>
                            <div style="text-align:center;">
                                <button class="btn btn-primary save-product" type="button" name="simpan" id="simpan" value=1 onclick="add_city()">Add</button>
                            </div>
                        </form>            
                    </div>
                </div><!-- /.row -->
            </div>
        </div>

        </div>
      </div>
    </div>
</div>

<?php include __DIR__ . '/../footer.php'; ?>