<?php include __DIR__ . '/../header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
  display:inline;
}

</style>
<!-- MAIN AREA -->
<div class="content-area">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v.php') ?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Kelola Order</h1>

            <?php if(isset($error)): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $error?>
                </div>
            <?php endif; ?>

            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                       
                        <div class="table-responsive">
                        
                          <table class="table datatable table-hover table-striped tablesorter">
                            <thead>
                              <tr>
                                <th>No.</th>
                                <th>Tanggal</th>
                                <th>Kode Order</th>
                                <th>Total</th>
                                <th>Status Pembayaran</th>
                                <th>Status Pengiriman</th>
                                <?php if($this->auth_m->get_user()->id_level!='5') { ?>
                                <th>Kelola Order</th>
                                <?php } ?>
                              </tr>
                            </thead>
                            <tbody>
                              <?php if(!empty($orders)):?>  
                              <?php $i=0;foreach ($orders as $data) { $i++;?>
                              <tr>
                                <td><?=$i;?></td>
                                <td><?=$data->date_added;?></td>
                                <td><?=$data->kode_order;?></td>
                                <td>Rp. <?=format_uang($data->total);?></td>
                                <td>
                                    <?=status_payment_label($data->status_payment)?>
                                </td>
                                <td><?= status_delivery_label($data->status_delivery);?></td> 
                                <?php if($this->auth_m->get_user()->id_level!='5') { ?>
                                <td>
                                  <a href="<?php echo base_url('merchant/edit_order/'.$data->id_order)?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>                                  
                                </td> 
                                <?php } ?>
                              </tr>
                              <?php } ?>
                              <?php else: ?>
                                <tr><td colspan='7'>Tidak ada Order</td></tr>
                              <?php endif; ?>
                            </tbody>
                          </table>
                        </div>
                       
                    </div>
                </div><!-- /.row -->
            </div>
        </div>

        </div>
      </div>
    </div>
</div>

<?php include __DIR__ . '/../footer.php'; ?>