<?php include __DIR__ . '/../header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
  display:inline;
}

</style>
<!-- MAIN AREA -->
<div class="content-area">
<?php include __DIR__ . '/../filter_mobile.php'; ?>
    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v.php') ?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Change Email</h1>

            <?php if($error!=''): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $error?>
                </div>
            <?php endif ?>

            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('merchant/email')?>">
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">Current Email</label>
                                <div class="col-lg-4">
                                    <?php echo $user->email?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">New Email</label>
                                <div class="col-lg-4">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="" value="<?php echo set_value('email')?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-lg-2 control-label">New Email Confirmation</label>
                                <div class="col-lg-4">
                                    <input type="email" name="email_confirm" id="email_confirm" class="form-control" placeholder="">
                                </div>
                            </div>
                            
                         
                            <div class="clearfix"></div>
                            <br><br>
                            <div style="text-align:center;">
                                <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                            </div>
                        </form>            
                    </div>
                </div><!-- /.row -->
            </div>
        </div>

        </div>
      </div>
    </div>
</div>

<?php include __DIR__ . '/../footer.php'; ?>