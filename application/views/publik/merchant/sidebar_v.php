<div class="col-xs-12 col-md-3">
    <div class="block-white common-box" style="padding-top:30px;">
        <h1 class="single-title" style="margin-top:-10px">Seller</h1>
        <ul class="nav nav-pills nav-stacked">
            <li <?php echo (isset($menu_myproduct))?$menu_myproduct:''?>><a href="<?=base_url();?>myproduct">Produk Saya</a></li>              
            <li <?php echo (isset($sidebar_member_myarticle))?$sidebar_member_myarticle:''?>><a href="<?=base_url();?>myarticle">Artikel</a></li>
            <li <?php echo (isset($menu_profile))?$menu_profile:''?>><a href="<?php echo base_url('merchant') ?>">Profile</a></li>                        
            <!--<li <?php echo (isset($menu_email))?$menu_email:''?>><a href="<?php echo base_url('merchant/email') ?>">Change Email</a></li>-->
            <li <?php echo (isset($menu_order))?$menu_order:''?>><a href="<?php echo base_url('merchant/orders') ?>">Kelola Order</a></li>
            <li <?php echo (isset($menu_food))?$menu_food:''?>><a href="<?php echo base_url('merchant/food_delivery') ?>">Wilayah Pengiriman</a></li>
        </ul>
    </div>
</div>