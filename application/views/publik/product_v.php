<?php include 'header.php'; ?>

  <!-- MAIN AREA -->
  <div class="content-area">
    <?php include 'filter_mobile.php'; ?>
            <?php if($produk->publish==1){?>
            <!-- PRODUCT SINGLE -->
            <div class="container-fluid block-container">
              <!-- user overview -->
              <header class="row user-overview" data-user-id="<?=$produk->id_user;?>">
                <div class="col-md-8 col-md-offset-2">
                  <div class="block-white single-head">
                    <div class="row">
                      <div class="col-xs-5">
                        <table>
                          <tbody>
                            <tr>
                              <th rowspan="2">
                                <a class="link" href="<?=base_url();?>store/id/<?=$produk->id_user;?>">
                                    <?php
                                $img=$this->user_m->get_single('tbl_user', 'id_user', $produk->id_user);
                                ?>
                                <?php if($img->image == NULL){ ?>
                                    <img class="avatar-round" src="<?=base_url();?>asset/img/no-avatar-single.png">
                                    <?php }else{ ?>
                                    <!--<img class="avatar-round" src="<?=base_url();?>asset/upload/profil/<?= $img->image ?>" width="80" height="80">-->
                                  <img class="avatar-round" src="<?=base_url();?>asset/pict.php?src=<?=base_url();?>asset/upload/profil/<?=$img->image;?>&w=80&h=80&z=1">
                                  <?php
                                    }
                                  ?>
                                </a>
                              </th>
                              <td>
                                <?php
                                    $seller = $this->commonlib->get_seller($produk->id_user);
                                    if(isset($seller->nama_store)){
                                        $store_name = ucwords($seller->nama_store);
                                    }else{
                                        $store_name = ucwords($produk->username);
                                    }
                                ?>
                                <h4><a class="link" href="<?=base_url();?>store/id/<?=$produk->id_user;?>"><?=$store_name;?></a> 
                                  <!-- <span class="badge-verified"><i class="glyphicon glyphicon-ok-sign"></i></span> -->
                                </h4>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <p><?=$btn_follow;?></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="col-xs-7">
                        <table class="table table-user-stats">
                          <tbody>
                            <tr>
                              <th>Menjual</th>
                              <th>Suka</th>
                              <!-- <th>Following</th> -->
                              <th>Followers</th>
                            </tr>
                            <tr class="table-user-stats-data">
                              <td><a href="#"><?=$jml_list;?></a></td>
                              <td><a href="#"><span class="count-love-all"><?=$jml_love_all;?></span></a></td>
                              <!-- <td><a href="#"><span class="count-following"><?=$jml_following;?></span></a></td> -->
                              <td><a href="#"><span class="count-follower"><?=$jml_follower;?></span></a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </header>
              <!-- /user overview -->

              <!-- Product detail -->
              <div class="row block-white product-overview">
                <div class="col-md-6 single-product-image img-responsive"><!-- product-image -->
                  <!-- Wrapper for slides -->
                  <div id="carousel-single" class="carousel-inner cont-slider slide">
                    <?php $i=0;foreach ($produk_foto as $fp) { $i++; ?>
                    <div class="item <?=($i==1)?'active':'';?>">
                      <img alt="" title="" src="<?=base_url()?>asset/pict.php?src=<?=$fp->image;?>&w=620&h=620&z=1">
                    </div>
                    <?php } ?>
                  </div>
                  <!-- Indicators -->
                  <ol class="carousel-indicators carousel-tabs">
                    <?php $i=0;foreach ($produk_foto as $fp) { $i++; ?>
                    <li class="<?=($i==1)?'active':'';?>" data-slide-to="<?=$i-1;?>" data-target="#carousel-single">
                      <img alt="" title="" src="<?=base_url()?>asset/pict.php?src=<?=$fp->image;?>&w=80&h=80&z=1">
                    </li>
                    <?php } ?>
                  </ol>
                </div>
                <div class="col-md-6 product-tile"  data-product-id="<?=$produk->id_produk;?>">
                  <header class="product-description-head clearfix">
                    <button type="button" class="product-thumb-love product-love <?php if($this->session->userdata('member')){if($this->auth_m->cek('tbl_love', 'id_produk', 'id_user', $produk->id_produk, $this->auth_m->get_user()->id_user)>0) echo 'user-loved';}?>"  <?php if(!$this->session->userdata('member')) {echo 'data-toggle="modal" data-target="#register-login-form"';}?>><i class="shoopicon-heart bigger-shoopicon"></i> <span class="love-count"><?=$jml_love;?></span> Orang menyukai ini</button>
                    <a class="product-comments" href="#"><i class="shoopicon-bubble"></i> <span class="count-comment"><?=$jml_komen;?></span> Komentar</a>
                    <div class="pull-right">

                      <!-- tweet -->
                      <a href="https://twitter.com/share" class="twitter-share-button" data-via="getshoop" data-related="getshoop" data-count="none">Tweet</a>
                      <!-- pin -->
                      <!--<a class="pin-button" href="single.html&media=<?=base_url();?>asset/content/single/1.png&description=Kaos%20kaki%20lucu" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>-->
                      <a href="//id.pinterest.com/pin/create/button/?url=<?php echo base_url()."product/detail/".$produk->id_produk; ?>&media=<?=$produk_foto[0]->image;?>&description=<?= $produk->deskripsi ?>" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
                      <!-- like and share -->
                      <div class="fb-like" data-href="<?php echo base_url()."product/detail/".$produk->id_produk; ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
                      <!--<iframe src="http://www.facebook.com/plugins/like.php?href=<?php echo base_url()."product/detail/".$produk->id_produk; ?>&layout=standard&show_faces=false&width=450&action=like&colorscheme=light" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; width:450px; height:60px;"></iframe>-->
                    </div>
                  </header>
                  <h1 class="single-title"><?=$produk->nama_produk;?></h1>
                  <p class="h3 block-price">Rp <?=$this->cart->format_number($produk->harga_jual);?> 
                    <!-- <span class="label label-danger"><i class="glyphicon glyphicon-shopping-cart"></i> sold out</span> -->
                  </p>
                  <div class="product-descriptions">
                    <h4>Deskripsi Produk</h4>
                    <p><?=$produk->deskripsi;?></p>
                    <p>Lokasi : <strong><?=$produk->nama_kabupaten;?></strong></p>
                  </div>
                  <div class="actions">
                    <p>
                      <?php if($produk->stok_produk>0){ ?>
                        <span class="btn btn-lg btn-warning add-to-cart-detail"><i class="glyphicon glyphicon-shopping-cart"></i> Beli produk ini</span>
                      <?php } else { ?>
                        <span class="label label-danger"><i class="glyphicon glyphicon-shopping-cart"></i> sold out</span>
                      <?php } ?>
                      
                      <!-- <span class="btn btn-lg btn-danger disabled"><i class="glyphicon glyphicon-shopping-cart"></i> sold out</span> -->
                      <!-- <a href="#" class="btn btn-link">Contact seller via chat</a> -->
                    </p>
                  </div>
                </div>

              </div>
              <!-- /Product detail -->

              <!-- Product detail -->
              <div class="row product-comments-related">
                <!-- COMMENTS -->
                <div class="col-md-6">
                  <div class="block-white box-comments">
                    <header class="clearfix">
                      <h3 class="pull-left"><i class="shoopicon-bubble"></i> <span class="count-comment"><?=$jml_komen;?></span> Komentar untuk produk ini.</h3>
                    </header>
                    <div id="commentform" class="comment-form clearfix">
                      <form class="form" id="form-komen" method="post">
                        <input type="text" name="id_produk" value="<?=$produk->id_produk;?>" class="hidden">
                        <input type="text" name="parent_id" value="0" class="hidden">
                        <textarea name="komen" placeholder="Masukkan komentar Anda ..." class="input-comment form-control"></textarea>
                        <?php if(!$this->session->userdata('member')) { ?>
                        <a id="login-to-comment-button" href="#" class="btn btn-warning pull-right" data-toggle="modal" data-target="#register-login-form">Masuk untuk komentar</a>
                        <?php } else { ?>
                        <input type="submit" class="btn btn-primary pull-right" value="Post comment">
                        <?php } ?>
                        <input type="reset" class="toggle-comment-form btn" value="Batal">
                      </form>
                    </div>
                    <ol id="comment-list">
                      <div class="loading-comment"></div>
                      <?php foreach($comment as $komen) { ?>
                      <li>
                        <section id="komen-<?=$komen->id_comment;?>" class="comment-item clearfix">
                          <div class="comment-avatar">
                            <a class="link" href="<?=base_url();?>store/id/<?=$komen->id_user;?>">
                              <?php if (isset($komen->image)){ ?>
                                <img class="img-responsive img-rounded" src="<?=base_url();?>asset/upload/profil/<?=$komen->image;?>">
                              <?php } else { ?>
                                <img class="img-responsive img-rounded" src="<?=base_url();?>asset/img/no-avatar-single.png">
                              <?php } ?>
                            </a>
                          </div>
                          <div class="comment-detail">
                            <h4 class="comment-author"><a href="#"><?=$komen->username;?></a></h4>
                            <p><?=$komen->comment;?></p>
                            <p class="small"><time><?=$komen->date_added;?></time></p>
                          </div>  
                        </section>
                      </li>
                      <?php } ?>
                    </ol>
                  </div>
                </div>
                <!-- /COMMENTS -->

                <!-- RELATED ITEMS -->
                <div class="col-md-6 related-items">
                  <header class="block-white clearfix">
                    <h3>Produk lain oleh dari <?=ucwords($produk->nama_store);?></h3>
                  </header>
                  <div class="row">
                    
                      <?php foreach ($produk_other as $po) { ?>
                      <div id="product-127" class="col-md-6 col-sm-4 col-xs-6 product-thumb-item"><!-- Item -->
                        <div class="product-thumb-inner product-tile" data-product-id="<?=$po->id_produk;?>">
                          <a class="link" href="<?=base_url();?>product/detail/<?=$po->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$po->nama_produk));?>">
                            <figure class="img-responsive">
                              <img src="<?=base_url();?>asset/pict.php?src=<?=$po->image;?>&w=300&h=300&z=1">
                            </figure>
                          </a>
                          <div class="product-thumb-prop">
                            <h4 class="product-thumb-title"><a href="<?=base_url();?>product/detail/<?=$po->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$po->nama_produk));?>"><?=$po->nama_produk;?></a></h4>
                            <p class="product-thumb-price">Rp <?=$this->cart->format_number($po->harga_jual);?>
                              <?php if($po->stok_produk>0){ ?>
                                <a class="add-to-cart label label-warning" href="#"><i class="glyphicon glyphicon-shopping-cart"></i> Beli</a>
                              <?php } else { ?>
                                <span class="label label-danger"><i class="glyphicon glyphicon-shopping-cart"></i> sold out</span>
                              <?php } ?>
                            </p>
                            <div class="product-thumb-author">
                              <a class="link" href="<?=base_url();?>store/id/<?=$po->id_user;?>">
                                <figure>
                                  <span>
                                      <?php
                                    $img=$this->user_m->get_single('tbl_user', 'id_user', $po->id_user);
                                    ?>
                                    <?php if($img->image == NULL){ ?>
                                    <img src="<?=base_url();?>asset/img/no-avatar.jpg">
                                        <?php }else{ ?>
                                      <!--<img src="<?=base_url();?>asset/upload/profil/<?= $img->image ?>" width="16" height="16">-->
                                      <img src="<?=base_url();?>asset/pict.php?src=<?=base_url();?>asset/upload/profil/<?=$img->image;?>&w=16&h=16&z=1">
                                      <?php
                                        }
                                      ?>
                                  </span>
                                  
                                  <figcaption>
                                   <?php
                                        $seller = $this->commonlib->get_seller($po->id_user);
                                        if(isset($seller->nama_store)){
                                            echo ucwords($seller->nama_store);
                                        }else{
                                            echo ucwords($po->username);
                                        }
                                    ?>
                                  </figcaption>
                                </figure>
                              </a>
                              <!-- <span class="badge-verified"><i class="glyphicon glyphicon-ok-sign"></i></span> -->
                            </div>
                          </div>
                          <div class="product-thumb-footer clearfix">
                            <p class="product-view pull-left"><i class="glyphicon glyphicon-eye-open"></i> <?=$po->viewed;?></p>
                            <p class="pull-right">
                              <a class="product-thumb-comments" href="#"><i class="shoopicon-bubble"></i> <?=$po->count_comment;?></a>
                              <button type="button" class="product-thumb-love <?php if($this->session->userdata('member')){if($this->auth_m->cek('tbl_love', 'id_produk', 'id_user', $po->id_produk, $this->auth_m->get_user()->id_user)>0) echo 'user-loved';}?>"  <?php if(!$this->session->userdata('member')) {echo 'data-toggle="modal" data-target="#register-login-form"';}?>><i class="shoopicon-heart bigger-shoopicon"></i> <span class="love-count"><?=$po->loved;?></span></button>
                            </p>
                          </div>
                        </div>
                      </div>
                      <?php } ?>              
                    
                  </div>
                </div>
                <!-- /RELATED ITEMS -->

              </div>

              <!-- PRODUCTS INFO -->
              <footer class="product-other-info clearfix">
                <div class="item-attr">
                  <p class="listed-on">Produk ini berada dalam kategori<br>
                    <?php foreach($kategori as $cat){ ?>
                    <a href="<?=base_url();?>search/?s=&sort=1&cat=<?=$cat->id_kategori;?>&tag=&loc=&pr=" class="link btn btn-sm btn-primary"><?=$cat->nama_kategori;?></a> 
                    <?php } ?>
                  </p>
                  <!--
                  <p>Tag 
                    <?php foreach($tag as $t){ ?>
                    #<a href="<?=base_url();?>search/?s=&sort=1&cat=&tag=<?=$t->id_tag;?>&loc=&pr="><?=$t->nama_tag;?></a>
                    <?php } ?>
                    <!-- <a href="#" class="pull-right">Report this item <i class="shoopicon-flag"></i></a> -->
                  <!-- </p> -->
                </div>
              </footer>
              <!-- PRODUCTS INFO -->

            </div>
            <?php }else{?>
                <div class="container-fluid block-container">
                  <!-- user overview -->
                  <header class="row user-overview" data-user-id="<?=$produk->id_user;?>">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="block-white single-head">
                        <div class="row" style="padding:100px 10px" align='center'>
                            
                                <h4>Maaf produk sedang tidak tersedia, <a style='color:red' href='javascript:history.back(-1)'>kembali ke halaman sebelumnya.</a></h4>
                            
                        </div>
                      </div>
                    </div>
                  </header>
            
                
            <?php }?>

    </div>
    <!-- PRODUCT INDEX -->
  </div>

<?php include 'footer.php'; ?>