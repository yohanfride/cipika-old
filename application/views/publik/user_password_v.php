<?php include 'header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
}

</style>
  <!-- MAIN AREA -->
  <div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <div class="col-xs-12 col-md-3">
          <div class="block-white common-box" style="padding-top:30px;">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="<?=base_url();?>myproduct">My Products</a></li>              
              <li><a href="<?=base_url();?>order">Order</a></li>
              <li><a href="<?=base_url();?>user/profile">Profile</a></li>
              <li class="active"  ><a href="<?=base_url();?>user/account">Account</a></li>
            </ul>
          </div>
        </div>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Change Password</h1>

            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>

          <div class="content">
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="<?=base_url();?>user/password">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Old Password</label>
                    <div class="col-lg-4">
                      <input value="" name="old_password" type="password" class="form-control" id="old_password" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">New Password</label>
                    <div class="col-lg-4">
                      <input name="password" type="password" class="form-control" id="password" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">New Password Confirmation</label>
                    <div class="col-lg-4">
                      <input name="passconf" type="password" class="form-control" id="passconf" placeholder="" >
                    </div>                  
                  </div>
                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->
          </div>
        </div>

        </div>
      </div>
    </div>
    
  </div>

<?php include 'footer.php'; ?>