<?php include 'header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
}

</style>
  <!-- MAIN AREA -->
  <div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v')?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">New Product</h1>

            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>

          <div class="content">
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="">
                  <input type="text" name="id_produk" value="<?=$produk->id_produk;?>" class="hidden">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Nama Produk</label>
                    <div class="col-lg-4">
                      <input value="<?=$produk->nama_produk;?>" name="nama_produk" type="text" class="form-control" id="nama_produk" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Deskripsi</label>
                    <div class="col-lg-4">
                      <textarea name="deskripsi" class="form-control" id="deskripsi" placeholder="" rows=5><?=$produk->deskripsi?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Berat</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <input onkeypress="return isNumberKey(event)" value="<?=$produk->berat;?>" name="berat" type="text" class="form-control" id="berat" placeholder="">
                      <span class="input-group-addon">Kg</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Dimensi</label>
                    <div class="col-lg-9">
                        <div class="input-group col-lg-3 pull-left">
                          <input onkeypress="return isNumberKey(event)" value="<?=$produk->panjang;?>" name="panjang" type="text" class="form-control" id="panjang" placeholder="">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input onkeypress="return isNumberKey(event)" value="<?=$produk->lebar;?>" name="lebar" type="text" class="form-control" id="lebar" placeholder="">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input onkeypress="return isNumberKey(event)" value="<?=$produk->tinggi;?>" name="tinggi" type="text" class="form-control" id="tinggi" placeholder="">
                          <span class="input-group-addon">cm</span>
                        </div>
                      <small style="color:red;"> *dihitung setelah packing</small>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Stok</label>
                    <div class="col-lg-4">
                      <input onkeypress="return isNumberKey(event)" value="<?=$produk->stok_produk;?>" name="stok_produk" type="number" class="form-control" id="stok_produk" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Harga</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input onkeypress="return isNumberKey(event)" value="<?=$produk->harga_produk;?>" name="harga_produk" type="number" class="form-control" id="harga_produk" placeholder="">
                    </div>
                    </div>
                  </div>
                  <div class="form-group diskon">
                    <label for="" class="col-lg-2 control-label">Diskon</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <input onkeypress="return isNumberKey(event)" value="<?=$produk->diskon;?>" name="diskon" type="number" class="form-control" id="diskon" placeholder="">
                      <span class="input-group-addon">%</span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group" id="category-parent" style="display:none;">
                    <label for="" class="col-lg-2 control-label">Category</label>
                    <div class="col-lg-4">
                      <select name="id_kategori[]" class="form-control parent_category" id="id_kategori" data-sub="0" placeholder="">
                        <option value="">-- Choose Category --</option>
                        <?php foreach($kategoris as $kat){ ?>
                        <option value="<?=$kat->id_kategori;?>"><?=ucwords($kat->nama_kategori);?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="sub-category-1"></div>

                  <div class="form-group select_cat">
                    <label for="" class="col-lg-2 control-label">Category</label>
                    <div class="col-lg-4">
                      <input value="<?php 
                      $i=0;foreach ($kategorip as $kat) { $i++;
                        if($i==1) {echo $kat->nama_kategori;} else{
                          echo ' >> '.$kat->nama_kategori;
                        }
                      }
                      ?>" name="" type="text" class="form-control" id="" placeholder="" disabled>
                    </div>
                    <button type="button" class="btn btn-lg edit_cat">Change</button>
                  </div>

<!--                   <div class="form-group show_kota">
                    <label for="" class="col-lg-2 control-label">Location</label>
                    <div class="col-lg-4">
                      <input value="<?=$produk->nama_kota;?>" name="" type="text" class="form-control" id="" placeholder="" disabled>
                    </div>
                    <button type="button" class="btn btn-lg edit_loc">Change</button>
                  </div>

            <div class="form-group location" style="display:none;">
                <label for="" class="col-lg-2 control-label">Location</label>                
                <div class="col-lg-4">
                    <select  class="form-control" name="id_provinsi" onchange="fillKota($(this).val())">
                        <?php foreach($provinsi as $p){ ?>
                        <option value="<?=$p->id_provinsi;?>"><?=$p->nama_provinsi;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div> -->

             

                  <!-- <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Tag</label>
                    <div class="col-lg-4">
                        <input name="nama_tag" type="text" class="form-control" id="nama_tag" placeholder="" value="<?php
                            if(!empty($tags)){
                                $no = 1;
                                foreach ($tags as $row) {
                                    echo $row->nama_tag;
                                    if($count_tag != $no){
                                        echo ', ';
                                    }
                                   $no++;
                                }
                            }
                            ?>">
                      comma separate
                    </div>
                  </div> -->
                  <div class="form-group">

                    <label for="" class="col-lg-2 control-label">Image</label>
                    <div class="col-lg-8">
                      <div class="row ui-sortable" id="photo-container">
                        <?php $i=0;foreach($fotos as $foto) { ?>
                        <div class="img-upload col-lg-2">
                          <img src="<?=$foto->image;?>">
                          <div class="del-image"></div>
                          <input class="photo_img_input" name="photo_img[<?=$i;?>]" value="<?=$foto->image;?>" type="hidden">                          
                        </div>
                        <?php $i++;} ?>
                        <?php for($j=0;$j<$i;$j++){ ?>
                        <div disabled="disabled" style="z-index: 0; display: none;" id="add-photo<?=$j+1;?>" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <?php } ?>
                        <?php for($k=0;$k<4-$i;$k++){ ?>
                        <div disabled="disabled" style="z-index: 0;" id="add-photo<?=$k+$j+1;?>" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <?php } ?>
                      </div>
                      <div class="row">
                         <div class="col-lg-8" style="color: #888">
                           * Maksimal ukuran foto adalah : <?php echo ini_get('upload_max_filesize') ?><br />
                           * Foto harus square (panjang dan lebar harus sama)<br />
                         </div>
                      </div>
                    </div>
                  
                  </div>
                  <!-- <div class="form-group">
                    <label for="" class="col-lg-2 control-label"></label>
                    <div class="col-lg-6">
                      <input type="checkbox" name="shipping_area" value="1" <?=($produk->shipping_area==1)?'checked':'';?>> Terapkan wilayah pengiriman untuk produk ini.
                    </div>
                  </div> -->

                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Save</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->
          </div>
        </div>

        </div>
      </div>
    </div>
    
  </div>

<?php include 'footer.php'; ?>
<?php include 'upload.php'; ?>
