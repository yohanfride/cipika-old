<?php include 'header.php'; ?>

<!-- MAIN AREA -->
<div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">

                <?php $this->load->view('publik/sidebar_member_v')?>

                <div class="block-white common-box common-page col-xs-12 col-md-9">          
                    <h1 class="single-title">Order [<?=$order->kode_order;?>]</h1>
                    <div class="content">
                        <div class="row" style="margin-top: 4em;">
                            <table border='0' width='100%'>
                                <tr valign='top'>
                                    <td>
                                        <strong>Alamat Pengiriman</strong><br /><span></span>  
                                        <?php
                                            // $lokasi = $this->lib_lokasi->get_lokasi($order->id_kecamatan);
                                            // $lokasi = (!empty($lokasi))?ucwords($lokasi->nama_kecamatan).','.ucwords($lokasi->nama_kabupaten).','.ucwords($lokasi->nama_propinsi):'';
                                            
                                            // echo    $order->alamat.'<br/>'.
                                            //         $lokasi.'<br/>'.
                                            //         $order->telpon.'<br/>'.
                                            //         $order->email.'<br/>';
                                                    
                                        ?>
                                        <?=$order->nama?><br>   
                                        <?=$order->alamat?><br>                                
                                        <?=$order->nama_kabupaten?><br>
                                        <?=$order->nama_propinsi?><br>
                                    </td>                                                                    
                                    <td>
                                        <strong>Metode Pembayaran</strong><br />
                                        <?=ucwords($order->payment);?><br />
                                        <strong>Status Pembayaran</strong><br />
                                        <?=status_payment_label($order->status_payment);?><br />
                                        <strong>Status Pengiriman</strong><br />
                                        <?=status_delivery_label($order->status_delivery);?><br />
                                        <strong>Paket Pengiriman</strong><br />
                                        <?=$order->paket_ongkir?><br />
                                    </td>                                
                                    <td>
                                        <?php       
                                            $resi       = (!empty($order->noresi))?$order->noresi:'-';
                                            $tgl_kirim  = (!empty($order->delivery_date))?$order->delivery_date:'-';
                                        
                                            echo    "<strong>AWB Number</strong><br/>".
                                                    $resi."<br/>".
                                                    "<strong>Tgl Kirim</strong><br/>".
                                                    $tgl_kirim."<br/>";
                                            
                                        ?>
                                    </td>
                                </tr>
                            </table><br/><br/>
                            
                        </div> 
                    </div>
                        <div class="row" style="margin-top: 2em;">
                            <table class="table table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Produk</th>
                                        <th>Berat</th>
                                        <th>Volume</th>
                                        <th>Jumlah</th>
                                        <th style="text-align: right">Harga</th>
                                        <th style="text-align: right">Diskon</th>
                                        <th style="text-align: right">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($order_item as $item)
                                    {
                                        $i++;
                                        $volume_prod=$prod[$item->id_produk]->panjang*$prod[$item->id_produk]->tinggi*$prod[$item->id_produk]->lebar;
                                        ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->nama_produk;?></td>
                                            <td><?=$prod[$item->id_produk]->berat;?></td>
                                            <td><?=$volume_prod;?></td>
                                            <td style="text-align: right;"><?=$item->jml_produk;?></td>
                                            <td style="text-align: right;">Rp. <?=$item->harga;?></td>
                                            <td style="text-align: right;"><?=$item->diskon;?></td>
                                            <td style="text-align: right;">Rp. <?=$item->total;?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td style="text-align: right; font-weight: bold" colspan="7">Total</td>
                                        <td style="text-align: right">Rp. <?=$order->total;?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; font-weight: bold" colspan="7">Biaya Pengiriman</td>
                                        <td style="text-align: right">Rp. <?=$order->ongkir_sementara;?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; font-weight: bold" colspan="7">GRAND TOTAL</td>
                                        <td style="text-align: right; font-weight: bold">Rp. <?=$order->total+$order->ongkir_sementara;?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>