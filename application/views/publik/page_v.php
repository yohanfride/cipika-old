<?php include 'header.php'; ?>  

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>

    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <div class="block-white common-box common-page col-xs-12 col-md-8 col-md-offset-2">
        <h1 class="single-title"><?=ucwords($data->title);?></h1>

        <div class="content"><?=$data->content;?>
        </div>
      </div>
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>
