<?php include 'header.php'; ?>
<style type="text/css">
    .form-control{
        border: 1px solid #eee;
    }

</style>
<!-- MAIN AREA -->
<div class="content-area">
    
<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">

                <?php $this->load->view('publik/sidebar_member_v') ?>

                <div class="block-white common-box common-page col-xs-12 col-md-9">
                    <h1 class="single-title">Akun Saya</h1>

                    <?php
                    if ($success != '')
                    {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $success; ?>
                        </div>
                    <?php } ?>

                    <?php
                    if ($error != '')
                    {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $error; ?>
                        </div>
                    <?php } ?>

                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="form-horizontal" name="user_pass"method="post" action="<?= base_url(); ?>user/account">
                                    <div class="form-group">
                                        <label for="" class="col-lg-2 control-label">Email</label>
                                        <div class="col-lg-4">
                                            <input value="<?= $data->email; ?>" name="email" type="email" class="form-control" id="nama_produk" placeholder="">                                            
                                        </div>
                                        <?php if(isset($array_error['email'])){ ?>
                                        <div class="col-lg-2">
                                            <span class="error glyphicon glyphicon-remove"></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-2 control-label">Password lama</label>
                                        <div class="col-lg-4">
                                            <input name="old_password" type="password" class="form-control" id="old_password" placeholder="">
                                        </div>
                                        <?php if(isset($array_error['old_password'])){ ?>
                                        <div class="col-lg-2">
                                            <span class="error glyphicon glyphicon-remove"></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-2 control-label">Password baru</label>
                                        <div class="col-lg-4">
                                            <input name="password" type="password" onblur="cek_pass()" class="form-control" id="password" placeholder="">
                                            <small style="color:red;">*password minimal 6 karakter</small>
                                        </div>
                                        <?php if(isset($array_error['password'])){ ?>
                                        <div class="col-lg-2">
                                            <span class="error glyphicon glyphicon-remove"></span>
                                        </div>
                                        <?php } ?>
                                        <div id="error" style="margin-top: 10px; color: red;font-size: 13px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-2 control-label">Konfirmasi password baru</label>
                                        <div class="col-lg-4">
                                            <input name="passconf" type="password"  onblur="cek_pass_confirm()"class="form-control" id="passconf" placeholder="" >
                                        </div>
                                        <?php if(isset($array_error['passconf'])){ ?>
                                        <div class="col-lg-2">
                                            <span class="error glyphicon glyphicon-remove"></span>
                                        </div>
                                        <?php } ?>
                                      <!-- <a href="<?= base_url() ?>user/password" data-toggle="modal" data-href="#change-password-form">Ganti kata sandi</a> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    <br><br>
                                    <div style="text-align:center;">
                                        <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Simpan</button>
                                    </div>                      

                            </div>
                            </form>            
                        </div>
                    </div><!-- /.row -->
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    function cek_pass()
    {
        var old = document.getElementById("password").value;
        var new_pass = document.getElementById("old_password").value;
        if (old == new_pass && old!="") {
            document.getElementById("error2").innerHTML = "";
            document.getElementById("error").innerHTML = "Maaf, Password Anda Sama";
            document.getElementById('passconf').setAttribute("disabled","disabled");
            document.getElementById("error").focus();
        }else{
            document.getElementById("error").innerHTML = "";
            document.getElementById('passconf').removeAttribute('disabled');
        }
    }
    function cek_pass_confirm() {
        var password = document.getElementById("password").value;
        var password_conf = document.getElementById("passconf").value;
        if (password != password_conf) {
            document.getElementById("error").innerHTML = "";
            document.getElementById("error2").innerHTML = "Maaf, Password Baru Anda Tidak Sama";
            document.getElementById('simpan').setAttribute("disabled","disabled");
            
        }else{
            document.getElementById('simpan').removeAttribute('disabled');
            document.getElementById("error2").innerHTML = "";
        }
    }
</script>

<?php include 'footer.php'; ?>
