<!DOCTYPE html><!--


     __            |                      
    (_ |_  _  _  _ | __  _  __ |  _ _|_
    __)| |(_)(_)|_)o |||(_| |  |<(/_ |_
                |                      


-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Cipika Store</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php
  if(isset($meta)){
  ?>
  <meta property="og:title" content="<?= htmlspecialchars($produk->nama_produk) ?> | Cipika Store">
  <meta property="og:url" content="<?= base_url() ?>product/detail/<?= $produk->id_produk ?>">
    <meta property="og:type" content="product.item">
    <meta property="og:site_name" content="Cipika Store">
    <meta property="og:description" content="<?= htmlspecialchars($produk->deskripsi) ?>">
    <meta property="og:ttl" content="100">
    <meta property="og:image" content="<?=base_url()?>asset/pict.php?src=<?=$produk_foto[0]->image;?>&w=620&h=620&z=1">     
  
    <?php
  }
    ?>
    
  <link rel="shortcut icon" href="<?=base_url();?>asset/img/favicon.ico" type="image/x-icon"/>

  <link rel="stylesheet" href="<?=base_url();?>asset/css/bootstrap.css">    
  <link rel="stylesheet" href="<?=base_url();?>asset/admin/css/jquery-ui.css">
  <link rel="stylesheet" href="<?=base_url();?>asset/css/nivo-slider.css" type="text/css" />
  <link rel="stylesheet" href="<?=base_url();?>asset/css/main.css">
  <link rel="stylesheet" href="<?=base_url();?>asset/css/style.css">
  <link rel="stylesheet" href="<?=base_url();?>asset/css/nprogress.css">
  <script type="text/javascript" src="<?=base_url();?>asset/js/print.js"></script>
  <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
  <script src="<?=base_url();?>asset/js/vendor/jquery-1.11.0.min.js"></script>
  <!--[if lt IE 9]>
      <script src="<?=base_url();?>asset/js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
  <![endif]-->
</head>
<body>
  <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '234905216704515',
          status     : true,
          xfbml      : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
  
  <!--[if lt IE 7]><p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->

  <!-- HEADER -->
  <div class="navbar navbar-white" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">          
        <a class="link navbar-brand" href="<?=base_url();?>"><img id="logo-cipika" height="100px" src="<?=base_url();?>asset/img/cipika-logo-beta.png" alt="Cipika Store"></a>
      </div> 

      <nav class="head-nav pull-right">
        <ul class="nav nav-pills">
          <li class="pull-right"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Menu</span>
              <span class="font-shoopicon">=</span>
            </button>
          </li>
          <?php if($this->session->userdata('member')) { ?> 
          
            <!--<li><a href="<?=base_url();?>myproduct">Produk Anda</a></li>-->     
            <?php $member_header = $this->user_m->get_single('tbl_user','id_user',$this->session->userdata('member')->id_user);?>
            <?php            
            if(MERCHANT){               
                $status_merchant = $this->commonlib->check_merchant_status($this->session->userdata('member')->id_user,$member_header->id_level,'approve');
                if(!$status_merchant): // level 6 = member ?>
                    <li><a class="link visible-lg" href="<?php echo base_url('merchant/register');?>">Daftar Menjadi Merchant</a></li>
                <?php endif;
            }
            ?>
          
          
          <li><a class="link visible-lg" href="<?=base_url();?>order">Order</a></li>
          <li><a class="link view-cart" href="<?=base_url();?>cart"><i class="glyphicon glyphicon-shopping-cart"></i> <strong class="view-cart-count"><?=($this->uri->segment(2)=='confirm')?'0':$this->cart->total_items();?></strong></a></li>
          <li class="no-right-border highlight avatar-head dropdown">
            <a href="#" id="dLabel" role="button" data-toggle="dropdown" data-target="#"><?=($this->auth_m->get_user()->username!=NULL)?ucwords($this->auth_m->get_user()->username):$this->auth_m->get_user()->email;?> <span class="caret"></span>
                <?php if($this->auth_m->get_user()->image == NULL){ ?>
                <img src="<?=base_url();?>asset/img/no-avatar-head.png">
                <?php }else{ ?>
              <!--<img src="<?=base_url();?>asset/upload/profil/<?= $this->auth_m->get_user()->image ?>">-->
              <img src="<?=base_url();?>asset/pict.php?src=<?=base_url();?>asset/upload/profil/<?= $this->auth_m->get_user()->image ?>&w=24&h=24&z=1">
              <?php
                }
              ?>
              </a>
            
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">              
              <li><a class="link" href="<?=base_url();?>user/profile">Akun Member</a></li>
              
              <?php if($this->commonlib->check_merchant_status($this->session->userdata('member')->id_user,$member_header->id_level,'approve')): // level 6 = member ?>
                <li><a class="link" href="<?=base_url();?>merchant">Akun Seller</a></li>
              <?php endif;?>
              <!-- <li><a class="link hidden-lg" href="<?=base_url();?>order">Order</a></li> -->
            <?php            
            if(MERCHANT){               
                $status_merchant = $this->commonlib->check_merchant_status($this->session->userdata('member')->id_user,$member_header->id_level,'approve');
                if(!$status_merchant): // level 6 = member ?>
                    <li><a class="link hidden-lg" href="<?php echo base_url('merchant/register');?>">Daftar Menjadi Merchant</a></li>
                <?php endif;
            }
            ?>

              <li class="divider"></li>
              <li><a class="link" href="<?=base_url();?>auth/logout">Keluar</a></li>
            </ul>
            
            
          </li>
          <?php } else { ?>
          <li><a class="link view-cart" href="<?=base_url();?>cart"><i class="glyphicon glyphicon-shopping-cart"></i> <strong class="view-cart-count"><?=$this->cart->total_items();?></strong></a></li>
          <li class="no-right-border highlight"><a id="login-btn" href="#" data-toggle="modal" data-target="#register-login-form">Masuk</a></li>
          <li class="no-right-border sparator">atau</li>
          <li class="no-right-border highlight"><a id="register-btn" href="#" data-toggle="modal" data-target="#register-login-form">Daftar</a></li>
          <?php } ?>
        </ul>
      </nav>

<!-- START TAMBAHAN NAV -->
      <div id="main-category" class="visible-lg">
        <ul class="nav navbar-nav"> 
          <?php  
            $kategori = $this->produk_m->get_parent_category();
            foreach ($kategori as $key => $value) {
          ?>
            <li class="page-scroll">
                <a class="link" href="<?=base_url()?>search/?s=&sort=1&cat=<?=$value->id_kategori?>&pr="><?=ucfirst($value->nama_kategori)?></a>
            </li>
          <?php } ?>
        </ul>
      </div>
      <style type="text/css">
        #main-category {
          left: 235px;
          font-size: 16px;
          line-height: 50px;
          margin-top: 30px;
          position: absolute;
        }
        #main-category a {
          color:#888;
        }
        #main-category a:hover, #main-category a:active {
          color:#D7121D;
          background: transparent;
        }
        #main-category li {
          margin-right:11px;
        }
      </style>
<!-- END TAMBAHAN NAV -->

    </div>
  </div>   
