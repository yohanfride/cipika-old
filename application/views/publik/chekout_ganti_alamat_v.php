<?php include 'header.php'; ?>

<div class="content-area container">
 <?php include 'filter_mobile.php'; ?>   
    <div class="content">

        <form method="POST" action="<?= base_url() ?>cart/ganti_alamat/<?= $id ?>" class="form-horizontal">

            <div class="checkout_step1">
                <h4>Alamat Pengiriman</h4>
                <hr />
                <?php if (isset($error))
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $error; ?>
                    </div>
                <?php } ?>
                <div class="row alamat_lain">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Lengkap</label>
                        <div class="col-sm-6">
                            <input value="<?= ucwords($ses['nama']); ?>" name="nama" type="text" class="form-control name" id="name" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat Penagihan</label>
                        <div class="col-sm-6">
                            <textarea name="alamat" type="text" class="form-control address" id="address" rows="4"><?= $ses['alamat'] ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-lg-2 control-label">Propinsi</label>
                        <div class="col-md-6">                                                   
                    <?php $lokasi = $this->lib_lokasi->get_lokasi($ses['id_kecamatan']); ?>

                            <select class="form-control" name="id_propinsi" id="propinsi" onchange="load_kabupaten('propinsi', 'div_kabupaten', 'id_kabupaten', 'id_kecamatan')">
                                <?php
                                if (!empty($propinsi))
                                {
                                    foreach ($propinsi as $row)
                                    {
                                        $selected = ($lokasi->id_propinsi == $row->id_propinsi) ? 'selected' : '';
                                        echo "<option value='" . $row->id_propinsi . "' " . $selected . ">" . ucwords($row->nama_propinsi) . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-lg-2 control-label">Kota</label>
                        <div class="col-md-6">  
                            <div id="div_kabupaten">                                         
                                <?php
                                if (!empty($lokasi))
                                {
                                    $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);
                                    if (!empty($kabupaten))
                                    {
                                        echo "<select class='form-control' name='id_kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                                        foreach ($kabupaten as $row)
                                        {
                                            $selected = ($lokasi->id_kabupaten == $row->id_kabupaten) ? 'selected' : '';
                                            echo "<option value='" . $row->id_kabupaten . "' " . $selected . ">" . ucwords($row->nama_kabupaten) . "</option>";
                                        }
                                        echo "</select>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-lg-2 control-label">Kecamatan</label>
                        <div class="col-md-6">  
                            <div id="div_kecamatan">
                                <?php
                                if (!empty($lokasi))
                                {
                                    $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
                                    if (!empty($kecamatan))
                                    {
                                        echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                                        foreach ($kecamatan as $row)
                                        {
                                            $selected = ($lokasi->id_kecamatan == $row->id_kecamatan) ? 'selected' : '';
                                            echo "<option value='" . $row->id_kecamatan . "' " . $selected . ">" . ucwords($row->nama_kecamatan) . "</option>";
                                        }
                                        echo "</select>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Handphone</label>
                        <div class="col-sm-6">
                            <input value="<?= $ses['telpon'] ?>" name="telpon" type="text" class="form-control phone" id="phone" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            <input value="<?= $ses['email'] ?>" name="email" type="text" class="form-control email" id="email" placeholder="" readonly>
                        </div>
                    </div>
                </div>   
                <br>
                <a href="<?= base_url(); ?>cart/billing" class="btn btn-danger">Kembali</a>
                <button value="1" type="submit" class="btn btn-warning pull-right" name="simpan">Lanjutkan</button>
            </div>

        </form>
    </div>
</div>
<?php include 'footer.php'; ?>