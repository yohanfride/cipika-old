<?php include 'header.php'; ?>  

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>

    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <div class="block-white common-box common-page col-xs-12 col-md-8 col-md-offset-2">
        <h1 class="single-title">Password Baru</h1>

        <div class="content">

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>

            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
              <?=$success;?>
            </div>
            <?php }?>
            
            <?php if($form):?>
              <form method="post" action="<?php echo base_url("auth/new_password/$idmeta/$time")?>" class="form-horizontal" role="form">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Password Baru</label>
                  <div class="col-sm-6">
                    <input name="password" type="password" class="form-control" id="password" placeholder="Password">
                  </div>
                </div>
                
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Konfirmasi Password Baru</label>
                      <div class="col-sm-6">
                        <input name="password2" type="password" class="form-control" id="password2" placeholder="Konfirmasi Password">
                      </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button name="submit" value="1" type="submit" class="btn btn-primary btn-lg">Kirim</button>
                  </div>
                </div>
              </form>
            <?php endif;?>

          
        </div>

      </div>
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>