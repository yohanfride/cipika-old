<?php include 'header.php'; ?>  

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>
    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <div class="block-white common-box shopping-cart col-xs-12 col-md-8 col-md-offset-2">

        <div class="content">
          <?php if($this->cart->total_items()!=0){ ?>
          <h3>Anda telah berbelanja <strong class="total-item"><?=$this->cart->total_items();?></strong> produk dari <strong><?=count($cart)?></strong> merchant</h3>
        <?php } else { ?>
          <h4>Anda belum memiliki produk di Keranjang Belanja.</h4>
            <p>Silahkan pilih dan masukan produk yang akan Anda beli terlebih dahulu.</p>
        <?php } ?>
          <?php if($this->input->get('error')=='minimal_order'){ ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Minimal Pembelian Rp. <?= $this->config->item('minimal_order') ?>
          </div>
        <?php } ?>
          <form method="POST" action=>
            <?php
            $grand_total = 0;
            foreach ($cart as $key => $value) { ?>
            <label># Nama Merchant : </label><strong><?=ucwords($this->cart_m->get_merchant($key)->nama_store)?></strong>      
            <div class="table-responsive">
            <table class="table table-striped">
            <thead>
                    <tr>
                        <th width="5%"><input type="checkbox" class="check-all"></th>
                        <th width="5%">Jumlah</th>
                        <th width="15%"></th>
                        <th width="21%" style="text-align: left">Nama Produk</th>
                        <th style="text-align: right" width="12%">Harga</th>
                        <th style="text-align: right" width="8%">Diskon</th>
                        <!--<th style="text-align: right" width="8%">Berat</th>-->
                        <th style="text-align: right" width="14%">Harga Setelah Diskon</th>
                        <th style="text-align: right" width="12%">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $i=0;$berat=0;$subtotal=0;foreach ($value as $row) { ?>
                        <tr data-price="<?=$row['price'];?>" class="row-item">
                          <td><input type="checkbox" class="cart-item-checkbox" name="delete-cart-item[<?=$i;?>]" value="<?=$row['rowid'];?>"></td>
                          <td><select data-id="<?=$row['rowid'];?>" class="cart-item-quantity" name="update-quantity">
                            <?php for($j=1;$j<=$row['stok'];$j++) { ?>
                                <option value="<?=$j;?>" <?=($j==$row['qty'])?'selected':'';?>><?=$j;?></option>
                            <?php } ?>
                              </select></td>
                              <td><img src="<?=base_url();?>asset/pict.php?src=<?=$row['image'];?>&w=50&h=50&z=1"></td>
                          <td style="text-align: left"><?=$row['name'];?></td>
                          <td style="text-align: right" class="price-item"><?=$this->cart->format_number($row['harga']);?></td>
                          <td style="text-align: right" class="price-item"><?=$row['discount'];?></td>
                          <!--<td style="text-align: right" class="price-item"><?=$row['berat']*$row['qty'];?> kg</td>-->
                          <td style="text-align: right" class="price-after-discount"><?=$this->cart->format_number($row['price']);?></td>
                          <td style="text-align: right" class="price-total"><?=$this->cart->format_number($row['subtotal']);?></td>
                      </tr>

                      <?php
                        $berat+=($row['berat']*$row['qty']);
                        $subtotal+=$row['subtotal'];
                        $i++; 
                        }
                        ?>

                      <tr> 
                        <!--
                          <th style="text-align: right" colspan="6">Berat</th>
                          <td style="text-align: right" colspan=""><span class="weight-total"><?=$berat;?></span> Kg</th>
                        -->
                          <th style="text-align: right" colspan="7">Total</th>
                          <td style="text-align: right; font-weight: bold" class="grand-total"><?=$this->cart->format_number($subtotal);?></td>
                      </tr>
                  </tbody>
              </table>
            </div>
            <!--<button class="btn btn-alert" onclick="location.reload();">Update</button>-->
              <!-- <label>Pilih Ongkos Kirim (JNE) >> </label>
              <select>
                <option>YES Rp. 15.000</option>
                <option>REG Rp. 10.000</option>
              </select> -->
              <hr>
              <br>

                        <?php
                    $grand_total += $subtotal;
                    }

                    ?>
              <div class="actions">
                <!-- <p> -->

          <?php if($this->cart->total_items()!=0){ ?>
                  <button class="btn btn-danger hidden-xs" name="btn-delete-cart-item" value="1" type="submit">Hapus yang Terpilih</button>
                  <button class="btn btn-danger visible-xs" name="btn-delete-cart-item" value="1" type="submit">Hapus</button>
                  <?php if(!$this->session->userdata('member')){ ?>
                  <button type="button" class="btn btn-primary hidden-xs" name="btn-goto-billing" data-toggle="modal" data-target="#register-login-form">Lanjutkan Pemesanan</button>
                  <button type="button" class="btn btn-primary visible-xs" name="btn-goto-billing" data-toggle="modal" data-target="#register-login-form">Lanjutkan</button>
                  <?php } 
                   else { ?>
                  <button class="btn btn-success link hidden-xs" name="btn-goto-billing" value="1" type="submit">Lanjutkan Pemesanan</button>
                  <button class="btn btn-success link visible-xs" name="btn-goto-billing" value="1" type="submit">Lanjutkan</button>
                  <?php } ?>
          <?php } else { ?>
                  <a class="btn btn-warning"  href="<?=base_url();?>">Lanjutkan Berbelanja</a>
          <?php } ?>
                <!-- </p> -->
              </div>
          </form>
        </div>

      </div>
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>