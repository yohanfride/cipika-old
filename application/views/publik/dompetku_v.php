<?php include 'header.php'; ?>  

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>

    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <div class="block-white common-box common-page col-xs-12 col-md-8 col-md-offset-2">
        <!--<h1 class="single-title">Dompetku</h1>-->

        <div class="content">
            <h3>Hi Indosaters,</h3>
            <p>Sambut hadirnya Indosat online store & mobile commerce  <strong>Cipikastore</strong> yang memiliki tiga kategori produk yaitu kuliner, pakaian dan kerajinan tangan yang fokus kepada produk lokal dan heritage.</p>
            <p>Banyak produk unggulan Indonesia menanti Anda  dalam serunya program <strong>CASHBACK 50 ribu.</strong> </p>
            <p><strong>Adapun mekanismenya  sebagai  berikut :</strong></p>
            <ol>
                <li>Mengisi dengan lengkap data diri pada saat registrasi di Cipika Store</li>
                <li>Cukup belanja minimal Rp 100.000 menggunakan Dompetku dengan saldo minimal pada account Dompetku lebih besar sama dengan Rp 100.000</li>
                <li>Setelah  bertransaksi Indosaters akan dikirim e-mail untuk mengisi form feedback dan mengirimnya ke <a href="mailto:e-care.store@cipika.co.id">e-care.store@cipika.co.id</a></li>
                <li>Setelah  Indosaters mengirim feedbacknya ke admin cipika store, Indosaters akan menerima cashback sebesar Rp 50.000 langsung ke saldo Dompetku Indosaters. Format  feedback terlampir.</li>
            </ol>
            <p>Rasakan keceriaan belanja online di <a href="http://cipikastore.com/">www.cipikastore.com</a>, sekarang juga!!!</p>
            <p>Promo ini berlangsung dari <strong>16 Juni  s/d 30 Juni 2014.</strong> </p>
            <p>So Indosaters, jangan sampai kehabisan, cashback terbatas !!!</p>
            <p>Saat ini Cipika Store sedang dalam masa Beta Test dan mengharapkan feedback dari Indosaters untuk peningkatan  layanan ini. Segera kunjungi website kami di <a href="http://cipikastore.com/">www.cipikastore.com</a> , dapatkan cashbacknya dan berikan feedback-nya melalui <a href="mailto:e-care.store@cipika.co.id">e-care.store@cipika.co.id</a></p>
            <p>Cipikastore,</p>
            <br>
            <p>Semuanya Menjadi Mudah…</p>
        </div>
      </div>
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>
