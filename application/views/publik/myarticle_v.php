<?php include 'header.php'; ?>
<!-- MAIN AREA -->
<div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">
                <?php $this->load->view('publik/merchant/sidebar_v')?>
                <div class="block-white common-box common-page col-xs-12 col-md-9">
                    <a style="margin-top:20px;" href="<?=base_url();?>myarticle/add" class="btn btn-primary pull-right">Add article</a>
                    <h1 class="single-title">My Article</h1>
                    <?php if($this->input->get('delete')=='success'){ ?>
                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      Success
                    </div>
                    <?php } ?>
                    <div class="content">
                        <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul Artikel</th>
                                    <!--<th>Content</th>-->
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0;
                                foreach ($datas as $p)
                                {
                                    $i++; ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$p->title;?></td>
                                        <!--<td><?=strip_tags(substr($p->content, 0, 50));?>...</td>-->
                                        <td><?=$p->date_added?></td>
                                        <?php
                                            if(1 == $p->publish)
                                                $status = "Publish";
                                            else if(2 == $p->publish)
                                                $status = "Unpublish";
                                            else
                                                $status = "Moderasi";
                                        ?>
                                        <td><?=$status;?></td>
                                        <td>
                                            <a class="btn btn-warning btn-xs" href="<?=base_url();?>myarticle/edit/<?=md5($p->id_article);?>/<?=md5($this->session->userdata('member')->id_user);?>">Edit</a>
                                            <a onclick="return confirm('Are you sure delete this article?')" class="btn btn-danger btn-xs"  href="<?=base_url();?>myarticle/delete/<?=md5($p->id_article);?>/<?=md5($this->session->userdata('member')->id_user);?>">Delete</a>
                        <!--                     <button id="dropdownMenu<?=$i?>" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> Choose <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu<?=$i?>">
                                              <li><a href="<?=base_url();?>myproduct/edit/<?=$p->id_produk;?>">Edit</a></li>
                                              <li><a href="<?=base_url();?>myproduct/delete/<?=$p->id_produk;?>">Edit</a></li> -->
                                            </ul>
                                        </td>
<?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                        <?=$paginator;?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
