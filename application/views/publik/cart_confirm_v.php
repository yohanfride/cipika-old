<?php include 'header.php'; ?>
<div class="content-area container">
<?php include 'filter_mobile.php'; ?>
    <ol class="progtrckr" data-progtrckr-steps="3">
        <li class="progtrckr-done">Metode Pengiriman</li>
        <li class="progtrckr-done">Metode Pembayaran</li>
        <li class="progtrckr-done">Konfirmasi Order</li>
    </ol>
    <div class="content">
        <div id="print">
        <?php
        if ($order->id_payment == 1)
        {
            ?>
            <h3>Yth : <?= $user->firstname ?> <?= $user->lastname ?></h3>
            <p>
            Terimakasih anda telah melakukan sejumlah pembelian melalui CipikaStore. <br>
            Pembelian anda telah sukses dilakukan.
            </p>
            <table style='margin: 2em 0;'>
                <tr>
                    <td style='width: 200px'>Nama Bank</td>
                    <td style='width: px'>:</td>
                    <td><strong>Bank Permata (kode bank 013)</strong></td>
                </tr>
                <tr>
                    <td style='width: 200px'>Nomor Rekening (VAN ID)</td>
                    <td style='width: 10px'>:</td>
                    <td><strong><?= $payment ?></strong></td>
            </tr>
            </table>
            <?php
                $waktu_exp = date('d-m-Y H:i:s', mktime(date('H') + 4, date('i') , date('s'), date('m'), date('d'), date('Y')));
            ?>
            <p>
                Pembayaran dapat Anda lakukan melalui Jaringan <strong>ATM Bersama</strong>, <strong>PRIMA</strong>, dan <strong>ALTO</strong>. Anda juga diperkenankan memanfaatkan kemudahan fasilitas E-Banking yang dilengkapi menu <strong>Transfer Bank Online</strong>. <br>
            </p>
            <p>
                Pastikan Anda menyelesaikan pembayaran sebelum <strong><?= $waktu_exp ?></strong>. Apabila melebihi periode tersebut, maka Virtual Account (VAN ID) secara otomatis ditutup dan transaksi tidak diproses lebih lanjut.
            </p>
            <table style="margin: 2em 0;">
            <tr>
                <td style="width: 200px">Nomor Invoice</td>
                <td style="width: px">:</td>
                <td><strong><?= $order->kode_invoice ?></strong></td>
            </tr>
            <tr>
                <td style="width: 200px">Nama</td>
                <td style="width: px">:</td>
                <td><?= $user->firstname ?> <?= $user->lastname ?></td>
            </tr>
            <tr>
                <td style="width: 200px">Alamat</td>
                <td style="width: 10px">:</td>
                <td><?=$user->alamat;?>, <?=$kabupaten->nama_kabupaten;?>,  <?=  strtoupper($propinsi->nama_propinsi);?></td>
            </tr>
            <tr>
                <td style="width: 200px">No Telp</td>
                <td style="width: 10px">:</td>
                <td><?=$user->telpon;?></td>
            </tr>
            <tr>
                <td style="width: 200px">No Handphone</td>
                <td style="width: 10px">:</td>
                <td><?=$user->hp;?></td>
            </tr>
            <tr>
                <td style="width: 200px">Total Harga</td>
                <td style="width: 4px">:</td>
                <td>Rp <?=$this->cart->format_number($order->total+$order->ongkir+$order->payment_fee);?></td>
            </tr>
            <tr>
                <td style="width: 200px">Tanggal</td>
                <td style="width: 4px">:</td>
                <td><?= $order->date_added ?></td>
            </tr>

            </table>
            <?php
        }
        else
        {
            ?>
            <h3>Terima Kasih Telah Berbelanja di Cipika Store</h3>
            <br />
            <p>
                <strong>Bapak/Ibu Yth.</strong><br />
                Terima kasih Anda telah berbelanja melalui Cipika Store. Apabila saldo yang ada pada Akun DompetKu anda mencukupi, maka secara otomatis pemesanan akan di proses lebih lanjut.
            </p>
            <table style="margin: 2em 0;">
                <tr>
                    <td style="width: 200px">Nomer Akun DompetKU</td>
                    <td style="width: px"> : </td>
                    <td><?=$payment->msisdn;?></td>
                </tr>
                <tr>
                    <td style="width: 200px">Jumlah yang sudah terbayar</td>
                    <td style="width: 4px"> : </td>
                    <td><strong> Rp <?= $this->cart->format_number($order->total + $order->ongkir + $order->payment_fee); ?></strong></td>
                </tr>
            </table>
            <p>
                Ketentuan pembayaran adalah:
            </p>
            <ol>
                <li>Bila pembayaran kurang dari jumlah biaya yang ditentukan, maka transfer Dompetku secara otomatis akan ditolak oleh system cipikastore.</li>
                <li>System Cipikastore akan mengambil saldo dari akun DompetKu Anda sesuai nomial pembayaran yang sudah disetujui.</li>
            </ol>
            <?php
        }
        ?>
        <smal><strong>Data Transaksi</strong></smal>
        <?php foreach ($orderdetail as $value) { ?>
        <table width="100%">
            <tr>
                <td width="40%">Merchant: <?=ucwords($this->cart_m->get_merchant($value->id_merchant)->nama_store)?></td>
                <td width="40%">Nomor Transaksi: <?= $value->kode_order ?></td>
                <td width="20%">Status: Success</td>
            </tr>
        </table>
        <div class="" style="margin-top: 1em;">
             <?php
                $shipping_info = $this->cart_m->get_order_shipping($value->id_order);
             ?>
            <table width="100%">
                <tr>
                    <td width="40%"><strong>Alamat Pengiriman</strong></td>
                    <td width="40%"><strong>Metode Pengiriman</strong></td>
                    <td width="40%"><strong>Metode Pembayaran</strong></td>
                </tr>
                <tr>
                    <td><?= ucwords($shipping_info->nama); ?></td>
                    <td><?= ucwords($value->paket_ongkir); ?></td>
                    <td><?= ucwords($order->payment); ?></td>
                </tr>
                <tr>
                    <td><?= ucwords($shipping_info->alamat); ?></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><?= ucwords($shipping_info->nama_kabupaten); ?> - <?= ucwords($shipping_info->nama_propinsi); ?></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><?= ucwords($shipping_info->telpon); ?></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="" style="margin-top: 2em;">
            <!--<label>#Belanja dari Merchant:</label> <strong><?=ucwords($this->cart_m->get_merchant($value->id_merchant)->username)?></strong>-->
            <table class="table table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center" width="10%">No.</th>
                        <th style="text-align: center">Nama Produk</th>
                        <th style="text-align: center" width="10%">Jumlah</th>
                        <th style="text-align: center" width="15%">Harga</th>
                        <th style="text-align: center" width="15%">Diskon</th>
                        <th style="text-align: center" width="20%">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_harga=0;
                    $itemorder = $this->cart_m->get_order_item(md5($value->id_order));
                    ?>
                    <?php $i=0;$berat=0;$subtotal=0;foreach($itemorder as $item){ $i++; ?>
                    <tr>
                        <td style="text-align: center"><?=$i;?></td>
                        <td><?=$item->nama_produk;?></td>
                        <td style="text-align: right;"><?=$item->jml_produk;?></td>
                        <td style="text-align: right;">Rp <?=$this->cart->format_number($item->harga);?></td>
                        <td style="text-align: right;"><?=$item->diskon;?>%</td>
                        <?php
                        if($item->diskon > 0){
                            $total = ($item->harga*$item->jml_produk)-($item->harga*$item->jml_produk)*($item->diskon/100);
                        }else{
                            $total = $item->harga*$item->jml_produk;
                        }
                        ?>
                        <td style="text-align: right;">Rp <?=$this->cart->format_number($total);?></td>
                    </tr>
                    <?php
                        $subtotal+=$total;
                    } 
                    $total_harga=$subtotal+$value->ongkir_sementara;
                    ?>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="5">Subtotal</td>
                        <td style="text-align: right">Rp <?=$this->cart->format_number($subtotal);?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="5">Ongkos Kirim</td>
                        <td style="text-align: right">Rp <?=$this->cart->format_number($value->ongkir_sementara);?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="5">Total</td>
                        <td style="text-align: right">Rp <?=$this->cart->format_number($total_harga);?></td>
                    </tr>
                </tbody>
            </table>
        </div>
            <?php } ?>
        <table class="table table table-bordered" width="100%">
            <?php
            if ($order->payment_fee > 0)
            {
                ?>
                <tr>
                    <td width="20%" style="text-align: right;"><strong>Convenience Fee</strong></td>
                    <td style="text-align: right">Rp <?= $this->cart->format_number($order->payment_fee); ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td width="80%" style="text-align: right; font-weight: bold">Grand Total </td>
                <td width="20%" style="text-align: right;">Rp <?= $this->cart->format_number($order->total+ $order->payment_fee+$order->ongkir); ?></td>
            </tr>
        </table>
<!--         <a href="<?= base_url(); ?>order/print_invoice/<?= $id; ?>"  onclick="window.open('<?= base_url(); ?>order/print_invoice/<?= $id; ?>', 'newwindow', 'width=900, height=500'); return false;">Download Invoice</a>         -->
        </div>
        <a href='#' onclick='javascript:void(printSpecial())'>Print Invoice</a>
    </div>
</div>
<?php include 'footer.php'; ?>