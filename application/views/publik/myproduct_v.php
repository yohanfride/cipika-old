<?php include 'header.php'; ?>
<!-- MAIN AREA -->
<div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">

               <?php $this->load->view('publik/merchant/sidebar_v')?>

                <div class="block-white common-box common-page col-xs-12 col-md-9">
                    <a style="margin-top:20px;" href="<?=base_url();?>myproduct/upload" class="btn btn-primary pull-right">Unggah Produk Baru</a>
                    <h1 class="single-title">Produk Saya</h1>
                    <div class="content">

                        <form id="mpsearch" method="get" action="<?=base_url()?>myproduct" class="form-inline pull-right" role="form">

                        <select class="form-control" name="cat">
                          <option value="">[Semua]</option>
                          <?php $this->produk_m->get_select_category(0, 0 , $kategori); ?>  

                        </select>
                        <?php //var_dump($kategori); ?>
                          <div class="form-group">
                            <input name="search" type="text" class="form-control" id="cari" placeholder="Nama produk" value="<?=(isset($search))?$search:'';?>">
                          </div>
                          <button type="submit" class="btn btn-primary btn-lg">Cari</button>
                        </form>
                        <br>
                        <br>
                        <br>
                        <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th></th>
                                    <th>Nama</th>
                                    <th>Kategori</th>
                                    <th>Stok</th>
                                    <th>Status</th>
                                    <th>Dilihat</th>
                                    <th>Suka</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody class="myproduct_list">
                                <?php
                                $i = $offset;
                                foreach ($produk as $p)
                                {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td>
                                            <img src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=150&h=150&z=1">
                                        </td>
                                        <td><?=$p->nama_produk;?></td>
                                        <td><?=$this->produk_m->get_kategori($p->id_produk);?></td>
                                        <td><?=$p->stok_produk;?></td>
                                        <td><?php 
                                            if($p->publish==0) echo 'Moderasi';
                                            else if($p->publish==1) echo 'Verified';
                                            else if($p->publish==2) echo 'Un Verified';
                                        ?></td>
                                        <td><?=$p->viewed;?></td>
                                        <td><?=$p->loved?></td>
                                        <td>
                                            <a class="btn btn-warning btn-xs" href="<?=base_url();?>myproduct/edit/<?=md5($p->id_produk);?>/<?=md5($this->session->userdata('member')->id_user);?>">Edit</a>
                                            <a onclick="return confirm('Are you sure delete this product?')" class="btn btn-danger btn-xs"  href="<?=base_url();?>myproduct/delete/<?=md5($p->id_produk);?>/<?=md5($this->session->userdata('member')->id_user);?>">Delete</a>
                        <!--                     <button id="dropdownMenu<?=$i?>" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> Choose <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu<?=$i?>">
                                              <li><a href="<?=base_url();?>myproduct/edit/<?=$p->id_produk;?>">Edit</a></li>
                                              <li><a href="<?=base_url();?>myproduct/delete/<?=$p->id_produk;?>">Edit</a></li> -->
                                            </ul>
                                        </td>
<?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                        <?=(isset($paginator))?$paginator:'';?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>