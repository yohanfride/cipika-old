<?php include 'header.php'; ?>

  <!-- MAIN AREA -->
  <div class="content-area">
    <!-- PRODUCT SINGLE -->
    <div class="container-fluid block-container">
      <!-- user overview -->
      <header class="row user-overview" data-user-id="<?=$id;?>">
        <div class="col-md-8 col-md-offset-2">
          <div class="block-white single-head">
            <div class="row">
              <div class="col-xs-5">
                <table>
                  <tbody>
                    <tr>
                      <th rowspan="2">
                        <a href="#">
                          <?php if($data->image == NULL){ ?>
                            <img class="avatar-round" src="<?=base_url();?>asset/img/no-avatar-single.png">
                            <?php }else{ ?>
                          <!--<img class="avatar-round" src="<?=base_url();?>asset/upload/profil/<?= $data->image ?>" width="80" height="80">-->
                          <img class="avatar-round" src="<?=base_url();?>asset/pict.php?src=<?=base_url();?>asset/upload/profil/<?=$data->image;?>&w=80&h=80&z=1">
                          
                          <?php
                            }
                          ?>
                        </a>
                      </th>
                      <td>
                        <?php
                            $seller = $this->commonlib->get_seller($data->id_user);
                                if(isset($seller->nama_store)){
                                    $store_name = ucwords($seller->nama_store);
                                }else{
                                    $store_name = ucwords($data->username);
                                }
                        ?>
                        <h4><a href="#"><?php echo ucwords($store_name);?></a>
                          <!-- <span class="badge-verified"><i class="glyphicon glyphicon-ok-sign"></i></span> -->
                        </h4>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p><?=$btn_follow;?></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-xs-7">
                <table class="table table-user-stats">
                  <tbody>
                    <tr>
                      <th>Menjual</th>
                      <th>Suka</th>
                      <!-- <th>Following</th> -->
                      <th>Followers</th>
                    </tr>
                    <tr class="table-user-stats-data">
                      <td><a href="#"><?=$jml_list;?></a></td>
                      <td><a href="#"><span class="count-love-all"><?=$jml_love_all;?></span></a></td>
                      <!-- <td><a href="#"><span class="count-following"><?=$jml_following;?></span></a></td> -->
                      <td><a href="#"><span class="count-follower"><?=$jml_follower;?></span></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- /user overview -->
      
<?php include 'filter_store.php'; ?>
      
    <!-- PRODUCT INDEX -->
    <div class="container-fluid block-container index">
      <div class="row product-thumb-row">
        <?php foreach ($produk as $p) { ?>
        <div id="product-<?=$p->id_produk;?>" class="col-lg-3 col-sm-4 col-xs-6 product-thumb-item"><!-- Item -->
          <div class="product-thumb-inner product-tile" data-product-id="<?=$p->id_produk;?>">
            <a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>">
              <figure class="img-responsive">
                <img src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=300&h=300&z=1">
              </figure>
            </a>
            <div class="product-thumb-prop">
              <h4 class="product-thumb-title"><a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><?=ucwords($p->nama_produk);?></a></h4>
              <p class="product-thumb-price">Rp <?=$this->cart->format_number($p->harga_jual);?> 
              <?php if($p->stok_produk>0){ ?>
                <a class="add-to-cart label label-warning" href="#"><i class="glyphicon glyphicon-shopping-cart"></i> Beli</a>
              <?php } else { ?>
                <span class="label label-danger"><i class="glyphicon glyphicon-shopping-cart"></i> sold out</span>
              <?php } ?>
              </p>
              <div class="product-thumb-author">
                <a class="link" href="<?=base_url();?>store/id/<?=$p->id_user;?>">
                  <figure>
                    <span>
                         <?php if($data->image == NULL){ ?>
                            <img src="<?=base_url();?>asset/img/no-avatar.jpg" >
                            <?php }else{ ?>
                          <!--<img src="<?=base_url();?>asset/upload/profil/<?= $data->image ?>" width="16" height="16">-->
                          <img src="<?=base_url();?>asset/pict.php?src=<?=base_url();?>asset/upload/profil/<?=$data->image;?>&w=16&h=16&z=1">
                          <?php
                            }
                          ?>                    
                    </span>
                    <figcaption><?=ucwords($store_name);?></figcaption>
                  </figure>
                </a>
                <!-- <span class="badge-verified"><i class="glyphicon glyphicon-ok-sign"></i></span> -->
              </div>
            </div>
            <div class="product-thumb-footer clearfix">
              <p class="product-view pull-left"><i class="glyphicon glyphicon-eye-open"></i> <?=$p->viewed;?></p>
              <p class="pull-right">
                <a class="product-thumb-comments" href="#"><i class="shoopicon-bubble"></i> <?=$p->count_comment;?></a>
                <button type="button" class="product-thumb-love <?php if($this->session->userdata('member')){if($this->auth_m->cek('tbl_love', 'id_produk', 'id_user', $p->id_produk, $this->auth_m->get_user()->id_user)>0) echo 'user-loved';}?>" <?php if(!$this->session->userdata('member')) {echo 'data-toggle="modal" data-target="#register-login-form"';}?>><i class="shoopicon-heart bigger-shoopicon"></i> <span class="love-count"><?=$p->loved;?></span></button>
              </p>
            </div>
          </div>
        </div>
        <?php } ?>        
      </div>

    </div>
    <!-- PRODUCT INDEX -->

    </div>
    
  </div>

<?php include 'footer.php'; ?>