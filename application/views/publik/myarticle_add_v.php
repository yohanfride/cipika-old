<?php include 'header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
}

</style>

<script type="text/javascript" src="<?=base_url('asset')?>/redactor/lib/jquery-1.9.0.min.js"></script>

<!-- Redactor is here -->
<link rel="stylesheet" href="<?= base_url('asset')?>/redactor/redactor/redactor.css" />
<script src="<?= base_url('asset')?>/redactor/redactor/redactor.min.js"></script>
<!--<script src="<?= base_url('asset')?>/redactor/redactor/redactor.js"></script>-->

<script type="text/javascript">
$(document).ready(
        function()
        {
                $('#redactor_content').redactor({
                        imageUpload: '<?= base_url() ?>myarticle/redactor_upload',
                        fileUpload: '<?= base_url() ?>myarticle/redactor_file',
                        imageGetJson: '<?= base_url() ?>myarticle/json'
                });
        }
);
</script>

  <!-- MAIN AREA -->
  <div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">
        <?php $this->load->view('publik/merchant/sidebar_v')?>
        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Produk Baru</h1>

            <?php if($success=='success'){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Success
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>

          <div class="content">
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="<?=base_url();?>myarticle/add">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-4">
                        <input name="title" type="text" class="form-control" id="nama_produk" placeholder="" value="<?php echo (!empty($title) && $success!='success') ? $title : ''  ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Content</label>
                    <div class="col-lg-4">
                        <!--<textarea name="content" class="form-control" id="redactor_content" placeholder="" rows=5><?php echo (!empty($content) && $success!='success') ? $content : ''  ?></textarea>-->
                        <textarea name="content" class="form-control" id="deskripsi" placeholder="" rows=5><?php echo (!empty($content) && $success!='success') ? $content : ''  ?></textarea>
                        <!--<textarea placeholder="Content" rows="10" class="input" id="redactor_content" name="post_content" id="post_content"><?php echo set_value('post_content')?></textarea>-->
                    </div>
                  </div>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label"></label>
                    <div class="col-lg-4">
                      <?= $this->lib_recaptcha->recaptcha(); ?>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Simpan</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->
          </div>
        </div>

        </div>
      </div>
    </div>
    
  </div>

<?php include 'footer.php'; ?>
<script type="text/javascript">
      var cktext='editor1';
    </script> 
    
    <?php include 'tinymce.php'; ?>
