<?php include 'header.php'; ?>
<!-- MAIN AREA -->
<div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">

                <?php $this->load->view('publik/sidebar_member_v')?>

                <div class="block-white common-box common-page col-xs-12 col-md-9">          
                    <h1 class="single-title">My Order</h1>
                    <div class="content">
                        <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Kode Order</th>
                                    <th>Kode Invoice</th>
                                    <th>Total</th>
                                    <th>Pembayaran</th>
                                    <th>Pengiriman</th>
                                    <th>Invoice</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($order as $o)
                                {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$o->date_added;?></td>
                                        <td><a href="<?=base_url();?>order/view/<?=md5($o->id_order);?>"><?=$o->kode_order;?></a></td>
                                        <td><?=$invoices[$o->kode_order] ;?></td>
                                        <td><?=$o->total;?></td>
                                        <td><?=status_payment_label($o->status_payment)?></td>
                                        <td><?=status_delivery_label($o->status_delivery);?></td>
                                        <td><a target="_blank" href="<?=base_url('order/print_invoice').'/'.md5($invoices[$o->kode_order]);?>/view">Download Invoice</a></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                        <?=$paginator;?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>