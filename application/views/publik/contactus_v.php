<?php include 'header.php'; ?>  
<style>
    .tabel { 
        border-spacing: 10px;
        border-collapse: separate;
    }
    
</style>

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>

    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <div class="block-white common-box common-page col-xs-12 col-md-8 col-md-offset-2">
        <h1 class="single-title">Hubungi Kami</h1>

        <div class="content">
            
            <?php if(!isset($_GET['s'])):?>
                
                <?php if(isset($error)): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
            
                <form name="f1" id="f1" method="post" action="">
                    <table border='0' width='100%' class='tabel'>
                        <tr>
                            <td width='20%'>Nama</td>                        
                            <td><input type="text" name="nama" id="nama" value="<?php echo set_value('nama')?>" size="40"></td>
                        </tr>
                        <tr>
                            <td width='20%'>Email</td>                        
                            <td><input type="text" name="email" id="email" value="<?php echo set_value('email')?>" size="40"></td>
                        </tr>
                        <tr valign='top'>
                            <td width='20%'>Pesan</td>                        
                            <td><textarea name="pesan" id="pesan" rows="5" cols="60"><?php echo set_value('pesan')?></textarea></td>
                        </tr>
                        <tr valign='top'>
                            <td width='20%'></td>                        
                            <td>                                
                                <input type="submit" value="Kirim" name="submit" class="btn btn-success" type="button" onclick="$('#f1').submit()">
                            </td>
                        </tr>
                    </table>
                </form>
            <?php else:?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Data berhasil disimpan, terima kasih sudah menghubungi kami.
                </div>
            <?php endif;?>
            
        </div>
      </div>
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>
