<?php include 'header.php'; ?>

<!-- MAIN AREA -->
<div class="content-area">
<?php include 'filter_mobile.php'; ?>
    <!-- PRODUCT SINGLE -->
    <div class="container-fluid block-container">
        <!-- user overview -->
        <header class="row user-overview" data-user-id="">
            <div class="col-md-8 col-md-offset-2">
                <div class="block-white single-head">
                    <div class="row">
                        <div align="center" style="font-size: 25px">
                            <strong>Peta Situs</strong>
                        </div>
                        <br>
                        <br>
                        <div class="col-xs-6" >
                            <div align="center">
                                <strong>
                                    Halaman :
                                </strong>
                            </div>
                            <br>
                            <div>
                                <ul>
                                    <?php
                                    foreach ($page as $v)
                                    {
                                        ?>
                                        <li style="margin-bottom: 10px"><a href="<?=base_url();?>page/?p=<?=$v->id_page;?>"><?=$v->title;?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6" > 
                            <div align="center">
                                Profil :
                            </div>
                            <br>
                            <div>
                                <ul>
                                    <?php
                                    foreach ($profil as $v)
                                    {
                                        ?>
                                        <li style="margin-bottom: 10px"><a href="<?=base_url();?><?=$v['id_page'];?>"><?=$v['title'];?></a></li>
                                            <?php
                                        }
                                        ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </header>
        <!-- /user overview -->
    </div>

</div>
<!-- PRODUCT INDEX -->

<?php include 'footer.php'; ?>