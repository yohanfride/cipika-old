<?php include 'header.php'; ?>  

  <!-- MAIN AREA -->
  <div class="content-area">
<?php include 'filter_mobile.php'; ?>

    <!-- PAGE SINGLE -->
    <div class="container-fluid block-container">

      <!-- Page detail -->
      <div class="block-white common-box common-page col-xs-12 col-md-8 col-md-offset-2">
        <h1 class="single-title"><?=ucwords($data->title);?></h1>

        <div class="content"><?=$data->content;?>
        </div>
        <div class="actions">
          <p>
            <!-- tweet -->
            <a href="https://twitter.com/share" class="twitter-share-button" data-via="getshoop" data-related="getshoop" data-count="none">Tweet</a>
            <!-- like and share -->
            <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
          </p>
        </div>

      </div>
      <!-- /Page detail -->

    </div>

    </div>
  </div>

<?php include 'footer.php'; ?>