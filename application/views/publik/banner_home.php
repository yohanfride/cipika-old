    <!-- HOME HEADER PROMO -->
    <div class="container-fluid block-container" role="banner">
      <header class="row header-promo">
        <div class="col-md-4 pull-right top-banner visible-lg visible-md ">

          <!-- RESPONSIVE BANNER: current version hide on mobile -->
          <div class="banner 1-bannerset-shoop" style="background-color: transparent">

            <div id="nivo" class="nivoSlider img-reponsive">
              <?php  
                if(!empty($banner)){
                  foreach ($banner as $key => $value) {
                    echo "<a class='link' target='_blank' href='".$value->banner_url."'><img src='".$value->banner_image."'></a>";
                  }
                } else {

                  echo "<img src='".base_url('asset/content/bannerset/dompetku.png')."'>"; 
                }

              ?>
            </div>
          </div>

        </div>
        <div class="col-md-8 featured"><!-- filter -->
          <div class="featured-wrap block-white">
            <div id="carousel-featured" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ul class="carousel-tabs nav nav-tabs">
                <li data-target="#carousel-featured" data-slide-to="0" class="active"><i class="shoopicon-star"></i> Curator Picks</li>
                <li data-target="#carousel-featured" data-slide-to="1"><i class="shoopicon-heart"></i> Most Loved</li>
                <li data-target="#carousel-featured" data-slide-to="2"><i class="shoopicon-bubble"></i> Most Discussed</li>
              </ul>
              <!-- Wrapper for slides -->
              <div class="carousel-inner img-reponsive">
                <div class="active item dead-row">
                  <?php foreach($produk_pick1 as $p) { ?>
                  <a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><img class="col-dead-half" src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=417&h=417&z=1"></a>
                  <?php } ?>
                  <div class="col-dead-half">
                    <div class="dead-row">
                      <?php foreach($produk_pick as $p) { ?>
                      <a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><img class="col-dead-half" src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=208&h=208&z=1"></a>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="item dead-row">
                  <?php foreach($produk_love1 as $p) { ?>
                  <a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><img class="col-dead-half" src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=417&h=417&z=1"></a>
                  <?php } ?>
                  <div class="col-dead-half">
                    <div class="dead-row">
                      <?php foreach($produk_love as $p) { ?>
                      <a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><img class="col-dead-half" src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=208&h=208&z=1"></a>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="item dead-row">
                  <?php foreach($produk_disqus as $p) { ?>
                  <a href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><img class="col-dead-fourth" src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=208&h=208&z=1"></a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </div>
    <!-- /HOME HEADER PROMO -->
