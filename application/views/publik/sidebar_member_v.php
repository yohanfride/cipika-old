<div class="col-xs-12 col-md-3">
    <div class="block-white common-box" style="padding-top:30px;">
        <h1 class="single-title" style="margin-top:-10px">Member</h1>
        <ul class="nav nav-pills nav-stacked">            
            <li <?php echo (isset($sidebar_member_order))?$sidebar_member_order:''?>><a href="<?=base_url();?>order">Order</a></li>
            <li <?php echo (isset($sidebar_member_wishlist))?$sidebar_member_wishlist:''?>><a href="<?=base_url();?>user/wishlist">Wishlist</a></li>            
            <li <?php echo (isset($sidebar_member_profile))?$sidebar_member_profile:''?>><a href="<?=base_url();?>user/profile">Profile</a></li>
            <li <?php echo (isset($sidebar_member_account))?$sidebar_member_account:''?>><a href="<?=base_url();?>user/account">Account</a></li>
        </ul>
    </div>
</div>