<?php include 'header.php'; ?>
    <div class="content-area container">
<?php include 'filter_mobile.php'; ?>
    <div class="content">
        <?php if($success=='success'){ ?>
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Success
        </div>
        <?php }else{ ?>
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" method="post" action="<?=base_url();?>user/confirm_product" enctype="multipart/form-data">
                <?php
                $no = 0;
                foreach($order_item as $item){
                ?>
              <div class="form-group">
                <label for="" class="col-lg-2 control-label">Nama Produk</label>
                <div class="col-lg-4">
                  <label for="" class="control-label"><b><?=$item->nama_produk;?></b></label>
                </div>
              </div>
              <div class="form-group" id="category-parent">
                <label for="" class="col-lg-2 control-label">Status</label>
                <div class="col-lg-4">
<!--                  <select name="id_kategori[]" class="form-control parent_category" id="id_kategori" data-sub="0" placeholder="">
                    <option value="">-- Pilih Status --</option>
                    <option value="received">Received</option>
                    <option value="not_received">Not Received</option>
                    <option value="broken">Broken</option>
                  </select>-->
                    <input name="status<?= $no ?>" type="radio" value="received" onclick="javascript: if(this.checked==true) $('#images<?= $no ?>').fadeOut(1000);"/><span class="text3"> Received </span>
                    <input name="status<?= $no ?>" type="radio" value="not_received" onclick="javascript: if(this.checked==true) $('#images<?= $no ?>').fadeOut(1000);"/><span class="text3"> Not Yet Received </span>
		  <input name="status<?= $no ?>" type="radio" value="broken" onclick="javascript: if(this.checked==true) $('#images<?= $no ?>').fadeIn(1000);"/><span class="text3"> Broken </span>
                </div>
              </div>
              <div id="images<?= $no ?>" class="form-group" style="display:none">
                <label for="" class="col-lg-2 control-label">Gambar</label>
                <div class="col-lg-8">
                  <div class="row" id="photo-container">
                      <input type="file" name="file<?= $no ?>">
                  </div>
                </div>
              </div>
                <input type="hidden" value="<?= $order->id_order?>" name="id_order<?= $no ?>">
                <input type="hidden" value="<?= $item->id_produk?>" name="id_produk<?= $no ?>">
              <div class="clearfix"></div>
              <br><br>
                                
              <?php
                $no++;
                }
              ?>
              <input type="hidden" value="<?= $no ?>" name="no">
              <div style="text-align:center;">
                <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Simpan</button>
              </div>    
                </form>    
              </div>        
          </div>
        <hr />
        <div class="row" style="margin-top: 4em;">
            
        <h3>Data Transaksi Anda</h3>
            <div class="col-md-6">
                <strong>Alamat Pengiriman</strong><br /><span></span>
                <?=ucwords($order->nama);?><br />
                <?=ucwords($order->alamat);?><br>
                <?=ucwords($order->nama_kota);?><br>
                <?=ucwords($order->nama_provinsi);?>
                <br />
                <br />
                <?=$order->telpon;?><br />
            </div>
            <div class="col-md-4">
                <strong>Metode Pembayaran</strong><br />
                <?=ucwords($order->payment);?>
            </div>
        </div>
        <div class="row" style="margin-top: 2em;">
          <div class="table-responsive">
            <table class="table table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Produk</th>
                        <th>Jumlah</th>
                        <th style="text-align: right">Harga</th>
                        <th style="text-align: right">Diskon</th>
                        <th style="text-align: right">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0;foreach($order_item as $item){ $i++; ?>
                    <tr>
                        <td><?=$i;?></td>
                        <td><?=$item->nama_produk;?></td>
                        <td style="text-align: right;"><?=$item->jml_produk;?></td>
                        <td style="text-align: right;">Rp <?=$this->cart->format_number($item->harga);?></td>
                        <td style="text-align: right;"><?=$item->diskon;?></td>
                        <td style="text-align: right;">Rp <?=$this->cart->format_number($item->total);?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="5">Total</td>
                        <td style="text-align: right">Rp <?=$this->cart->format_number($order->total);?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="5">Biaya Pengiriman</td>
                        <td style="text-align: right">Rp <?=$this->cart->format_number($order->ongkir_sementara);?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="5">GRAND TOTAL</td>
                        <td style="text-align: right; font-weight: bold">Rp <?=$this->cart->format_number($order->total+$order->ongkir_sementara);?></td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php include 'footer.php'; ?>