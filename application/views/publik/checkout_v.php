<?php include 'header.php'; ?>

<div class="content-area container">
<?php include 'filter_mobile.php'; ?>
    <ol class="progtrckr" data-progtrckr-steps="3">
        <li class="progtrckr-done">Metode Pengiriman</li>
        <li class="progtrckr-todo">Metode Pembayaran</li>
        <li class="progtrckr-todo">Konfirmasi Order</li>
    </ol>

    <div class="content">
        <!--<form id="billing-form" method="POST" action="<?= base_url() ?>cart/billing" class="form-horizontal">-->
        <form id="billing-form" method="POST" action="<?= base_url() ?>cart/billing" class="form-horizontal">
            <div class="checkout_step1">   
                <div>

                    <?php
                    $moneyCeiling = new \Cipika\View\Helper\MoneyCeiling();
                    
                    $no_order = 0;
                    $dis_api = 0;
                    foreach ($cart as $key => $value)
                    {
                        ?>
                        <label># Nama Merchant : </label><strong><?= ucwords($this->cart_m->get_merchant($key)->nama_store) ?></strong><br>
                        Lokasi: <?= strtoupper($this->cart_m->get_merchant($key)->nama_propinsi) . ',' . $this->cart_m->get_merchant($key)->nama_kabupaten ?>

                        <?php
                        if($this->input->get('update_alamat'))
                            $this->session->unset_userdata('alamat'.$key);
                        if (!$this->session->userdata('alamat' . $key))
                        {
                            $array = array(
                                'nama' => $user->firstname . " " . $user->lastname,
                                'alamat' => $user->alamat,
                                'telpon' => $user->hp,
                                'email' => $user->email,
                                'id_provinsi' => $user->id_propinsi,
                                'id_kota' => $user->id_kabupaten,
                                'id_kecamatan' => $user->id_kecamatan,
                                'propinsi' => $user->propinsi,
                                'kabupaten' => $user->kabupaten,
                                'kecamatan' => $user->kecamatan
                            );
                            $this->session->set_userdata('alamat' . $key, $array);
                        }
                        $ses = $this->session->userdata('alamat' . $key)
                        ?>
                        <div class="alamat_profil alamat_profil">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Nama Lengkap</label>
                                </div>
                                <div class="col-md-6">
                                    <span><?= ucwords($ses['nama']); ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Alamat Penagihan</label>
                                </div>
                                <div class="col-md-6">
                                    <span><?= ucwords($ses['alamat']); ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Propinsi</label>
                                </div>
                                <div class="col-md-6">
                                    <span><?= ucwords($ses['propinsi']); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Kota</label>
                                </div>
                                <div class="col-md-6">
                                    <span class="kota_awal"><?= ucwords($ses['kabupaten']); ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Kecamatan</label>
                                </div>
                                <div class="col-md-6">
                                    <span class="kota_awal"><?= ucwords($ses['kecamatan']); ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">No Handphone</label>
                                </div>
                                <div class="col-md-6">
                                    <span><?= $ses['telpon']; ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Email</label>
                                </div>
                                <div class="col-md-6">
                                    <span><?= $ses['email']; ?></span>
                                </div>
                            </div>
                        </div>
                        <span class="pull-right"><a href="<?= base_url() ?>cart/ganti_alamat/<?= $key ?>" class="">Ganti alamat pengiriman</a></span>

                        <hr/>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                /*$('.ganti_alamat<?= $key ?>').click(function () {
                                 $('.alamat_lain<?= $key ?>').toggle();
                                 $('.alamat_profil<?= $key ?>').toggle();           
                                 $('.alamat_check<?= $key ?>').click();
                                     
                                 });
                                 */
                            });
                        </script>
                        <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                $berat = 0;
                                $volume = 0;
                                $subtotal = 0;
                                $cek_makanan = 0;
                                $cek_food = 0;
                                foreach ($value as $row)
                                {
                                    ?>

                                    <?php
                                    $berat+=($row['berat'] * $row['qty']);
                                    $volume+=($row['dimensi'] * $row['qty']);
                                    $subtotal+=$row['subtotal'];
                                    $i++;
                                    $cek_makanan += $this->cart_m->check_kategori($row['id'], 38);
                                    $cek_food += $this->cart_m->check_kategori($row['id'], 30);
                                }
                                ?>
                                <tr>
                                    <th width="15%"></th>
                                    <th style="text-align: left" width="23%">Nama Produk</th>
                                    <th style="text-align: right" width="8%">Jumlah</th>
                                    <th style="text-align: right" width="12%">Harga</th>
                                    <th style="text-align: right" width="8%">Diskon</th>
                                    <th style="text-align: right" width="8%">Berat</th>
                                    <th style="text-align: right" width="14%">Harga Setelah Diskon</th>
                                    <th style="text-align: right" width="12%">Subtotal</th>
                                </tr>
                                <?php
                                foreach ($value as $row)
                                {
                                    ?>
                                    <tr class="row-item">
                                        <td><img src="<?= base_url(); ?>asset/pict.php?src=<?= $row['image']; ?>&w=50&h=50&z=1"></td>
                                        <td style="text-align: left"><?= $row['name']; ?></td>
                                        <td style="text-align: right" class="price-item"><?= $row['qty']; ?></td>
                                        <td style="text-align: right" class="price-item">Rp <?= $this->cart->format_number($row['harga']); ?></td>
                                        <td style="text-align: right" class="price-item"><?= $row['discount']; ?></td>
                                        <td style="text-align: right" class="price-item"><?= $row['berat'] * $row['qty']; ?> Kg</td>
                                        <td style="text-align: right" class="price-after-discount">Rp <?= $this->cart->format_number($row['price']); ?></td>
                                        <td style="text-align: right" class="price-total">Rp <?= $this->cart->format_number($row['subtotal']); ?></td>
                                    </tr>
    <?php } ?>
                                <tr>                        
                                    <th style="text-align: left" colspan="4"><div>Berat yang tercantum adalah berat produk setelah dikemas</div></th>
                            <th style="text-align: right" colspan="">Berat</th>
                            <th style="text-align: right" colspan=""><?= $berat; ?> Kg</th>
                            <th style="text-align: right" colspan="">Total</th>
                            <td style="text-align: right; font-weight: bold" class="grand-total">Rp <?= $this->cart->format_number($subtotal); ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                        <div class="row">
                            <div class="pull-right">
                                <?php
                                $cek_jne = FALSE;
                                $origin = $this->lib_rest->rest_get(base_url('api/shipping/get_origin?origin=' . $this->cart_m->get_merchant($key)->nama_kabupaten));
                                $destination = $this->lib_rest->rest_get(base_url('api/shipping/get_destination?destination=' . $ses['kabupaten']));

                                $origin = json_decode($origin);
                                $destination = json_decode($destination);

                                $api = array();
                                if (isset($origin->detail[0]->code) && isset($destination->detail[0]->code))
                                {
                                    $api = $this->lib_rest->rest_get(base_url('api/shipping/get_tarif?origin=' . $origin->detail[0]->code . '&destination=' . $destination->detail[0]->code . '&weight=' . $berat));
                                    $api = json_decode($api);
                                    if (isset($api->price)) {
                                        $api = $api->price;
                                        $cek_jne = TRUE;
                                    }
                                }
                                ?>
                                <div class="list_ongkir"> Ongkos Kirim : 

                                    <?php
                                    if ($cek_jne)
                                    {
                                        $cekjne = 0;
                                        foreach ($api as $row){
                                            if ($cek_makanan > 0){
                                                if (stristr($row->service_display, 'YES') == TRUE){
                                                  $cekjne += 1;  
                                                }
                                            }else if ($cek_food > 0){
                                                if (stristr($row->service_display, 'YES') == TRUE || stristr($row->service_display, 'REG') == TRUE)
                                                {
                                                    $cekjne += 1;
                                                }
                                            }else{
                                                if (stristr($row->service_display, 'SPS') == FALSE && stristr($row->service_display, 'PELIK') == FALSE)
                                                {
                                                    $cekjne += 1;
                                                }
                                            }
                                        }
                                        if($cekjne > 0){
                                        ?>
                                        <select name="ongkir_sementara<?=$key?>">
                                            <?php
                                            foreach ($api as $row)
                                            {
                                                $jne_price = $moneyCeiling->ceil($row->price + ($row->price*($this->config->item('markup_jne'))/100),1000);
                                                if ($cek_makanan > 0)
                                                {
                                                    if (stristr($row->service_display, 'YES') == TRUE)
                                                    {
                                                        ?>    
                                                        <option value="<?= $jne_price . " " . $row->service_display ?>">JNE Paket <?= $row->service_display . ' Rp.' . $jne_price ?></option>
                                                        <?php
                                                    }
                                                }
                                                else if ($cek_food > 0)
                                                {
                                                    if (stristr($row->service_display, 'YES') == TRUE || stristr($row->service_display, 'REG') == TRUE)
                                                    {
                                                        ?>  
                                                        <option value="<?= $jne_price . " " . $row->service_display ?>">JNE Paket <?= $row->service_display . ' Rp.' . $jne_price ?></option>
                                                        <?php
                                                    }
                                                }
                                                else
                                                {
                                                    if (stristr($row->service_display, 'SPS') == FALSE && stristr($row->service_display, 'PELIK') == FALSE) 
                                                    {
                                                        ?> 
                                                        <option value="<?= $jne_price . " " . $row->service_display ?>">JNE Paket <?= $row->service_display . ' Rp.' . $jne_price ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>                                           
                                        <?php
                                    }else{
                                        echo 'Biaya pengiriman tidak ditemukan.';
                                        $dis_api += 1;
                                        }
                                    }
                                    else
                                    {
                                        echo 'Biaya pengiriman tidak ditemukan.';
                                        $dis_api += 1;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div><br>
                        <?php
                        $no_order++;
                    }
                    ?>
                    <script>
                        $('.ganti-alamat').click(function(e) {
                            e.preventDefault();

                            var alamatLain = $('.alamat-lain');

                            // sementara, harusnya bisa jalan di browser lain
                            if (!alamatLain.css('visible')) {
                                $('.alamat_lain').show();
                                $('.alamat_profil').hide();
                            } else {
                                $('.alamat_lain').hide();
                                $('.alamat_profil').show();
                            }
                        });
                    </script>

                </div>
                <br>
                <a href="<?= base_url(); ?>cart" class="btn btn-danger">Kembali</a>
                <!--<button type="submit" class="btn btn-warning lanjut_step3 pull-right">Lanjutkan</button>-->
                <!--<a href="<?= base_url(); ?>cart" class="btn btn-danger">Kembali</a>-->
                <?php
                $hid = ($dis_api > 0) ? 'disabled="disabled"' : '';
                ?>
                <button type="submit" value="1" name="submit" class="btn btn-warning pull-right link" <?= $hid ?>>Lanjutkan</button>
                <br />
                <br>
            </div>
        </form>

        <script>
            $('#billing-form').on('submit', function(e) {
                $('#lanjutkan').hide();
                $('.kembali_step1').attr('disabled', 'disabled');
                $('#id_payment').attr('readonly', 'readonly');
                $('.action-buttons').append('<button class="btn btn-warning pull-right" disabled="disabled">Transaksi sedang diproses</button>');
            });
        </script>

    </div>
</div>
<?php include 'footer.php'; ?>
