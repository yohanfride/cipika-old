<?php include 'header.php'; ?>
<style type="text/css">
.form-control{
  border: 1px solid #eee;
}

</style>
  <!-- MAIN AREA -->
  <div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
      <div class="block-container index">
        <div class="row">

        <?php $this->load->view('publik/merchant/sidebar_v')?>

        <div class="block-white common-box common-page col-xs-12 col-md-9">
          <h1 class="single-title">Kelola Produk</h1>

            <?php if($success!=''){ ?>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$success;?>
            </div>
            <?php } ?>

            <?php if($error!=''){ ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?=$error;?>
            </div>
            <?php } ?>

          <div class="content">
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="<?=base_url();?>myproduct/upload">
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Nama Produk</label>
                    <div class="col-lg-4">
                      <input value="<?php echo set_value('nama_produk')?>" name="nama_produk" type="text" class="form-control" id="nama_produk" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Deskripsi</label>
                    <div class="col-lg-4">
                      <textarea name="deskripsi" class="form-control" id="deskripsi" placeholder="Stik Wortel merupakan camilan stik yang biasa ditemui di pasaran terbuat dari bahan standar yakni tepung .." rows=5><?php echo set_value('nama_produk')?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Berat</label>
                    <div class="input-group col-lg-4" style="padding: 0 10px;">
                      <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('berat')?>" name="berat" type="text" class="form-control" id="berat" placeholder="0.00">
                      <span class="input-group-addon">Kg</span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Dimension</label>
                    <div class="col-lg-9">
                        <div class="input-group col-lg-3 pull-left">
                          <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('panjang')?>" name="panjang" type="text" class="form-control" id="panjang" placeholder="panjang">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('lebar')?>" name="lebar" type="text" class="form-control" id="lebar" placeholder="lebar">
                          <span class="input-group-addon">cm</span>
                        </div>
                        <div class="input-group col-lg-3 pull-left">
                          <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('tinggi')?>" name="tinggi" type="text" class="form-control" id="tinggi" placeholder="tinggi">
                          <span class="input-group-addon">cm</span>
                        </div>
                      <small style="color:red;"> *dihitung setelah packing</small>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Stok</label>
                    <div class="col-lg-4">
                      <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('stok_produk')?>" name="stok_produk" type="text" class="form-control" id="stok_produk" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Harga</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('harga_produk')?>" name="harga_produk" type="text" class="form-control" id="harga_produk" placeholder="15000">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Diskon</label>
                    <div class="col-lg-4">
                      <div class="input-group">
                      <input onkeypress="return isNumberKey(event)" value="<?php echo set_value('diskon')?>" name="diskon" type="text" class="form-control" id="diskon" placeholder="">                      
                        <span class="input-group-addon">%</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" id="category-parent">
                    <label for="" class="col-lg-2 control-label">Kategori</label>
                    <div class="col-lg-4">
                      <select name="id_kategori[]" class="form-control parent_category" id="id_kategori" data-sub="0" placeholder="">
                        <option value="">-- Pilih Kategori --</option>
                        <?php foreach($kategori as $kat){ ?>
                        <option value="<?=$kat->id_kategori;?>"><?=ucwords($kat->nama_kategori);?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="sub-category-1"></div>

                  <div class="form-group">
                    <label for="" class="col-lg-2 control-label">Gambar</label>
                    <div class="col-lg-8">
                      <div class="row" id="photo-container">
                        <div id="add-photo1" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <div id="add-photo2" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <div id="add-photo3" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                        <div id="add-photo4" class="col-lg-2 add-foto"><span class="plus">+</span></div>
                      </div>
                      <div class="row">
                         <div class="col-lg-8" style="color: #888">
                           * Maksimal ukuran foto adalah : <?php echo ini_get('upload_max_filesize') ?><br />
                           * Foto harus square (panjang dan lebar harus sama)<br />
                         </div>
                     </div>
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="" class="col-lg-2 control-label"></label>
                    <div class="col-lg-6">
                      <input type="checkbox" name="shipping_area" value="1"> Terapkan wilayah pengiriman untuk produk ini.
                    </div>
                  </div> -->
                  <div class="clearfix"></div>
                  <br><br>
                  <div style="text-align:center;">
                    <button class="btn btn-primary save-product" type="submit" name="simpan" value=1>Simpan</button>
                  </div>                      
                  
                  </div>
                </form>            
              </div>
            </div><!-- /.row -->
          </div>
        </div>

        </div>
      </div>
    </div>
    
  </div>

<?php include 'footer.php'; ?>
<?php include 'upload.php'; ?>
