<?php include 'header.php'; ?>

<!-- MAIN AREA -->
<div class="content-area">

<?php include 'banner_home.php'; ?>
<?php include 'filter.php'; ?>

    <!-- PRODUCT INDEX -->
    <div class="container-fluid block-container index">
      <div class="row product-thumb-row">
        <?php foreach ($produk as $p) { ?>
        <div id="product-<?=$p->id_produk;?>" class="col-lg-3 col-sm-4 col-xs-6 product-thumb-item"><!-- Item -->
          <div class="product-thumb-inner product-tile" data-product-id="<?=$p->id_produk;?>">
            <a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>">
              <figure class="img-responsive">
                <img src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=300&h=300&z=1">
              </figure>
            </a>
            <div class="product-thumb-prop">
              <h4 class="product-thumb-title"><a class="link" href="<?=base_url();?>product/detail/<?=$p->id_produk.'/'.urlencode(preg_replace('/[^\\pL0-9]+/u','-',$p->nama_produk));?>"><?=ucwords($p->nama_produk);?></a></h4>
              <p class="product-thumb-price">Rp <?=$this->cart->format_number($p->harga_jual);?>
              <?php if($p->stok_produk>0){ ?>
                <a class="add-to-cart label label-warning" href="#"><i class="glyphicon glyphicon-shopping-cart"></i> Beli</a>
              <?php } else { ?>
                <span class="label label-danger"><i class="glyphicon glyphicon-shopping-cart"></i> sold out</span>
              <?php } ?>
              </p>
              <div class="product-thumb-author">
                <a class="link" href="<?=base_url();?>store/id/<?=$p->id_user;?>">
                  <figure>
                    <span>
                        <?php
                        $img=$this->user_m->get_single('tbl_user', 'id_user', $p->id_user);
                        ?>
                        <?php if($img->image == NULL){ ?>
                        <img src="<?=base_url();?>asset/img/no-avatar.jpg">
                            <?php }else{ ?>
                          <!--<img src="<?=base_url();?>asset/upload/profil/<?= $img->image ?>" width="16" height="16">-->
                          <img src="<?=base_url();?>asset/pict.php?src=<?=base_url();?>asset/upload/profil/<?=$img->image;?>&w=16&h=16&z=1">
                          <?php
                            }
                          ?>
                    
                    </span>                    
                    <figcaption>
                        <?php
                            $seller = $this->commonlib->get_seller($p->id_user);
                            if(isset($seller->nama_store)){
                                echo ucwords($seller->nama_store);
                            }else{
                                echo ucwords($p->username);
                            }
                        ?>
                    </figcaption>
                  </figure>
                </a>
                <!-- <span class="badge-verified"><i class="glyphicon glyphicon-ok-sign"></i></span> -->
              </div>
            </div>
            <div class="product-thumb-footer clearfix">
              <p class="product-view pull-left"><i class="glyphicon glyphicon-eye-open"></i> <?=$p->viewed;?></p>
              <p class="pull-right">
                <a class="product-thumb-comments" href="#"><i class="shoopicon-bubble"></i> <?=$p->count_comment;?></a>
                <button type="button" class="product-thumb-love <?php if($this->session->userdata('member')){if($this->auth_m->cek('tbl_love', 'id_produk', 'id_user', $p->id_produk, $this->auth_m->get_user()->id_user)>0) echo 'user-loved';}?>" <?php if(!$this->session->userdata('member')) {echo 'data-toggle="modal" data-target="#register-login-form"';}?>><i class="shoopicon-heart bigger-shoopicon"></i> <span class="love-count"><?=$p->loved;?></span></button>
              </p>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>

<!-- Loading Content Here -->

    </div>
    <!-- PRODUCT INDEX -->
  </div>

 <?php include 'footer.php'; ?>