<?php include 'header.php'; ?>
<!-- MAIN AREA -->
<div class="content-area">

<?php include 'filter_mobile.php'; ?>

    <div class="container-fluid block-container">
        <div class="block-container index">
            <div class="row">

                <?php $this->load->view('publik/sidebar_member_v')?>

                <div class="block-white common-box common-page col-xs-12 col-md-9">
                    <h1 class="single-title">My Wishlist</h1>
                    <div class="content">
                        <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th></th>
                                    <th>Nama</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($data as $p)
                                {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td>
                                            <img src="<?=base_url();?>asset/pict.php?src=<?=$p->image;?>&w=150&h=150&z=1">
                                        </td>
                                        <td><?=$p->nama_produk;?></td>
                                        <td>
                                            <a href="<?=base_url();?>product/detail/<?=$p->id_produk;?>" class="btn btn-danger btn-xs"  >View</a>
                                            <a onclick="return confirm('Are you sure delete this product?')" class="btn btn-danger btn-xs"  href="<?=base_url();?>user/delete_wishlish/<?=md5($p->id_produk);?>/<?=md5($this->session->userdata('member')->id_user);?>">Delete</a>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>