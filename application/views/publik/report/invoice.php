<?php

define('FPDF_FONTPATH', 'fpdf/font/');
require('fpdf/fpdf.php');

class PDF extends FPDF {

    var $tablewidths;
    var $headerset;
    var $footerset;

    //Page header
    function Header()
    {
        //Logo
        //Times bold 15
        $this->SetFont('Times', 'B', 15);
        //pindah ke posisi ke tengah untuk membuat judul
        $this->Cell(80);
        //judul
        $this->Cell(30, 10, 'INVOICE', 0, 0, 'C');
        //pindah baris
        $this->Ln(20);
        //buat garis horisontal
        $this->Line(10, 25, 200, 25);
    }

    //Page Content
    function Content()
    {
        $this->SetFont('Times', '', 12);
        for ($i = 1; $i <= 40; $i++)
            $this->Cell(0, 10, 'line report ' . $i, 0, 1);
    }

    //Page footer
    function Footer()
    {
        //atur posisi 1.5 cm dari bawah
        $this->SetY(-15);
        //buat garis horizontal
        $this->Line(10, $this->GetY(), 200, $this->GetY());
        //Times italic 9
        $this->SetFont('Times', 'I', 9);
        //nomor halaman
        $this->Cell(-15, 10, 'Halaman ' . $this->PageNo() . ' dari {nb}', 0, 0, 'R');
    }

}

$pdf = new FPDF();
//Halaman 1

$pdf->_beginpage('P', 'A4');
$pdf->SetY(5);
$pdf->Image(base_url() . 'asset/img/cipika-logo2.png', 14, 8);

$pdf->Cell(16);
$pdf->SetX(15);
$pdf->SetFont('Times', 'B', 17);
$pdf->Line(95, 13, 120, 13);
$pdf->Cell(0, 10, 'INVOICE', 0, 0, 'C');
$pdf->SetX(50);
$pdf->SetY(25);
$pdf->SetFont('Times', 'B', 12);
$pdf->Cell(75, 6, 'Yth : ' . ucfirst($user->firstname) . " " . ucfirst($user->lastname), 0, 0, 'L');
$pdf->Ln();
$pdf->SetFont('Times', 'B', 8);
$pdf->Cell(100, 6, 'Terimakasih anda telah melakukan sejumlah pembelian melalui CipikaStore', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(100, 6, 'Pembelian anda telah sukses dilakukan.', 0, 0, 'L');

$pdf->Ln(10);
$pdf->Cell(25, 6, 'Nomor Invoice', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$pdf->Cell(50, 6, $invoices->kode_invoice, 0, 0, 'L');
$pdf->Cell(50, 6, "", 0, 0, 'L');
$pdf->Cell(50, 6, "Pembayaran", 1, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'Nama', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$pdf->Cell(50, 6, ucfirst($user->firstname) . " " . ucfirst($user->lastname), 0, 0, 'L');
$pdf->Cell(50, 6, "", 0, 0, 'L');
$paymentStr = '';
if (is_object($payment) && property_exists($payment, 'payment')) {
//    $paymentStr = $payment->payment;
    $paymentStr = $pay->nama_payment;
}
$pdf->Cell(50, 6, "Metode : ".strtoupper($paymentStr), 1, 0, 'L');

$pdf->Ln();
$pdf->Cell(25, 6, 'Alamat', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$pdf->Cell(100, 6, $user->alamat, 0, 0, 'L');
if (isset($payment->payment) && $payment->payment == "DOMPETKU")
{
    $pdf->Cell(50, 6, "Phone : " . $payment->msisdn, 1, 0, 'L');
}
else
{
    $pdf->Cell(50, 6, "VAN ID : " . $payment->respond, 1, 0, 'L');
}
$pdf->Ln();
$pdf->Cell(30, 6, '', 0, 0, 'L');
$pdf->Cell(100, 6, "".ucfirst(strtolower($user->nama_kabupaten)).", ".ucfirst(strtolower($user->nama_propinsi))."", 0, 0, 'L');
if (isset($payment->payment) && $payment->payment == "BANKTRANFER")
{
    $date = new DateTime($invoices->date_added);
    $date->add(new DateInterval('PT4H'));
    $timenya = $date->format('Y-m-d H:i:s') . "\n";
    $pdf->Cell(50, 6, 'Batas Waktu : ' . $timenya, 1, 0, 'L');
}
$pdf->Ln();
$pdf->Cell(25, 6, 'No Telp', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$pdf->Cell(50, 6, $user->telpon, 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'No Handphone', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$pdf->Cell(50, 6, $user->hp, 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'Total Harga', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$pdf->Cell(50, 6, "Rp ".number_format($invoices->total + $invoices->ongkir + $invoices->payment_fee, 0 , '' , ',' ), 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'Tanggal', 0, 0, 'L');
$pdf->Cell(5, 6, ':', 0, 0, 'L');
$date = strtotime($invoices->date_added);
$new_date = date('d-m-Y H:i:s', $date);
$pdf->Cell(50, 6, $new_date, 0, 0, 'L');

$pdf->Ln(8);
$pdf->Cell(25, 6, 'Data Transaksi :', 0, 0, 'L');
$tabel_border = 0;
$tabel_border_2 = 1;
$totalnya=0;
foreach ($order as $v)
{
    $pdf->Ln();
    $pdf->SetFont('Times', 'B', 8);
    $pdf->Cell(40, 4, 'Merchant : ' . $order_merchant[$v->id_merchant]->nama_store, 0, 0, 'L');
    $pdf->Cell(20, 4, '', 0, 0, 'L');
    $pdf->Cell(40, 4, 'Order : ' . $v->kode_order, 0, 0, 'L');
    $pdf->Cell(40, 4, '', 0, 0, 'L');
    $pdf->Cell(25, 4, 'Status : Success', 0, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(40, 4, '' . strtoupper($this->cart_m->get_merchant($v->id_merchant)->nama_propinsi). ', '.strtoupper($this->cart_m->get_merchant($v->id_merchant)->nama_kabupaten), 0, 0, 'L');

    $pdf->SetFont('Times', 'B', 8);
    $pdf->Ln(8);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->Cell(140, 6, 'Alamat Pengiriman :', $tabel_border, 0, 'L');
    $pdf->Cell(50, 6, 'Metode Pengiriman :', $tabel_border, 0, 'L');
    $pdf->Ln();
    $pdf->SetFont('Times', '', 8);
    $pdf->Cell(140, 6, ucwords($v->nama), $tabel_border, 0, 'L');
    $pdf->Cell(50, 6, $v->paket_ongkir, $tabel_border, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(140, 6, $v->alamat, $tabel_border, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(140, 6, strtoupper($v->nama_kabupaten) . ", " . strtoupper($v->nama_propinsi), $tabel_border, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(140, 6, $v->telpon, $tabel_border, 0, 'L');

    $pdf->Ln(10);

    $pdf->Cell(7, 6, "No", $tabel_border_2, 0, 'C');
    $pdf->Cell(100, 6, "Nama Produk", $tabel_border_2, 0, 'L');
    $pdf->Cell(11, 6, "Berat", $tabel_border_2, 0, 'C');
    $pdf->Cell(12, 6, "Jumlah", $tabel_border_2, 0, 'C');
    $pdf->Cell(20, 6, "Harga", $tabel_border_2, 0, 'C');
    $pdf->Cell(15, 6, "Diskon", $tabel_border_2, 0, 'C');
    $pdf->Cell(25, 6, "Sub Total", $tabel_border_2, 0, 'C');
    $pdf->Ln();

    $no = 1;
    foreach ($order_item[$v->id_order] as $p)
    {
        $volume = $product[$p->id_produk]->tinggi * $product[$p->id_produk]->panjang * $product[$p->id_produk]->lebar;
        $pdf->Cell(7, 6, $no, $tabel_border_2, 0, 'C');
        $pdf->Cell(100, 6, $p->nama_produk, $tabel_border_2, 0, 'L');
        $pdf->Cell(11, 6, $product[$p->id_produk]->berat. " kg", $tabel_border_2, 0, 'C');
        $pdf->Cell(12, 6, $p->jml_produk, $tabel_border_2, 0, 'C');
        $pdf->Cell(20, 6, "Rp ".number_format($p->harga, 0 , '' , ',' ), $tabel_border_2, 0, 'C');
        $pdf->Cell(15, 6, $p->diskon, $tabel_border_2, 0, 'C');
        $pdf->Cell(25, 6, "Rp ".number_format($p->total, 0 , '' , ',' ), $tabel_border_2, 0, 'C');
        $pdf->Ln();

        $no++;
    }
    $pdf->Cell(165, 6, "Subtotal", $tabel_border_2, 0, 'R');
    $pdf->Cell(25, 6,"Rp ".number_format($v->total, 0 , '' , ',' ), $tabel_border_2, 0, 'C');
    $pdf->Ln();
    $pdf->Cell(165, 6, "Biaya Pengiriman", $tabel_border_2, 0, 'R');
    $pdf->Cell(25, 6, "Rp ".number_format($v->ongkir_sementara, 0 , '' , ',' ), $tabel_border_2, 0, 'C');
    $pdf->Ln();
    $pdf->Cell(165, 6, "Total", $tabel_border_2, 0, 'R');
    $pdf->Cell(25, 6, "Rp " . number_format($v->total + $v->ongkir_sementara, 0, '', ','), $tabel_border_2, 0, 'C');
    $pdf->Ln(10);
    $totalnya+=$v->total + $v->ongkir_sementara;
}
if ((int) $invoices->payment_fee > 0)
{
    $pdf->Cell(165, 6, "Convenience Fee", $tabel_border_2, 0, 'R');
    $pdf->Cell(25, 6, "Rp " . number_format($invoices->payment_fee, 0, '', ','), $tabel_border_2, 0, 'C');
    $pdf->Ln();
}
$pdf->Cell(165, 6, "Grand Total", $tabel_border_2, 0, 'R');
$totalAll = $totalnya + $invoices->payment_fee;
$pdf->Cell(25, 6, "Rp " . number_format($totalAll, 0, '', ','), $tabel_border_2, 0, 'C');
$pdf->Ln(10);
//footer
$pdf->SetY(-28);
$pdf->SetFont('Times', 'I', 9);
$pdf->Cell(160, 7, 'Nomor Invoice : ' . $invoices->kode_invoice, 0, 0, 'L');
$pdf->Cell(30, 7, 'Halaman ' . $pdf->PageNo() . '', 0, 0, 'C');

if ($action == "view")
{
    $pdf->Output();
}
else
{
    $path = realpath(__DIR__ . '/../../../../public/invoice');
    
    $pdf->Output($path ."/". $invoices->kode_invoice . ".pdf", "F");
}
?>
