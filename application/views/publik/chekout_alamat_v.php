<?php include 'header.php'; ?>

<div class="content-area container">
<?php include 'filter_mobile.php'; ?>
<!--<ol class="progtrckr" data-progtrckr-steps>
    <li class="progtrckr-done">Alamat Pengiriman</li>-->
<!--</ol>-->
<?php if(isset($error)){ ?>
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?=$error;?>
</div>
<?php } ?>
<div class="content">

<form method="POST" action="<?=base_url()?>cart/alamat" class="form-horizontal">
    <input class="alamat_check hidden" type="checkbox" name="alamat" value="sama" checked>
<div class="checkout_step1">
<h4>Alamat Pengiriman</h4>
<hr />
<div class="row alamat_lain">
  <div class="form-group">
    <label class="col-sm-2 control-label">Nama Depan</label>
    <div class="col-sm-6">
        <input <?=(isset($user->firstname))?'value="'.$user->firstname.'"':'';?> name="firstname" type="text" class="form-control name" id="name" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Nama Belakang</label>
    <div class="col-sm-6">
        <input <?=(isset($user->lastname))?'value="'.$user->lastname.'"':'';?> name="lastname" type="text" class="form-control name" id="name" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Email</label>
    <div class="col-sm-6">
        <input <?=(isset($user->email))?'value="'.$user->email.'"':'';?> name="email" type="text" class="form-control email" id="email" placeholder="" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-6">
      <textarea name="alamat" type="text" class="form-control address" id="address" rows="4"><?=(isset($user->alamat))? $user->alamat:'';?></textarea>
    </div>
  </div>
<!--  <div class="form-group">
        <label for="user" class="col-lg-2 control-label">Provinsi</label>
        <div class="col-lg-4">
            <select name="id_propinsi" type="text" class="form-control" id="id_propinsi" placeholder="">
                <option value="" selected>Pilih Provinsi</option>
                <?php if($user->id_propinsi != null){ ?>
                <option value="<?=$user->id_propinsi;?>" selected><?= strtoupper($nama_propinsi->nama_propinsi);?></option>
                <?php } ?>
                <?php
                foreach ($provinsi as $k)
                {
                    ?>
                <option value="<?=$k->id_propinsi;?>"><?= strtoupper($k->nama_propinsi);?></option>

                <?php } ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label">Kabupaten / Kota</label>
        <div class="col-lg-4">
            <select  class="form-control" id="select_kabupaten" name="id_kabupaten">
            <?php if($user->id_propinsi != null){ ?>
                <option value="<?=$user->id_kabupaten;?>" selected><?= strtoupper($nama_kabupaten->nama_kabupaten);?></option>
            <?php } ?>
            </select>
        </div>
    </div> 

    <div class="form-group">
        <label class="col-lg-2 control-label">Kecamatan</label>
        <div class="col-lg-4">
            <select  class="form-control" id="select_kecamatan" name="id_kecamatan">
                <?php if($user->id_propinsi != null){ ?>
                <option value="<?=$user->id_kecamatan;?>" selected><?= strtoupper($nama_kecamatan->nama_kecamatan);?></option>
                <?php } ?>
            </select>
        </div>
    </div>-->
    <div class="form-group">
    <label for="user" class="col-lg-2 control-label">Provinsi</label>
    <div class="col-md-6">                                                   
    <?php $lokasi = $this->lib_lokasi->get_lokasi($user->id_kecamatan);?>
    
    <select class="form-control" name="id_propinsi" id="propinsi" onchange="load_kabupaten('propinsi','div_kabupaten','id_kabupaten', 'id_kecamatan')">
        <?php

            if(!empty($propinsi)){
                foreach($propinsi as $row){
                    $selected = ($lokasi->id_propinsi==$row->id_propinsi)?'selected':'';
                    echo "<option value='".$row->id_propinsi."' ".$selected.">".ucwords($row->nama_propinsi)."</option>";
                }
            }
        ?>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label for="user" class="col-lg-2 control-label">Kabupaten</label>
    <div class="col-md-6">  
    <div id="div_kabupaten">                                         
        <?php
                                                
            if(!empty($lokasi)){
            $kabupaten = $this->lib_lokasi->get_kabupaten($lokasi->id_propinsi);
            if(!empty($kabupaten)){
                echo "<select class='form-control' name='id_kabupaten' id='kabupaten' onchange='load_kecamatan(\"kabupaten\",\"div_kecamatan\",\"kecamatan\")'>";
                foreach($kabupaten as $row){
                    $selected = ($lokasi->id_kabupaten==$row->id_kabupaten)?'selected':'';
                    echo "<option value='".$row->id_kabupaten."' ".$selected.">".ucwords($row->nama_kabupaten)."</option>";
                }
                echo "</select>";
            }
            }
        ?>
    </div>
    </div>
    </div>
    <div class="form-group">
    <label for="user" class="col-lg-2 control-label">Kecamatan</label>
    <div class="col-md-6">  
    <div id="div_kecamatan">
        <?php
            if(!empty($lokasi)){
            $kecamatan = $this->lib_lokasi->get_kecamatan($lokasi->id_kabupaten);
            if(!empty($kecamatan)){
                echo "<select class='form-control' name='kecamatan' id='kecamatan'>";
                foreach($kecamatan as $row){
                    $selected = ($lokasi->id_kecamatan==$row->id_kecamatan)?'selected':'';
                    echo "<option value='".$row->id_kecamatan."' ".$selected.">".ucwords($row->nama_kecamatan)."</option>";
                }
                echo "</select>";
            }
            }
        ?>
    </div>
    </div>
    </div>

    <div class="form-group">
    <label class="col-sm-2 control-label">Hp</label>
    <div class="col-sm-6">
      <input onkeypress="return isNumberKey(event)" <?=(isset($user->hp))?'value="'.$user->hp.'"':'';?> name="hp" type="text" class="form-control phone" id="phone" placeholder="">
    </div>
  </div>
</div>   
<br>
<a href="<?=base_url();?>cart" class="btn btn-danger">Kembali</a>
<button value="1" type="submit" class="btn btn-warning pull-right link" name="simpan">Lanjutkan</button>
</div>

</form>
</div>
</div>
<?php include 'footer.php'; ?>