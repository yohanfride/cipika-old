-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2014 at 05:25 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cipika_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_propinsi`
--

CREATE TABLE IF NOT EXISTS `tbl_propinsi` (
  `id_propinsi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_propinsi` varchar(45) NOT NULL,
  PRIMARY KEY (`id_propinsi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `tbl_propinsi`
--

INSERT INTO `tbl_propinsi` (`id_propinsi`, `nama_propinsi`) VALUES
(1, 'bali'),
(2, 'banten'),
(3, 'bengkulu'),
(4, 'di yogyakarta'),
(5, 'dki jakarta'),
(6, 'gorontalo'),
(7, 'jambi'),
(8, 'jawa barat'),
(9, 'jawa tengah'),
(10, 'jawa timur'),
(11, 'kalimantan barat'),
(12, 'kalimantan selatan'),
(13, 'kalimantan tengah'),
(14, 'kalimantan timur'),
(15, 'kepulauan bangka belitung'),
(16, 'kepulauan riau'),
(17, 'lampung'),
(18, 'maluku'),
(19, 'maluku utara'),
(20, 'nanggroe aceh darussalam'),
(21, 'nusa tenggara barat'),
(22, 'nusa tenggara timur'),
(23, 'papua'),
(24, 'papua barat'),
(25, 'riau'),
(26, 'sulawesi barat'),
(27, 'sulawesi selatan'),
(28, 'sulawesi tengah'),
(29, 'sulawesi tenggara'),
(30, 'sulawesi utara'),
(31, 'sumatera barat'),
(32, 'sumatera selatan'),
(33, 'sumatera utara');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
