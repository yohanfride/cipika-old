<?php

namespace Cipika\View\Helper;

class EmailBuyerPembayaran {

    protected $bcc;
    protected $dbLibrary;
    protected $from;

    public function __construct($dbLibrary, $from, $bcc)
    {
        $this->dbLibrary = $dbLibrary;
        $this->from = $from;
        $this->bcc= $bcc;
    }

    public function send($to, $single, $inputan_order, $inputan_shipping, $inputan_merchant, $order_item, $link_index, $payment, $user)
    {
        $email_merchant = "";
        foreach ($inputan_order as $v)
        {
            $email_merchant = '<style>* {font-family:Arial, serif;font-size:12px} table tr td {padding:2px 5px;} tr.judul td {font-weight:bold;}</style>';
            $email_merchant.= "<h3>Bapak/Ibu " . ucwords($user->firstname) . " " . ucwords($user->lastname) . " Yth.</h3>
                <br />
                <p>
                    Terima kasih anda telah menyelesaikan seluruh proses belanja di Cipika Store. 
                    Kami telah mencatat daftar belanja dan pembayaran Anda dengan baik.
                </p><br>
                <p>
                    Informasi lebih lanjut mengenai proses pengiriman produk akan kami sampaikan dalam waktu maksimal 3 (tiga) hari kedepan. 
                    Pengiriman produk akan dilakukan langsung oleh Merchant kami melalui jasa ekspedisi dan layanan yang telah disepakati. Anda akan menerima informasi lebih lanjut apabila diperlukan perpanjangan waktu pengiriman.
                </p><br>
                ";
            $email_merchant .= "<table width='100%'>
                            <tr>
                                <td width='40%'>Merchant: " . ucwords($inputan_merchant[$v->id_merchant]) . "</td>
                                <td width='40%'>Nomor Transaksi: " . $v->kode_order . "</td>
                                <td width='20%'>Status: Success</td>
                            </tr>
                        </table>";
            $email_merchant .= "<table width='100%'>
                            <tr>
                                <td width='40%'>
                                    <strong>Alamat Pengiriman</strong><br /><span></span>
                                    " . ucwords($inputan_shipping[$v->id_order]->nama) . "<br />
                                    " . ucwords($inputan_shipping[$v->id_order]->alamat) . "<br>
                                    " . ucwords($inputan_shipping[$v->id_order]->nama_kabupaten) . " - " . ucwords($inputan_shipping[$v->id_order]->nama_propinsi) . "<br>
                                    " . $inputan_shipping[$v->id_order]->telpon . "<br />
                                </td>
                                <td width='40%'>
                                    <strong>Metode Pengiriman</strong><br />
                                    " . ucwords($v->paket_ongkir) . "
                                </td>
                               <td width='20%'>
                                    <strong>Metode Pembayaran</strong><br>";
            $email_merchant.= $payment->nama_payment;
            $email_merchant .= "</td>
                            </tr>
                            </table><br>";
            $email_merchant .= '<table style="border-collapse:collapse; width:100%;">
                <tr style="background:#666666; color:white;" class="judul"><td width="10%" style="text-align:center">No</td><td>Nama Produk</td><td width="10%" style="text-align:right">Jumlah</td><td width="15%" style="text-align:right">Harga</td><td width="10%" style="text-align:right">Diskon</td><td width="15%" style="text-align:right">Harga setelah diskon</td><td width="20%" style="text-align:right">Subtotal</td></tr>';
            $i = 0;
            $total_sub = 0;
            $sub_total = 0;
            $totalAll = 0;
            foreach ($order_item[$v->id_order] as $items)
            {
                if ($items->diskon > 0)
                    $harga_diskon = $items->harga - ($items->harga) * ($items->diskon / 100);
                else
                    $harga_diskon = $items->harga;
                $total_sub = $harga_diskon * $items->jml_produk;
                $i++;
                $email_merchant .= '<tr style="text-align:center"><td>' . $i . '</td> 
                        <td style="text-align:left"><a href="' . base_url() . $link_index . 'product/detail/' . $items->id_produk . '" >' . $items->nama_produk . '</a></td>
                        <td style="text-align:right">' . $items->jml_produk . '</td>
                        <td style="text-align:right">Rp. ' . number_format($items->harga, 0, '.', ',') . '</td>                        
                        <td style="text-align:right">' . $items->diskon . '%</td>
                        <td style="text-align:right">Rp. ' . number_format($harga_diskon, 0, '.', ',') . '</td>
                        <td style="text-align:right">Rp. ' . number_format($total_sub, 0, '.', ',') . '</td>
                    </tr>';
                $sub_total += $total_sub;
            }
            $email_merchant .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Sub Total</td><td >Rp. ' . $sub_total . '</td></tr>';
            $email_merchant .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Ongkos Kirim</td><td >Rp. ' . $v->ongkir_sementara . '</td></tr>';
            $total_harga = $sub_total + $v->ongkir_sementara;
            $email_merchant .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Total</td><td >Rp. ' . $total_harga . '</td></tr>            
                </table><div style="height:10px;"></div>';
            $totalAll+=$total_harga;
            $totalPrint = $totalAll + $single->payment_fee;
        }
        if ((int) $single->payment_fee > 0)
        {
            $email_merchant .= "<table width='100%'>
                <tr>
                    <td width='80%' style='text-align: right;'>Convenience Fee</td>
                    <td width='20%' style='text-align: right;'>Rp " . number_format($single->payment_fee, 0, '.', ',') . "</td>
                </tr>
            </table>";
        }
        $email_merchant .= "<table width='100%'>
                <tr>
                    <td width='80%' style='text-align: right;'>Grand Total </td>
                    <td width='20%' style='text-align: right;'>Rp " . $totalPrint . "</td>
                </tr>
            </table><br>";
        $email_merchant .="
                    <p>Kami sarankan Anda menyimpan dengan baik bukti pembayaran 
                    hingga produk telah Anda terima dan transaksi dinyatakan selesai.</p><br>
                    <p>Untuk informasi, silahkan menghubungi e-care.store@cipika.co.id. 
                    Terima kasih atas kepercayaan Anda berbelanja di Cipika Store.</p>";
        $email_merchant .= '<br/><p>Terima Kasih,<br/><br/><br/>
                    <strong>Cipika Store &trade;</strong><br>
                    <a href="' . base_url() . '">www.cipikastore.com</a>
                    </p>
                    </div>
                    ';
        $list = $this->bcc;
        $list = implode(",", $list);
        $insert = array('idmailer' => null,
            'mailer_module' => 'Konfirmasi Pembayaran',
            'mailer_from' => $this->from,
            'mailer_to' => $to,
            'mailer_bcc' => $list,
            'mailer_subject' => 'Konfirmasi Pembayaran di Cipika Store',
            'mailer_message' => $email_merchant,
            'mailer_status' => 'new',
            'mailer_created' => date('Y-m-d H:i:s')
        );
        $this->dbLibrary->insert('mailer', $insert);
        return $this->dbLibrary->insert_id();
    }

    public function sends($to, $single, $inputan_order, $inputan_shipping, $inputan_merchant, $order_item, $link_index, $payment, $user)
    {
        $email_merchant = "<h3>Bapak/Ibu " . ucwords($user->firstname) . " " . ucwords($user->lastname) . " Yth.</h3>
                <br />
                <p>
                    Terima kasih anda telah menyelesaikan seluruh proses belanja di Cipika Store. 
                    Kami telah mencatat daftar belanja dan pembayaran Anda dengan baik.
                </p><br>
                <p>
                    Informasi lebih lanjut mengenai proses pengiriman produk akan kami sampaikan dalam waktu maksimal 3 (tiga) hari kedepan. 
                    Pengiriman produk akan dilakukan langsung oleh Merchant kami melalui jasa ekspedisi dan layanan yang telah disepakati. Anda akan menerima informasi lebih lanjut apabila diperlukan perpanjangan waktu pengiriman.
                </p><hr>
                ";
        $totalPrint = 0;
        $totalAll = 0;
        foreach ($inputan_order as $v)
        {
            $email_merchant .= "<table width='100%'>
                            <tr>
                                <td width='40%'>Merchant: " . ucwords($inputan_merchant[$v->id_merchant]) . "</td>
                                <td width='40%'>Nomor Transaksi: " . $v->kode_order . "</td>
                                <td width='20%'>Status: Success</td>
                            </tr>
                        </table>";
            $email_merchant .= "<table width='100%'>
                            <tr>
                                <td width='40%'>
                                    <strong>Alamat Pengiriman</strong><br /><span></span>
                                    " . ucwords($inputan_shipping[$v->id_order]->nama) . "<br />
                                    " . ucwords($inputan_shipping[$v->id_order]->alamat) . "<br>
                                    " . ucwords($inputan_shipping[$v->id_order]->nama_kabupaten) . " - " . ucwords($inputan_shipping[$v->id_order]->nama_propinsi) . "<br>
                                    " . $inputan_shipping[$v->id_order]->telpon . "<br />
                                </td>
                                <td width='40%'>
                                    <strong>Metode Pengiriman</strong><br />
                                    " . ucwords($v->paket_ongkir) . "
                                </td>
                               <td width='20%'>
                                    <strong>Metode Pembayaran</strong><br>";
            $email_merchant.= $payment->nama_payment;
            $email_merchant .= "</td>
                            </tr>
                            </table><br>";
            $email_merchant .= '<table style="border-collapse:collapse; width:100%;">
                <tr style="background:#666666; color:white;" class="judul"><td width="10%" style="text-align:center">No</td><td>Nama Produk</td><td width="10%" style="text-align:right">Jumlah</td><td width="15%" style="text-align:right">Harga</td><td width="10%" style="text-align:right">Diskon</td><td width="15%" style="text-align:right">Harga setelah diskon</td><td width="20%" style="text-align:right">Subtotal</td></tr>';
            $i = 0;
            $total_sub = 0;
            $sub_total = 0;
            foreach ($order_item[$v->id_order] as $items)
            {
                if ($items->diskon > 0)
                    $harga_diskon = $items->harga - ($items->harga) * ($items->diskon / 100);
                else
                    $harga_diskon = $items->harga;
                $total_sub = $harga_diskon * $items->jml_produk;
                $i++;
                $email_merchant .= '<tr style="text-align:center"><td>' . $i . '</td> 
                        <td style="text-align:left"><a href="' . base_url() . $link_index . 'product/detail/' . $items->id_produk . '" >' . $items->nama_produk . '</a></td>
                        <td style="text-align:right">' . $items->jml_produk . '</td>
                        <td style="text-align:right">Rp. ' . number_format($items->harga, 0, '.', ',') . '</td>                        
                        <td style="text-align:right">' . $items->diskon . '%</td>
                        <td style="text-align:right">Rp. ' . number_format($harga_diskon, 0, '.', ',') . '</td>
                        <td style="text-align:right">Rp. ' . number_format($total_sub, 0, '.', ',') . '</td>
                    </tr>';
                $sub_total += $total_sub;
            }
            $email_merchant .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Sub Total</td><td >Rp. ' . number_format($sub_total, 0, '.', ',') . '</td></tr>';
            $email_merchant .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Ongkos Kirim</td><td >Rp. ' . number_format($v->ongkir_sementara, 0, '.', ',') . '</td></tr>';
            $total_harga = $sub_total + $v->ongkir_sementara;
            $email_merchant .= '<tr style="background:#666666; color:white; text-align:right"><td colspan="6" >Total</td><td >Rp. ' . number_format($total_harga, 0, '.', ',') . '</td></tr>            
                </table><div style="height:10px;"></div>';
            $totalAll+=$total_harga;
        }
        $totalPrint = $totalAll + $single->payment_fee;
        if ((int) $single->payment_fee > 0)
        {
            $email_merchant .= "<table width='100%'>
                <tr>
                    <td width='80%' style='text-align: right;'>Convenience Fee</td>
                    <td width='20%' style='text-align: right;'>Rp " . number_format($single->payment_fee, 0, '.', ',') . "</td>
                </tr>
            </table>";
        }

        $email_merchant .= "<table width='100%'>
                <tr>
                    <td width='80%' style='text-align: right;'>Grand Total </td>
                    <td width='20%' style='text-align: right;'>Rp " . number_format($totalPrint, 0, '.', ',') . "</td>
                </tr>
            </table><br>";
        $email_merchant .="
                    <p>Kami sarankan Anda menyimpan dengan baik bukti pembayaran 
                    hingga produk telah Anda terima dan transaksi dinyatakan selesai.</p><br>
                    <p>Untuk informasi, silahkan menghubungi e-care.store@cipika.co.id. 
                    Terima kasih atas kepercayaan Anda berbelanja di Cipika Store.</p>";
        $email_merchant .= '<br/><p>Terima Kasih,<br/><br/><br/>
                    <strong>Cipika Store &trade;</strong><br>
                    <a href="' . base_url() . '">www.cipikastore.com</a>
                    </p>
                    ';
        $list = $this->bcc;
        $list = implode(",", $list);

        $insert = array('idmailer' => null,
            'mailer_module' => 'Konfirmasi Pembayaran',
            'mailer_from' => $this->from,
            'mailer_to' => $to,
            'mailer_bcc' => $list,
            'mailer_subject' => 'Konfirmasi Pembayaran di Cipika Store',
            'mailer_message' => $email_merchant,
            'mailer_status' => 'new',
            'mailer_created' => date('Y-m-d H:i:s')
        );
        $this->dbLibrary->insert('mailer', $insert);
        return $this->dbLibrary->insert_id();
    }

}
