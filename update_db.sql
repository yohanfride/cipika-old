create or replace view view_comment as
select a.id_produk, count(b.id_comment) as count_comment
from tbl_produk a
left join tbl_comment b on a.id_produk=b.id_produk
group by a.id_produk;

create or replace view view_love as
select a.id_produk, count(b.id_user) as count_love
from tbl_produk a
left join tbl_love b on a.id_produk=b.id_produk
group by a.id_produk;

ALTER TABLE tbl_produk
ADD pick int(1) not null default 0;

ALTER TABLE tbl_produk
ADD harga_jual int(11) not null default 0;

ALTER TABLE `tbl_payment_buyers`
	ADD COLUMN `paid` INT(1) NOT NULL DEFAULT '0' AFTER `status`;

ALTER TABLE tbl_ongkir
DROP FOREIGN KEY fk_tbl_ongkir_tbl_kota1;

ALTER TABLE tbl_ongkir
DROP FOREIGN KEY fk_tbl_ongkir_tbl_kota2;

alter table tbl_produk modify column deskripsi text;

ALTER TABLE `tbl_produk`
	ADD COLUMN `panjang` FLOAT NULL DEFAULT '0' AFTER `berat`;
	
ALTER TABLE `tbl_produk`
	ADD COLUMN `lebar` FLOAT NULL DEFAULT '0' AFTER `panjang`;
	
ALTER TABLE `tbl_produk`
	ADD COLUMN `tinggi` FLOAT NULL DEFAULT '0' AFTER `lebar`;